#RISK#
##A project for CS3099-STP##

###Contributors:###
*   Timur Garipov, tg33
*   John Heenan, jcbh
*   Angus Ireland, aai
*   Geraint Spackman, gs63

###Dependencies:###
*   Gradle
*   Java 1.8

###To build the program:###

####For development:####
    $ gradle clean build
  
####For production:####
    $ gradle clean jar

###To run the program:###

####For development:####
    $ gradle run

####For production:####
    $ java -jar build/libs/CS3099-STP_Group-H-RISK.jar

####In your browser:####
`http://localhost:8080/`

Note: You will need access to ports 8080 and 8081 for the web interface, and port 9000 for the game server.

###Paths:###

####Project Root:####
*   Gradle Configuration: `build.gradle`
*   Java Sources: `src/`
*   Web Assets: `res/web-assets`

###Copyright###
Copyright 2015 the contributors, all rights reserved.

In submitting this project report to the University of St Andrews, we give permission for it to be made available for use in accordance with the regulations of the University Library.
We also give permission for the report to be made available on the World Wide Web.
We give permission for this work to be used in research within the University of St Andrews, and for any software to be released on an open source basis.
We retain the copyright in this work, and ownership of any resulting intellectual property.

####NanoHTTPD Library####
    Copyright (c) 2012-2013 by Paul S. Hawke, 2001,2005-2013 by Jarno Elonen, 2010 by Konstantinos Togias All rights reserved.

    Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

        Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

        Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

        Neither the name of the NanoHttpd organization nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


