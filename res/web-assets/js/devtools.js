sendMessage = function(message) {
    window.opener.GroupHRisk.objects.messageQueue.push(message);
    window.opener.GroupHRisk.functions.processSocketMessage();
};


gameCreated = function () {
    var message = {};
    message.status = "ok";
    message.type = "gameCreated";

    sendMessage(message);

    return false;
};

joinAccepted = function () {
    var message = {};
    message.status = "ok";
    message.type = "joinAccepted";

    sendMessage(message);

    return false;
};

gameStarted = function () {
    var message = {};
    message.status = "ok";
    message.type =  "gameStarted";
    message.playOrder = [0,1];

    sendMessage(message);

    return false;
};

ack = function () {
    var message = {};
    message.status = "ok";
    message.type = "ack";

    sendMessage(message);

    return false;
};

joinNotification = function () {
    var message = {};
    message.status = "ok";
    message.type = "joinNotification";

    sendMessage(message);

    return false;
};

mapTransfer = function () {
    var message = {};
    message.status = "ok";
    message.type = "mapTransfer";
    message.payload = JSON.parse('{"data":"map","continent_values":{"0":5,"1":2,"2":5,"3":3,"4":7,"5":2},"continent_names":{"0":"NorthAmercia","1":"SouthAmerica","2":"Europe","3":"Africa","4":"Asia","5":"Australia"},"country_names":{"0":"Alaska","1":"Northwestterritory","2":"Greenland","3":"Alberta","4":"Ontario","5":"Quebec","6":"WesternUnitedStates","7":"EasternUnitedStates","8":"CentralAmerica","9":"Venezuela","10":"Peru","11":"Brazil","12":"Argentina","13":"Iceland","14":"Scandinavia","15":"Ukraine","16":"GreatBritain","17":"NorthernEurope","18":"WesternEurope","19":"SouthernEurope","20":"NorthAfrica","21":"Egypt","22":"Congo","23":"EastAfrica","24":"SouthAfrica","25":"Madagaskar","26":"Ural","27":"Siberia","28":"Yakutsk","29":"Kamichatka","30":"Irkutsk","31":"Mongolia","32":"Japan","33":"Afghanistan","34":"China","35":"MiddleEast","36":"India","37":"Siam","38":"Indonesia","39":"NewGuinea","40":"WesternAustralia","41":"EasternAustralia"},"wildcards":2,"continents":{"0":["0","1","2","3","4","5","6","7","8"],"1":["9","10","11","12"],"2":["13","14","15","16","17","18","19"],"3":["20","21","22","23","24","25"],"4":["26","27","28","29","30","31","32","33","34","35","36","37"],"5":["38","39","40","41"]},"connections":[[0,1],[0,3],[0,29],[1,3],[1,2],[1,4],[2,4],[2,5],[2,13],[3,4],[3,6],[4,5],[4,6],[4,7],[5,7],[6,7],[6,8],[7,8],[8,9],[9,10],[9,10],[10,11],[10,12],[11,12],[11,20],[13,14],[13,16],[14,16],[14,17],[14,15],[15,17],[15,19],[15,26],[15,33],[15,35],[16,17],[16,18],[17,18],[17,19],[18,19],[18,20],[19,20],[19,21],[19,35],[20,21],[20,22],[20,23],[21,23],[21,35],[22,23],[22,24],[23,25],[23,24],[23,35],[24,25],[26,27],[26,34],[26,33],[27,28],[27,30],[27,31],[27,34],[28,29],[28,30],[29,30],[29,32],[29,31],[30,31],[31,32],[31,34],[33,34],[33,35],[33,36],[34,36],[34,37],[35,36],[36,37],[37,38],[38,39],[38,40],[39,40],[39,41],[40,41]],"country_card":{"0":2,"1":1,"2":1,"3":2,"4":2,"5":2,"6":2,"7":2,"8":0,"9":1,"10":1,"11":0,"12":0,"13":1,"14":0,"15":0,"16":0,"17":1,"18":0,"19":0,"20":0,"21":1,"22":2,"23":0,"24":2,"25":1,"26":0,"27":0,"28":2,"29":2,"30":2,"31":1,"32":1,"33":0,"34":2,"35":2,"36":1,"37":1,"38":0,"39":1,"40":2,"41":1}}');

    sendMessage(message);

    return false;
};

gameStarted = function () {
    var message = {};
    message.status = "ok";
    message.type = "gameStarted";
    message.playOrder = [0,1];

    sendMessage(message);

    return false;
};

cheatNotification = function () {
    var message = {};
    message.status = "ok";
    message.type = "cheatNotification";
    message.playerId = 1;

    sendMessage(message);

    return false;
};

cardDrawNotification = function () {
    var message = {};
    message.status = "ok";
    message.type = "cardDrawNotification";
    message.playerId = 0;
    message.countryId = $("#cardDrawCountryId").val();
    message.cardType = $("#cardDrawCardType").val();

    sendMessage(message);

    return false;
};

lobbyUpdate = function(type) {
    var message = {};
    message.status = "ok";
    message.type = "lobbyUpdate";
    message.myPlayerId = 0;

    if(type === 1) {
        message.players = [{"playerId" : 0}];
    } else {
        message.players = [{"playerId" : 0}, {"playerId": 1}];
    }

    sendMessage(message);

    return false;
};

turnNotification = function(type) {
    var message = {};
    message.status = "ok";
    message.type = "turnNotification";

    if(type === 1) {
        message.newPlayer = 0;
        message.oldPlayer = 1;
        message.claimMode = false;
    } else if(type === 2) {
        message.newPlayer = 1;
        message.oldPlayer = 0;
        message.claimMode = false;
    } else {
        message.newPlayer = 0;
        message.oldPlayer = 1;
        message.claimMode = true;
    }

    sendMessage(message);

    return false;
};

attackNotification = function (){
    var message = {};
    message.status = "ok";
    message.type = "attackNotification";

    message.fromPlayerId = $("#attackFromPlayerId").val();
    message.toPlayerId = $("#attackToPlayerId").val();
    message.fromCountryId = $("#attackFromCountryId").val();
    message.toCountryId = $("#attackToCountryId").val();
    message.armyCount = $("#attackArmyCount").val();

    sendMessage(message);

    return false;
};

battleResult = function (){
    var message = {};
    message.status = "ok";
    message.type = "battleResult";

    message.attackingPlayer = $("#battleFromPlayerId").val();
    message.attackingLosses = $("#battleFromLosses").val();
    message.defendingPlayer = $("#battleToPlayerId").val();
    message.defendingLosses = $("#battleToLosses").val();

    sendMessage(message);

    return false;
};

defendNotification = function (){
    var message = {};
    message.status = "ok";
    message.type = "defendNotification";

    message.fromPlayerId = $("#attackFromPlayerId").val();
    message.armyCount = $("#attackArmyCount").val();

    sendMessage(message);

    return false;
};

rollRequestNotification = function () {
    var message = {};
    message.status = "ok";
    message.type = "requestRollNotification";

    message.numDice = $("#numDice").val();

    sendMessage(message);

    return false;
};

rollNotification = function () {
    var message = {};
        message.status = "ok";
        message.type = "rollResult";
        message.numDice = $("#numDice").val();

    message.results = [];
    if(numDice >= 1) {
        message.results.push($("#dieOne").val());
    }
    if(numDice >= 2) {
        message.results.push($("#dieTwo").val());
    }
    if(numDice >= 3) {
        message.results.push($("#dieThree").val());
    }

    sendMessage(message);

    return false;
};

territoryChangeNotification = function () {
    var message = {};
    message.status = "ok";
    message.type = "territoryChangeNotification";

    message.countryId = $("#territoryCountryID").val();
    message.armyCount = $("#territoryArmyCount").val();
    message.playerId = $("#territoryNewPlayer").val();

    sendMessage(message);

    return false;
};

setupClaim = function () {
    var message = {};
    message.status = "ok";
    message.type = "requestSetupClaim";

    message.countryId = $("#territoryCountryID").val();
    message.playerId = $("#territoryNewPlayer").val();

    sendMessage(message);

    return false;
};

reinforceCount = function () {
    var message = {};
    message.status = "ok";
    message.type = "requestReinforceCount";

    message.armyCount = $("#reinforceCount").val();

    sendMessage(message);

    return false;
};

updateState = function () {
    window.opener.GroupHRisk.values.state = parseInt($("#newstate").val(),10);
    retrieveState();
};

retrieveState = function () {
    var newText;
    switch(window.opener.GroupHRisk.values.state) {
        case window.opener.GroupHRisk.enums.STATES.NOTINGAME:
            newText = "NOTINGAME - Join/Host Menu";
            break;
        case window.opener.GroupHRisk.enums.STATES.HOSTINGLOBBY:
            newText = "HOSTINGLOBBY - Hosting, waiting for players to join";
            break;
        case window.opener.GroupHRisk.enums.STATES.JOINLOBBY:
            newText = "JOINLOBBY - Not hosting, waiting for players to join/game start";
            break;
        case window.opener.GroupHRisk.enums.STATES.SETUPPHASE:
            newText = "SETUPPHASE - \"Claim\" phase, not our turn";
            break;
        case window.opener.GroupHRisk.enums.STATES.WAITINGFOROTHERPLAYER:
            newText = "WAITINGFOROTHERPLAYER - Game started, not our turn";
            break;
        case window.opener.GroupHRisk.enums.STATES.GAMEOVER:
            newText = "GAMEOVER - Game has ended";
            break;
        case window.opener.GroupHRisk.enums.STATES.CLAIM:
            newText = "CLAIM - \"Claim\" phase, our turn";
            break;
        case window.opener.GroupHRisk.enums.STATES.TRADE:
            newText = "TRADE - Our turn, trade cards";
            break;
        case window.opener.GroupHRisk.enums.STATES.DEPLOY:
            newText = "DEPLOY - Our turn, deploy armies";
            break;
        case window.opener.GroupHRisk.enums.STATES.ATTACK_FROM:
            newText = "ATTACK_FROM - Our turn, choose where to attack from";
            break;
        case window.opener.GroupHRisk.enums.STATES.ATTACK_TO:
            newText = "ATTACK_TO - Our turn, choose where to attack to";
            break;
        case window.opener.GroupHRisk.enums.STATES.FORTIFY_FROM:
            newText = "FORTIFY_FROM - Our turn, choose where to move troops from";
            break;
        case window.opener.GroupHRisk.enums.STATES.FORTIFY_TO:
            newText = "FORTIFY_TO - Our turn, choose where to move troops to";
            break;
        case window.opener.GroupHRisk.enums.STATES.DICE_WAIT:
            newText = "DICE_WAIT - Waiting for the engine to give us dice results";
            break;
        case window.opener.GroupHRisk.enums.STATES.REINFORCE:
            newText = "REINFORCE - Choose a country to reinforce.";
        case undefined:
            newText = "UNDEFINED - Error?";
            break;
        default:
            newText = "UNKNOWN - Error?";
            break;
    }
    $("#currentstate").text(newText);
    
};

repeatRetrieveStateWrapper = function () {
    retrieveState();
    setTimeout(repeatRetrieveStateWrapper, 1000);
};

$(document).ready(function () {
    repeatRetrieveStateWrapper();
});