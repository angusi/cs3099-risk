/*jslint browser: true*/ //Force JSLint to accept that this script is for the browser.
/*jslint devel:true */ //Force JSLint to accept 'alert' - which it treats as a debugging aid.
/*jslint white: true */ //Make JSLint ignore whitespace (since our switch/case style is against JSLint's convention)
/*global $ */ //Tell JSLint about JQuery, included externally.
/*global cytoscape */ //Tell JSLint about Cytoscape, included externally.
/*global WebSocket */ //Tell JSLint about WebSockets, which are too new for it...
/**
 * Check if jQuery has been successfully loaded.
 * If not, issue an alert - since we rely on jQuery.
 */
if ($ === undefined) {
    alert("Jquery is not loaded. GroupHRisk cannot continue.\nPlease reload and try again, or check your network connectivity.");
}

/**
 * The GroupHRisk object contains all the functions and values related to the game.
 * @type {GroupHRisk}
 */
var GroupHRisk = GroupHRisk || { };


/**
 * Constants
 * @type {{}}
 * @namespace GroupHRisk.constants
 */
GroupHRisk.constants = GroupHRisk.constants || { };
/**
 * The maximum number of players supported
 * @type {number}
 */
GroupHRisk.constants.MAX_PLAYERS = 6;
/**
 * Names of continents to pull from.
 * @type {string[]}
 */
GroupHRisk.constants.CONTINENT_NAMES = ["Vaalbara", "Kenorland", "Protopangea-Paleopangea", "Columbia", "Rodinia", "Pannotia", "Pangea"];
/**
 * Colours to use in the player list (using hex notation, with # prefix)
 * @type {string[]}
 */
GroupHRisk.constants.COLORS = ["#FF0000", "#00FF00", "#0000FF", "#FFFF00", "#FF00FF", "#00FFFF"];

/**
 * Enums
 * @type {{}}
 * @namespace GroupHRisk.enums
 */
GroupHRisk.enums = GroupHRisk.enums || { };
/**
 * States (for ensuring operations can only be performed at the right time)
 * @type {{}}
 */
GroupHRisk.enums.STATES = {
    NOTINGAME : 0,
    HOSTINGLOBBY : 1,
    JOINLOBBY : 2,
    SETUPPHASE : 3,
    WAITINGFOROTHERPLAYER : 4,
    GAMEOVER : 5,
    CLAIM : 6,
    TRADE : 7,
    DEPLOY : 8,
    ATTACK_FROM : 9,
    ATTACK_TO : 10,
    FORTIFY_FROM : 11,
    FORTIFY_TO: 12,
    DICE_WAIT: 13,
    REINFORCE: 14,
    MOVE : 15
};

GroupHRisk.enums.CARDTYPES = {
    INFANTRY : 0,
    CAVALRY : 1,
    ARTILLERY : 2,
    WILD : 3
};

GroupHRisk.enums.PHASES = {
    CLAIM : 0,
    DEPLOY : 1,
    STANDARD : 2
};

/**
 * Fields
 * @type {{}}
 * @namespace GroupHRisk.values
 */
GroupHRisk.values = GroupHRisk.values || { };
/**
 * The current game state, drawn from GroupHRisk.enums.STATES
 * Default: enums.STATES.NOTINGAME
 * @type {number}
 */
GroupHRisk.values.state = GroupHRisk.enums.STATES.NOTINGAME;
/**
 * The set of players in game
 * @type {Array}
 */
GroupHRisk.values.players = [];
/**
 * The GroupHRisk.protos.player.id of the user
 * @type {number}
 */
GroupHRisk.values.ownPlayerId = 0;
/**
 * The number of armies the player has available to deploy
 * @type {number}
 */
GroupHRisk.values.freeArmies = 0;
/**
 * Map graph object
 * @type {{}}
 */
GroupHRisk.values.cytoscapeGraph = { };
/**
 * "True" if the queue is already being processed
 * @type {boolean|*}
 */
GroupHRisk.values.messagesBeingProcessed = GroupHRisk.values.messagesBeingProcessed || false;

/**
 * The set of cards drawn by the user
 * @type {Array}
 */
GroupHRisk.values.ownCards = [];

/**
 * The set of keys currently down (for getting into dev mode!)
 * @type {Array}
 */
GroupHRisk.values.konami = 0;

/**
 * If true, continents cannot be moved.
 * @type {boolean}
 */
GroupHRisk.values.continentsLocked = false;

/**
 * Dice roll results received before the user has asked for them
 * @type {Array}
 */
GroupHRisk.values.cachedDiceResults = [];

/**
 * Count of websocket (re)connection attempts
 * @type {boolean}
 */
GroupHRisk.values.webSocketRetry = 0;

/**
 * The country currently the subject of a reinforcement
 * @type {Number}
 */
GroupHRisk.values.currentReinforce = null;

/**
 * Sub-objects
 * @type {{}}
 * @namespace GroupHRisk.objects
 */
GroupHRisk.objects = GroupHRisk.objects || { };
/**
 * The WebSocket used to receive data from the UI Server
 * @type {WebSocket}
 */
GroupHRisk.objects.webSocket = GroupHRisk.objects.webSocket || { };
/**
 * Data associated with an attack currently underway
 * @type {GroupHRisk.protos.attack}
 */
GroupHRisk.objects.currentAttack = GroupHRisk.objects.currentAttack || { };
/**
 * Data associated with a fortification currently underway
 * @type {GroupHRisk.protos.fortify}
 */
GroupHRisk.objects.currentFortification = GroupHRisk.objects.currentFortification || { };
/**
 * Data associated with a card trade in progress
 * @type {GroupHRisk.protos.trade}
 */
GroupHRisk.objects.currentTrade = GroupHRisk.objects.currentTrade || { };

/**
 * Queue of messages to process.
 * @type {Array|*}
 */
GroupHRisk.objects.messageQueue = GroupHRisk.objects.messageQueue || [ ];

/**
 * Array of [country,count] for reinforcements
 * @type {Array|*}
 */
GroupHRisk.objects.currentReinforce = GroupHRisk.objects.currentReinforce || [];

/**
 * Classes
 * @type {{}}
 * @namespace GroupHRisk.protos
 */
GroupHRisk.protos = GroupHRisk.protos || { };
/**
 * Information on a player
 * @param playerid The players unique ID
 * @param color The colour used in the player list for this player
 */
GroupHRisk.protos.player = function (playerid, color) {
    'use strict'; //Force ECMAScript 5 Strict mode
    this.id = playerid; //Unique ID
    this.color = color; //"color", not "colour", to avoid inconsistencies...
};
/**
 * Information associated with an attack
 */
GroupHRisk.protos.attack = function () {
    'use strict'; //Force ECMAScript 5 Strict mode
    this.attackFrom = null; //The ID of the country attacking from
    this.attackTo = null; //The ID of the country being attacked
    this.armyCount = 0; //The number of armies attacking
};
/**
 * Information associated with a fortification
 */
GroupHRisk.protos.fortify = function () {
    'use strict'; //Force ECMAScript 5 Strict mode
    this.fortifyFrom = null; //The ID of the country the armies are sourced from
    this.fortifyTo = null; //The ID of the country the armies are moving to
    this.armyCount = 0; //The number of armies to move
};

GroupHRisk.protos.card = function () {
    'use strict'; //Force ECMAScript5 Strict mode
    this.countryId = null;
    this.cardType = null;
};

GroupHRisk.protos.trade = function () {
    'use strict'; //Force ECMAScript5 Strict Mode
    this.cards = [];
};

/**
 * Functions
 * @type {{}}
 * @namespace GroupHRisk.functions
 */
GroupHRisk.functions = GroupHRisk.functions || { };
/**
 * Start-up
 * Loads the join/host interface into the sidebar, and requests a new websocket to be opened
 * Only useful when not in game. Will have no effect otherwise.
 */
GroupHRisk.functions.initialise = function () {
    'use strict'; //Force ECMAScript 5 strict mode

    var rightBarDiv;

    if (window.location.hash && window.location.hash === "#devtools") {
        window.open("/devtools.html", "devtools", "toolbar=no, scrollbars=yes, resizable=yes, location=no, top=20, left=220, width=1260, height=600");
    }

    if (GroupHRisk.values.state === GroupHRisk.enums.STATES.NOTINGAME) {

        console.info("[GroupHRisk.functions.initialise]: Initialising new game");

        rightBarDiv = $("#rightbar").empty();

        $.get("/ajax/join-host.html")
            .done(function (data) {
                rightBarDiv.append(data);
            })
            .fail(function () {
                console.warn("[GroupHRisk.functions.initialise]: Error occurred requesting side bar data");
                alert("An error occurred. Reload and try again.");
            })
            .always(function () {
                GroupHRisk.functions.initialiseWebSocket();
            });


    } else {
        console.warn("[GroupHRisk.functions.initialise]: Ignoring request to initialise game when already in game");
        alert("Game state is inconsistent. Play may be able to continue, but you may need to reload.");
    }

};

/**
 * Opens a new WebSocket connection to the UI Server
 */
GroupHRisk.functions.initialiseWebSocket = function () {
    'use strict'; //Force ECMAScript 5 strict mode

    console.info("[GroupHRisk.functions.initialiseWebSocket]: Opening new WebSocket");

    GroupHRisk.objects.webSocket = new WebSocket("ws://" + window.location.hostname + ":" +
    (parseInt(window.location.port, 10) + 1)); //TODO: Make this port number configurable??


    GroupHRisk.objects.webSocket.onopen = function (event) {
        console.info("[GroupHRisk.objects.webSocket.onopen]: WebSocket open!");
        if(GroupHRisk.values.webSocketRetry > 0) {
            setTimeout(function() { GroupHRisk.values.webSocketRetry = 0;}, 30000); //Reduce retry count after 30s of success.
        }
        $("#loader").fadeOut("fast");
    };

    /**
     * Function triggered when a new message is received over the websocket
     * @param event The triggering event
     */
    GroupHRisk.objects.webSocket.onmessage = function (event) {
        var message = JSON.parse(event.data); //The received JSON data

        //Ensure everything went well and the server isn't giving us an error
        if (message.status !== "ok") {
            console.warn("[GroupHRisk.objects.webSocket.onmessage]: Non-OK message received with ID " + message.id);
            alert("An error occurred. Reload and try again.");
            return;
        }

        console.info("[GroupHRisk.objects.webSocket.onmessage]: Message of type " + message.type + " received. Adding it to queue.");
        GroupHRisk.objects.messageQueue.push(message);
        if (GroupHRisk.values.messagesBeingProcessed === false) {
            console.info("[GroupHRisk.objects.webSocket.onmessage]: Calling GroupHRisk.functions.processSocketMessage");
            GroupHRisk.functions.processSocketMessage();
        }
    };

    GroupHRisk.objects.webSocket.onclose = function (event) {
        $("#loader").fadeIn("fast");
        if(GroupHRisk.values.webSocketRetry < 5) {
            console.warn("[GroupHRisk.objects.webSocket.onclose]: Socket closed - retry attempt "+(GroupHRisk.values.webSocketRetry+1));
            setTimeout(function() { GroupHRisk.functions.initialiseWebSocket(); }, (GroupHRisk.values.webSocketRetry++ * 1000));
        } else {
            console.warn("[GroupHRisk.objects.webSocket.onclose]: Socket closed - retry attempt 5 failed, so giving up.");
            alert("Oops... we lost connection to the server and couldn't get it back.\nCheck the server is still running, then reload.");
        }
    }
};

GroupHRisk.functions.processSocketMessage = function () {
    'use strict'; //Force ECMAScript5 Strict mode

    var message = GroupHRisk.objects.messageQueue.shift();
    if (typeof (message) === "undefined") {
        console.info("[GroupHRisk.functions.processSocketMessage]: No more messages to process.");
        GroupHRisk.values.messagesBeingProcessed = false;
        return;
    }
    GroupHRisk.values.messagesBeingProcessed = true;
    switch (message.type) {
        case "gameCreated":
            //We are now "hosting" a game.
            // (Note, this is not necessarily host in the sense that we are game master,
            // merely that we initiated the game)
            GroupHRisk.socketHandlers.requestToHostAccepted(message);
            break;
        case "joinNotification":
            //A player has joined the game session
            GroupHRisk.socketHandlers.playerJoinNotification(message);
            break;
        case "lobbyUpdate":
            //The lobby has been updated
            GroupHRisk.socketHandlers.lobbyUpdate(message);
            break;
        case "mapTransfer":
            //The server is sending a new map
            GroupHRisk.socketHandlers.mapTransfer(message);
            break;
        case "joinAccepted":
            //We have now "joined" a game.
            // (Note, we may now be game master, even though we're not "hosting"...)
            GroupHRisk.socketHandlers.requestToJoinAccepted(message);
            break;
        case "gameStarted":
            //All players have joined and the map has been received. Commence phase 1, country claim.
            GroupHRisk.socketHandlers.gameStartedNotification(message);
            break;
        case "territoryChangeNotification":
            //A territory has changed either ruler or army count
            GroupHRisk.socketHandlers.territoryChangeNotification(message);
            break;
        case "attackNotification":
            //An attack is occurring. Note this may not involve this user.
            GroupHRisk.socketHandlers.attackNotification(message);
            break;
        case "rollResult":
            //The engine has rolled the dice, here are the results
            GroupHRisk.socketHandlers.rollResult(message);
            break;
        case "turnNotification":
            //Turn change
            GroupHRisk.socketHandlers.turnNotification(message);
            break;
        case "battleResult":
            //Result of an attack/defend
            GroupHRisk.socketHandlers.battleResult(message);
            break;
        case "cheatNotification":
            //Someone is cheating
            GroupHRisk.socketHandlers.cheatNotification(message);
            break;
        case "defendNotification":
            //Someone we're attacking is defending
            GroupHRisk.socketHandlers.defendNotification(message);
            break;
        case "requestReinforceCount":
            //We've been given some armies to place!
            GroupHRisk.socketHandlers.requestReinforceCount(message);
            break;
        case "rollRequestNotification":
            //Please roll the dice.
            GroupHRisk.socketHandlers.rollRequestNotification(message);
            break;
        case "requestTracer":
            //Tracer message
            GroupHRisk.socketHandlers.requestTracer(message);
            break;
        case "cardDrawNotification":
            //Card Draw
            GroupHRisk.socketHandlers.cardDrawn(message);
            break;
        default:
            //An unknown message was received
            //alert(JSON.stringify(message));
            console.warn("[GroupHRisk.functions.processSocketMessage]: Unknown message (type "+message.type+") received. Ignoring it.");
            console.info("[GroupHRisk.functions.processSocketMessage]: Calling GroupHRisk.functions.processSocketMessage");
            GroupHRisk.functions.processSocketMessage();
    }
};

/**
 * We would like to join a pre-existing game
 * Only useful when not in game. Will have no effect otherwise
 * @returns {boolean} False, to prevent page redirection
 */
GroupHRisk.functions.joinGame = function () {
    'use strict'; //Force ECMAScript 5 Strict mode

    if(GroupHRisk.values.state === GroupHRisk.enums.STATES.NOTINGAME) {

        console.info("[GroupHRisk.functions.joinGame]: Asking to join a game");

        $("#loader").fadeIn("fast", function () {
            //Send a post request to the API Endpoint containing the values needed to join a game
            $.post("/api/v1/join", $("#joinForm").serialize())
                .done(function (data) { /* Success, do nothing */
                })
                .fail(function () {
                    console.warn("[GroupHRisk.functions.hostGame]: Error occurred asking to join a game");
                    alert("An error occurred. Reload and try again.");
                })
                .always(function () {
                    $("#loader").fadeOut("fast");
                });
        });
    } else {
        console.warn("[GroupHRisk.functions.joinGame]: Ignoring request to join game when already in a game.");
        alert("Game state is inconsistent. Play may be able to continue, but you may need to reload.");
    }

    return false;
};

/**
 * We would like to host (initiate) a game
 * Only useful when not in a game. Will have no effect otherwise
 * @returns {boolean} False, to prevent page redirection
 */
GroupHRisk.functions.hostGame = function () {
    'use strict'; //Force ECMAScript 5 Strict mode

    if(GroupHRisk.values.state === GroupHRisk.enums.STATES.NOTINGAME) {

        console.info("[GroupHRisk.functions.hostGame]: Asking to host a game");

        $("#loader").fadeIn("fast", function () {
            //Send a post request to the API Endpoint containing the values needed to host a game
            $.post("/api/v1/create", $("#hostForm").serialize())
                .done(function (data) { /* Success, do nothing */
                })
                .fail(function () {
                    console.warn("[GroupHRisk.functions.hostGame]: Error occurred asking to host a game");
                    alert("An error occurred. Reload and try again.");
                })
                .always(function () {
                    $("#loader").fadeOut("fast");
                });
        });
    } else {
        console.warn("[GroupHRisk.functions.hostGame]: Ignoring request to host game when already in a game.");
        alert("Game state is inconsistent. Play may be able to continue, but you may need to reload.");
    }

    return false;
};

/**
 * Updates the list of players in the game lobby
 * Only useful before play has commenced. Will redirect to in game list update otherwise.
 */
GroupHRisk.functions.updateLobbyPlayerList = function () {
    'use strict'; //Force ECMAScript 5 Strict mode

    var playerDiv,
        i,
        thatsYouText;

    if(GroupHRisk.values.state === GroupHRisk.enums.STATES.HOSTINGLOBBY || GroupHRisk.values.state === GroupHRisk.enums.STATES.JOINLOBBY) {

        console.info("[GroupHRisk.functions.updateLobbyPlayerList]: Updating player list");

        playerDiv = $("#players").find("ol").empty();

        for (i = 0; i < GroupHRisk.values.players.length; i += 1) {
            if (GroupHRisk.values.players[i].id === GroupHRisk.values.ownPlayerId) {
                thatsYouText = " (That's you!)";
            } else {
                thatsYouText = "";
            }

            playerDiv.append($("<li style=\"color:" + GroupHRisk.values.players[i].color + "\"/>")
                .append("Player " + (GroupHRisk.values.players[i].id+1) + thatsYouText));
        }
    } else {
        console.warn("[GroupHRisk.functions.updateLobbyPlayerList]: Redirecting request to update player list in lobby to update player list in game due to game state.");
        GroupHRisk.functions.updateInGamePlayerList(-1);
        alert("Game state is inconsistent. Play may be able to continue, but you may need to reload.");
    }
};

/**
 * Updates the list of players in the game
 * Only useful when a game is in progress. Will redirect to lobby list update otherwise, unless already redirected to here.
 * @param activePlayerId The ID of the player currently taking their turn
 */
GroupHRisk.functions.updateInGamePlayerList = function (activePlayerId) {
    'use strict'; //Force ECMAScript 5 Strict mode

    var playerDiv,
        i,
        thatsYouText,
        activePlayerStyle;

    if(GroupHRisk.values.state > GroupHRisk.enums.STATES.JOINLOBBY) {

        console.info("[GroupHRisk.functions.updateInGamePlayerList]: Updating player list");

        playerDiv = $("#players").find("ol").empty();

        for (i = 0; i < GroupHRisk.values.players.length; i += 1) {
            if (GroupHRisk.values.players[i].id === GroupHRisk.values.ownPlayerId) {
                thatsYouText = " (That's you!)";
            } else {
                thatsYouText = "";
            }
            if (GroupHRisk.values.players[i].id === activePlayerId) {
                activePlayerStyle = " font-style:italic;";
            } else {
                activePlayerStyle = "";
            }
            playerDiv.append($("<li style=\"color:" + GroupHRisk.values.players[i].color + ";" + activePlayerStyle + "\"/>")
                .append("Player " + (GroupHRisk.values.players[i].id+1) + thatsYouText));
        }
    } else {
        if(activePlayerId === -1) {
            console.warn("[GroupHRisk.functions.updateInGamePlayerList]: Ignoring redirected request due to game state.");
        } else {
            console.warn("[GroupHRisk.functions.updateInGamePlayerList]: Redirecting request to update player list in game to update player list in lobby due to game state.");
            GroupHRisk.functions.updateLobbyPlayerList();
        }
        alert("Game state is inconsistent. Play may be able to continue, but you may need to reload.");
    }
};

/**
 * Inform the server that we are ready to begin the game, and that it should stop accepting player joins.
 */
GroupHRisk.functions.commenceGame = function () {
    'use strict'; //Force ECMAScript 5 Strict mode

    console.info("[GroupHRisk.functions.commenceGame]: Asking server to begin the game");

    $("#loader").fadeIn("fast", function () {
        //Send a GET request to the API Endpoint to let them know we're ready to begin playing
        $.get("/api/v1/play")
            .done(function (data) { /* Success, do nothing */ })
            .fail(function () {
                console.warn("[GroupHRisk.functions.commenceGame]: An error occurred asking the server to begin the game.");
                alert("An error occurred. Reload and try again.");
            })
            .always(function () { $("#loader").fadeOut("fast"); });
    });
};

/**
 * Handle a click on a country node
 * @param e The click event data
 */
GroupHRisk.functions.nodeClicked = function (e) {
    'use strict'; //Force ECMAScript 5 Strict mode

    var node = e.cyTarget,
        i,
        neighborhood;

    console.info("[GroupHRisk.functions.nodeClicked]: Node "+node.data('id')+" click in state "+GroupHRisk.values.state);

    if(typeof node.data('countryId') === 'undefined') {
        //Ignore clicks on things that aren't countries
        return;
    }

    if (GroupHRisk.values.state === GroupHRisk.enums.STATES.DEPLOY) {

        if (node.data('ownedBy') !== GroupHRisk.values.ownPlayerId) {
            //Not ours to deploy to.
            return;
        }

        if(GroupHRisk.values.freeArmies <= 0) {
            //We haven't been given any armies to deploy... yet...
            return;
        }

        GroupHRisk.values.currentReinforce = node.data('countryId');

        $("#turnDetail").html(
            "<p>Sending army to "+node.data('baseName')+"</p>" +
            "<button onclick=\"GroupHRisk.functions.doDeploy()\">Deploy</button>" +
            "<p>Click another country to change...</p>"
        );
    } else if (GroupHRisk.values.state === GroupHRisk.enums.STATES.REINFORCE) {

        if (node.data('ownedBy') !== GroupHRisk.values.ownPlayerId) {
            //Not ours to deploy to.
            return;
        }

        if(GroupHRisk.values.freeArmies <= 0) {
            //We haven't been given any armies to deploy... yet...
            return;
        }

        GroupHRisk.values.currentReinforce = node.data('countryId');

        $("#turnDetail").html(
            "<p>Sending armies to "+node.data('baseName')+"</p>" +
            "<input type=\"number\" min=\"1\" max=\""+GroupHRisk.values.freeArmies+"\" id=\"reinforceArmyCount\" value=\"1\"><br />" +
            "<button onclick=\"GroupHRisk.functions.updateReinforce()\">Reinforce</button>" +
            "<button onclick=\"GroupHRisk.functions.undoReinforce()\">Remove</button>" +
            "<button onclick=\"GroupHRisk.functions.listReinforce()\">List Selected</button>" +
            "<p>Click another country to change...</p>"
        );
    } else if (GroupHRisk.values.state === GroupHRisk.enums.STATES.ATTACK_FROM) {
        if (node.data('ownedBy') !== GroupHRisk.values.ownPlayerId) {
            //Not ours to attack from.
            return;
        }
        if(node.data('armies') <= 1) {
            //Not enough armies to attack from.
            return;
        }

        GroupHRisk.objects.currentAttack = new GroupHRisk.protos.attack();
        GroupHRisk.objects.currentAttack.attackFrom = node.data('countryId');

        $("#attackFormFrom").text(node.data('baseName'));
        $("#attackPhaseDetail").text("Choose one of your opponents' territories to attack.");
        $("#attackFormUndoButton").removeAttr("disabled");

        GroupHRisk.values.state = GroupHRisk.enums.STATES.ATTACK_TO;
    } else if (GroupHRisk.values.state === GroupHRisk.enums.STATES.ATTACK_TO) {
        if (node.data('ownedBy') === GroupHRisk.values.ownPlayerId) {
            //Our's, can't attack ourselves.
            return;
        }

        neighborhood = GroupHRisk.values.cytoscapeGraph.getElementById("Country" + GroupHRisk.objects.currentAttack.attackFrom).neighborhood(".countries");

        for(i = 0; i < neighborhood.length; i += 1) {
            if(neighborhood[i].data('countryId') === node.data('countryId')) {
                GroupHRisk.objects.currentAttack.attackTo = node.data('countryId');

                $("#attackFormTo").text(node.data('baseName'));
                $("#attackFormArmyCount").removeAttr("readonly")
                    .attr("max", (
                        GroupHRisk.values.cytoscapeGraph.getElementById("Country" + GroupHRisk.objects.currentAttack.attackFrom).data('armies') > 4 ?
                            3 : GroupHRisk.values.cytoscapeGraph.getElementById("Country" + GroupHRisk.objects.currentAttack.attackFrom).data('armies') - 1)
                        )
                    .attr("min", 1)
                    .val(1);
                $("#attackFormSubmit").removeAttr("disabled");
                break;
            }
        }
    } else if (GroupHRisk.values.state === GroupHRisk.enums.STATES.FORTIFY_FROM) {
        if (node.data('ownedBy') !== GroupHRisk.values.ownPlayerId) {
            //Not ours to fortify from.
            return;
        }
        if(node.data('armies') <= 1) {
            //Not enough armies to fortify from.
            return;
        }

        GroupHRisk.objects.currentFortification = new GroupHRisk.protos.fortify();
        GroupHRisk.objects.currentFortification.fortifyFrom = node.data('countryId');

        $("#fortifyFormFrom").text(node.data('baseName'));
        $("#fortifyPhaseDetail").text("Choose one of your territories to fortify.");
        $("#fortifyFormUndoButton").removeAttr("disabled");

        GroupHRisk.values.state = GroupHRisk.enums.STATES.FORTIFY_TO;
    } else if (GroupHRisk.values.state === GroupHRisk.enums.STATES.FORTIFY_TO) {
        if (node.data('ownedBy') !== GroupHRisk.values.ownPlayerId) {
            //Not ours to fortify to.
            return;
        }

        neighborhood = GroupHRisk.values.cytoscapeGraph.getElementById("Country" + GroupHRisk.objects.currentFortification.fortifyFrom).neighborhood(".countries");

        for(i = 0; i < neighborhood.length; i += 1) {
            if(neighborhood[i].data('countryId') === node.data('countryId')) {
                GroupHRisk.objects.currentFortification.fortifyTo = node.data('countryId');

                $("#fortifyFormTo").text(node.data('baseName'));
                $("#fortifyFormArmyCount").removeAttr("readonly")
                    .attr("max", GroupHRisk.values.cytoscapeGraph.getElementById("Country" + GroupHRisk.objects.currentFortification.fortifyFrom).data('armies')-1)
                    .attr("min", 1)
                    .val(1);
                $("#fortifyFormSubmit").removeAttr("disabled");
                break;
            }
        }
    } else if (GroupHRisk.values.state === GroupHRisk.enums.STATES.CLAIM) {
        if (node.data('ownedBy') !== -1) {
            //Not available for claiming
            return;
        }
        $("#loader").fadeIn("fast", function () {
            $.post("/api/v1/claim", {"countryId": node.data('countryId')})
                .done(function (data) { /*Success, do nothing*/ })
                .fail(function () {
                    console.warn("[GroupHRisk.functions.nodeClicked]: Error claiming");
                    alert("An error occurred. Reload and try again.");
                })
                .always(function () {
                    $("#loader").fadeOut("fast");
                });
        });

        GroupHRisk.values.state = GroupHRisk.enums.STATES.SETUPPHASE;
    }

};


GroupHRisk.functions.hideGreyout = function () {
    'use strict'; //Force ECMAScript5 Strict mode
    $("#greyoutinner").remove();
    $("#greyout").remove();
};

GroupHRisk.functions.resetViewArea = function () {
    'use strict'; //Force ECMAScript5 Strict mode
    GroupHRisk.values.cytoscapeGraph.reset();
};

GroupHRisk.functions.toggleContinentLock = function () {
    'use strict'; //Force ECMAScript5 Strict mode
    var continents = GroupHRisk.values.cytoscapeGraph.nodes(".continents");

    if(GroupHRisk.values.continentsLocked) {
        continents.grabify();
        continents.selectify();
        continents.unlock();
    } else {
        continents.ungrabify();
        continents.unselectify();
        continents.lock();
    }

    GroupHRisk.values.continentsLocked = !GroupHRisk.values.continentsLocked;

};


GroupHRisk.functions.rollDice = function () {
    'use strict'; //Force ECMAScript 5 Strict mode

    console.info("[GroupHRisk.functions.rollDice]: Rolling dice in defence");

    $("#loader").fadeIn("fast", function () {
        $.post("/api/v1/defend", {"numDice": $("#defendingDice").val()})
            .done(function (data) {
                if(GroupHRisk.values.cachedDiceResults.length > 0) {
                    GroupHRisk.functions.displayDiceResults();
                } else {
                    $("#turnDetail").html("<p>Please wait...</p>");
                    GroupHRisk.values.state = GroupHRisk.enums.STATES.DICE_WAIT;
                }
            })
            .fail(function () {
                console.warn("[GroupHRisk.functions.rollDice]: Error rolling");
                alert("An error occurred. Reload and try again.");
            })
            .always(function () {
                $("#loader").fadeOut("fast");
            });
    });
};

GroupHRisk.functions.cardClicked = function (cardId) {
    'use strict'; //Force ECMAScript 5 Strict mode

    var arrayPos;

    console.info("[GroupHRisk.functions.cardClicked]: Card "+cardId+" clicked in state "+GroupHRisk.values.state);

    if(GroupHRisk.values.state === GroupHRisk.enums.STATES.TRADE) {
        if(typeof(GroupHRisk.objects.currentTrade.cards) === 'undefined') {
            GroupHRisk.objects.currentTrade = new GroupHRisk.protos.trade();
        }
        if((arrayPos = $.inArray(GroupHRisk.values.ownCards[cardId], GroupHRisk.objects.currentTrade.cards)) !== -1) {
            GroupHRisk.objects.currentTrade.cards.splice(arrayPos, 1);
            $("#card"+cardId).removeClass("selectedCard");
            $("#tradeFormSubmit").attr("disabled", "disabled");
        } else {
            if(GroupHRisk.objects.currentTrade.cards.length === 3) {
                return;
            }
            GroupHRisk.objects.currentTrade.cards.push(GroupHRisk.values.ownCards[cardId]);
            $("#card" + cardId).addClass("selectedCard");

            if (GroupHRisk.objects.currentTrade.cards.length === 3) {
                if (GroupHRisk.objects.currentTrade.cards[0].cardType === GroupHRisk.objects.currentTrade.cards[1].cardType
                    && GroupHRisk.objects.currentTrade.cards[1].cardType === GroupHRisk.objects.currentTrade.cards[2].cardType) {
                    //3 of a kind
                    $("#tradeFormSubmit").removeAttr("disabled");
                } else if(GroupHRisk.objects.currentTrade.cards[0].cardType !== GroupHRisk.objects.currentTrade.cards[1].cardType
                    && GroupHRisk.objects.currentTrade.cards[1].cardType !== GroupHRisk.objects.currentTrade.cards[2].cardType
                    && GroupHRisk.objects.currentTrade.cards[0].cardType !== GroupHRisk.objects.currentTrade.cards[2].cardType) {
                    //3 unique
                    $("#tradeFormSubmit").removeAttr("disabled");
                } else if(GroupHRisk.objects.currentTrade.cards[0].cardType === GroupHRisk.objects.currentTrade.cards[1].cardType
                    && GroupHRisk.objects.currentTrade.cards[2].cardType === GroupHRisk.enums.CARDTYPES.WILD) {
                    //0=1;WILD
                    $("#tradeFormSubmit").removeAttr("disabled");
                } else if(GroupHRisk.objects.currentTrade.cards[0].cardType === GroupHRisk.objects.currentTrade.cards[2].cardType
                    && GroupHRisk.objects.currentTrade.cards[1].cardType === GroupHRisk.enums.CARDTYPES.WILD) {
                    //0=2;WILD
                    $("#tradeFormSubmit").removeAttr("disabled");
                } else if(GroupHRisk.objects.currentTrade.cards[1].cardType === GroupHRisk.objects.currentTrade.cards[2].cardType
                    && GroupHRisk.objects.currentTrade.cards[0].cardType === GroupHRisk.enums.CARDTYPES.WILD) {
                    //1=2;WILD
                    $("#tradeFormSubmit").removeAttr("disabled");
                }
            }
        }
    }
};

GroupHRisk.functions.doTrade = function () {
    'use strict'; //Force ECMAScript 5 Strict mode

    var i;

    console.info("[GroupHRisk.functions.doTrade]: Trading in cards");

    $("#loader").fadeIn("fast", function () {
        $.post("/api/v1/tradecards", {"cards": GroupHRisk.objects.currentTrade.cards})
            .done(function (data) {
                for(i = 0; i < GroupHRisk.objects.currentTrade.cards.length; i += 1) {
                    GroupHRisk.values.ownCards.splice($.inArray(GroupHRisk.objects.currentTrade.cards[i]), 1);
                }
                GroupHRisk.objects.currentTrade = new GroupHRisk.protos.trade();
                $("#turnPhase").find("h3").text("Reinforce Armies");
                $("#turnDetail").html("<p>Click a country to deploy your armies there.</p><p id=\"freeArmyMessage\">(Retrieving army count...)</p>");
                GroupHRisk.values.state = GroupHRisk.enums.STATES.REINFORCE;
            })
            .fail(function () {
                console.warn("[GroupHRisk.functions.doTrade]: Error trading");
                alert("An error occurred. Reload and try again.");
            })
            .always(function () {
                $("#loader").fadeOut("fast");
            });
    });
};

GroupHRisk.functions.skipTrade = function () {
    'use strict'; //Force ECMAScript 5 Strict mode

    $("#turnPhase").find("h3").text("Reinforce Armies");
    $("#turnDetail").html("<p>Click a country to deploy your armies there.</p><p id=\"freeArmyMessage\">(Retrieving army count...)</p>");
    GroupHRisk.values.state = GroupHRisk.enums.STATES.REINFORCE;
};

GroupHRisk.functions.doAttack = function () {
    'use strict'; //Force ECMAScript 5 Strict mode

    console.info("[GroupHRisk.functions.doAttack]: Beginning attack");

    GroupHRisk.objects.currentAttack.armyCount = parseInt($("#attackFormArmyCount").val(),10);

    $("#loader").fadeIn("fast", function () {
        $.post("/api/v1/attack", {"sourceCountry": GroupHRisk.objects.currentAttack.attackFrom,
            "destinationCountry": GroupHRisk.objects.currentAttack.attackTo,
            "armyCount": GroupHRisk.objects.currentAttack.armyCount})
            .done(function (data) {
                $("#turnDetail").html("<p>Attack in progress!</p>");
                GroupHRisk.values.state = GroupHRisk.enums.STATES.DICE_WAIT;
            })
            .fail(function () {
                console.warn("[GroupHRisk.functions.doAttack]: Error attacking");
                alert("An error occurred. Reload and try again.");
            })
            .always(function () {
                $("#loader").fadeOut("fast");
            });
    });
};

GroupHRisk.functions.doDeploy = function () {
    'use strict'; //Force ECMAScript 5 Strict mode

    var deployArray = [[]];

    deployArray[0][0] = GroupHRisk.values.currentReinforce;
    deployArray[0][1] = 1;

    console.info("[GroupHRisk.functions.doDeploy]: Deploying to "+GroupHRisk.values.currentReinforce);

    $("#loader").fadeIn("fast", function () {
        $.post("/api/v1/deploy", {"deploy": JSON.stringify(deployArray)})
            .done(function (data) {
                GroupHRisk.values.players[GroupHRisk.values.ownPlayerId].numArmies += 1;
                GroupHRisk.values.freeArmies -= 1;
                GroupHRisk.values.state = GroupHRisk.enums.STATES.WAITINGFOROTHERPLAYER;
                $("#turnPhase").find("h3").text("Please wait");
                $("#turnDetail").html("<p>Other players are taking their turns.</p>");
            })
            .fail(function () {
                console.warn("[GroupHRisk.functions.nodeClicked]: Error deploying/reinforcing");
                alert("An error occurred. Reload and try again.");
            })
            .always(function () {
                $("#loader").fadeOut("fast");
            });
    });
};

GroupHRisk.functions.updateReinforce = function () {
    'use strict'; //Force ECMAScript 5 Strict mode

    var i,
        arrayPos = null,
        armyCount = $("#reinforceArmyCount").val();

    console.info("[GroupHRisk.functions.updateReinforce]: Reinforcing "+GroupHRisk.values.currentReinforce+" with "+armyCount);

    for(i = 0; i < GroupHRisk.objects.currentReinforce.length; i += 1) {
        if(GroupHRisk.objects.currentReinforce[i][0] === GroupHRisk.values.currentReinforce) {
            arrayPos = i;
            break;
        }
    }

    if(arrayPos === null) {
        GroupHRisk.objects.currentReinforce.push([GroupHRisk.values.currentReinforce, armyCount]);
    } else {
        GroupHRisk.values.freeArmies += parseInt(GroupHRisk.objects.currentReinforce[arrayPos][1],10);
        GroupHRisk.objects.currentReinforce[arrayPos] = [GroupHRisk.values.currentReinforce, armyCount];
    }

    GroupHRisk.values.freeArmies -= parseInt(armyCount,10);

    GroupHRisk.functions.listReinforce();
};

GroupHRisk.functions.undoReinforce = function () {
    'use strict'; //Force ECMAScript 5 Strict mode

    var i,
        arrayPos;

    console.info("[GroupHRisk.functions.undoReinforce]: No longer reinforcing "+GroupHRisk.values.currentReinforce);

    for(i = 0; i < GroupHRisk.objects.currentReinforce.length; i += 1) {
        if(GroupHRisk.objects.currentReinforce[i][0] === GroupHRisk.values.currentReinforce) {
            arrayPos = i;
            break;
        }
    }

    if(arrayPos !== null) {
        GroupHRisk.values.freeArmies += parseInt(GroupHRisk.objects.currentReinforce[i][1],10);
        GroupHRisk.objects.currentReinforce.splice(arrayPos, 1);
    }

    GroupHRisk.functions.listReinforce();
};

GroupHRisk.functions.listReinforce = function () {
    'use strict'; //Force ECMAScript 5 Strict mode

    var i,
        countryName,
        armyCount;

    $("#turnDetail").html(
        "<p>Reinforcing (click to edit/remove):</p>" +
        "<ul id=\"reinforceList\"></ul>" +
        ((GroupHRisk.values.freeArmies === 0) ?
            "<button onclick=\"GroupHRisk.functions.doReinforce()\">Complete Reinforce</button>" :
            "<p>You have "+GroupHRisk.values.freeArmies+" units to place.</p>"));

    for(i = 0; i < GroupHRisk.objects.currentReinforce.length; i += 1) {
        countryName = GroupHRisk.values.cytoscapeGraph.getElementById("Country"+GroupHRisk.objects.currentReinforce[i][0]).data('baseName');
        armyCount = GroupHRisk.objects.currentReinforce[i][1];
       $("#reinforceList").append(
           $("<li onclick=\"GroupHRisk.functions.editReinforce("+i+")\">"+countryName+" - "+armyCount+" armies</li>")
       );
    }
};

GroupHRisk.functions.editReinforce = function (id) {
    'use strict'; //Force ECMAScript 5 Strict mode

    var currentArmyCount = GroupHRisk.objects.currentReinforce[id][1],
        countryName = GroupHRisk.values.cytoscapeGraph.getElementById("Country"+GroupHRisk.objects.currentReinforce[id][0]).data('baseName');

    GroupHRisk.values.currentReinforce = GroupHRisk.objects.currentReinforce[id][0];

    $("#turnDetail").html(
        "<p>Sending armies to "+countryName+"</p>" +
        "<input type=\"number\" min=\"1\" max=\""+(GroupHRisk.values.freeArmies+currentArmyCount)+"\" id=\"reinforceArmyCount\" value=\""+currentArmyCount+"\"><br />" +
        "<button onclick=\"GroupHRisk.functions.updateReinforce()\">Reinforce</button>" +
        "<button onclick=\"GroupHRisk.functions.undoReinforce()\">Remove</button>" +
        "<button onclick=\"GroupHRisk.functions.listReinforce()\">List Selected</button>" +
        "<p>Click another country to change...</p>"
    );
};



GroupHRisk.functions.doReinforce = function () {
    'use strict'; //Force ECMAScript 5 Strict mode

    console.info("[GroupHRisk.functions.doReinforce]: Sending Reinforce data");
    $("#loader").fadeIn("fast", function () {
        $.post("/api/v1/deploy", {"deploy": JSON.stringify(GroupHRisk.objects.currentReinforce)})
            .done(function (data) {
                GroupHRisk.functions.enterAttackPhase();
            })
            .fail(function () {
                console.warn("[GroupHRisk.functions.nodeClicked]: Error reinforcing");
                alert("An error occurred. Reload and try again.");
            })
            .always(function () {
                $("#loader").fadeOut("fast");
            });
    });
};


GroupHRisk.functions.doFortify = function () {
    'use strict'; //Force ECMAScript 5 Strict mode

    console.info("[GroupHRisk.functions.doFortify]: Beginning fortify in state "+GroupHRisk.values.state);

    GroupHRisk.objects.currentFortification.armyCount = parseInt($("#fortifyFormArmyCount").val(),10);

    $("#loader").fadeIn("fast", function () {
        $.post("/api/v1/fortify", {"sourceCountry": GroupHRisk.objects.currentFortification.fortifyFrom,
            "destinationCountry": GroupHRisk.objects.currentFortification.fortifyTo,
            "armyCount": GroupHRisk.objects.currentFortification.armyCount})
            .done(function (data) {
                if(GroupHRisk.values.state === GroupHRisk.enums.STATES.MOVE) {
                    GroupHRisk.functions.enterAttackPhase();
                } else {
                    $("#turnDetail").html("<p>Fortifying...</p>");
                }
            })
            .fail(function () {
                console.warn("[GroupHRisk.functions.doFortify]: Error fortifying");
                alert("An error occurred. Reload and try again.");
            })
            .always(function () {
                $("#loader").fadeOut("fast");
            });
    });
};

GroupHRisk.functions.attackFormUndo = function () {
    'use strict'; //Force ECMAScript5 Strict mode

    if(typeof(GroupHRisk.objects.currentAttack.attackTo) === "undefined" || GroupHRisk.objects.currentAttack.attackTo === null) {
        GroupHRisk.objects.currentAttack.attackFrom = undefined;
        $("#attackFormFrom").text("?");
        $("#attackFormUndoButton").attr("disabled", "disabled");
        GroupHRisk.values.state = GroupHRisk.enums.STATES.ATTACK_FROM;
    } else {
        GroupHRisk.objects.currentAttack.attackTo = undefined;
        $("#attackFormTo").text("?");
        $("#attackFormSubmit").attr("disabled", "disabled");
        $("#attackFormArmyCount").attr("readonly").val("");
        GroupHRisk.values.state = GroupHRisk.enums.STATES.ATTACK_TO;
    }
};

GroupHRisk.functions.fortifyFormUndo = function () {
    'use strict'; //Force ECMAScript5 Strict mode

    if(typeof(GroupHRisk.objects.currentFortification.fortifyTo) === 'undefined' || GroupHRisk.objects.currentFortification.fortifyTo === null) {
        GroupHRisk.objects.currentFortification.fortifyFrom = undefined;
        $("#fortifyFormFrom").text("?");
        $("#fortifyFormUndoButton").attr("disabled", "disabled");
        GroupHRisk.values.state = GroupHRisk.enums.STATES.FORTIFY_FROM;
    } else {
        GroupHRisk.objects.currentFortification.fortifyTo = undefined;
        $("#fortifyFormTo").text("?");
        $("#fortifyFormSubmit").attr("disabled", "disabled");
        $("#fortifyFormArmyCount").attr("readonly").val("");
        GroupHRisk.values.state = GroupHRisk.enums.STATES.FORTIFY_TO;
    }
};

GroupHRisk.functions.enterAttackPhase = function () {
    'use strict'; //Force ECMAScript5 Strict mode

    GroupHRisk.values.state = GroupHRisk.enums.STATES.ATTACK_FROM;
    $("#turnPhase").find("h3").text("Attack A Country");
    $("#turnDetail").html("<p>Attack From: <span id=\"attackFormFrom\">?</span><br />" +
    "Attack To: <span id=\"attackFormTo\">?</span><br />" +
    "Attack With: <input type=\"number\" id=\"attackFormArmyCount\" readonly=\"readonly\"/></p>" +
    "<p id=\"attackPhaseDetail\">Choose one of your territories to attack from.</p>" +
    "<button onclick=\"GroupHRisk.functions.attackFormUndo()\" id=\"attackFormUndoButton\" disabled=\"disabled\">Undo</button>" +
    "<button onclick=\"GroupHRisk.functions.doAttack()\" id=\"attackFormSubmit\" disabled=\"disabled\">Attack</button>" +
    "<button onclick=\"GroupHRisk.functions.endAttack()\">End</button>");
};

GroupHRisk.functions.enterFortifyPhase = function () {
    'use strict'; //Force ECMAScript5 Strict mode

    GroupHRisk.values.state = GroupHRisk.enums.STATES.FORTIFY_FROM;
    $("#turnPhase").find("h3").text("Fortify Country");
    $("#turnDetail").html("<p>Fortify From: <span id=\"fortifyFormFrom\">?</span><br />" +
    "Fortify To: <span id=\"fortifyFormTo\">?</span><br />" +
    "Fortify With: <input type=\"number\" id=\"fortifyFormArmyCount\" readonly=\"readonly\"/></p>" +
    "<p id=\"attackPhaseDetail\">Choose one of your territories to fortify from.</p>" +
    "<button onclick=\"GroupHRisk.functions.fortifyFormUndo()\" id=\"fortifyFormUndoButton\" disabled=\"disabled\">Undo</button>" +
    "<button onclick=\"GroupHRisk.functions.doFortify()\" id=\"fortifyFormSubmit\" disabled=\"disabled\">Fortify</button>" +
    "<button onclick=\"GroupHRisk.functions.yieldTurn()\">Skip</button>");
};

GroupHRisk.functions.endAttack = function () {
    'use strict'; //Force ECMAScript 5 Strict mode

    GroupHRisk.functions.enterFortifyPhase();
};

GroupHRisk.functions.yieldTurn = function () {
    'use strict'; //Force ECMAScript5 Strict mode

    $("#loader").fadeIn("fast", function () {
        $.get("/api/v1/yield")
            .done(function (data) {
                $("#turnPhase").find("h3").text("Turn completed");
                $("#turnDetail").html("<p>Next player...</p>");
            })
            .fail(function () {
                console.warn("[GroupHRisk.functions.yieldTurn]: Error yielding turn");
                alert("An error occurred. Reload and try again.");
            })
            .always(function () {
                $("#loader").fadeOut("fast");
            });
    });
};

GroupHRisk.functions.updateCardSet = function () {
    'use strict'; //Force ECMAScript5 Strict mode

    var playerDiv = $("#cards").find("ul").empty(),
        i,
        cardTypeText,
        countryId;

    for (i = 0; i < GroupHRisk.values.ownCards.length; i += 1) {
        countryId = GroupHRisk.values.ownCards[i].countryId;
        switch(GroupHRisk.values.ownCards[i].cardType) {
            case GroupHRisk.enums.CARDTYPES.ARTILLERY:
                cardTypeText = "Artillery";
                break;
            case GroupHRisk.enums.CARDTYPES.CAVALRY:
                cardTypeText = "Cavalry";
                break;
            case GroupHRisk.enums.CARDTYPES.INFANTRY:
                cardTypeText = "Infantry";
                break;
            case GroupHRisk.enums.CARDTYPES.WILD:
                cardTypeText = "Wildcard";
                break;
        }

        playerDiv.append($("<li id=\"card"+i+"\" onclick=\"GroupHRisk.functions.cardClicked("+i+")\"/>")
            .append(cardTypeText+" - "+GroupHRisk.values.cytoscapeGraph.getElementById("Country"+countryId).data('baseName')));
    }
};

GroupHRisk.functions.displayDiceResults = function () {
    'use strict'; //Force ECMAScript 5 Strict mode

    var resultsTable = $("<table />"),
        resultsRow = $("<tr id=\"resultsrow\" />"),
        i;

    resultsTable.append(resultsRow);

    for(i = 0; i < GroupHRisk.values.cachedDiceResults.length; i++) {
        resultsRow.append($("<td style=\"width:33%\"><img src=\"/images/dice/"+GroupHRisk.values.cachedDiceResults[i]+".png\"></td>"));
    }

    $("#turnPhase").find("h3").text("Dice Results");
    $("#turnDetail").html(resultsTable);

    GroupHRisk.values.cachedDiceResults = [];
};


//Socket Message Handlers
/**
 * Received Socket Message Handlers
 * @type {{}}
 * @namespace GroupHRisk.socketHandlers
 */
GroupHRisk.socketHandlers = GroupHRisk.socketHandlers || { };
/**
 * Our request to host a game was accepted
 * @param socketMessage The message from the UI Server
 */
GroupHRisk.socketHandlers.requestToHostAccepted = function (socketMessage) {
    'use strict'; //Force ECMAScript 5 Strict mode

    var rightBarDiv;

    $("#loader").fadeIn("fast", function () {
        if (GroupHRisk.values.state !== GroupHRisk.enums.STATES.NOTINGAME) {
            console.warn("[GroupHRisk.socketHandlers.requestToHostAccepted]: Request to host accepted in wrong stage.");
            alert("An error occurred. Reload and try again.");
            GroupHRisk.functions.processSocketMessage();
            return;
        }

        console.info("[GroupHRisk.socketHandlers.requestToHostAccepted]: Request to host game accepted.");

        GroupHRisk.values.state = GroupHRisk.enums.STATES.HOSTINGLOBBY;
        console.info("State set to "+GroupHRisk.values.state);
        rightBarDiv = $("#rightbar").empty();
        $.get("/ajax/game-lobby.html")
            .done(function (data) {
                rightBarDiv.append(data);
                GroupHRisk.values.players.push(new GroupHRisk.protos.player(0, "#FF0000"));
                GroupHRisk.values.ownPlayerId = 0;
                GroupHRisk.functions.updateLobbyPlayerList();
                rightBarDiv.append($("<input/>").attr("id", "commenceGameButton").attr("type", "submit")
                    .attr("value", "Start").attr("onclick", "GroupHRisk.functions.commenceGame()").attr("disabled", "disabled"));
            })
            .fail(function () {
                console.warn("[GroupHRisk.socketHandlers.requestToHostAccepted]: Error while loading game lobby for side bar");
                alert("An error occurred. Reload and try again.");
            })
            .always(function () {
                $("#loader").fadeOut("fast");
                GroupHRisk.functions.processSocketMessage();
            });
    });
};

/**
 * Our request to join a game was accepted
 * @param socketMessage The message from the UI Server
 */
GroupHRisk.socketHandlers.requestToJoinAccepted = function (socketMessage) {
    'use strict'; //Force ECMAScript 5 Strict mode

    var myPlayerId = parseInt(socketMessage.myPlayerId,10),
        rightBarDiv;

    console.info("[GroupHRisk.socketHandlers.requestToJoinAccepted]: Request to join game processing");

    $("#loader").fadeIn("fast", function () {
        if (GroupHRisk.values.state !== GroupHRisk.enums.STATES.NOTINGAME) {
            console.info("[GroupHRisk.socketHandlers.requestToJoinAccepted]: Request to join game accepted, but in wrong state");
            alert("An error occurred. Reload and try again.");
            GroupHRisk.functions.processSocketMessage();
            return;
        }
        GroupHRisk.values.state = GroupHRisk.enums.STATES.JOINLOBBY;
        rightBarDiv = $("#rightbar").empty();
        $.get("/ajax/game-lobby.html")
            .done(function (data) {
                rightBarDiv.append(data);
                GroupHRisk.values.players.push(new GroupHRisk.protos.player(myPlayerId, GroupHRisk.constants.COLORS[myPlayerId]));
                GroupHRisk.values.ownPlayerId = myPlayerId;
                GroupHRisk.functions.updateLobbyPlayerList();
            })
            .fail(function () {
                console.warn("[GroupHRisk.socketHandlers.requestToJoinAccepted]: Error while loading game lobby for side bar");
                alert("An error occurred. Reload and try again.");
            })
            .always(function () {
                $("#loader").fadeOut("fast");
                GroupHRisk.functions.processSocketMessage();
            });


    });
};

/**
 * The game is about to start.
 * (The next phase is territory claiming, which we'll be notified about by another message)
 * @param socketMessage The message from the UI Server
 */
GroupHRisk.socketHandlers.gameStartedNotification = function (socketMessage) {
    'use strict'; //Force ECMAScript 5 Strict mode

    var i,
        rightBarDiv;

    if (GroupHRisk.values.state > GroupHRisk.enums.STATES.JOINLOBBY) {
        console.warn("[GroupHRisk.socketHandlers.gameStartedNotification]: Ignoring request for game start while game has already started.");
        alert("An error occurred. Reload and try again.");
        GroupHRisk.functions.processSocketMessage();
        return;
    }

    console.info("[GroupHRisk.socketHandlers.gameStartedNotification]: Game is about to commence. Good luck!");

    rightBarDiv = $("#rightbar").empty();
    $.get("/ajax/in-game.html")
        .done(function (data) {
            rightBarDiv.append(data);
            GroupHRisk.values.state = GroupHRisk.enums.STATES.SETUPPHASE;
            GroupHRisk.functions.updateInGamePlayerList(0);
        })
        .fail(function () {
            console.warn("[GroupHRisk.socketHandlers.gameStartedNotification]: Error while loading in game side bar data");
            alert("An error occurred. Reload and try again.");
        })
        .always(function () {
            $("#loader").fadeOut("fast");
            GroupHRisk.functions.processSocketMessage();
        });

};

/**
 * A Player has joined the game
 * @param socketMessage The message from the UI Server
 */
GroupHRisk.socketHandlers.playerJoinNotification = function (socketMessage) {
    'use strict'; //Force ECMAScript 5 Strict mode

    var playerId;

    if (GroupHRisk.values.state !== GroupHRisk.enums.STATES.HOSTINGLOBBY && GroupHRisk.values.state !== GroupHRisk.enums.STATES.JOINLOBBY) {
        console.warn("[GroupHRisk.socketHandlers.playerJoinNotification]: Ignoring join notification before game has been set up or after game started");
        alert("Received a join notification while not waiting for players to join. Ignoring.");
        GroupHRisk.functions.processSocketMessage();
        return;
    }

    console.info("[GroupHRisk.socketHandlers.playerJoinNotification]: Player has joined");

    playerId = GroupHRisk.values.players.length;

    GroupHRisk.values.players.push(new GroupHRisk.protos.player(playerId, GroupHRisk.constants.COLORS[playerId]));
    GroupHRisk.functions.updateLobbyPlayerList();
    GroupHRisk.functions.processSocketMessage();

};

/**
 * The engine has sent us the map to use
 * @param socketMessage The message from the UI Server
 */
GroupHRisk.socketHandlers.mapTransfer = function (socketMessage) {
    'use strict'; //Force ECMAScript 5 Strict mode

    var mapSource = socketMessage.payload,
        mapNodes = [],
        mapEdges = [],
        i,
        j;

    if (GroupHRisk.values.state >= 3) {
        console.warn("[GroupHRisk.socketHandlers.mapTransfer]: Ignoring map transfer before game has been set up or after game started - "+GroupHRisk.values.state);
        alert("Received a map transfer while not waiting for map. Ignoring.");
        GroupHRisk.functions.processSocketMessage();
        return;
    }
    console.info("[GroupHRisk.socketHandlers.mapTransfer]: Receiving and parsing map from Engine");
    console.info(JSON.stringify(socketMessage));

    for (i = 0; i < Object.keys(mapSource.continents).length; i+= 1) {
        mapNodes.push(
            {
                data: {
                    id: "Continent" + i,
                    displayText: mapSource.continent_names[i],
                    parent: undefined,
                    value: mapSource.continent_values[i]
                },
                //selectable: true,
                classes: 'continents'
            }
        );
        for (j = 0; j < mapSource.continents[i].length; j += 1) {
            mapNodes.push(
                {
                    data : {
                        armies: 0,
                        id: "Country" + mapSource.continents[i][j],
                        parent: "Continent" + i,
                        countryId: mapSource.continents[i][j],
                        baseName: mapSource.country_names[mapSource.continents[i][j]],
                        displayText: mapSource.country_names[mapSource.continents[i][j]] + " (0)",
                        ownedBy: -1
                    },
                    //grabbable: false,
                    classes: 'countries'
                }
            );
        }
    }

    for (i = 0; i < mapSource.connections.length; i += 1) {
        mapEdges.push(
            {
                data: {
                    id: "Connection" + mapSource.connections[i][0] + "-" + mapSource.connections[i][1],
                    source: "Country" + mapSource.connections[i][0],
                    target: "Country" + mapSource.connections[i][1]
                },
                selectable: false
            }
        );
    }

    $("#gamemapinner").cytoscape({
        elements: {
            nodes: mapNodes,
            edges: mapEdges
        },
        layout: {
            name: 'cose',
            padding: 5
        },
        style: [
            {
                selector: 'node',
                css: {
                    'content': 'data(displayText)'
                }
            },
            {
                selector: 'node.continents',
                css: {
                    'font-size': '0.5em',
                    'font-style': 'italic'
                }
            },
            {
                selector: 'node.countries',
                css: {
                    'text-halign': 'center',
                    'text-valign': 'center'
                }
            },
            {
                selector: 'edge',
                css: {
                    'curve-style': 'force-bezier'
                }
            },
            {
                selector: 'node.nolabel',
                css: {
                    'content': ''
                }
            }
        ],
        selectionType: 'single',

        ready: function () {
            GroupHRisk.values.cytoscapeGraph = this;

            GroupHRisk.values.cytoscapeGraph.elements().unselectify();

            GroupHRisk.values.cytoscapeGraph.on('tap', 'node', function (e) {
                GroupHRisk.functions.nodeClicked(e);
            });
        }
    });


    //Only allow this if we are the HOST, since we can only say when we're ready to start if we are the host
    if (GroupHRisk.values.state === GroupHRisk.enums.STATES.HOSTINGLOBBY && GroupHRisk.values.players.length > 1) {
        $("#commenceGameButton").removeAttr("disabled");
    }
    GroupHRisk.functions.processSocketMessage();
};

/**
 * It's someone's (maybe us) turn to play (post-setup)!
 */
GroupHRisk.socketHandlers.turnNotification = function (socketMessage) {
    'use strict'; //Force ECMAScript 5 Strict mode

    var newPlayerId = parseInt(socketMessage.newPlayer,10),
        phase = parseInt(socketMessage.phase,10);

    if (GroupHRisk.values.state <= GroupHRisk.enums.STATES.JOINLOBBY) {
        console.warn("[GroupHRisk.socketHandlers.turnNotification]: Ignoring request for turn change when not in game.");
        alert("An error occurred. Reload and try again.");
        GroupHRisk.functions.processSocketMessage();
        return;
    }

    console.info("[GroupHRisk.socketHandlers.turnNotification]: It's now "+newPlayerId+"'s turn.");

    if(phase < GroupHRisk.enums.PHASES.STANDARD) {
        GroupHRisk.values.state = GroupHRisk.enums.STATES.SETUPPHASE;
    } else {
        GroupHRisk.values.state = GroupHRisk.enums.STATES.WAITINGFOROTHERPLAYER;
    }

    if(newPlayerId === GroupHRisk.values.ownPlayerId) {
        GroupHRisk.objects.currentReinforce = [];
        if(phase === GroupHRisk.enums.PHASES.DEPLOY) {
            GroupHRisk.values.state = GroupHRisk.enums.STATES.DEPLOY;
            $("#turnPhase").find("h3").text("Deploy Armies");
            $("#turnDetail").html("<p>Click a country to deploy your armies there.</p><p id=\"freeArmyMessage\">(Retrieving army count...)</p>");
            GroupHRisk.values.state = GroupHRisk.enums.STATES.DEPLOY;
        } else if(phase === GroupHRisk.enums.PHASES.CLAIM) {
            GroupHRisk.values.state = GroupHRisk.enums.STATES.CLAIM;
            $("#turnPhase").find("h3").text("Claim Territory");
            $("#turnDetail").html("<p>Click an unclaimed territory to claim it.</p>");
        } else {
            if(GroupHRisk.values.ownCards.length === 0) {
                //TODO: Auto-send no-trade?
                $("#turnPhase").find("h3").text("Reinforce Armies");
                $("#turnDetail").html("<p>Click a country to deploy your armies there.</p><p id=\"freeArmyMessage\">(Retrieving army count...)</p>");
                GroupHRisk.values.state = GroupHRisk.enums.STATES.REINFORCE;
            } else {
                GroupHRisk.values.state = GroupHRisk.enums.STATES.TRADE;
                $("#turnPhase").find("h3").text("Trade cards");
                $("#turnDetail").html(
                    "<p>Click three of your cards below to trade.</p>" +
                    "<p>They should three of a kind, or three unique. You can use a wildcard anywhere.</p>" +
                    ((typeof(GroupHRisk.values.ownCards) !== 'undefined' && GroupHRisk.values.ownCards.length >= 5) ?
                        "<p>You must trade in a set this turn.</p>" :
                        "<button onclick=\"GroupHRisk.functions.skipTrade()\">Skip</button>") +
                    "<button id=\"tradeFormSubmit\" onclick=\"GroupHRisk.functions.doTrade()\" disabled=\"disabled\">Trade</button>"
                );
            }
        }
        if($("#audioding")[0].isPlaying) {
            $("#audioding")[0].currentTime = 0;
        }
        $("#audioding")[0].play();
    } else {
        $("#turnPhase").find("h3").text("Please wait");
        $("#turnDetail").html("<p>Other players are taking their turns.</p>");
    }
    GroupHRisk.functions.updateInGamePlayerList(newPlayerId);
    GroupHRisk.functions.processSocketMessage();
};

/**
 * A territory is either changing ruler or forces count
 * @param socketMessage The message from the UI Server
 */
GroupHRisk.socketHandlers.territoryChangeNotification = function (socketMessage) {
    'use strict'; //Force ECMAScript 5 Strict mode

    var territoryId = parseInt(socketMessage.countryId, 10),
        newOwnerPlayerId = parseInt(socketMessage.playerId, 10),
        newArmyCount = parseInt(socketMessage.armyCount, 10),
        territoryNode,
        otherTerritoryNode,
        i;

    if (GroupHRisk.values.state <= GroupHRisk.enums.STATES.JOINLOBBY) {
        console.warn("[GroupHRisk.socketHandlers.territoryChangeNotification]: Ignoring request for territory change when not in game.");
        alert("An error occurred. Reload and try again.");
        GroupHRisk.functions.processSocketMessage();
        return;
    }
    console.info("[GroupHRisk.socketHandlers.territoryChangeNotification]: Territory "+territoryId+" has changed to owner"+newOwnerPlayerId+" with "+newArmyCount+" armies.");

    territoryNode = GroupHRisk.values.cytoscapeGraph.getElementById("Country" + territoryId);

    if(newOwnerPlayerId === GroupHRisk.values.ownPlayerId) {
        if (territoryNode.data('ownedBy') !== GroupHRisk.values.ownPlayerId) {
            if(GroupHRisk.values.state !== GroupHRisk.enums.STATES.CLAIM) {
                if(newArmyCount === 0) {
                    otherTerritoryNode = GroupHRisk.values.cytoscapeGraph.getElementById("Country"+GroupHRisk.objects.currentAttack.attackFrom);
                    GroupHRisk.objects.currentFortification = new GroupHRisk.protos.fortify();
                    GroupHRisk.objects.currentFortification.fortifyFrom = otherTerritoryNode.data('countryId');
                    GroupHRisk.objects.currentFortification.fortifyTo = territoryNode.data('countryId');

                    GroupHRisk.values.states = GroupHRisk.enums.STATES.MOVE;

                    $("#turnPhase").find("h3").text("Move Armies");
                    $("#turnDetail").html("<p>Move From: <span id=\"fortifyFormFrom\">"+otherTerritoryNode.data('baseName')+"</span><br />" +
                    "Move To: <span id=\"fortifyFormTo\">"+territoryNode.data('baseName')+"</span><br />" +
                    "Move: <input type=\"number\" id=\"fortifyFormArmyCount\" min=\"1\" max=\""+(otherTerritoryNode.data('armies')-1)+"\"/></p>" +
                    "<p id=\"attackPhaseDetail\">Choose how many armies to move to your new territory.</p>" +
                    "<button onclick=\"GroupHRisk.functions.doFortify()\" id=\"fortifyFormSubmit\">Fortify</button>");
                }
                if($("#audiosuccess")[0].isPlaying) {
                    $("#audiosuccess")[0].currentTime = 0;
                }
                $("#audiosuccess")[0].play();
            }
        }
    } else if(territoryNode.data('ownedBy') === GroupHRisk.values.ownPlayerId) {
        if(newOwnerPlayerId !== GroupHRisk.values.ownPlayerId) {
            if($("#audiowahwah")[0].isPlaying) {
                $("#audiowahwah")[0].currentTime = 0;
            }
            $("#audiowahwah")[0].play();
        }
    }

    territoryNode.data('ownedBy', newOwnerPlayerId);
    territoryNode.data('displayText', territoryNode.data('baseName')+" ("+newArmyCount+")");
    territoryNode.data('armies', newArmyCount);

    for(i = 0 ; i < GroupHRisk.values.players.length; i += 1) {
        if(GroupHRisk.values.players[i].id === newOwnerPlayerId) {
            GroupHRisk.values.cytoscapeGraph.style().selector("node#Country" + territoryId)
                .css({"background-color": GroupHRisk.values.players[i].color}).update();
            break;
        }
    }

    territoryNode.flashClass('nolabel',1);

    GroupHRisk.functions.processSocketMessage();

};

/**
 * Someone (not us) is attacking someone else (maybe us)
 * @param socketMessage The message from the UI Server
 */
GroupHRisk.socketHandlers.attackNotification = function (socketMessage) {
    'use strict'; //Force ECMAScript 5 Strict mode

    var fromPlayerId = parseInt(socketMessage.fromPlayerId,10),
        fromCountryId = parseInt(socketMessage.fromCountryId,10),
        toPlayerId = parseInt(socketMessage.toPlayerId,10),
        toCountryId = parseInt(socketMessage.toCountryId,10),
        armyCount = parseInt(socketMessage.armyCount,10),
        fromCountryName,
        toCountryName,
        defendingArmyCount;

    if (GroupHRisk.values.state <= GroupHRisk.enums.STATES.JOINLOBBY) {
        console.warn("[GroupHRisk.socketHandlers.attackNotification]: Ignoring attack notification when not in game.");
        alert("An error occurred. Reload and try again.");
        GroupHRisk.functions.processSocketMessage();
        return;
    }

    if(toPlayerId === GroupHRisk.values.ownPlayerId) {
        console.info("[GroupHRisk.socketHandlers.attackNotification]: User is under attack!");

        fromCountryName = GroupHRisk.values.cytoscapeGraph.getElementById("Country" + fromCountryId).data('baseName');
        toCountryName = GroupHRisk.values.cytoscapeGraph.getElementById("Country" + toCountryId).data('baseName');
        defendingArmyCount = (GroupHRisk.values.cytoscapeGraph.getElementById("Country" + toCountryId).data('armies') === 1 ?
            GroupHRisk.values.cytoscapeGraph.getElementById("Country" + toCountryId).data('armies') : 2);

        $("#turnPhase").find("h3").text("Under Attack!");
        $("#turnDetail").html(
            "<p>Player "+(GroupHRisk.values.players[fromPlayerId].id+1)+
            " has attacked country <strong>" + toCountryName + "</strong> from country <strong>" +
            fromCountryName + "</strong> with " + armyCount + " armies...</p>" +
            "<p>Select how many dice you wish to defend with: " +
            "<input id=\"defendingDice\" type=\"number\" min=\"1\" max=\""+defendingArmyCount+"\"></p>" +
            "<button onclick=\"GroupHRisk.functions.rollDice()\" id=\"diceRollButton\" disabled=\"disabled\">Please Wait</button>"
        );
        if($("#audiodingding")[0].isPlaying) {
            $("#audiodingding")[0].currentTime = 0;
        }
        $("#audiodingding")[0].play();
        GroupHRisk.functions.processSocketMessage();
    } else {
        console.info("[GroupHRisk.socketHandlers.attackNotification]: Ignoring attack on someone else");
        GroupHRisk.functions.processSocketMessage();
    }



};

/**
 * The engine has rolled dice
 * @param socketMessage The message from the UI Server
 */
GroupHRisk.socketHandlers.rollResult = function (socketMessage) {
    'use strict'; //Force ECMAScript 5 Strict mode

    if (GroupHRisk.values.state <= GroupHRisk.enums.STATES.JOINLOBBY) {
        console.warn("[GroupHRisk.socketHandlers.rollResult]: Ignoring roll notification when not in game.");
        alert("An error occurred. Reload and try again.");
        GroupHRisk.functions.processSocketMessage();
        return;
    }

    GroupHRisk.values.cachedDiceResults = socketMessage.results;

    if(GroupHRisk.values.state === GroupHRisk.enums.STATES.DICE_WAIT) {
        GroupHRisk.functions.displayDiceResults();
    }

    GroupHRisk.functions.processSocketMessage();
};

/**
 * The set of players connected to the game has changed
 * @param socketMessage
 */
GroupHRisk.socketHandlers.lobbyUpdate = function (socketMessage) {
    'use strict'; //Force ECMAScript 5 Strict mode

    var playerSet = socketMessage.players,
        i,
        playerId;

    console.info("[GroupHRisk.socketHandlers.lobbyUpdate]: Processing lobby update");

    GroupHRisk.values.ownPlayerId = parseInt(socketMessage.myPlayerId,10);
    GroupHRisk.values.players.length = 0;

    for(i = 0; i < playerSet.length; i += 1) {
        playerId = parseInt(playerSet[i].playerId,10);

        GroupHRisk.values.players.push(new GroupHRisk.protos.player(playerId, GroupHRisk.constants.COLORS[playerId]));
    }

    GroupHRisk.functions.updateLobbyPlayerList();

    if(GroupHRisk.values.state === GroupHRisk.enums.STATES.HOSTINGLOBBY && typeof GroupHRisk.values.cytoscapeGraph.elements !== 'undefined' && playerSet.length > 1) {
        $("#commenceGameButton").removeAttr("disabled");
    }

    GroupHRisk.functions.processSocketMessage();

};

GroupHRisk.socketHandlers.battleResult = function (socketMessage) {
    'use strict'; //Force ECMAScript 5 Strict mode

    var attackingPlayerId = parseInt(socketMessage.attackingPlayer,10),
        attackingLosses = parseInt(socketMessage.attackingLosses,10),
        defendingPlayerId = parseInt(socketMessage.defendingPlayer,10),
        defendingLosses = parseInt(socketMessage.defendingLosses,10);

    console.info("[GroupHRisk.socketHandlers.battleResult]: Processing battle result between "+attackingPlayerId+" and "+defendingPlayerId);

    if(attackingPlayerId === GroupHRisk.values.ownPlayerId) {
        $("#turnPhase").find("h3").text("Battle Ended!");
        $("#turnDetail").append(
            "<p>" +
            "   You lost <strong>"+attackingLosses+"</strong> armies.<br />" +
            "   Your opponent lost <strong>"+defendingLosses+"</strong> armies.<br />" +
            "    <button onclick=\"GroupHRisk.functions.enterAttackPhase()\">Continue</button>" +
            "</p>"
        );
        GroupHRisk.values.state = GroupHRisk.enums.STATES.MOVE;
    } else if(defendingPlayerId === GroupHRisk.values.ownPlayerId) {
        $("#turnPhase").find("h3").text("Battle Ended!");
        $("#turnDetail").append(
            "<p>" +
            "   You lost <strong>"+defendingLosses+"</strong> armies.<br />" +
            "   Your opponent lost <strong>"+attackingLosses+"</strong> armies." +
            "</p>"
        );
        GroupHRisk.values.state = GroupHRisk.enums.STATES.WAITINGFOROTHERPLAYER;
    }

    GroupHRisk.functions.processSocketMessage();

};

GroupHRisk.socketHandlers.cheatNotification = function (socketMessage) {
    'use strict'; //Force ECMAScript 5 Strict mode

    var cheatingPlayer = parseInt(socketMessage.playerId,10);

    console.info("[GroupHRisk.socketHandlers.cheatNotification]: Stopping gameplay due to cheater!");
    alert("Left game, because Player "+(cheatingPlayer+1)+" was cheating.");
    GroupHRisk.values.state = GroupHRisk.enums.STATES.GAMEOVER;

    $("#turnPhase").find("h3").text("Game Over");
    $("#turnDetail").html("<p>Left game, because Player "+(cheatingPlayer+1)+" was cheating.</p>");
};

GroupHRisk.socketHandlers.defendNotification = function (socketMessage) {
    'use strict'; //Force ECMAScript 5 Strict mode

    var defendingPlayerId = parseInt(socketMessage.fromPlayerId,10),
        armyCount = parseInt(socketMessage.armyCount,10);

    if(GroupHRisk.values.state === GroupHRisk.enums.STATES.ATTACK_TO) {

        console.info("[GroupHRisk.socketHandlers.defendNotification]: Defence notification received.");

        $("#turnDetail").append("<p>Player " + (defendingPlayerId + 1) + " is defending with " + armyCount + " armies.</p>");
    } else {
        console.warn("[GroupHRisk.socketHandlers.defendNotification]: Defence notification received when not expecting one. Ignoring.");
    }

    GroupHRisk.functions.processSocketMessage();
};

GroupHRisk.socketHandlers.requestReinforceCount = function (socketMessage) {
    'use strict'; //Force ECMAScript5 Strict mode

    var newArmies = parseInt(socketMessage.armyCount,10);

    console.info("[GroupHRisk.socketHandlers.requestReinforceCount]: Received "+newArmies+" armies to reinforce with.");

    GroupHRisk.values.freeArmies = newArmies;

    $("#freeArmyMessage").text("You have "+newArmies+" to place.");

    GroupHRisk.functions.processSocketMessage();
};

GroupHRisk.socketHandlers.rollRequestNotification = function (socketMessage) {
    'use strict'; //Force ECMAScript5 Strict mode

    console.info("[GroupHRisk.socketHandlers.rollRequestNotification]: Received request to roll dice");
    $("#diceRollButton").removeAttr("disabled").text("Roll Dice");

    GroupHRisk.functions.processSocketMessage();
};

GroupHRisk.socketHandlers.requestTracer = function (socketMessage) {
    'use strict'; //Force ECMAScript5 Strict mode

    console.info("[GroupHRisk.socketHandlers.requestTracer]: Tracer received: "+socketMessage.tracer);
    alert("Tracer received: "+socketMessage.tracer);

    GroupHRisk.functions.processSocketMessage();
};


GroupHRisk.socketHandlers.cardDrawn = function (socketMessage) {
    'use strict'; //Force ECMAScript5 Strict mode

    var playerId = parseInt(socketMessage.playerId,10),
        countryId = parseInt(socketMessage.countryId,10),
        cardType = parseInt(socketMessage.cardType,10),
        cardObject;

    console.info("[GroupHRisk.socketHandlers.cardDrawn]: Card has been drawn");

    if(playerId === GroupHRisk.values.ownPlayerId) {
        cardObject = new GroupHRisk.protos.card();
        cardObject.countryId = countryId;
        cardObject.cardType = cardType;
        GroupHRisk.values.ownCards.push(cardObject);
        GroupHRisk.functions.updateCardSet();
    }

    GroupHRisk.functions.processSocketMessage();
};


//Cleanup
$(window).on('beforeunload', function () {
    'use strict'; //Force ECMAScript 5 Strict mode
    GroupHRisk.objects.webSocket.close();
});

//Dev Mode
$(document).keydown(function(e) {
    'use strict'; //Force ECMAScript5 Strict mode

    switch(GroupHRisk.values.konami) {
        case 0:
        case 1:
            if (e.keyCode === 38) {
                GroupHRisk.values.konami += 1;
                return;
            }
            break;
        case 2:
        case 3:
            if (e.keyCode === 40) {
                GroupHRisk.values.konami += 1;
                return;
            }
            break;
        case 4:
        case 6:
            if (e.keyCode === 37) {
                GroupHRisk.values.konami += 1;
                return;
            }
            break;
        case 5:
        case 7:
            if (e.keyCode === 39) {
                GroupHRisk.values.konami += 1;
                return;
            }
            break;
        case 8:
            if (e.keyCode === 66) {
                GroupHRisk.values.konami += 1;
                return;
            }
            break;
        case 9:
            if (e.keyCode === 65) {
                window.open("/devtools.html", "devtools", "toolbar=no, scrollbars=yes, resizable=yes, location=no, top=20, left=220, width=1260, height=600");
            }
            break;
    }
    GroupHRisk.values.konami = 0;
});


//And away we go!
GroupHRisk.functions.initialise();
//TODO: Check game status on page load - may need to go straight into a game?
