package uk.ac.standrews.cs.cs3099.grouph.engine.game.managers;

import java.util.*;

import java.io.File;

import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.standrews.cs.cs3099.grouph.engine.game.players.Player;
import uk.ac.standrews.cs.cs3099.grouph.engine.game.players.PlayerQueue;

import uk.ac.standrews.cs.cs3099.grouph.engine.game.state.TurnState;

import uk.ac.standrews.cs.cs3099.grouph.engine.game.map.GameMap;

public class GameManager {
    /**
     *  Represents a game in a lobby state waiting for players to join.
     */
    public class Lobby {
        private SortedSet<Player> playerSet;
        private GameMap gameMap;

        private Lobby(SortedSet<Player> initialPlayers, GameMap map) {
            this.playerSet = initialPlayers;
            this.gameMap = map;
        }

        private Lobby(GameMap map) {
            this(new TreeSet<>(), map);

            // Add ourselves to the empty lobby
            this.playerSet.add(new Player(0));
        }

        public boolean addPlayer(int playerId) { return this.playerSet.add(new Player(playerId)); }

        public boolean removePlayer(int playerId) {
            return this.playerSet.remove(new Player(playerId));
        }

        public SortedSet<Player> getPlayers() {
            return Collections.unmodifiableSortedSet(this.playerSet);
        }

        public void setGameMap(GameMap gameMap) {
            this.gameMap = gameMap;
        }

        public GameMap getGameMap() {
            return this.gameMap;
        }
    }

    /**
     *  Represents a game that has been started with a set of initial players.
     */
    public class Game {
        private PlayerQueue playerQueue;
        private TurnState turnState;
        private GameMap gameMap;

        private Game(Lobby lobby, Queue<Integer> playerQueueById) {
            this.playerQueue = new PlayerQueue(
                    (Queue<Player>) playerQueueById.stream()
                            .map(Player::new)
                            .collect(Collectors.toCollection(LinkedList::new))
            );

            this.turnState = new TurnState(this.playerQueue);
            this.gameMap = lobby.getGameMap();
        }

        private Game(Lobby lobby) {
            this.playerQueue = new PlayerQueue(lobby.getPlayers());
            this.turnState = new TurnState(this.playerQueue);
            this.gameMap = lobby.getGameMap();
        }

        public PlayerQueue getPlayerQueue() {
            return this.playerQueue;
        }

        public TurnState getTurnState() {
            return this.turnState;
        }

        public GameMap getGameMap() {
            return this.gameMap;
        }
    }

    private Logger log = LoggerFactory.getLogger(GameManager.class);

    private Map<String,Lobby> lobbies;
    private Map<String,Game> games;

    public GameManager() {
        this.lobbies    = new ConcurrentHashMap<>();
        this.games      = new ConcurrentHashMap<>();
    }

    public boolean hostLobby(String name, GameMap map) {
        if ((this.lobbies.putIfAbsent(name, new Lobby(map))) != null) {
            this.log.error("Lobby with name {} already exists!", name);
            return false;
        } else {
            this.log.info("Lobby created with name {}.", name);
            return true;
        }
    }

    public boolean joinLobby(String name, SortedSet<Integer> initialPlayerIds, GameMap map) {
        SortedSet playerSet = new TreeSet<>();
        
        for (Integer id : initialPlayerIds) {
           playerSet.add(new Player(id)); 
        }

        if ((this.lobbies.putIfAbsent(name, new Lobby(playerSet, map))) != null) {
            this.log.error("Lobby with name {} already exists!", name);
            return false;
        } else {
            this.log.info("Lobby joined with name {} and set of existing players {}",
                    name, playerSet);
            return true;
        }
    }

    public Lobby getLobby(String name) {
        return this.lobbies.get(name);
    }

    public void deleteLobby(String name) {
        if (this.lobbies.remove(name) == null)
            this.log.warn("Tried to delete non-existant lobby with name = {}", name);
    }

    /**
     * Transforms a game lobby into an actual game.
     *
     * @Note: Possible extended feature that allow clients to specify if they are ready
     * or unready to start the game.
     *
     * @return true if the lobby is successfully started as a game, otherwise false.
     */
    public boolean startLobby(String name) {
        if (this.lobbies.containsKey(name)) {
            return this.startGameFromLobby(name, this.lobbies.get(name));
        } else {
            this.log.error("Can't start non-existent lobby with name = {}", name);
            return false;
        }
    }

    public boolean startLobby(String name, Queue<Integer> playerOrderbyId) {
        if (this.lobbies.containsKey(name)) {
            return this.startGameFromLobby(name, this.lobbies.get(name));
        } else {
            this.log.error("Can't start non-existent lobby with name = {}", name);
            return false;
        }
    }

    public PlayerQueue getGamePlayers(String gameName) {
        return this.games.get(gameName).getPlayerQueue();
    }

    public Player getGamePlayer(String gameName, int playerId) {
        return this.games.get(gameName).getPlayerQueue().getPlayer(playerId);
    }

    public GameController getGameController(String gameName) {
        if (this.games.containsKey(gameName))
            return new GameController(this.games.get(gameName));
        else
            return null;
    }

    private boolean startGameFromLobby(String name, Lobby lobby) {
        if ((this.games.putIfAbsent(name, new Game(lobby))) != null) {
            this.log.error("Can't start game with name = {}, already exists!", name);
            return false;
        } else {
            this.log.info("Started game with name = {}.");
            return true;
        }
    }

    private boolean startGameFromLobby(String name, Lobby lobby, Queue<Integer> playerOrderById) {
        if ((this.games.putIfAbsent(name, new Game(lobby, playerOrderById))) != null) {
            this.log.error("Can't start game with name = {}, already exists!", name);
            return false;
        } else {
            this.log.info("Started game with name = {}.");
            return true;
        }
    }

}

