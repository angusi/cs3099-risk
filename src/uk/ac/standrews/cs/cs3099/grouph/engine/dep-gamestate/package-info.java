/**
 * Contains data structures and transactional controls for the RISK game state.
 */
package uk.ac.standrews.cs.cs3099.grouph.engine.gamestate;