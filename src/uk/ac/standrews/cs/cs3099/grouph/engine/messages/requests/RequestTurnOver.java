package uk.ac.standrews.cs.cs3099.grouph.engine.messages.requests;

import uk.ac.standrews.cs.cs3099.grouph.shared.AbstractEngineMessage;
import uk.ac.standrews.cs.cs3099.grouph.shared.struct.players.Player;

/**
 * Signifies a player ending their turn.
 *
 * Destination:
 * - UI: Signifies that the stated player has ended their turn.
 * - Net: Signifies that the current player has ended their turn
 */
public class RequestTurnOver extends AbstractEngineMessage {
    /**
     * The phase that the game is currently in.
     *
     * Possible phases are:
     * - CLAIM: The initial setup phase where each player places a single army on an unclaimed territory.
     * - DEPLOY: The deployment phase where each player places a single army on a territory that they have previously claimed.
     * - STANDARD: The standard phase where each player reinforces, trades cards, attacks, gets cards, and/or fortifies.
     */
    public enum Phase {
        CLAIM(0),
        DEPLOY(1),
        STANDARD(2);

        private final int claimPhaseNumber;

        Phase(final int claimPhaseNumber) {
            this.claimPhaseNumber = claimPhaseNumber;
        }

        public int getClaimPhaseNumber() {
            return claimPhaseNumber;
        }
    }

    private Player player;
    private Player nextPlayer;
    private Phase phase;

    public RequestTurnOver(int index, Player player, Player nextPlayer, Phase phase) {
        super(index);

        this.player = player;
        this.nextPlayer = nextPlayer;
        this.phase = phase;
    }

    /**
     *
     * @return the  of the player ending their turn.
     */
    public Player getPlayer() {
        return this.player;
    }

    /**
     *
     * @return the  of the next player whose turn it is.
     */
    public Player getNextPlayer() {
        return this.nextPlayer;
    }

    public Phase getPhase() {
        return this.phase;
    }
}
