package uk.ac.standrews.cs.cs3099.grouph.engine.messages.requests;

import uk.ac.standrews.cs.cs3099.grouph.shared.AbstractEngineMessage;

/**
 * Signifies a map transfer to be used for the current game.
 *
 * This should proceed a RequestInitGame to the network if we are the host, otherwise it should proceed a
 * RequestInitGame to the UI if we are not the host.
 *
 * Destination:
 * - UI: If we are not the host.
 * - Net: If we are the host.
 */
public class RequestMapTransfer extends AbstractEngineMessage {

    private final String jsonMapString;

    public RequestMapTransfer(int index, String jsonMapString) {
        super(index);

        this.jsonMapString = jsonMapString;
    }

    /**
     *
     * @return the map for this game as a JSON string.
     */
    public String getJsonMapString() {
        return jsonMapString;
    }

}
