package uk.ac.standrews.cs.cs3099.grouph.engine.messages.requests;

import uk.ac.standrews.cs.cs3099.grouph.shared.AbstractEngineMessage;
import uk.ac.standrews.cs.cs3099.grouph.shared.struct.players.Player;

/**
 * Signifies an attack being carried out by a player.
 *
 * Destinations:
 * - UI: Signifies that another player has requested an attack.
 * - Net: Signifies that the current player has requested an attack.
 */
public class RequestAttack extends AbstractEngineMessage {

    private final Player attackingPlayer;
    private final Player defendingPlayer;
    private final int attackingArmyCount;
    private final int attackingCountryId;
    private final int defendingCountryId;

    public RequestAttack(int index, int attackingCountryId, int defendingCountryId,
                         Player attackingPlayer, Player defendingPlayer, int attackingArmyCount) {
        super(index);

        this.attackingCountryId = attackingCountryId;
        this.defendingCountryId = defendingCountryId;
        this.attackingPlayer = attackingPlayer;
        this.defendingPlayer = defendingPlayer;
        this.attackingArmyCount = attackingArmyCount;
    }

    /**
     *
     * @return the Id of the country from which the attackers are launching from.
     */
    public int getAttackingCountryId() {
        return attackingCountryId;
    }

    /**
     *
     * @return the Id of the country from which the defenders are defending at.
     */
    public int getDefendingCountryId() {
        return defendingCountryId;
    }

    /**
     *
     * @return the Id of the player launching the attack.
     */
    public Player getAttackingPlayer() {
        return attackingPlayer;
    }

    /**
     *
     * @return the Id of the player defending from the attack.
     */
    public Player getDefendingPlayer() {
        return defendingPlayer;
    }

    /**
     *
     * @return the number of armies (1-3) that the attacker is attacking with.
     */
    public int getAttackingArmyCount() {
        return attackingArmyCount;
    }
}
