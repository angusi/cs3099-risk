package uk.ac.standrews.cs.cs3099.grouph.engine.gamestate;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class JSONReader {

    public static ArrayList<Country> countries = new ArrayList<Country>();
    public static ArrayList<Continent> allContinents = new ArrayList<Continent>();

    public static ArrayList<String> allConnections = new ArrayList<String>();
    //continent is not exported anywhere, it is just here for now
    public static ArrayList<Country> continent = new ArrayList<Country>();


    public static ArrayList<Country> getMap(String jsonString)
            throws IOException {

        if (jsonString == null) {

            FileReader fr = new FileReader("JSON.txt");

            BufferedReader br = new BufferedReader(fr);

            StringBuilder sb = new StringBuilder();

            String line = null;
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
            jsonString = sb.toString();
            br.close();
        }

        JSONObject obj = new JSONObject(jsonString);

        JSONObject continents = obj.getJSONObject("continents");

        JSONArray connections = obj.getJSONArray("connections");
        // JSONArray values
        for (int i = 0; i < 5; i++) {
            JSONArray arr = continents.getJSONArray(Integer.toString(i));
            System.out.println(arr.length());

            for (int j = 0; j < arr.length(); j++) {

                // System.out.println(connections.length());
                for (int k = 0; k < connections.length(); k++) {
                    if (connections.get(k).toString()
                            .contains(arr.get(j).toString())) {
                        String wtf = connections.get(k).toString();

                        wtf = wtf.replace("[", "").replace("]", "");
                        // System.out.println(wtf);
                        wtf = wtf.replace(",", "");

                        wtf = wtf.replace(arr.get(j).toString(), "");

                        allConnections.add(wtf);
                    }

                }
                Country temp = new Country(arr.get(j).toString(), 0,
                        allConnections, Integer.toString(i));
                //

                // temp.printDetails();
                countries.add(temp);
                continent.add(temp);
                allConnections.clear();
                ;
                // System.out.println(connections.get(0));
                // Country temp = new Country(arr.getString(j), 0,
                // Integer.toString(i))
            }

            Continent tempContinent = new Continent(continent);
            allContinents.add(tempContinent);
            continent.clear();

        }
        return countries;
    }

    public static Country findCountryById(String name, ArrayList<Country> countries) {
        for (Country a : countries) {
            if (a.getName().equals(name)) {
                return a;
            }
        }
        return null;
    }

    public static ArrayList<Card> getCards(String jsonMap) throws IOException {
        if (jsonMap == null) {

            FileReader fr = new FileReader("JSON.txt");

            BufferedReader br = new BufferedReader(fr);

            StringBuilder sb = new StringBuilder();

            String line = null;

            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
            jsonMap = sb.toString();
            br.close();
        }
        JSONObject obj = new JSONObject(jsonMap);

        JSONObject cards = obj.getJSONObject("country_card");

        Integer wildcards = obj.getInt("wildcards");

        ArrayList<Country> countries = JSONReader.getMap(jsonMap);
        ArrayList<Card> deck = new ArrayList<Card>();

        for (Integer i = 0; i < wildcards; i++) {
            deck.add(new Card(Card.Type.WILD, null));
        }

        // System.out.println(cards.length() + "        " + cards.toString());
        Country tempCountry;

        Card tempCard;

        Integer tempValue = -1;

        JSONArray countryNames = cards.names();

        // since the cards are written in the same order as their values, no
        // need to iterate twice

        for (Integer i = 0; i < cards.length(); i++) {

            tempValue = cards.getInt(i.toString());

            tempCountry = findCountryById(countryNames.get(i).toString(),
                    countries);

            if (tempValue == 0) {
                tempCard = new Card(Card.Type.INFANTRY, tempCountry);
                deck.add(tempCard);
            }
            if (tempValue == 1) {
                tempCard = new Card(Card.Type.CALVARY, tempCountry);
                deck.add(tempCard);
            }
            if (tempValue == 2) {
                tempCard = new Card(Card.Type.ARTILLERY, tempCountry);
                deck.add(tempCard);
            }
            if (tempValue == -1) {
                System.out.println("ERROR IN READING CARDS");
            }
            tempValue = -1;

        }

        return deck;
    }

}
