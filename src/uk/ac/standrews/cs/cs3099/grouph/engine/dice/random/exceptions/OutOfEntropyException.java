package uk.ac.standrews.cs.cs3099.grouph.engine.dice.random.exceptions;

public class OutOfEntropyException extends Exception {

    private static final long serialVersionUID = 1L;

    public OutOfEntropyException(String message)
    {
        super(message);
    }

}