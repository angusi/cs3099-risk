package uk.ac.standrews.cs.cs3099.grouph.engine.game.map;

/**
 * Represents a Country containing a specified number of armies.
 */
public class Country {

    private final int id;
    private final String name;

    private long numberOfArmies;

    public Country(int id, String name, long numberOfArmies) {
        this.id = id;
        this.name = name;
        this.numberOfArmies = numberOfArmies;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public long getNumberOfArmies() {
        return numberOfArmies;
    }

    public void setNumberOfArmies(long numberOfArmies) {
        this.numberOfArmies = numberOfArmies;
    }

    @Override
    public boolean equals(Object obj) {
        return (obj instanceof Country && this.id == ((Country) obj).getId());
    }

}

