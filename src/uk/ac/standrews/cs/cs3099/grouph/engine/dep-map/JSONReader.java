package uk.ac.standrews.cs.cs3099.grouph.engine.map;

import org.json.simple.*;	
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class JSONReader {
	//for now it is a string
	public final static String jsonString = "\"data\":\"map\",\"continents\":{\"1\":[1,2,3],\"2\":[4]},\"connections\":[[\"1\", \"2\"],[\"2\",\"3\"]],\"continent_values\":{\"1\":5,\"2\" : 2}";
	//it is not valid JSON, so i will work on writing correct JSON and working from there
	public static void main(String[] args) {
		JSONParser parser = new JSONParser();
		KeyMatcher finder = new KeyMatcher();

		finder.setMatchKey("continents");
		//it is not working yet
		try {
			while (!finder.isEnd()) {
				parser.parse(jsonString, finder, true);
				if (finder.isFound()) {
					finder.setFound(false);
					System.out.println("found id:");
					System.out.println(finder.getValue());
				}
			}
		} catch (ParseException pe) {
			pe.printStackTrace();
		}
	}
}
