package uk.ac.standrews.cs.cs3099.grouph.engine.map;

import java.io.IOException;

import org.json.simple.parser.ContentHandler;
import org.json.simple.parser.ParseException;

//idk much about this library, except that is how it was in the example. looks relatively simple
public class KeyMatcher implements ContentHandler{
	  private Object value;
	  private boolean found = false;
	  private boolean end = false;
	  private String key;
	  private String matchKey;
	        
	  public void setMatchKey(String matchKey){
	    this.matchKey = matchKey;
	  }
	        
	  public Object getValue(){
	    return value;
	  }
	        
	  public boolean isEnd(){
	    return end;
	  }
	        
	  public void setFound(boolean found){
	    this.found = found;
	  }
	        
	  public boolean isFound(){
	    return found;
	  }
	        

	  
	  public boolean primitive(Object value) throws ParseException, IOException {
	    if(key != null){
	      if(key.equals(matchKey)){
	        found = true;
	        this.value = value;
	        key = null;
	        return false;
	      }
	    }
	    return true;
	  }
	
	@Override
	public boolean endArray() throws ParseException, IOException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void endJSON() throws ParseException, IOException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean endObject() throws ParseException, IOException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean endObjectEntry() throws ParseException, IOException {
		// TODO Auto-generated method stub
		return false;
	}




	@Override
	public boolean startArray() throws ParseException, IOException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void startJSON() throws ParseException, IOException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean startObject() throws ParseException, IOException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean startObjectEntry(String arg0) throws ParseException,
			IOException {
		// TODO Auto-generated method stub
		return false;
	}

}
