package uk.ac.standrews.cs.cs3099.grouph.engine.events.ui;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import uk.ac.standrews.cs.cs3099.grouph.engine.dice.DiceRoller;
import uk.ac.standrews.cs.cs3099.grouph.engine.dice.random.exceptions.HashMismatchException;
import uk.ac.standrews.cs.cs3099.grouph.engine.events.EngineEventManager;
import uk.ac.standrews.cs.cs3099.grouph.engine.events.Event;
import uk.ac.standrews.cs.cs3099.grouph.engine.game.managers.GameController;
import uk.ac.standrews.cs.cs3099.grouph.engine.game.managers.exceptions.GameRuleException;
import uk.ac.standrews.cs.cs3099.grouph.engine.messages.requests.*;
import uk.ac.standrews.cs.cs3099.grouph.shared.struct.players.InvalidPlayerException;
import uk.ac.standrews.cs.cs3099.grouph.shared.struct.players.Player;
import uk.ac.standrews.cs.cs3099.grouph.shared.struct.players.Players;
import uk.ac.standrews.cs.cs3099.grouph.ui.messages.DefendDiceMessage;

import java.util.*;
import java.util.stream.IntStream;

/**
 * Created by gs63 on 21/04/15.
 */
public class EventUIDefend extends Event {

    public EventUIDefend() {
        super(Priority.MEDIUM);
    }

    @Override
    public void run() {
        boolean result = false;

        DefendDiceMessage diceMessage = (DefendDiceMessage) this.trigger;
        GameController gc;

        if ((gc = this.gameManager.getGameController("BASIC")) != null) {
            try {
                gc.getModifier().defend(Players.me().getId(), diceMessage.getNumDice());

                result = true;
            } catch (InvalidPlayerException e) {
                this.log.error("Invalid player exception: {}", e.getMessage());
            } catch (GameRuleException e) {
                this.log.error("Game rule exception: {}", e.getRule());
            }
        }

        this.respond(result, EngineEventManager.ComponentReference.UI);

        if (result) {
            gc = this.gameManager.getGameController("BASIC");

            try {
                this.message(new RequestDefend(this.messageInterface.getMessageCounter(), Players.me(),
                        diceMessage.getNumDice()), EngineEventManager.ComponentReference.NET);

                List<Integer> rolls;
                if ((rolls = this.rollDice(gc)) != null) {
                    this.log.info("Rolled: {}", StringUtils.join(rolls, ","));

                    int numberOfAttackers = gc.getModifier().getNumberOfAttackers();
                    int numberOfDefenders = gc.getModifier().getNumberOfDefenders();
                    List<Integer> attackingDiceList = new ArrayList<>(rolls.subList(0, numberOfAttackers));
                    List<Integer> defendingDiceList = new ArrayList<>(rolls.subList(numberOfAttackers, rolls.size()));

                    this.log.info("{} attackers rolled the following values: {}", gc.getModifier().getNumberOfAttackers(),
                            StringUtils.join(attackingDiceList, ","));
                    this.log.info("{} defenders rolled the following values: {}", gc.getModifier().getNumberOfDefenders(),
                            StringUtils.join(defendingDiceList, ","));

                    this.message(new RequestRollResult(this.messageInterface.getMessageCounter(), Players.me(),
                            defendingDiceList.size(), new ArrayList<>(defendingDiceList)), EngineEventManager.ComponentReference.UI);

                    int attackingLosses = 0;
                    int defendingLosses = 0;

                    for (int id = 0; id < Math.min(numberOfAttackers, numberOfDefenders); id++) {
                        int highestAttackingDice = Collections.max(attackingDiceList);
                        int highestDefendingDice = Collections.max(defendingDiceList);

                        if (highestAttackingDice > highestDefendingDice) {
                            defendingLosses++;
                        } else {
                            attackingLosses++;
                        }

                        attackingDiceList.remove(new Integer(highestAttackingDice));
                        defendingDiceList.remove(new Integer(highestDefendingDice));
                    }

                    try {
                        gc.getModifier().result(gc.getModifier().getAttackingPlayerId(), attackingLosses, defendingLosses);

                        this.message(new RequestBattleResult(this.messageInterface.getMessageCounter(), null,
                                Players.getPlayerById(gc.getModifier().getAttackingPlayerId()),
                                Players.getPlayerById(gc.getModifier().getDefendingPlayerId()),
                                gc.getModifier().getAttackingCountryId(),
                                gc.getModifier().getDefendingCountryId(),
                                attackingLosses, defendingLosses), EngineEventManager.ComponentReference.UI);

                        this.message(new RequestTerritoryChange(this.messageInterface.getMessageCounter(),
                                        gc.getModifier().getAttackingCountryId(),
                                        Players.getPlayerById(gc.getModifier().getGameMap().getCountryOwner(gc.getModifier().getAttackingCountryId()).getId()),
                                        (int) gc.getModifier().getGameMap().getCountryArmies(gc.getModifier().getAttackingCountryId())),
                                EngineEventManager.ComponentReference.UI);

                        this.message(new RequestTerritoryChange(this.messageInterface.getMessageCounter(),
                                        gc.getModifier().getDefendingCountryId(),
                                        Players.getPlayerById(gc.getModifier().getGameMap().getCountryOwner(gc.getModifier().getDefendingCountryId()).getId()),
                                        (int) gc.getModifier().getGameMap().getCountryArmies(gc.getModifier().getDefendingCountryId())),
                                EngineEventManager.ComponentReference.UI);
                    } catch (GameRuleException e) {
                        this.log.error("Game Rule Exception: {}", e.getMessage());
                    } catch (InvalidPlayerException e) {
                        this.log.error("Invalid Player Exception: {}", e.getMessage());
                    }
                }
            } catch (InvalidPlayerException e) {
                this.log.error("Invalid player exception: {}", e.getMessage());
            }

            DiceRoller.getInstance().reset();
            DiceRoller.getInstance().clear();
        }
    }

    private List<Integer> rollDice(GameController gc) {
        // Send our hash
        try {
            int numberOfAttackingArmies = gc.getModifier().getNumberOfAttackers();
            int numberOfDefendingArmies = gc.getModifier().getNumberOfDefenders();

            this.log.info("There are {} players according to Players.getPlayerCount()", gc.getModifier().getNumberOfPlayers());
            DiceRoller.loadInstance(numberOfAttackingArmies + numberOfDefendingArmies, 6, gc.getModifier().getNumberOfPlayers());
            DiceRoller.getInstance().generateMe();
            this.message(new RequestDefend(this.messageInterface.getMessageCounter(), Players.me(),
                    numberOfDefendingArmies), EngineEventManager.ComponentReference.NET);
            this.message(new RequestRollHash(this.messageInterface.getMessageCounter(),
                    DiceRoller.getInstance().getMyHashAsHexString()), EngineEventManager.ComponentReference.NET);

            this.log.info("Waiting for hashes...");
            while (!DiceRoller.getInstance().hasAllHashes()) {
                this.log.info("Waiting for hashes - Sleeping for 100ms");
                Thread.sleep(100);
            }

            this.log.info("Received hashes!");
            this.log.info("Waiting for numbers...");

            this.message(new RequestRollNumber(this.messageInterface.getMessageCounter(),
                    DiceRoller.getInstance().getMyNumberAsHexString()), EngineEventManager.ComponentReference.NET);

            while (!DiceRoller.getInstance().isReady()) {
                this.log.info("Waiting for numbers - Sleeping for 100ms");
                Thread.sleep(100);
            }

            this.log.info("Received numbers!");
            this.log.info("Rolling {} dice with {} faces...",
                    DiceRoller.getInstance().getNumberOfDice(), DiceRoller.getInstance().getNumberOfFaces());

            this.log.info("We have {} hashes and {} numbers.", DiceRoller.getInstance().getHashCount(),
                    DiceRoller.getInstance().getNumberCount());

            // Generate dice rolls
            List<Integer> errorList;
            if ((errorList = DiceRoller.getInstance().verify()).size() == 0) {
                DiceRoller.getInstance().roll();
                return DiceRoller.getInstance().getResultList();
            } else {
                this.log.error("Error verifying the following players: {}", ArrayUtils.toString(errorList));
                return null;
            }
        } catch (InvalidPlayerException | InterruptedException | HashMismatchException e) {
            e.printStackTrace();
            return null;
        }
    }
}
