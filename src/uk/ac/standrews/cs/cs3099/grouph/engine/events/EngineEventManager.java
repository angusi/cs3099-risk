package uk.ac.standrews.cs.cs3099.grouph.engine.events;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang.SerializationUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.standrews.cs.cs3099.grouph.engine.EngineComponentConnector;
import uk.ac.standrews.cs.cs3099.grouph.engine.events.internal.EventInternalUnrecognisedMessage;
import uk.ac.standrews.cs.cs3099.grouph.shared.AbstractMessage;

import uk.ac.standrews.cs.cs3099.grouph.engine.game.managers.GameManager;

public class EngineEventManager {
	public static enum ComponentReference {
		UI,
		NET
	}
	
	private Logger log;
	
	private boolean running;
	
	private Map<String, Event> eventMaps;
	
	private BlockingQueue<Runnable> execQueue;
	
	private MessageInterface messageInterface;
	private EventInterface eventInterface;
	
	private EventExecutor eventExecutor;

    private GameManager gameManager;
	
	public EngineEventManager(int msgCapacity, boolean msgFair, 
			int coreThreads, int maxThreads, long keepAlive, EngineComponentConnector ui, EngineComponentConnector net) {
		this.log = LoggerFactory.getLogger(EngineEventManager.class);
		
		// Create mapping between messages and events
		this.eventMaps = new HashMap<>();
		
		// Create PriorityBlockingQueue for event execution queue
		this.execQueue = new PriorityBlockingQueue<>();
		
		// Create and configure message interface
		this.messageInterface = new MessageInterface(msgCapacity, msgFair);
		this.messageInterface.attachConnector(ui, ComponentReference.UI);
		this.messageInterface.attachConnector(net, ComponentReference.NET);
		
		// Create and configure event interface
		this.eventInterface = new EventInterface(this.execQueue);
		
		// Create and configure event executor
		this.eventExecutor = new EventExecutor(this.execQueue, coreThreads, maxThreads, keepAlive);

        // Create and configure game manager 
        this.gameManager = new GameManager();
	}
	
	/**
	 * Register an event that is triggered by the specified message.
	 * @param triggerClassName the message that triggers the event
	 * @param bullet the event to execute
	 */
	public void registerEvent(String triggerClassName, Event bullet) {
		this.eventMaps.put(triggerClassName, bullet);
	}

	public void start() {
		this.running = true;
		
		this.eventExecutor.prestartAllCoreThreads();

		while (this.running) {
			AbstractMessage msg;
			while ((msg = this.messageInterface.internalInbound.poll()) != null) {
				this.log.info("{} received from {}",
						msg.name, msg.originator);
				Event bullet = this.eventMaps.get(msg.name);
				
				if (bullet == null)
					bullet = new EventInternalUnrecognisedMessage();
				else
					bullet = (Event) SerializationUtils.clone(bullet);

                bullet.setMessageInterface(this.getMessageInterface());
                bullet.setEventInterface(this.getEventInterface());

                bullet.setGameManager(this.getGameManager());
                
                bullet.setTrigger(msg);
				
				this.execQueue.add(bullet);
				this.log.info("Queued {} for execution triggered by {}",
						bullet.getClass().getSuperclass().getSimpleName(), msg);
			}
		}
		
		// Prevent corrupted states; allow all threads in thread pool to execute before shutting down
		this.eventExecutor.shutdown();
	}
	
	// Getters and Setters
	
	protected void setMessageInterface(MessageInterface messageInterface) {
		this.messageInterface = messageInterface;
	}
	
	public MessageInterface getMessageInterface() {
		return this.messageInterface;
	}
	
	protected void setEventInterface(EventInterface eventInterface) {
		this.eventInterface = eventInterface;
	}
	
	public EventInterface getEventInterface() {
		return this.eventInterface;
	}
	
	protected void setEventExecutor(EventExecutor eventInterface) {
		this.eventExecutor = eventInterface;
	}
	
	protected EventExecutor getEventExecutor() {
		return this.eventExecutor;
	}

    public GameManager getGameManager() {
        return this.gameManager;
    }
	
	/**
	 * Internal EngineEventManager class that handles executing events acting as a ThreadPoolExecutor.
	 * 
	 * @see EngineEventManager
	 * @see ThreadPoolExecutor
	 */
	protected class EventExecutor extends ThreadPoolExecutor {
		private Logger log;
		
		private EventExecutor(BlockingQueue<Runnable> execQueue, int coreThreads, int maxThreads, long keepAlive) {		
			super(coreThreads, maxThreads, keepAlive, TimeUnit.MILLISECONDS, execQueue);
			
			this.log = LoggerFactory.getLogger(EventExecutor.class);
			this.log.info("Initialised with {} core threads, {} max threads.",
					coreThreads, maxThreads, keepAlive, "(ms)");
		}

		@Override
		protected void beforeExecute(Thread t, Runnable r) {
			super.beforeExecute(t, r);
		}

		@Override
		protected void afterExecute(Runnable t, Throwable r) {
			super.afterExecute(t, r);
		}
	}
	
	public class EventInterface {
		private BlockingQueue<Runnable> execQueue;
		
		private EventInterface(BlockingQueue<Runnable> execQueue) {
			this.execQueue = execQueue;
		}
		
		/**
		 * Add an event for execution.
		 * @param event the event to add
		 */
		public void queue(Event event) {
			this.execQueue.add(event);
		}
	}

	public class MessageInterface {
		private Logger log;

        private volatile int messageCounter = 0;
		
		private BlockingQueue<AbstractMessage> internalInbound;
		private BlockingQueue<AbstractMessage> internalUIOutbound;
		private BlockingQueue<AbstractMessage> internalNetOutbound;
		
		private MessageInterface(int capacity, boolean fair) {
			this.log = LoggerFactory.getLogger(MessageInterface.class);
			
			this.internalInbound = new ArrayBlockingQueue<AbstractMessage>(capacity, fair);

			this.internalUIOutbound = new ArrayBlockingQueue<AbstractMessage>(capacity, fair);
			this.internalNetOutbound = new ArrayBlockingQueue<AbstractMessage>(capacity, fair);
		}
		
		public void attachConnector(EngineComponentConnector con,
				EngineEventManager.ComponentReference ref) {
			con.setInternalInboundQueue(this.internalInbound);
			
			switch (ref) {
			case UI:
				con.setInternalOutboundQueue(this.internalUIOutbound);
				break;
			case NET:
				con.setInternalOutboundQueue(this.internalNetOutbound);
				break;
			default:
				con.setInternalOutboundQueue(null);
			}
		}
		
		public void push(AbstractMessage msg, EngineEventManager.ComponentReference destRef) throws InterruptedException {
			switch (destRef) {
			case UI:
				this.internalUIOutbound.put(msg);
				break;
			case NET:
				this.internalNetOutbound.put(msg);
				break;
			default:
				// TODO: Should throw exception
				break;
			}
			this.messageCounter++;

			this.log.info("Queued outbound message {} to component {}", msg, destRef);
		}

        public int getMessageCounter() {
            return this.messageCounter;
        }
	}
	
}
