package uk.ac.standrews.cs.cs3099.grouph.engine.events.net;

import uk.ac.standrews.cs.cs3099.grouph.engine.events.EngineEventManager;
import uk.ac.standrews.cs.cs3099.grouph.engine.events.Event;

import uk.ac.standrews.cs.cs3099.grouph.engine.messages.requests.RequestAttack;

import uk.ac.standrews.cs.cs3099.grouph.engine.messages.requests.RequestRoll;
import uk.ac.standrews.cs.cs3099.grouph.shared.struct.players.InvalidPlayerException;
import uk.ac.standrews.cs.cs3099.grouph.shared.struct.players.Player;
import uk.ac.standrews.cs.cs3099.grouph.shared.struct.players.Players;

import uk.ac.standrews.cs.cs3099.grouph.engine.game.managers.exceptions.GameRuleException;
import uk.ac.standrews.cs.cs3099.grouph.engine.game.managers.GameManager;
import uk.ac.standrews.cs.cs3099.grouph.engine.game.managers.GameController;

import uk.ac.standrews.cs.cs3099.grouph.net.messages.AttackOccurM;

public class EventNetAttack extends Event {
    
    public EventNetAttack() {
        super(Event.Priority.MEDIUM);
    }
    
    @Override
    public void run() {
        boolean result = false;

        Player player = ((AttackOccurM) this.trigger).getPlayer();
        int attackingCountryId = ((AttackOccurM) this.trigger).getFrom();
        int defendingCountryId = ((AttackOccurM) this.trigger).getTo();
        int numberOfArmies = ((AttackOccurM) this.trigger).getNumberArmies();

        GameController gc;

        try {
            if ((gc = this.gameManager.getGameController("BASIC")) != null) {
                gc.getModifier().attack(player.getId(), attackingCountryId, defendingCountryId, numberOfArmies);

                Player defendingPlayer = Players.getPlayerById(gc.getModifier().getGameMap().getCountryOwner(defendingCountryId).getId());

                this.message(new RequestAttack(this.messageInterface.getMessageCounter(),
                            attackingCountryId, defendingCountryId, player, defendingPlayer,
                                numberOfArmies),
                        EngineEventManager.ComponentReference.UI);

                if (defendingPlayer.getId() == Players.me().getId()) {
                    this.message(new RequestRoll(this.messageInterface.getMessageCounter(), null, 0, 0),
                            EngineEventManager.ComponentReference.UI);
                }

                result = true;            
            }
        } catch (GameRuleException g) {
            this.log.error("Game rule violation = {}", g.getRule());
        } catch (InvalidPlayerException e) {
            this.log.error("Invalid player exception = {}", e.getMessage());
        }

        this.respond(result, EngineEventManager.ComponentReference.NET);
    }

}

