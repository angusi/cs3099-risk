package uk.ac.standrews.cs.cs3099.grouph.engine.game.state.exceptions;

/**
 * Exception thrown when an operation cannot be completed due to an invalid turn state.
 */
public class InvalidState extends Exception {

    public InvalidState(String msg) {
        super(msg);
    }
}
