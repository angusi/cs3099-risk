package uk.ac.standrews.cs.cs3099.grouph.engine.gamestate;


import org.jgrapht.graph.DefaultWeightedEdge;
import org.jgrapht.graph.SimpleWeightedGraph;

import java.util.List;

public class Map {

    private SimpleWeightedGraph<Country, DefaultWeightedEdge> mapGraph;

    private Map(List<Country> countryList) {
        this.mapGraph = new SimpleWeightedGraph<>(DefaultWeightedEdge.class);

        // creates vertices of graph based on arraylist of
        // connects this together, because well, graph needs connections
        countryList.forEach(mapGraph::addVertex);

        for (Country c : countryList) {
            for (Country d : countryList) {
                // cant connect to itself
                if (c.getConnections().contains(d.getName())) {
                    mapGraph.addEdge(c, d);
                }
            }

        }
    }
    public static Map createMap(List<Country> countryList) {
        return new Map(countryList);
    }

}
