package uk.ac.standrews.cs.cs3099.grouph.engine.messages.requests;

import uk.ac.standrews.cs.cs3099.grouph.shared.AbstractEngineMessage;
import uk.ac.standrews.cs.cs3099.grouph.shared.struct.players.Player;

/**
 * Signifies a territory claim at the start of the game.
 *
 * Destinations:
 * - UI: Signifies that the stated player has claimed a territory.
 * - Net: Signifies that the current player is claiming a territory.
 */
public class RequestSetupClaim extends AbstractEngineMessage {

    private final Player player;
    private int countryId;

    public RequestSetupClaim(int index, Player player, int countryId) {
        super(index);

        this.player = player;
        this.countryId = countryId;
    }

    /**
     *
     * @return the Id of the country that the player is claiming.
     */
    public int getCountryId() {
        return this.countryId;
    }

    /**
     *
     * @return the Id of the player that is claiming the territory.
     */
    public Player getPlayer() {
        return player;
    }
}
