package uk.ac.standrews.cs.cs3099.grouph.engine.events.net;

import uk.ac.standrews.cs.cs3099.grouph.engine.events.Event;
import uk.ac.standrews.cs.cs3099.grouph.engine.events.EngineEventManager;

import uk.ac.standrews.cs.cs3099.grouph.engine.game.managers.GameManager;
import uk.ac.standrews.cs.cs3099.grouph.engine.game.managers.exceptions.PlayerLobbyException;
import uk.ac.standrews.cs.cs3099.grouph.engine.messages.requests.RequestLobbyUpdate;
import uk.ac.standrews.cs.cs3099.grouph.net.messages.PlayerCountM;
import uk.ac.standrews.cs.cs3099.grouph.shared.struct.players.InvalidPlayerException;
import uk.ac.standrews.cs.cs3099.grouph.shared.struct.players.Player;
import uk.ac.standrews.cs.cs3099.grouph.shared.struct.players.Players;

import java.util.Set;
import java.util.HashSet;

public class EventNetPlayerCount extends Event {

    public EventNetPlayerCount() {
        super(Event.Priority.HIGH);
    }

    @Override
    public void run() {
        boolean result = false;

        try {
            int myPlayerId = Players.me().getId();
            int numberOfPlayers = ((PlayerCountM) this.trigger).getPlayerCount();

            // Update lobby
            GameManager.Lobby lobby;
            if ((lobby = this.gameManager.getLobby("BASIC")) != null) {
                for (int i = 0; i < numberOfPlayers; i++) {
                    if (i != myPlayerId) {
                        if (!lobby.addPlayer(myPlayerId)) {
                            throw new PlayerLobbyException("Lobby Error", i);
                        }
                    }
                }
            }

            // Create Player Struct for UI and Send
            Set<Player> playerSet = new HashSet<>();

            for (int i = 0; i < numberOfPlayers; i++) {
                playerSet.add(Players.getPlayerById(i));
            }

            this.message(new RequestLobbyUpdate(this.messageInterface.getMessageCounter(),
                    playerSet, null, null, Players.me()), EngineEventManager.ComponentReference.UI);

            result = true;
        } catch (InvalidPlayerException e) {
            this.log.error("Could not find current player id! (Are we a playing host?)");
        } catch (PlayerLobbyException s) {
            this.log.warn("Player with id = {} already exists in lobby, omitting!", s.getId());
        }

        this.respond(result, EngineEventManager.ComponentReference.NET);
    }
}
