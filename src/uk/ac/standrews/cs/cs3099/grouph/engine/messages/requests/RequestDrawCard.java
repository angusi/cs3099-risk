package uk.ac.standrews.cs.cs3099.grouph.engine.messages.requests;

import uk.ac.standrews.cs.cs3099.grouph.engine.interfaces.game.cards.ICard;
import uk.ac.standrews.cs.cs3099.grouph.shared.AbstractEngineMessage;
import uk.ac.standrews.cs.cs3099.grouph.shared.struct.players.Player;

/**
 * Signifies a request to draw a card. As per rules this request will only be accepted if the corresponding player
 * has had a RequestTerritoryChange where they gained a territory.
 *
 * @implNote Since it is undecided how card drawing will be handled (face-up or face-down?) cardCountryId may be
 * set to null to signify an unknown card has been drawn for other players.
 *
 * Destinations:
 * - UI: Signifies that another player has drawn a card.
 * - Net: Signifies that the current player has drawn a card.
 *
 * @see uk.ac.standrews.cs.cs3099.grouph.engine.messages.requests.RequestTerritoryChange
 */
public class RequestDrawCard extends AbstractEngineMessage {

    private final Player player;
    private final ICard card;

    public RequestDrawCard(int index, Player player, ICard card) {
        super(index);

        this.player = player;
        this.card = card;
    }

    /**
     *
     * @return the country Id containing the card.
     */
    public ICard getCard() {
        return card;
    }

    /**
     *
     * @return the player Id who is drawing a card.
     */
    public Player getPlayer() {
        return player;
    }
}
