package uk.ac.standrews.cs.cs3099.grouph.engine.events.ui;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.standrews.cs.cs3099.grouph.engine.events.Event;
import uk.ac.standrews.cs.cs3099.grouph.engine.events.EngineEventManager;

import uk.ac.standrews.cs.cs3099.grouph.shared.struct.players.InvalidPlayerException; 
import uk.ac.standrews.cs.cs3099.grouph.engine.messages.responses.ResponseMessage;
import uk.ac.standrews.cs.cs3099.grouph.engine.messages.requests.RequestAttack;

import uk.ac.standrews.cs.cs3099.grouph.shared.struct.players.Player;
import uk.ac.standrews.cs.cs3099.grouph.shared.struct.players.Players;

import uk.ac.standrews.cs.cs3099.grouph.engine.game.managers.exceptions.GameRuleException;
import uk.ac.standrews.cs.cs3099.grouph.engine.game.managers.GameManager;
import uk.ac.standrews.cs.cs3099.grouph.engine.game.managers.GameController;

import uk.ac.standrews.cs.cs3099.grouph.ui.messages.AttackMessage;

public class EventUIAttack extends Event {
    
    public EventUIAttack() {
        super(Event.Priority.MEDIUM);
    }

    @Override
    public void run() {
        boolean result = false;

        try {
            GameController gc;
            if ((gc = this.gameManager.getGameController("BASIC")) != null) {
                Player player = Players.me();

                int attackingCountryId = ((AttackMessage) this.trigger).getSourceCountry();
                int defendingCountryId = ((AttackMessage) this.trigger).getDestinationCountry();
                int numberOfArmies     = ((AttackMessage) this.trigger).getNumberOfArmies();

                gc.getModifier().attack(player.getId(), attackingCountryId, defendingCountryId,
                            numberOfArmies);
                
                this.message(new RequestAttack(this.messageInterface.getMessageCounter(),
                            attackingCountryId, defendingCountryId, player, null, numberOfArmies),
                        EngineEventManager.ComponentReference.NET);

                result = true;
            }

            this.respond(result, EngineEventManager.ComponentReference.UI);
        } catch (GameRuleException g) {
            this.log.error("Game rule violation = {}", g.getRule());
            this.respond(false, EngineEventManager.ComponentReference.UI);
        } catch (InvalidPlayerException p) {
            this.log.error("Player not found!)");
            this.respond(false, EngineEventManager.ComponentReference.UI);
        }
    }
}

