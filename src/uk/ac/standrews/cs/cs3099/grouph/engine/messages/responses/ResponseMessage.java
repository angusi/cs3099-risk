package uk.ac.standrews.cs.cs3099.grouph.engine.messages.responses;

import uk.ac.standrews.cs.cs3099.grouph.shared.AbstractEngineMessage;
import uk.ac.standrews.cs.cs3099.grouph.shared.AbstractMessage;

/**
 * Standard response sent to originator of incoming message.
 */
public class ResponseMessage extends AbstractEngineMessage {
    public enum Code {
        ACCEPT(1),
        REJECT(0),
        UNKNOWN(-1);

        private final int value;

        private Code(int value) {
            this.value = value;
        }

        public int asInt() {
            return this.value;
        }
    }

    public final Code code;
    public final AbstractMessage trigger;

    /**
     * Default constructor.
     * @param index The index of the response message.
     * @param trigger The received message that this response is responding to.
     * @param code The response code of this response.
     */
    public ResponseMessage(int index, AbstractMessage trigger, Code code) {
        super(index);

        this.trigger = trigger;
        this.code = code;
    }
}
