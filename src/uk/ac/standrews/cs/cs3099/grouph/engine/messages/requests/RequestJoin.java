package uk.ac.standrews.cs.cs3099.grouph.engine.messages.requests;


import uk.ac.standrews.cs.cs3099.grouph.shared.AbstractEngineMessage;

import java.net.InetAddress;
import java.util.Set;

/**
 * Signifies a player joining a game.
 *
 * Destinations:
 * - UI: Signifies another player joining our game when we are hosting.
 * - Net: Signifies the current player joining an externally hosted game.
 */
public class RequestJoin extends AbstractEngineMessage {

    private final Set<String> supportedFeatures;
    //private final int id;
    //private final String name;
    private final InetAddress hostAddress;

    public RequestJoin(int index, Set<String> supportedFeatures,
            /*int id, String name,*/ InetAddress hostAddress) {
        super(index);

        this.supportedFeatures = supportedFeatures;
        this.hostAddress = hostAddress;
       // this.name = name;
       // this.id = id;
    }

    /**
     *
     * @return Address of the host or the player joining us.
     */
    public InetAddress getHostAddress() {
        return this.hostAddress;
    }

    /**
     *
     * @return set of features we support.
     */
    public Set<String> getSupportedFeatures() {
        return supportedFeatures;
    }

    // TODO: Ommited for testing
    // public int getId() {
    //    return this.id;
    // }

    // public String getName() {
    //    return this.name;
    // }

}
