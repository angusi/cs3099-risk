package uk.ac.standrews.cs.cs3099.grouph.engine.gamestate;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Contains data pertaining to a single player in the game.
 */
public class Player implements Serializable {

    public final int id;

    private List <Card> cards;

    public Player(int id) {
        this.id = id;

        this.cards = new ArrayList<>(6);
    }

    public void addCard(Card card) throws ImpossibleException {
        if (this.cards.size() < 6)
            this.cards.add(card);
        else
            throw new ImpossibleException();
    }

    public boolean tradeCardSet(Set<Card> cardSet) {
        return cardSet.size() == 3 && this.cards.removeAll(cardSet);
    }

    @Override
    public boolean equals(Object o) {
        return o instanceof Player && (this.id == ((Player) o).id);
    }

    private class ImpossibleException extends Exception {
        public ImpossibleException() {
            super("Player can never have more than 6 un-traded cards!");
        }

    }
}
