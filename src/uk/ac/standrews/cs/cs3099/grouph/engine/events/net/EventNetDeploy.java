package uk.ac.standrews.cs.cs3099.grouph.engine.events.net;

import uk.ac.standrews.cs.cs3099.grouph.engine.events.EngineEventManager;
import uk.ac.standrews.cs.cs3099.grouph.engine.events.Event;

import uk.ac.standrews.cs.cs3099.grouph.engine.messages.requests.RequestDeploy;

import uk.ac.standrews.cs.cs3099.grouph.engine.game.managers.exceptions.GameRuleException;
import uk.ac.standrews.cs.cs3099.grouph.engine.game.managers.GameManager;
import uk.ac.standrews.cs.cs3099.grouph.engine.game.managers.GameController;

import uk.ac.standrews.cs.cs3099.grouph.engine.messages.requests.RequestTerritoryChange;
import uk.ac.standrews.cs.cs3099.grouph.engine.messages.requests.RequestTurnOver;
import uk.ac.standrews.cs.cs3099.grouph.net.messages.DeployArmiesM;

import uk.ac.standrews.cs.cs3099.grouph.shared.struct.players.InvalidPlayerException;
import uk.ac.standrews.cs.cs3099.grouph.shared.struct.players.Player;
import uk.ac.standrews.cs.cs3099.grouph.shared.struct.players.Players;

public class EventNetDeploy extends Event {

    public EventNetDeploy() {
        super(Event.Priority.MEDIUM);
    }

    @Override
    public void run() {
        boolean result = false;

        GameController gc;

        if ((gc = this.gameManager.getGameController("BASIC")) != null) {
            Player player = ((DeployArmiesM) this.trigger).getPlayer();

            try {
                for (DeployArmiesM.Movement m : ((DeployArmiesM) this.trigger).getMoves()) {
                    this.log.info("Player {} is trying to reinforce {} armies to country id {}",
                            player.getId(), m.getnArmies(), m.getTerritoryId());

                    int countryId = m.getTerritoryId();
                    int numberOfArmies = m.getnArmies();

                    gc.getModifier().reinforce(player.getId(), countryId, numberOfArmies);

                    this.message(new RequestTerritoryChange(this.messageInterface.getMessageCounter(),
                                    countryId, player, (int) gc.getModifier().getGameMap().getCountryArmies(countryId)),
                            EngineEventManager.ComponentReference.UI);

                    if (gc.getModifier().getPlayerById(player.getId()).getReinforcements() == 0) {
                        int previousPlayerId = player.getId();
                        int currentPlayerId = gc.getModifier().getCurrentPlayer().getId();

                        this.message(new RequestTurnOver(this.messageInterface.getMessageCounter(),
                                        Players.getPlayerById(previousPlayerId), Players.getPlayerById(currentPlayerId),
                                        RequestTurnOver.Phase.STANDARD),
                                EngineEventManager.ComponentReference.UI);
                    }
                }

                result = true;
            } catch (GameRuleException g) {
                this.log.error("Game rule violation = {}", g.getRule());
            } catch (InvalidPlayerException e) {
                this.log.error("Couldn't find player id from Players!");
            }

        }

        this.respond(result, EngineEventManager.ComponentReference.NET);
    
    }

}
