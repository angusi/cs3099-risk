package uk.ac.standrews.cs.cs3099.grouph.engine.map;

import java.util.ArrayList;


public class Country {
	//country with all parameters of it
		public  String name;
		public  String continent;
		public  ArrayList<String> connections = new ArrayList<String>();
		public  int value;
		public int PlayerID=0;
		
		public Country(String name, int value, ArrayList<String> connections,String continent){
		this.name=name;
		this.continent=continent;
		this.connections=connections;
		this.value=value;
		}
		
}
