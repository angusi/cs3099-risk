package uk.ac.standrews.cs.cs3099.grouph.engine.events.net;

import java.util.List;
import java.util.Set;
import java.util.SortedSet;
import java.util.HashSet;

import java.net.InetAddress;
import java.net.UnknownHostException;

import uk.ac.standrews.cs.cs3099.grouph.engine.events.Event;
import uk.ac.standrews.cs.cs3099.grouph.engine.events.EngineEventManager;

import uk.ac.standrews.cs.cs3099.grouph.engine.game.managers.GameManager;

import uk.ac.standrews.cs.cs3099.grouph.net.messages.JoinGameRequestM;
import uk.ac.standrews.cs.cs3099.grouph.engine.messages.requests.RequestLobbyUpdate;

import uk.ac.standrews.cs.cs3099.grouph.shared.struct.players.Player;
import uk.ac.standrews.cs.cs3099.grouph.shared.struct.players.Players;
import uk.ac.standrews.cs.cs3099.grouph.shared.struct.players.InvalidPlayerException;

public class EventNetJoinGameRequest extends Event {
    
    public EventNetJoinGameRequest() {
        super(Event.Priority.HIGH);
    }

    @Override
    public void run() {
        boolean result = false;
        GameManager.Lobby lobby;

        if ((lobby = this.gameManager.getLobby("BASIC")) != null) {
            try {
                Player playerInfo = ((JoinGameRequestM) this.trigger).getPlayer();
                this.log.info("Player with claimed id = {}, hostname = {}, is trying to join the game.", playerInfo.getId(), playerInfo.getHostname());

                // TODO: Store list of supported features
                Set<Player> playerSet = new HashSet<>();
                Set<Player> joinedSet = new HashSet<>();

                if ((lobby = this.gameManager.getLobby("BASIC")) != null) {
                    lobby.addPlayer(((JoinGameRequestM) this.trigger).getPlayer().getId());

                    for (uk.ac.standrews.cs.cs3099.grouph.engine.game.players.Player player : lobby.getPlayers()) {
                        if (player.getId() == 0) {
                            playerSet.add(Players.me());
                        } else {
                            playerSet.add(Players.getPlayerById(player.getId()));
                        }
                    }

                    joinedSet.add(((JoinGameRequestM) this.trigger).getPlayer());
                    
                    this.message(new RequestLobbyUpdate(this.messageInterface.getMessageCounter(),
                            playerSet, joinedSet, null, Players.me()), EngineEventManager.ComponentReference.UI);

                    result = true;
                }
            } catch (InvalidPlayerException i) {
                i.printStackTrace();
            }
        }

        this.respond(result, EngineEventManager.ComponentReference.NET);
    }
}
