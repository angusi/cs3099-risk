package uk.ac.standrews.cs.cs3099.grouph.engine.interfaces.game.cards;

public interface ICard {
    public enum Type {
        INFANTRY,
        CALVARY,
        ARTILLERY,
        WILD
    };

    public String getCountryId();
    public Type getType();
}
