package uk.ac.standrews.cs.cs3099.grouph.engine.messages.requests;

import uk.ac.standrews.cs.cs3099.grouph.shared.AbstractEngineMessage;

import uk.ac.standrews.cs.cs3099.grouph.shared.struct.players.Player;

import java.util.Set;

/**
 * Signifies a change in the lobby containing the players waiting to play the game.
 */
public class RequestLobbyUpdate extends AbstractEngineMessage {

    private Set<Player> playerSet;
    private Set<Player> joinedSet;
    private Set<Player> leftSet;
    private Player me;

    public RequestLobbyUpdate(int index, Set<Player> playerSet, Set<Player> joinedSet, Set<Player> leftSet, Player me) {
        super(index);

        this.playerSet = playerSet;
        this.joinedSet = joinedSet;
        this.leftSet = leftSet;
        this.me = me;
    }

    public Set<Player> getPlayerSet() {
        return this.playerSet;
    }

    public Set<Player> getJoinedSet() {
        return this.joinedSet;
    }

    public Set<Player> getLeftSet() {
        return this.leftSet;
    }

    public Player getMe() {
        return this.me;
    }
}