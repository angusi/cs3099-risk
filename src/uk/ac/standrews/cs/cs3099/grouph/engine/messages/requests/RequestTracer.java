package uk.ac.standrews.cs.cs3099.grouph.engine.messages.requests;

import uk.ac.standrews.cs.cs3099.grouph.shared.AbstractEngineMessage;

/**
 * Signifies a tracer message.
 */
public class RequestTracer extends AbstractEngineMessage {

    private final String tracer;

    public RequestTracer(int index, String tracer) {
        super(index);

        this.tracer = tracer;
    }

    /**
     *
     * @return the tracer message
     */
    public String getTracer() {
        return tracer;
    }

}
