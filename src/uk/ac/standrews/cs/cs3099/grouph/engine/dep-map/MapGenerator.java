package uk.ac.standrews.cs.cs3099.grouph.engine.map;

import java.util.ArrayList;	
import java.util.List;
import java.util.Map;

import org.jgraph.graph.Edge;
import org.jgrapht.*;
import org.jgrapht.graph.DefaultWeightedEdge;

import org.jgrapht.graph.SimpleWeightedGraph;

public class MapGenerator {
	SimpleWeightedGraph<Country, DefaultWeightedEdge> graph = null;

	// assuming we get an ArrayList of countries, generates graph
	public SimpleWeightedGraph<Country, DefaultWeightedEdge> generateMap(
			ArrayList<Country> countries) {
		if (graph == null) {
			graph = new SimpleWeightedGraph<Country, DefaultWeightedEdge>(
					DefaultWeightedEdge.class);
		}
		// creates vertices of graph based on arraylist of
		for (Country c : countries) {
			graph.addVertex(c);

			// connects this shit together, because well, graph needs
			// connections
		}
		for (Country c : countries) {
			for (Country d : countries) {
				// cant connect to itself
				if (!c.equals(d)) {
					graph.addEdge(c, d);
				}
			}

		}
		return graph;

	}
	
	
	

	public static void main(String[] args) {
		MapGenerator mg = new MapGenerator();
		ArrayList<Country> countries = new ArrayList<Country>();

		ArrayList<String> oneConnection = new ArrayList<String>();
		oneConnection.add("Alaska");
		oneConnection.add("China");
		Country one = new Country("Rusland", 4, oneConnection, "Asia");
		countries.add(one);

		ArrayList<String> twoConnection = new ArrayList<String>();
		twoConnection.add("Canada");
		twoConnection.add("China");
		twoConnection.add("Alaska");
		Country two = new Country("Usa", 4, twoConnection, "Murica");
		countries.add(two);

		ArrayList<String> threeConnection = new ArrayList<String>();
		threeConnection.add("Usa");
		threeConnection.add("Alaska");
		Country three = new Country("Canada", 4, threeConnection, "Murica");
		countries.add(three);

		ArrayList<String> fourConnection = new ArrayList<String>();
		fourConnection.add("Canada");
		fourConnection.add("Rusland");
		Country four = new Country("Alaska", 4, fourConnection, "Murica");
		countries.add(four);

		ArrayList<String> fiveConnection = new ArrayList<String>();
		fiveConnection.add("Rusland");
		fiveConnection.add("Usa");
		Country five = new Country("China", 4, fiveConnection, "Asia");
		countries.add(five);

		SimpleWeightedGraph<Country, DefaultWeightedEdge> graph = mg
				.generateMap(countries);

		System.out.println(graph.toString());
	}
}
