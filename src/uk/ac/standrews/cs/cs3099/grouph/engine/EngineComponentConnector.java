package uk.ac.standrews.cs.cs3099.grouph.engine;

import java.util.AbstractQueue;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.standrews.cs.cs3099.grouph.shared.AbstractMessage;

public class EngineComponentConnector {
	private class InboundWorker extends Thread {
		private volatile boolean running;
		private Logger log;
		
		private BlockingQueue<AbstractMessage> internal;
		private BlockingQueue<AbstractMessage> inbound;
		
		public InboundWorker(BlockingQueue<AbstractMessage> internal, BlockingQueue<AbstractMessage> inbound) {
			this.log = LoggerFactory.getLogger(InboundWorker.class);
			
			this.internal = internal;
			this.inbound = inbound;
		}
		
		@Override
		public void run() {
			this.running = true;
			this.log.info("InboundWorker Started.");
			
			while (this.running) {
				AbstractMessage msg;
				while ((msg = this.inbound.poll()) != null) {
					this.log.info("Recieved message {} from {}", msg.id, msg.originator);
                    try {
                        this.internal.put(msg);
                    } catch (InterruptedException e) {
                        //TODO: Handle interruptions while waiting to put a message into the internal queue
                        e.printStackTrace();
                    }
                }
			}
		}
		
		public void shutdown() {
			this.running = false;
		}
	}
	
	private class OutboundWorker extends Thread {
		private volatile boolean running;
		
		private BlockingQueue<AbstractMessage> internal;
		private BlockingQueue<AbstractMessage> outbound;
		
		public OutboundWorker(BlockingQueue<AbstractMessage> internal, BlockingQueue<AbstractMessage> outbound) {
			this.internal = internal;
			this.outbound = outbound;
		}
		
		@Override
		public void run() {
			this.running = true;
			
			while (this.running) {
				AbstractMessage msg;
				while ((msg = this.internal.poll()) != null) {
					try {
						this.outbound.put(msg);
					} catch (InterruptedException e) {
						//TODO: Handle interruptions while waiting to put a message into the internal queue
						e.printStackTrace();
					}
				}
			}
		}
		
		public void shutdown() {
			this.running = false;
		}
	}
	
	private boolean internalLock = false;
	private BlockingQueue<AbstractMessage> internalInbound;
	private BlockingQueue<AbstractMessage> internalOutbound;
	private BlockingQueue<AbstractMessage> inbound;
	private BlockingQueue<AbstractMessage> outbound;
	
	private InboundWorker inboundWorker;
	private OutboundWorker outboundWorker;
	
	public EngineComponentConnector(int capacity, boolean fairness) {
		this.internalInbound = null;
		this.internalOutbound = null;
		
		// Set inbound queue to outbound of specified component
		this.inbound = null;
				
		// Set outbound queue of specified component to connector's queue
		this.outbound = new ArrayBlockingQueue<AbstractMessage>(capacity, fairness);
	}
	
	public void startWorkers() throws IllegalThreadStateException, RuntimeException {
		if (this.internalInbound == null || this.internalOutbound == null) {
			// TODO: Lazy, should implement custom exception
			throw new RuntimeException("Tried to start workers without setting internal queues!");
		}
		
		if (this.inbound == null) {
			throw new RuntimeException("Tried to start workers without setting inbound queue!");
		}
		
		// Create new worker threads
		this.inboundWorker = new InboundWorker(this.internalInbound, this.inbound);
		this.outboundWorker = new OutboundWorker(this.internalOutbound, this.outbound);
		
		// Start worker threads
		this.inboundWorker.start();
		this.outboundWorker.start();
	}
	
	public void stopWorkers() {
		this.inboundWorker.shutdown();
		this.outboundWorker.shutdown();
	}
	
	public void setInternalQueueLock(boolean lock) {
		this.internalLock = lock;
	}
	
	public boolean getInternalQueueLock() {
		return this.internalLock;
	}
	
	public ArrayBlockingQueue<AbstractMessage> getOutboundQueue() {
		return (ArrayBlockingQueue<AbstractMessage>)this.outbound;
	}
	
	@SuppressWarnings("unchecked")
	public void setInboundQueue(AbstractQueue<AbstractMessage> q) {
		this.inbound = (BlockingQueue<AbstractMessage>) q;
	}

	/**
	 * @return The internal inbound message queue
	 */
	public BlockingQueue<AbstractMessage> getInternalInboundQueue() {
		return this.internalInbound;
	}
	
	public void setInternalInboundQueue(BlockingQueue<AbstractMessage> queue) {
		if (!this.internalLock)
			this.internalInbound = queue;
		else
			// TODO: Lazy, should implement custom exception
			throw new RuntimeException("Trying to modify internal queues, but queues were locked!");
	}
	
	/**
	 * @return The internal outbound message queue
	 */
	public BlockingQueue<AbstractMessage> getInternalOutboundQueue() {
		return this.internalOutbound;
	}
	
	public void setInternalOutboundQueue(BlockingQueue<AbstractMessage> queue) throws RuntimeException {
		if (!this.internalLock)
			this.internalOutbound = queue;
		else
			// TODO: Lazy, should implement custom exception
			throw new RuntimeException("Trying to modify internal queues, but queues were locked!");
	}
}
