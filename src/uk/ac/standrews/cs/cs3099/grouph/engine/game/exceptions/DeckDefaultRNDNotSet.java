package uk.ac.standrews.cs.cs3099.grouph.engine.game.exceptions;

/**
 * Created by gs63 on 23/02/15.
 */
public class DeckDefaultRNDNotSet extends RuntimeException {

    public DeckDefaultRNDNotSet() {
        super("Tried to auto shuffle when RND generator for deck was not set.");
    }
}
