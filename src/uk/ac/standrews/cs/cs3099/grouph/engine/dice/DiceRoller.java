package uk.ac.standrews.cs.cs3099.grouph.engine.dice;

import uk.ac.standrews.cs.cs3099.grouph.engine.dice.random.SeededGenerator;
import uk.ac.standrews.cs.cs3099.grouph.engine.dice.random.exceptions.HashMismatchException;
import uk.ac.standrews.cs.cs3099.grouph.shared.struct.players.InvalidPlayerException;
import uk.ac.standrews.cs.cs3099.grouph.shared.struct.players.Player;
import uk.ac.standrews.cs.cs3099.grouph.shared.struct.players.Players;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import java.util.stream.IntStream;

/**
 * Created by 120011452 on 20/04/15.
 */
public class DiceRoller {
    private static final DiceRoller INSTANCE = new DiceRoller();

    private Map<Integer, byte[]> playerHashMap;
    private Map<Integer, byte[]> playerNumberMap;

    private List<Integer> resultList;

    private int numberOfPlayers;
    private int numberOfDice;
    private int numberOfFaces;

    private boolean verified;
    private boolean rolled;
    private boolean loaded;

    private SeededGenerator seededGenerator;

    private DiceRoller() {
        this.playerHashMap = new HashMap<>();
        this.playerNumberMap = new HashMap<>();
        this.resultList = new ArrayList<>();
        this.seededGenerator = new SeededGenerator();
    }

    public static DiceRoller loadInstance(int numberOfDice, int numberOfFaces, int numberOfPlayers) {
        DiceRoller instance = DiceRoller.INSTANCE;

        instance.reset();
        instance.setNumberOfDice(numberOfDice);
        instance.setNumberOfFaces(numberOfFaces);
        instance.setNumberOfPlayers(numberOfPlayers);

        instance.loaded = true;

        return instance;
    }

    public static DiceRoller getInstance() {
        return DiceRoller.INSTANCE;
    }

    public synchronized void reset() {
        this.verified = false;
        this.rolled = false;
        this.loaded = false;
        this.seededGenerator = new SeededGenerator();
    }

    public synchronized void clear() {
        this.playerHashMap.clear();
        this.playerNumberMap.clear();
        this.resultList.clear();
    }

    /**
     * Register a hash from a player.
     * @param player the player registering the hash.
     * @param hash the hash to be registered.
     * @return false if the given player has already registered a hash, otherwise true.
     */
    public synchronized boolean registerHash(Player player, byte[] hash) {
        return (this.playerHashMap.putIfAbsent(player.getId(), hash) != null);
    }

    public synchronized boolean registerHash(Player player, String hash) {
        return (this.playerHashMap.putIfAbsent(player.getId(), SeededGenerator.hexToByte(hash)) != null);
    }

    public synchronized int getHashCount() { return this.playerHashMap.size(); }

    public synchronized int getNumberCount() { return this.playerNumberMap.size(); }

    /**
     * Register a number from a player.
     * @param player the player registering the number.
     * @param number the number to be registered.
     * @return false if the given player has already registered a number, otherwise true.
     */
    public synchronized boolean registerNumber(Player player, byte[] number) {
        return (this.playerNumberMap.putIfAbsent(player.getId(), number) != null);
    }

    public synchronized boolean registerNumber(Player player, String number) {
        return (this.playerNumberMap.putIfAbsent(player.getId(), SeededGenerator.hexToByte(number)) != null);
    }

    public synchronized void roll() throws HashMismatchException {
        this.seededGenerator.finalise();

        IntStream.range(0, this.numberOfDice).forEach(i ->
                        this.resultList.add((int) (this.seededGenerator.nextInt() % this.numberOfFaces) + 1)
        );

        this.rolled = true;
    }

    /**
     * Get our own hash.
     * @return the generated hash.
     * @throws InvalidPlayerException
     */
    public synchronized byte[] getMyHash() throws InvalidPlayerException {
        return this.playerHashMap.get(Players.me().getId());
    }

    public synchronized String getMyHashAsHexString() throws InvalidPlayerException {
        return SeededGenerator.byteToHex(this.playerHashMap.get(Players.me().getId()));
    }

    public synchronized byte[] getMyNumber() throws InvalidPlayerException {
        return this.playerNumberMap.get(Players.me().getId());
    }

    public synchronized String getMyNumberAsHexString() throws InvalidPlayerException {
        return SeededGenerator.byteToHex(this.playerNumberMap.get(Players.me().getId()));
    }

    /**
     * Generate and register our own hash.
     */
    public synchronized void generateMe() throws InvalidPlayerException {
        byte[] number = this.seededGenerator.generateNumber();
        this.registerNumber(Players.me(), number);
        this.registerHash(Players.me(), this.seededGenerator.hashByteArr(number));
    }

    public synchronized int getNumberOfPlayers() {
        return numberOfPlayers;
    }

    private synchronized void setNumberOfPlayers(int numberOfPlayers) {
        this.numberOfPlayers = numberOfPlayers;
    }

    /**
     * Sets the number of dice to roll.
     * @param numberOfDice the number of dice to roll.
     */
    private synchronized void setNumberOfDice(int numberOfDice) {
        this.numberOfDice = numberOfDice;
    }

    /**#
     * Returns the number of dice to roll.
     * @return the number of dice to roll.
     */
    public synchronized int getNumberOfDice() {
        return numberOfDice;
    }

    /**
     * Sets the number of faces for all of the dice being rolled.
     * @param numberOfFaces the number of faces for all of the dice.
     */
    private synchronized void setNumberOfFaces(int numberOfFaces) {
        this.numberOfFaces = numberOfFaces;
    }

    /**
     * Gets the number of faces for all of the dice being rolled.
     * @return the number of faces for all of the dice.
     */
    public synchronized int getNumberOfFaces() {
        return numberOfFaces;
    }

    public List<Integer> getResultList() {
        return resultList;
    }

    public synchronized boolean isReady() {
        return (this.hasAllHashes() &&
                this.playerNumberMap.size() == this.numberOfPlayers);
    }

    public synchronized boolean hasAllHashes() {
        return this.playerHashMap.size() == this.numberOfPlayers;
    }

    public synchronized List<Integer> verify() {
        if (this.isReady()) {
            List<Integer> mismatchedPlayers = new ArrayList<>();

            this.playerHashMap.entrySet()
                    .stream()
                    .forEach(entry -> {
                        try {
                            this.seededGenerator.addHash(entry.getKey(),
                                    SeededGenerator.byteToHex(entry.getValue()));
                        } catch (HashMismatchException e) {
                            e.printStackTrace();
                            mismatchedPlayers.add(entry.getKey());
                        }
                    });

            this.playerNumberMap.entrySet()
                    .stream()
                    .forEach(entry -> {
                        try {
                            this.seededGenerator.addNumber(entry.getKey(),
                                    SeededGenerator.byteToHex(entry.getValue()));
                        } catch (HashMismatchException e) {
                            e.printStackTrace();
                            mismatchedPlayers.add(entry.getKey());
                        }
                    });

            if (mismatchedPlayers.size() == 0) {
                this.verified = true;
            }

            return mismatchedPlayers;
        } else {
            return null;
        }
    }

    public synchronized boolean isVerified() { return this.verified; }

    public synchronized boolean isRolled() { return this.rolled; }

    public synchronized boolean isLoaded() { return this.loaded; }
}
