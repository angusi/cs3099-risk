package uk.ac.standrews.cs.cs3099.grouph.engine.events.net;

import uk.ac.standrews.cs.cs3099.grouph.engine.events.EngineEventManager;
import uk.ac.standrews.cs.cs3099.grouph.engine.events.Event;

import uk.ac.standrews.cs.cs3099.grouph.engine.messages.requests.RequestReinforceCount;
import uk.ac.standrews.cs.cs3099.grouph.engine.messages.requests.RequestTerritoryChange;
import uk.ac.standrews.cs.cs3099.grouph.engine.messages.requests.RequestTurnOver;

import uk.ac.standrews.cs.cs3099.grouph.engine.game.managers.exceptions.GameRuleException;
import uk.ac.standrews.cs.cs3099.grouph.engine.game.managers.GameManager;
import uk.ac.standrews.cs.cs3099.grouph.engine.game.managers.GameController;

import uk.ac.standrews.cs.cs3099.grouph.net.messages.SetupM;

import uk.ac.standrews.cs.cs3099.grouph.shared.struct.players.Player;
import uk.ac.standrews.cs.cs3099.grouph.shared.struct.players.Players;
import uk.ac.standrews.cs.cs3099.grouph.shared.struct.players.InvalidPlayerException;

public class EventNetClaim extends Event {

    public EventNetClaim() {
        super(Event.Priority.MEDIUM);
    }

    @Override
    public void run() {
        boolean result = false;

        Player player = ((SetupM) this.trigger).getPlayer();
        int countryId = ((SetupM) this.trigger).getTerritory();

        GameController gc;
        RequestTurnOver.Phase phase;
        int previousPlayerId;
        int currentPlayerId;

        if ((gc = this.gameManager.getGameController("BASIC")) != null) {
            try {
                this.log.info("Entering event with game state = {}", gc.getModifier().getTurnStep().toString());

                previousPlayerId = gc.getModifier().getCurrentPlayer().getId();

                if (gc.getModifier().isDeploy()) {
                    gc.getModifier().deploy(player.getId(), countryId, 1);

                    if (gc.getModifier().isDeploy()) {
                        phase = RequestTurnOver.Phase.DEPLOY;
                    } else {
                        phase = RequestTurnOver.Phase.STANDARD;
                    }

                    this.message(new RequestTerritoryChange(this.messageInterface.getMessageCounter(),
                                    countryId, player, (int) gc.getModifier().getGameMap().getCountryArmies(countryId)),
                            EngineEventManager.ComponentReference.UI);
                } else {
                    gc.getModifier().claim(player.getId(), countryId);

                    if (gc.getModifier().isClaim()) {
                        phase = RequestTurnOver.Phase.CLAIM;
                    } else {
                        phase = RequestTurnOver.Phase.DEPLOY;
                    }

                    this.message(new RequestTerritoryChange(this.messageInterface.getMessageCounter(),
                            countryId, player, 1), EngineEventManager.ComponentReference.UI);
                }

                this.log.info("Now in game state = {}", gc.getModifier().getTurnStep().toString());

                currentPlayerId = gc.getModifier().getCurrentPlayer().getId();

                this.message(new RequestTurnOver(this.messageInterface.getMessageCounter(),
                                Players.getPlayerById(previousPlayerId),
                                Players.getPlayerById(currentPlayerId), phase),
                        EngineEventManager.ComponentReference.UI);

                if (!gc.getModifier().isClaim() && !gc.getModifier().isDeploy()) {
                    gc.getModifier().setupPlayerTurn(currentPlayerId);
                    this.log.info("Player {} now has {} reinforcements ready to deploy.",
                            currentPlayerId, gc.getModifier().getCurrentPlayer().getReinforcements());
                }

                if (!gc.getModifier().isClaim()) {
                    this.message(new RequestReinforceCount(this.messageInterface.getMessageCounter(),
                                    Players.getPlayerById(currentPlayerId),
                                    gc.getModifier().getCurrentPlayer().getReinforcements()),
                            EngineEventManager.ComponentReference.UI);
                }

                result = true;
            } catch (GameRuleException g) {
                this.log.error("Game rule violation = {}", g.getRule());
            } catch (InvalidPlayerException p) {
                this.log.error("Player not found!)");
            }
        }

        this.respond(result, EngineEventManager.ComponentReference.NET);
    }
}
