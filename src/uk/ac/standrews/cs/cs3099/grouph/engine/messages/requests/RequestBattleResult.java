package uk.ac.standrews.cs.cs3099.grouph.engine.messages.requests;

import uk.ac.standrews.cs.cs3099.grouph.shared.AbstractEngineMessage;
import uk.ac.standrews.cs.cs3099.grouph.shared.struct.players.Player;

/**
 * Signifies the completion of a battle. This will happen after a series of RequestAttack -> RequestDefend requests.
 *
 * Destinations:
 * - UI: Signifies the result of a battle between any two players.
 * - Net: N/A
 *
 * @see uk.ac.standrews.cs.cs3099.grouph.engine.messages.requests.RequestAttack
 * @see uk.ac.standrews.cs.cs3099.grouph.engine.messages.requests.RequestDefend
 */
public class RequestBattleResult extends AbstractEngineMessage {

    private Player attackingPlayer;
    private Player defendingPlayer;
    private int attackingCountryId;
    private int defendingCountryId;
    private int attackingLosses;
    private int defendingLosses;

    public RequestBattleResult(int index, String countryId,
                               Player attackingPlayer, Player defendingPlayer,
                               int attackingCountryId, int defendingCountryId, int attackingLosses, int defendingLosses) {
        super(index);

        this.attackingPlayer = attackingPlayer;
        this.defendingPlayer = defendingPlayer;
        this.attackingCountryId = attackingCountryId;
        this.defendingCountryId = defendingCountryId;
        this.defendingLosses = defendingLosses;
        this.attackingLosses = attackingLosses;
    }

    /**
     *
     * @return the Id of the attacking player in the battle.
     */
    public Player getAttackingPlayer() {
        return attackingPlayer;
    }

    /**
     *
     * @return the Id of the defending player in the battle.
     */
    public Player getDefendingPlayer() {
        return defendingPlayer;
    }

    /**
     *
     * @return the number of lost armies the attacked incurred.
     */
    public int getAttackingLosses() {
        return attackingLosses;
    }

    /**
     *
     * @return the number of lost armies the defender incurred.
     */
    public int getDefendingLosses() {
        return defendingLosses;
    }

    /**
     *
     * @return the Id of the defending country in the battle.
     */
    public int getDefendingCountryId() {
        return defendingCountryId;
    }

    /**
     *
     * @return the Id of the attacking country in the battle.
     */
    public int getAttackingCountryId() {
        return attackingCountryId;
    }
}
