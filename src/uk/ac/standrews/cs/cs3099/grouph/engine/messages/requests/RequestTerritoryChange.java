package uk.ac.standrews.cs.cs3099.grouph.engine.messages.requests;

import uk.ac.standrews.cs.cs3099.grouph.shared.AbstractEngineMessage;
import uk.ac.standrews.cs.cs3099.grouph.shared.struct.players.Player;

/**
 * Signifies a territory changing hands.
 *
 * Destinations:
 * - UI: Signifies that the stated country has been conquered by the specified player and they wish to advance the
 * stated number of armies.
 * - Net: Signifies that the current player has conquered the stated territory and wishes to advance the stated number
 * of armies.
 */
public class RequestTerritoryChange extends AbstractEngineMessage {

    private final int countryId;
    private final Player player;
    private final int advancingArmyCount;

    public RequestTerritoryChange(int index, int countryId, Player player, int advancingArmyCount) {
        super(index);

        this.countryId = countryId;
        this.player = player;
        this.advancingArmyCount = advancingArmyCount;
    }

    /**
     *
     * @return the Id of the country that is changing hands.
     */
    public int getCountryId() {
        return countryId;
    }

    /**
     *
     * @return the player Id that the country is changing hands to.
     */
    public Player getPlayer() {
        return player;
    }

    /**
     *
     * @return the number of armies the player will be advancing into this territory.
     */
    public int getAdvancingArmyCount() {
        return advancingArmyCount;
    }
}
