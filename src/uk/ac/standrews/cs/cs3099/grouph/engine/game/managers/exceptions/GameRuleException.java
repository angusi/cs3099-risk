package uk.ac.standrews.cs.cs3099.grouph.engine.game.managers.exceptions;

import java.lang.Exception;

public class GameRuleException extends Exception {
    public enum Rule {
        PLAYER_ILLEGAL_STEP,
        PLAYER_ILLEGAL_TURN,
        COUNTRY_ILLEGAL_CONNECTION,
        COUNTRY_ILLEGAL_ID,
        COUNTRY_INCORRECT_OWNER,
        ARMY_ATTACKER_INSUFFICIENT,
        ARMY_ATTACKER_TOO_LARGE,
        ARMY_DEFENDER_INSUFFICIENT,
        ARMY_DEFENDER_TOO_LARGE,
        ARMY_NEGATIVE,
        ARMY_REINFORCE_TOO_LARGE,
        ARMY_MOVE_TOO_LARGE,
        FORTIFY_NO_PATH
    }

    private Rule rule;

    public GameRuleException(Rule rule) {
        this.rule = rule;
    }

    public Rule getRule() {
        return this.rule;
    }
}
