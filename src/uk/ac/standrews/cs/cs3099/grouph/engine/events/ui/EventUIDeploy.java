package uk.ac.standrews.cs.cs3099.grouph.engine.events.ui;

import uk.ac.standrews.cs.cs3099.grouph.engine.events.Event;
import uk.ac.standrews.cs.cs3099.grouph.engine.events.EngineEventManager;

import uk.ac.standrews.cs.cs3099.grouph.engine.messages.requests.*;
import uk.ac.standrews.cs.cs3099.grouph.ui.messages.DeployMessage;

import uk.ac.standrews.cs.cs3099.grouph.shared.struct.players.InvalidPlayerException;
import uk.ac.standrews.cs.cs3099.grouph.shared.struct.players.Player;
import uk.ac.standrews.cs.cs3099.grouph.shared.struct.players.Players;

import uk.ac.standrews.cs.cs3099.grouph.engine.game.managers.exceptions.GameRuleException;
import uk.ac.standrews.cs.cs3099.grouph.engine.game.managers.GameController;

import java.util.Arrays;

public class EventUIDeploy extends Event {

    public EventUIDeploy() {
        super(Event.Priority.MEDIUM);
    }

    @Override
    public void run() {
        boolean result = false;
        GameController gc;

        try {
            if ((gc = this.gameManager.getGameController("BASIC")) != null) {
                Player player = Players.me(); // Get current player UID
                DeployMessage deployMessage = ((DeployMessage) this.trigger);

                if (gc.getModifier().isDeploy()) {
                    int[][] deployment = deployMessage.getDeployments();
                    gc.getModifier().deploy(player.getId(), deployment[0][0], 1);

                    if (!gc.getModifier().isDeploy()) {
                        gc.getModifier().setupPlayerTurn(gc.getModifier().getCurrentPlayer().getId());
                    }

                    this.message(new RequestSetupClaim(this.messageInterface.getMessageCounter(), player, deployment[0][0]),
                            EngineEventManager.ComponentReference.NET);

                    this.message(new RequestTerritoryChange(this.messageInterface.getMessageCounter(), deployment[0][0],
                            player, (int) gc.getModifier().getGameMap().getCountryArmies(deployment[0][0])),
                            EngineEventManager.ComponentReference.UI);

                    int previousPlayerId = player.getId();
                    int currentPlayerId = gc.getModifier().getCurrentPlayer().getId();

                    if (gc.getModifier().isDeploy()) {
                        this.message(new RequestTurnOver(this.messageInterface.getMessageCounter(),
                                        Players.getPlayerById(previousPlayerId), Players.getPlayerById(currentPlayerId),
                                        RequestTurnOver.Phase.DEPLOY),
                                EngineEventManager.ComponentReference.UI);
                    } else {
                        this.message(new RequestTurnOver(this.messageInterface.getMessageCounter(),
                                        Players.getPlayerById(previousPlayerId), Players.getPlayerById(currentPlayerId),
                                        RequestTurnOver.Phase.STANDARD),
                                EngineEventManager.ComponentReference.UI);
                    }

                    result = true;
                } else {
                    result = Arrays.stream(deployMessage.getDeployments()).map(deployment -> {
                        int countryId = deployment[0];
                        int numberOfArmies = deployment[1];

                        try {
                            gc.getModifier().reinforce(player.getId(), countryId, numberOfArmies);

                            this.message(new RequestTerritoryChange(this.messageInterface.getMessageCounter(),
                                            countryId, player, (int) gc.getModifier().getGameMap().getCountryArmies(countryId)),
                                    EngineEventManager.ComponentReference.UI);

                            return true;
                        } catch (GameRuleException e) {
                            this.log.error("Game rule exception: {}");
                            return false;
                        }
                    }).filter(a -> !a).count() == 0;

                    if (result) {
                        this.message(new RequestDeploy(this.messageInterface.getMessageCounter(), player,
                                        deployMessage.getDeployments()),
                                EngineEventManager.ComponentReference.NET);

                        int previousPlayerId = player.getId();
                        int currentPlayerId = gc.getModifier().getCurrentPlayer().getId();

                        this.message(new RequestReinforceCount(this.messageInterface.getMessageCounter(),
                                        Players.getPlayerById(player.getId()),
                                        gc.getModifier().getCurrentPlayer().getReinforcements()),
                                EngineEventManager.ComponentReference.UI);

                        if (gc.getModifier().isDeploy()) {
                            this.message(new RequestTurnOver(this.messageInterface.getMessageCounter(),
                                            Players.getPlayerById(previousPlayerId), Players.getPlayerById(currentPlayerId),
                                            RequestTurnOver.Phase.DEPLOY),
                                    EngineEventManager.ComponentReference.UI);
                        } else if (currentPlayerId != player.getId()) {
                            this.message(new RequestTurnOver(this.messageInterface.getMessageCounter(),
                                            Players.getPlayerById(previousPlayerId), Players.getPlayerById(currentPlayerId),
                                            RequestTurnOver.Phase.STANDARD),
                                    EngineEventManager.ComponentReference.UI);
                        }
                    }
                }
            }
        } catch (InvalidPlayerException e) {
            this.log.error("Invalid Player Exception: {}", e.getMessage());
        } catch (GameRuleException e) {
            this.log.error("Game Rule Exception: {}", e.getMessage());
        }

        this.respond(result, EngineEventManager.ComponentReference.UI);
    }
}
