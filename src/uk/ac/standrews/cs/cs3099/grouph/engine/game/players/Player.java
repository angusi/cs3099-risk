package uk.ac.standrews.cs.cs3099.grouph.engine.game.players;

import java.lang.Math;
import java.util.List;

/**
 * Represents a player in a game of risk.
 */
public class Player implements Comparable<Player> {
    public enum CardType {
        SOLDIER,
        HORSEMAN,
        CANNON,
        WILD
    }

    private final int id;

    private List<CardType> cards;

    private int reinforcements;
    private int territories;

    public Player(int id) {
        this.id = id;
        this.reinforcements = 0;
        this.territories = 0;
    }

    public int getId() {
        return id;
    }

    public synchronized void addCard(CardType card) {
        this.cards.add(card);
    }

    public synchronized void removeCard(CardType card) {
        for (int i = 0; i < this.cards.size(); i++) {
            if (this.cards.get(i) == card) {
                this.cards.remove(i);
            }
        }
    }

    public synchronized int getReinforcements() {
        return this.reinforcements;
    }

    public synchronized void setReinforcements(int i) {
        this.reinforcements = i;
    }

    public synchronized void calculateTurnReinforcements() {
        this.reinforcements = (int) Math.floor((double) (this.territories / 3));
    }

    public synchronized int getTerritories() {
        return this.territories;
    }

    public synchronized void setTerritories(int i) {
        this.territories = i;
    }

    @Override
    public boolean equals(Object obj) {
        return (obj instanceof Player &&
                ((Player) obj).getId() == this.getId());
    }

    @Override
    public int compareTo(Player o) {
        return (o != null) ? this.id - o.getId() : 0;
    }

}

