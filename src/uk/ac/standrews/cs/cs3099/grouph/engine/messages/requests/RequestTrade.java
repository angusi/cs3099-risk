package uk.ac.standrews.cs.cs3099.grouph.engine.messages.requests;

import java.util.Set;

import uk.ac.standrews.cs.cs3099.grouph.shared.AbstractEngineMessage;
import uk.ac.standrews.cs.cs3099.grouph.shared.struct.players.Player;

/**
 * Signifies a request to trade a set of cards.
 *
 * Destination:
 * - UI: Signifies that a player has made a trade.
 * - Net: Signifies that the current player has made a trade.
 */
public class RequestTrade extends AbstractEngineMessage {
    
    private final Player player;
    private final Set<String> cardContinentIdSet;
    
    public RequestTrade(int index, Player player, Set<String> cardContinentIdSet) {
        super(index);

        this.player = player;
        this.cardContinentIdSet = cardContinentIdSet;
    }

    /**
     *
     * @return the Id of the player that is trading in the cards.
     */
    public Player getPlayer() {
        return this.player;
    }

    /**
     *
     * @return the single set of cards that the player is trading in.
     */
    public Set<String> getCardContinentIdSet() {
        return this.cardContinentIdSet;
    }

}
