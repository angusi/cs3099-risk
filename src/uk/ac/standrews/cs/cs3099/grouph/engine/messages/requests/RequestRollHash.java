package uk.ac.standrews.cs.cs3099.grouph.engine.messages.requests;

import uk.ac.standrews.cs.cs3099.grouph.shared.AbstractEngineMessage;

/**
 * Created by 120011452 on 20/04/15.
 */
public class RequestRollHash extends AbstractEngineMessage {
    private final String hashHex;

    public RequestRollHash(int index, String hashHex) {
        super(index);

        this.hashHex = hashHex;
    }

    public String getHashHex() {
        return hashHex;
    }
}
