package uk.ac.standrews.cs.cs3099.grouph.engine.messages.requests;

import uk.ac.standrews.cs.cs3099.grouph.shared.AbstractEngineMessage;
import uk.ac.standrews.cs.cs3099.grouph.shared.struct.players.Player;

import java.util.List;

/**
 * Signifies the result of a roll that has been made.
 *
 * Destinations:
 * - UI: Signifies the roll result of the stated player.
 * - Net: Not Applicable.
 */
public class RequestRollResult extends AbstractEngineMessage {

    private final Player player;
    private final int numberOfDice;
    private final List<Integer> rollResults;

    public RequestRollResult(int index, Player player, int numberOfDice, List<Integer> rollResults) {
        super(index);

        this.player = player;
        this.numberOfDice = numberOfDice;
        this.rollResults = rollResults;
    }

    /**
     *
     * @return the Id of the player to which this roll result pertains to.
     */
    public Player getPlayer() {
        return player;
    }

    /**
     *
     * @return the number of dice that were rolled.
     */
    public int getNumberOfDice() {
        return numberOfDice;
    }

    /**
     *
     * @return the results of the dice that were rolled.
     */
    public List<Integer> getRollResults() {
        return rollResults;
    }
}
