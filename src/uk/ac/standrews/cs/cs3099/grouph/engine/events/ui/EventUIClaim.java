package uk.ac.standrews.cs.cs3099.grouph.engine.events.ui;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.standrews.cs.cs3099.grouph.engine.events.Event;
import uk.ac.standrews.cs.cs3099.grouph.engine.events.EngineEventManager;

import uk.ac.standrews.cs.cs3099.grouph.engine.game.managers.GameManager;

import uk.ac.standrews.cs.cs3099.grouph.engine.messages.requests.RequestReinforceCount;
import uk.ac.standrews.cs.cs3099.grouph.shared.struct.players.InvalidPlayerException;
import uk.ac.standrews.cs.cs3099.grouph.engine.messages.responses.ResponseMessage;
import uk.ac.standrews.cs.cs3099.grouph.engine.messages.requests.RequestSetupClaim;
import uk.ac.standrews.cs.cs3099.grouph.engine.messages.requests.RequestTerritoryChange;
import uk.ac.standrews.cs.cs3099.grouph.engine.messages.requests.RequestTurnOver;

import uk.ac.standrews.cs.cs3099.grouph.ui.messages.ClaimTerritoryMessage;

import uk.ac.standrews.cs.cs3099.grouph.shared.struct.players.Player;
import uk.ac.standrews.cs.cs3099.grouph.shared.struct.players.Players;
import uk.ac.standrews.cs.cs3099.grouph.shared.struct.players.InvalidPlayerException;

import uk.ac.standrews.cs.cs3099.grouph.engine.game.managers.exceptions.GameRuleException;
import uk.ac.standrews.cs.cs3099.grouph.engine.game.managers.GameManager;
import uk.ac.standrews.cs.cs3099.grouph.engine.game.managers.GameController;

public class EventUIClaim extends Event {
    
    public EventUIClaim() {
        super(Event.Priority.MEDIUM);
    }

    @Override
    public void run() {
        try {
            Player player = Players.me();
            int countryId = ((ClaimTerritoryMessage) this.trigger).getCountryId();

            this.log.info("Player {} is trying to claim {}", player.getId(), countryId);

            GameController gc;
            int previousPlayerId;
            int currentPlayerId;
            
            if ((gc = this.gameManager.getGameController("BASIC")) != null) {
                previousPlayerId = gc.getModifier().getCurrentPlayer().getId();

                gc.getModifier().claim(player.getId(), countryId);

                currentPlayerId = gc.getModifier().getCurrentPlayer().getId();

                this.message(new RequestSetupClaim(this.messageInterface.getMessageCounter(),
                            player, countryId), EngineEventManager.ComponentReference.NET);

                this.message(new RequestTerritoryChange(this.messageInterface.getMessageCounter(),
                            countryId, player, 1),
                        EngineEventManager.ComponentReference.UI);

                RequestTurnOver.Phase phase;

                if (gc.getModifier().isClaim()) {
                    phase = RequestTurnOver.Phase.CLAIM;
                } else {
                    phase = RequestTurnOver.Phase.DEPLOY;
                }

                this.message(new RequestTurnOver(this.messageInterface.getMessageCounter(),
                            Players.getPlayerById(previousPlayerId),
                            Players.getPlayerById(currentPlayerId), phase),
                        EngineEventManager.ComponentReference.UI);

                this.respond(true, EngineEventManager.ComponentReference.UI);

                if (!gc.getModifier().isClaim()) {
                   this.message(new RequestReinforceCount(this.messageInterface.getMessageCounter(),
                                    Players.getPlayerById(currentPlayerId),
                                    gc.getModifier().getCurrentPlayer().getReinforcements()),
                            EngineEventManager.ComponentReference.UI);
                }
            } else {
                this.respond(false, EngineEventManager.ComponentReference.UI);
            }
        } catch (GameRuleException g) {
            this.log.error("Game rule violation = {}", g.getRule());
            this.respond(false, EngineEventManager.ComponentReference.UI);
        } catch (InvalidPlayerException p) {
            this.log.error("Player not found!)");
            this.respond(false, EngineEventManager.ComponentReference.UI);
        }
    }
}
