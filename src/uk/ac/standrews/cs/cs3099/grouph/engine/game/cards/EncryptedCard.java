package uk.ac.standrews.cs.cs3099.grouph.engine.game.cards;


import uk.ac.standrews.cs.cs3099.grouph.engine.interfaces.game.cards.ICard;

/**
 * An encrypted version of a Card.
 *
 * The contents of an encrypted card cannot be viewed until it is decrypted.
 *
 * @see Card
 */
public class EncryptedCard extends Card {

    private String encryptedContents;
    private boolean encrypted = true;

    public EncryptedCard(String encryptedContents) {
        super(null, null);
    }

    /**
     * Get the flag that states if the card is currently encrypted.
     * @return the encrypted flag.
     */
    public synchronized boolean isEncrypted() {
        return this.encrypted;
    }

    /**
     * Decrypt the card using the given card values.
     * @param decryptedCard the card containing the required decrypted card values.
     */
    public synchronized void decrypt(ICard decryptedCard) {
        if (this.encrypted && decryptedCard != null) {
            this.type = decryptedCard.getType();
            this.countryId = decryptedCard.getCountryId();

            this.encrypted = false;
        }
    }

    /**
     * Get the String containing the encrypted representation of the card.
     * @return the encrypted string representation of this card.
     */
    public String getEncryptedContents() {
        return this.encryptedContents;
    }

    @Override
    public String getCountryId() {
        return (this.encrypted) ? null : this.countryId;
    }

    @Override
    public Type getType() {
        return (this.encrypted) ? null : this.type;
    }
}
