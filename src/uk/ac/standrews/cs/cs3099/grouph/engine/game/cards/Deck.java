package uk.ac.standrews.cs.cs3099.grouph.engine.game.cards;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.standrews.cs.cs3099.grouph.engine.interfaces.game.cards.ICard;
import uk.ac.standrews.cs.cs3099.grouph.engine.interfaces.game.cards.IDeck;
import uk.ac.standrews.cs.cs3099.grouph.engine.game.exceptions.DeckDefaultRNDNotSet;
import uk.ac.standrews.cs.cs3099.grouph.engine.game.exceptions.InvalidPileState;

import java.util.Collection;
import java.util.Random;

/**
 * A standard implementation of a deck of cards.
 *
 * This implementation contains a single standard draw pile and a single standard discard pile.
 *
 * @implNote shuffle() will put all of the cards in the discard pile into the draw pile before shuffling if it is
 * not empty.
 */
public class Deck implements IDeck {

    private Logger log = LoggerFactory.getLogger(Deck.class);

    private Collection<ICard> containsCards;

    private DiscardPile discardPile;
    private DrawPile drawPile;

    private Random defaultRND = null;

    private boolean autoReshuffle = false;

    public Deck(int deckSize, Collection<ICard> containsCards) {
        this.discardPile = new DiscardPile(deckSize);
        this.drawPile = new DrawPile(deckSize, containsCards);

        this.containsCards = containsCards;
    }

    public Deck(int deckSize, Collection<ICard> containsCards, Random defaultRND) {
        this(deckSize, containsCards);

        this.defaultRND = defaultRND;
        this.autoReshuffle = true;
    }


    @Override
    public synchronized ICard drawCard() throws DeckDefaultRNDNotSet {
        if (this.drawPile.getNumberOfCards() != 0) {
            try {
                return drawPile.drawCard();
            } catch (InvalidPileState e) {
                this.log.error("Corrupted draw pile.");
                return null;
            }
        } else if (this.autoReshuffle) {
            this.log.info("Draw requested when draw pile is empty, proceeding to auto shuffle.");
            this.shuffle(null);
            return this.drawPile.drawCard();
        } else {
            this.log.info("Rejected draw from empty deck.");
            return null;
        }
    }

    @Override
    public synchronized void discardCard(ICard card) {
        this.discardPile.placeCard(card);
    }

    @Override
    public synchronized boolean containsCard(ICard card) {
        return this.containsCards.contains(card);
    }

    @Override
    public synchronized void shuffle(Random rnd) throws DeckDefaultRNDNotSet {
        if (rnd == null && this.defaultRND == null)
            throw new DeckDefaultRNDNotSet();

        // Select correct RND generator
        Random r = (rnd == null) ? this.defaultRND : rnd;

        // Warn if trying to shuffle when draw pile not empty
        if (this.drawPile.getNumberOfCards() != 0)
            this.log.warn("Shuffling deck when draw pile is not empty.");

        // Append discard pile into draw pile
        this.drawPile.append(this.discardPile);

        // Shuffle draw deck
        this.drawPile.shuffle(rnd);
    }

    @Override
    public synchronized boolean getAutoReshuffle() {
        return this.autoReshuffle;
    }

    @Override
    public synchronized void setAutoReshuffle(boolean b) {
        this.autoReshuffle = b;
    }
}
