package uk.ac.standrews.cs.cs3099.grouph.engine.messages.responses;

import uk.ac.standrews.cs.cs3099.grouph.shared.AbstractMessage;

/**
 * Standard response for messages that modify game state and trigger a game state that is invalid.
 */
public class ResponseGameError extends ResponseMessage {

    public ResponseGameError(int index, AbstractMessage trigger) {
        super(index, trigger, Code.REJECT);
    }

}
