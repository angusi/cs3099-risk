package uk.ac.standrews.cs.cs3099.grouph.engine.events.internal;

import uk.ac.standrews.cs.cs3099.grouph.engine.events.Event;
import uk.ac.standrews.cs.cs3099.grouph.engine.events.EngineEventManager;
import uk.ac.standrews.cs.cs3099.grouph.engine.messages.responses.ResponseMessage;

public class EventInternalUnrecognisedMessage extends Event {
    
    public EventInternalUnrecognisedMessage() {
        super(Event.Priority.LOW);
    }

    @Override
    public void run() {
        try {
            this.messageInterface.push(
                    new ResponseMessage(
                        this.messageInterface.getMessageCounter(),
                        this.trigger,
                        ResponseMessage.Code.UNKNOWN),
                    (this.trigger.originator.equals("UI")) ? EngineEventManager.ComponentReference.UI : EngineEventManager.ComponentReference.NET);
        } catch (InterruptedException e) {
            this.log.error("Interrupted whilst trying to send tracer.");
        }
    }

}

