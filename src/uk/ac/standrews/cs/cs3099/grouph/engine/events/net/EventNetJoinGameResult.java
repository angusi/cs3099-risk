package uk.ac.standrews.cs.cs3099.grouph.engine.events.net;

import uk.ac.standrews.cs.cs3099.grouph.engine.events.Event;
import uk.ac.standrews.cs.cs3099.grouph.engine.events.EngineEventManager;

import uk.ac.standrews.cs.cs3099.grouph.engine.game.managers.GameManager;

import uk.ac.standrews.cs.cs3099.grouph.engine.game.map.GameMap;
import uk.ac.standrews.cs.cs3099.grouph.engine.game.map.MapParser;
import uk.ac.standrews.cs.cs3099.grouph.net.messages.JoinGameResultM;
import uk.ac.standrews.cs.cs3099.grouph.engine.messages.requests.RequestJoinResult;
import uk.ac.standrews.cs.cs3099.grouph.engine.messages.requests.RequestMapTransfer;

import uk.ac.standrews.cs.cs3099.grouph.shared.struct.players.Player;
import uk.ac.standrews.cs.cs3099.grouph.shared.struct.players.Players;

public class EventNetJoinGameResult extends Event {
    
    public EventNetJoinGameResult() {
        super(Event.Priority.HIGH);
    }

    @Override
    public void run() {
        boolean accepted = ((JoinGameResultM) this.trigger).isJoinSuccess();

        this.message(new RequestJoinResult(this.messageInterface.getMessageCounter(), accepted),
                EngineEventManager.ComponentReference.UI);

        String jsonMap = ((JoinGameResultM) this.trigger).getMap();

        if (jsonMap != null) {
            MapParser parser = MapParser.createStringParser(jsonMap);

            if (parser.parse()) {
                GameMap map = GameMap.createMapFromParser(parser);

                this.gameManager.getLobby("BASIC").setGameMap(map);

                this.message(new RequestMapTransfer(this.messageInterface.getMessageCounter(), jsonMap),
                        EngineEventManager.ComponentReference.UI);
            }
        }

        this.respond(accepted, EngineEventManager.ComponentReference.NET);
    }
}
