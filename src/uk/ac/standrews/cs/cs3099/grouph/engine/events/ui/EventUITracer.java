package uk.ac.standrews.cs.cs3099.grouph.engine.events.ui;

import uk.ac.standrews.cs.cs3099.grouph.engine.events.EngineEventManager;
import uk.ac.standrews.cs.cs3099.grouph.engine.events.Event;

import uk.ac.standrews.cs.cs3099.grouph.net.messages.TracerM;

/**
 * A basic tracer event that reacts to tracer messages sent from the UI.
 * 
 * Tracer events are logged in the engine internally then simply rerouted to the network layer.
 *
 * - Event Trigger
 * 		TracerMessage from UI component.
 * - Event Action
 * 		Trigger internal event EngineTracer then reroute TracerMessage to network as TracerM.
 *
 * TODO: Implement triggering of EventInternalTracer
 */
public class EventUITracer extends Event {
    
    public EventUITracer() {
        super(Event.Priority.LOWEST);
    }

    @Override
    public void run() {
        try {
            this.messageInterface.push(
                    new TracerM(this.messageInterface.getMessageCounter()),
                    EngineEventManager.ComponentReference.NET);
        } catch (InterruptedException e) {
            this.log.error("Interrupted whilst trying to send tracer.");
        } 
    }

}

