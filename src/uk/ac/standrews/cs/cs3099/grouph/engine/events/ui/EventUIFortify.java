package uk.ac.standrews.cs.cs3099.grouph.engine.events.ui;

import uk.ac.standrews.cs.cs3099.grouph.engine.events.Event;
import uk.ac.standrews.cs.cs3099.grouph.engine.events.EngineEventManager;

import uk.ac.standrews.cs.cs3099.grouph.engine.messages.requests.RequestMove;

import uk.ac.standrews.cs.cs3099.grouph.engine.messages.requests.RequestTerritoryChange;
import uk.ac.standrews.cs.cs3099.grouph.engine.messages.requests.RequestTurnOver;
import uk.ac.standrews.cs.cs3099.grouph.ui.messages.FortifyMessage;

import uk.ac.standrews.cs.cs3099.grouph.shared.struct.players.InvalidPlayerException; 
import uk.ac.standrews.cs.cs3099.grouph.shared.struct.players.Player;
import uk.ac.standrews.cs.cs3099.grouph.shared.struct.players.Players;

import uk.ac.standrews.cs.cs3099.grouph.engine.game.managers.exceptions.GameRuleException;
import uk.ac.standrews.cs.cs3099.grouph.engine.game.managers.GameManager;
import uk.ac.standrews.cs.cs3099.grouph.engine.game.managers.GameController;

public class EventUIFortify extends Event {

    public EventUIFortify() {
        super(Event.Priority.MEDIUM);
    }

    @Override
    public void run() {
        boolean result = false;

        GameController gc;

        try {
            if ((gc = this.gameManager.getGameController("BASIC")) != null) {
                Player player = Players.me();

                int startingCountryId = ((FortifyMessage) this.trigger).getSourceCountry();
                int destinationCountryId = ((FortifyMessage) this.trigger).getDestinationCountry();
                int numberOfArmies = ((FortifyMessage) this.trigger).getNumberOfArmies();

                if (gc.getModifier().isAttack()) {
                    gc.getModifier().fortify(player.getId(), startingCountryId, destinationCountryId, numberOfArmies);

                    this.message(new RequestMove(this.messageInterface.getMessageCounter(), player,
                                    numberOfArmies, startingCountryId, destinationCountryId),
                            EngineEventManager.ComponentReference.NET);

                    this.message(new RequestTerritoryChange(this.messageInterface.getMessageCounter(), startingCountryId,
                                    player, (int) gc.getModifier().getGameMap().getCountryArmies(startingCountryId)),
                            EngineEventManager.ComponentReference.UI);

                    this.message(new RequestTerritoryChange(this.messageInterface.getMessageCounter(), destinationCountryId,
                                    player, (int) gc.getModifier().getGameMap().getCountryArmies(destinationCountryId)),
                            EngineEventManager.ComponentReference.UI);

                    gc.getModifier().turnover(player.getId());
                    gc.getModifier().setupPlayerTurn(gc.getModifier().getCurrentPlayer().getId());

                    this.message(new RequestTurnOver(this.messageInterface.getMessageCounter(),
                                    player, Players.getPlayerById(gc.getModifier().getCurrentPlayer().getId()),
                                    RequestTurnOver.Phase.STANDARD),
                            EngineEventManager.ComponentReference.UI);
                } else {
                    gc.getModifier().move(player.getId(), numberOfArmies);

                    this.message(new RequestMove(this.messageInterface.getMessageCounter(), player,
                                    numberOfArmies, startingCountryId, destinationCountryId),
                            EngineEventManager.ComponentReference.NET);

                    this.message(new RequestTerritoryChange(this.messageInterface.getMessageCounter(), startingCountryId,
                                    player, (int) gc.getModifier().getGameMap().getCountryArmies(startingCountryId)),
                            EngineEventManager.ComponentReference.UI);

                    this.message(new RequestTerritoryChange(this.messageInterface.getMessageCounter(), destinationCountryId,
                                    player, (int) gc.getModifier().getGameMap().getCountryArmies(destinationCountryId)),
                            EngineEventManager.ComponentReference.UI);
                }
            }
        } catch (GameRuleException g) {
            this.log.error("Game rule violation = {}", g.getRule());
            this.respond(false, EngineEventManager.ComponentReference.UI);
        } catch (InvalidPlayerException p) {
            this.log.error("Player not found!)");
            this.respond(false, EngineEventManager.ComponentReference.UI);
        }
    }
}
