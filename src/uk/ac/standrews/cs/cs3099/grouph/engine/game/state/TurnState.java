package uk.ac.standrews.cs.cs3099.grouph.engine.game.state;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.standrews.cs.cs3099.grouph.engine.game.players.Player;
import uk.ac.standrews.cs.cs3099.grouph.engine.game.players.PlayerQueue;
import uk.ac.standrews.cs.cs3099.grouph.engine.game.state.exceptions.InvalidState;

/**
 * Stores the current state of a turn that a given player is in.
 */
public class TurnState {
    public enum Step {
        CLAIM, //
        DEPLOY,  //
        TRADE,
        REINFORCE, //
        ATTACK, //
        DEFEND, //
        MOVE,	//
        RESULT,
        FORTIFY,//
        FINISH	//
    }

    private Logger log = LoggerFactory.getLogger(TurnState.class);

    private PlayerQueue playerQueue;

    private Player currentPlayer;
    private Player defendingPlayer;

    private Step currentStep;

    private int numberOfAttackers;
    private int numberOfDefenders;

    private int attackingCountryId;
    private int defendingCountryId;

    public TurnState(PlayerQueue playerQueue) {
        this.playerQueue = playerQueue;

        this.currentPlayer = playerQueue.nextPlayer();
        this.defendingPlayer = null;
        this.currentStep = Step.CLAIM;
    }

    /**
     * Step the current player's turn state to the next turn step.
     */
    public synchronized void nextStep() {
        switch (this.currentStep) {
            case CLAIM:
                this.currentStep = Step.CLAIM;
                break;
            case DEPLOY:
                this.currentStep = Step.DEPLOY;
                break;
            case TRADE:
                this.currentStep = Step.REINFORCE;
                break;
            case REINFORCE:
                this.defendingPlayer = null;
                this.numberOfAttackers = 0;
                this.numberOfDefenders = 0;
                this.attackingCountryId = -1;
                this.defendingCountryId = -1;
                this.currentStep = Step.ATTACK;
                break;
            case ATTACK:
                this.currentStep = Step.DEFEND;
                break;
            case DEFEND:
                this.currentStep = Step.RESULT;
                break;
            case RESULT:
                this.currentStep = Step.ATTACK;
                break;
            case MOVE:
                this.defendingPlayer = null;
                this.numberOfAttackers = 0;
                this.numberOfDefenders = 0;
                this.attackingCountryId = -1;
                this.defendingCountryId = -1;
                this.currentStep = Step.ATTACK;
                break;
            case FORTIFY:
                this.currentStep = Step.FINISH;
                break;
            case FINISH:
                this.currentStep = Step.REINFORCE;
                break;
        }

        this.log.info("Switching to state {}", this.currentStep);
    }

    public synchronized void finishClaim() {
        if (this.currentStep == Step.CLAIM)
            this.currentStep = Step.DEPLOY;
    }

    public synchronized void finishDeploy() {
        if (this.currentStep == Step.DEPLOY)
            this.currentStep = Step.REINFORCE;
    }

    public synchronized void finishAttack() {
        if (this.currentStep == Step.ATTACK) {
            this.defendingPlayer = null;
            this.currentStep = Step.FORTIFY;
        }
    }

    public synchronized void yield() {
        this.currentStep = Step.FINISH;
    }

    public synchronized Step getCurrentStep() {
        return this.currentStep;
    }
    /**
-     * Resets the turn order.
-     */

   public synchronized void resetTurnOrder() {
        this.playerQueue.resetOrder();
       this.currentPlayer = this.playerQueue.nextPlayer();
    }

    /**
     * Move to the next player in turn order.
     * @throws InvalidState thrown when trying to go to the next player without the correct state.
     */
    public synchronized void nextPlayer() throws InvalidState {
        if (this.currentStep == Step.DEPLOY || 
            this.currentStep == Step.CLAIM ||
            this.currentStep == Step.FINISH) {
            this.currentPlayer = this.playerQueue.nextPlayer();
            this.log.info("Current player is now player with id = {}", this.currentPlayer.getId());
        } else {
            throw new InvalidState("Cannot go to next player unless in CLAIM or FINISH states.");
        }
    }

    /**
     * Get the current player whose turn it is.
     * @return the current Player.
     */
    public synchronized Player getCurrentPlayer() {
        if (this.currentStep != Step.DEFEND)
            return this.currentPlayer;
        else
            return this.defendingPlayer;
    }

    public synchronized String printTurnOrder() {
        return this.playerQueue.toString();
    }

    public synchronized void triggerMoveStep() {
        if (this.currentStep == Step.RESULT)
            this.currentStep = Step.MOVE;
    }

    public synchronized Player getDefendingPlayer() {
        return this.defendingPlayer;
    }

    public synchronized Player getAttackingPlayer() { return this.currentPlayer; }

    public synchronized void setNumberOfAttackers(int n) {
        this.numberOfAttackers = n;
    }

    public synchronized void setNumberOfDefenders(int n) {
        this.numberOfDefenders = n;
    }

    public synchronized int getNumberOfAttackers() {
        return this.numberOfAttackers;
    }

    public synchronized int getNumberOfDefenders() {
        return this.numberOfDefenders;
    }

    public synchronized void setDefendingPlayer(Player player) {
        this.defendingPlayer = player;
    }

    public synchronized int getDefendingCountryId() {
        return this.defendingCountryId;
    }

    public synchronized int getAttackingCountryId() {
        return this.attackingCountryId;
    }

    public synchronized void setDefendingCountryId(int id) {
        this.defendingCountryId = id;
    }

    public synchronized void setAttackingCountryId(int id) {
        this.attackingCountryId = id;
    }

}

