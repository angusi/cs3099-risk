package uk.ac.standrews.cs.cs3099.grouph.engine.events.interfaces;

import uk.ac.standrews.cs.cs3099.grouph.engine.events.EngineEventManager;

/**
 * Implemented events that require communication with the network component.
 */
public interface EventQueuer {

	public void setEventInterface(EngineEventManager.EventInterface m);
}
