package uk.ac.standrews.cs.cs3099.grouph.engine.gamestate;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class GameState {
    Map map;
    List<Card> cards;
    List<Player> players;

    public GameState(String jsonString) throws IOException {
        this.map = Map.createMap(JSONReader.getMap(jsonString));
        this.cards = JSONReader.getCards(jsonString);
        this.players = new ArrayList<>();
        this.players.add(new Player(0));
    }

    public synchronized void addPlayer() {
        this.players.add(new Player(players.size()));
    }
    public synchronized void removePlayer(Player player) { this.players.remove(player); }

}
