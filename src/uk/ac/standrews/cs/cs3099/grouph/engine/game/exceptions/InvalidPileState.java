package uk.ac.standrews.cs.cs3099.grouph.engine.game.exceptions;

/**
 * Created by gs63 on 23/02/15.
 */
public class InvalidPileState extends RuntimeException {

    public InvalidPileState() {
        super("Action failed due to invalid pile state!");
    }
}
