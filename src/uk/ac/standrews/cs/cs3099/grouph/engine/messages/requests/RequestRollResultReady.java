package uk.ac.standrews.cs.cs3099.grouph.engine.messages.requests;

import uk.ac.standrews.cs.cs3099.grouph.shared.AbstractEngineMessage;
/**
 * Created by gs63 on 20/04/15.
 */
public class RequestRollResultReady extends AbstractEngineMessage {
    public RequestRollResultReady(int index) {
        super(index);
    }
}
