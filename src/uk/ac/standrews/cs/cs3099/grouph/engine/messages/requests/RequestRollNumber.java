package uk.ac.standrews.cs.cs3099.grouph.engine.messages.requests;

import uk.ac.standrews.cs.cs3099.grouph.shared.AbstractEngineMessage;

/**
 * Created by gs63 on 20/04/15.
 */
public class RequestRollNumber extends AbstractEngineMessage {
    private final String numberHex;

    public RequestRollNumber(int index, String numberHex) {
        super(index);

        this.numberHex = numberHex;
    }

    public String getNumberHex() {
        return numberHex;
    }
}
