package uk.ac.standrews.cs.cs3099.grouph.engine.events.ui;

import uk.ac.standrews.cs.cs3099.grouph.engine.events.EngineEventManager;
import uk.ac.standrews.cs.cs3099.grouph.engine.events.Event;
import uk.ac.standrews.cs.cs3099.grouph.engine.game.managers.GameController;
import uk.ac.standrews.cs.cs3099.grouph.engine.game.managers.exceptions.GameRuleException;
import uk.ac.standrews.cs.cs3099.grouph.engine.messages.requests.RequestMove;
import uk.ac.standrews.cs.cs3099.grouph.engine.messages.requests.RequestTurnOver;
import uk.ac.standrews.cs.cs3099.grouph.shared.struct.players.InvalidPlayerException;
import uk.ac.standrews.cs.cs3099.grouph.shared.struct.players.Players;

/**
 * Event for handling YieldMessage(s) from the UI
 */
public class EventUIYield extends Event {

    public EventUIYield() {
        super(Priority.MEDIUM);
    }

    @Override
    public void run() {
        boolean result = false;

        GameController gc;
        if ((gc = this.gameManager.getGameController("BASIC")) != null) {
            try {
                gc.getModifier().yield();
                gc.getModifier().turnover(Players.me().getId());
                gc.getModifier().setupPlayerTurn(gc.getModifier().getCurrentPlayer().getId());

                this.message(new RequestMove(this.messageInterface.getMessageCounter(), Players.me(),
                        0, -1, -1), EngineEventManager.ComponentReference.NET);

                this.message(new RequestTurnOver(this.messageInterface.getMessageCounter(), Players.me(),
                        Players.getPlayerById(gc.getModifier().getCurrentPlayer().getId()), RequestTurnOver.Phase.DEPLOY),
                        EngineEventManager.ComponentReference.UI);

                result = true;
            } catch (GameRuleException e) {
                this.log.error("Game Rule Exception: {}", e.getRule());
            } catch (InvalidPlayerException e) {
                this.log.error("Invalid Player Exception: {}", e.getMessage());
            }
        }

        this.respond(result, EngineEventManager.ComponentReference.UI);

    }
}
