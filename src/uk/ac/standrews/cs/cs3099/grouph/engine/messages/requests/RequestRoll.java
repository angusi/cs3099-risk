package uk.ac.standrews.cs.cs3099.grouph.engine.messages.requests;

import uk.ac.standrews.cs.cs3099.grouph.shared.AbstractEngineMessage;
import uk.ac.standrews.cs.cs3099.grouph.shared.struct.players.Player;

/**
 * Signifies a request for a roll to be made and the number of dice to roll.
 *
 * Destinations:
 * - UI: Signifies that the stated player should make a roll.
 * - Net: Signifies that a roll should be made for the current player.
 */
public class RequestRoll extends AbstractEngineMessage {

    private final Player player;
    private final int numberOfDice;
    private final int numberOfFaces;

    public RequestRoll(int index, Player playerId, int numberOfDice, int numberOfFaces) {
        super(index);

        this.player = playerId;
        this.numberOfDice = numberOfDice;
        this.numberOfFaces = numberOfFaces;
    }

    /**
     *
     * @return the Id of the player making the roll.
     */
    public Player getPlayer() {
        return player;
    }

    /**
     *
     * @return the number of dice to roll.
     */
    public int getNumberOfDice() {
        return numberOfDice;
    }

    /**
     *
     * @return the number of faces on all of the dice to roll.
     */
    public int getNumberOfFaces() { return numberOfFaces; }
}
