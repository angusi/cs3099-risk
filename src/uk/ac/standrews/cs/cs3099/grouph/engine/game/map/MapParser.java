package uk.ac.standrews.cs.cs3099.grouph.engine.game.map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;

import java.io.IOException;
import java.io.File;

import com.fasterxml.jackson.core.*;

public class MapParser {
    
    protected Logger logger = LoggerFactory.getLogger(MapParser.class);

    /**
     *  Manages the parsing and temporary storage of countries read from the map JSON.
     */
    public class CountryStore {
        public class CountryData {
            private int id;
            private String name;

            public CountryData(int id, String name) {
                this.id = id;
                this.name = name;
            }

            public int getId() { return this.id; }
            public String getName() { return this.name; }
        }

        private List<CountryData> countryList;

        public CountryStore() {
            this.countryList = new ArrayList<>();
        }

        public void parse(JsonParser parser) throws IOException {
            while (parser.nextToken() != JsonToken.END_OBJECT) {
                int id;
                String name;
                
                id = Integer.parseInt(parser.getCurrentName());
                parser.nextToken();
                name = parser.getText();

                logger.debug("Parsed country (id = {}, name = {})", id, name);

                this.countryList.add(new CountryData(id, name));
            }
        }

        public List<CountryData> getCountryList() { return countryList; }
    }

    /**
     *  Manages the parsing and temporary storage of continents read from the map JSON.
     */
    public class ContinentStore {
        public class ContinentData {
            private int id;
            private int value;
            private String name;
            private List<Integer> countryIdList;
            
            public void setId(int id) { this.id = id; }
            public void setValue(int value) { this.value = value; }
            public void setName(String name) { this.name = name; }
            public void setCountryList(List<Integer> countryIdList) { this.countryIdList = countryIdList;}

            public int getId() { return this.id; }
            public int getValue() { return this.value; }
            public String getName() { return this.name; }
            public List<Integer> getCountryIdList() { return this.countryIdList; }
        }

        private Map<Integer, ContinentData> continentMap;
        
        public ContinentStore() {
            this.continentMap = new HashMap<>();
        }

        public void parseContinentValues(JsonParser parser) throws IOException {
            while (parser.nextToken() != JsonToken.END_OBJECT) {
                ContinentData data;
                int id, value;

                id = Integer.parseInt(parser.getCurrentName());

                if ((data = this.continentMap.get(id)) == null)
                    data = new ContinentData();

                data.setId(id);
                parser.nextToken();

                value = parser.getIntValue();
                data.setValue(value);

                if (this.continentMap.get(id) == null)
                    this.continentMap.put(id, data);

                logger.debug("Parsed continent value (id = {}, value = {})", id, value);
            }
        }

        public void parseContinentMembers(JsonParser parser) throws IOException {
            while (parser.nextToken() != JsonToken.END_OBJECT) {
                ContinentData data;
                List<Integer> countryIdList;
                int id;

                id = Integer.parseInt(parser.getCurrentName());

                if ((data = this.continentMap.get(id)) == null)
                    data = new ContinentData();

                data.setId(id);
                parser.nextToken();

                countryIdList = new ArrayList<>();
                while (parser.nextToken() != JsonToken.END_ARRAY) {
                    countryIdList.add(Integer.parseInt(parser.getText()));   
                }

                data.setCountryList(countryIdList);

                // Only insert a new record if one doesn't already exist
                if (this.continentMap.get(id) == null)
                    this.continentMap.put(id, data);

                logger.debug("Parsed continent values (id = {}, values = {})", id, countryIdList);
            }
        }

        public void parseContinentNames(JsonParser parser) throws IOException {
            while (parser.nextToken() != JsonToken.END_OBJECT) {        
                ContinentData data;
                String name;
                int id;

                id = Integer.parseInt(parser.getCurrentName());

                if ((data = this.continentMap.get(id)) == null)
                    data = new ContinentData();

                data.setId(id);
                parser.nextToken();

                name = parser.getText();
                data.setName(name);

                if (this.continentMap.get(id) == null)
                    this.continentMap.put(id, data);

                logger.debug("Parsed continent name (id = {}, name = {})", id, name);
            }
        }


        public Map<Integer, ContinentData> getContinentMap() { return this.continentMap; }
    }

    /**
     *  Manages the parsing and temporary storage of connections read from the map JSON.
     */
    public class ConnectionStore {
        public class Connection {
            private int leftId;
            private int rightId;
            
            public Connection(int leftId, int rightId) {
                this.leftId = leftId;
                this.rightId = rightId;
            }

            public int getLeftId() { return this.leftId; }
            public int getRightId() { return this.rightId; }
        }

        private List<Connection> connectionList;

        public ConnectionStore() {
            this.connectionList = new ArrayList<>();
        }

        public void parse(JsonParser parser) throws IOException {
            while (parser.nextToken() != JsonToken.END_ARRAY) {
                int leftId, rightId;

                parser.nextToken();
                leftId = parser.getIntValue();
                parser.nextToken();
                rightId = parser.getIntValue();
                parser.nextToken();

                this.connectionList.add(new Connection(leftId, rightId));

                logger.debug("Parsed connection (left = {}, right = {})", leftId, rightId);
            }
        }

        public List<Connection> getConnectionList() { return this.connectionList; }
    }

    public class CardStore {
        public class CardData {
            private int type;
            private int countryId; // -1 if wildcard

            public CardData(int countryId, int type) {
                this.countryId = countryId;
                this.type = type;
            }

            public int getType() { return this.type; }
            public int getCountryId() { return this.countryId; }
        }

        private List<CardData> cardList;

        public CardStore() {
            this.cardList = new ArrayList<>();
        }

        public void parse(JsonParser parser) throws IOException {
            while (parser.nextToken() != JsonToken.END_OBJECT) {
                String fieldName = parser.getCurrentName();

                int countryId, value;

                countryId = Integer.parseInt(parser.getCurrentName());
                parser.nextToken();
                value = parser.getIntValue();

                logger.debug("Parsed card (id = {}, value = {})", countryId, value);
                
                this.cardList.add(new CardData(countryId, value));
            }
        }

        public void parseWild(JsonParser parser) throws IOException {
            int numberOfWilds;

            numberOfWilds = parser.getIntValue();

            for (int i = 0; i < numberOfWilds; i++) {
                this.cardList.add(new CardData(-1, 0)); 
            }

            logger.debug("Parsed wildcards (count = {})", numberOfWilds);
        }

        public List<CardData> getCardList() { return this.cardList; }
    }

    private CountryStore countryStore;
    private ContinentStore continentStore;
    private ConnectionStore connectionStore;
    private CardStore cardStore;

    private JsonParser parser;

    private MapParser(JsonParser parser) {
        this.countryStore = new CountryStore();
        this.continentStore = new ContinentStore();
        this.connectionStore = new ConnectionStore();
        this.cardStore = new CardStore();

        this.parser = parser;
    }

    public static MapParser createEmptyParser() {
        return new MapParser(null);
    }

    public static MapParser createFileParser(File file) {
        try {
            return new MapParser(new JsonFactory().createJsonParser(file));
        } catch (IOException io) {
            return null;
        }
    }

    public static MapParser createStringParser(String str) {
        try {
            return new MapParser(new JsonFactory().createJsonParser(str));
        } catch (IOException io) {
            return null;
        }
    }

    public boolean parse() {
        boolean res = false;

        try {
            parser.nextToken(); // Goto START_OBJECT
            while (parser.nextToken() != JsonToken.END_OBJECT) {
                String fieldName = parser.getCurrentName();
                parser.nextToken();

                switch (fieldName) {
                case "data":
                    // Ignore, this field is irrelevant since we don't get any other kind of data
                    break;
                case "continents":
                    this.continentStore.parseContinentMembers(parser);
                    break;
                case "connections":
                    this.connectionStore.parse(parser);
                    break;
                case "continent_values":
                    this.continentStore.parseContinentValues(parser);
                    break;
                case "country_names":
                    this.countryStore.parse(parser);
                    break;
                case "continent_names":
                    this.continentStore.parseContinentNames(parser);
                    break;
                case "country_card":
                    this.cardStore.parse(parser);
                    break;
                case "wildcards":
                    this.cardStore.parseWild(parser);
                    break;
                default:
                    throw new IllegalStateException("Unknown field name: " + fieldName);
                }
            }

            res = true;
        } catch (IOException io) {
            this.logger.error("IO Exception: {}", io.getMessage());
        } catch (IllegalStateException is) {
            this.logger.error("State Exception: {}", is.getMessage());
        }

        return res;
    }

    public CountryStore getCountryStore() { return this.countryStore; }
    public ContinentStore getContinentStore() { return this.continentStore; }
    public ConnectionStore getConnectionStore() { return this.connectionStore; }
    public CardStore getCardStore() { return this.cardStore; }

}
