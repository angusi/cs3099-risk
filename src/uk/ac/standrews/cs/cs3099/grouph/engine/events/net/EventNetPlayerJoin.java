package uk.ac.standrews.cs.cs3099.grouph.engine.events.net;

import uk.ac.standrews.cs.cs3099.grouph.engine.events.EngineEventManager;
import uk.ac.standrews.cs.cs3099.grouph.engine.events.Event;
import uk.ac.standrews.cs.cs3099.grouph.engine.game.managers.GameManager;
import uk.ac.standrews.cs.cs3099.grouph.engine.game.managers.exceptions.PlayerLobbyException;
import uk.ac.standrews.cs.cs3099.grouph.engine.messages.requests.RequestLobbyUpdate;
import uk.ac.standrews.cs.cs3099.grouph.net.messages.PlayerJoinedM;

import uk.ac.standrews.cs.cs3099.grouph.shared.struct.players.InvalidPlayerException;
import uk.ac.standrews.cs.cs3099.grouph.shared.struct.players.Player;
import uk.ac.standrews.cs.cs3099.grouph.shared.struct.players.Players;

import java.util.List;
import java.util.Set;
import java.util.HashSet;

/**
 * Created by gs63 on 07/04/15.
 */
public class EventNetPlayerJoin extends Event {

    public EventNetPlayerJoin() {
        super(Event.Priority.HIGH);
    }

    @Override
    public void run() {
        boolean result = false;

        try {
            GameManager.Lobby lobby;
            List<PlayerJoinedM.PlayerJoinedData> playerList;
            Set<Player> playerSet = new HashSet<>();
            
            if ((lobby = this.gameManager.getLobby("BASIC")) != null) {
                playerList = ((PlayerJoinedM) this.trigger).getPlayerData();
                
                for (PlayerJoinedM.PlayerJoinedData p : playerList) {
                    this.gameManager.getLobby("BASIC").addPlayer(p.id);
                    playerSet.add(Players.getPlayerById(p.id));
                    this.log.info("Added player with id = {}", p.id);
                }

                this.message(new RequestLobbyUpdate(this.messageInterface.getMessageCounter(),
                        playerSet, null, null, Players.me()), EngineEventManager.ComponentReference.UI);

                result = true;
            }
        } catch (InvalidPlayerException e) {
            this.log.error("Could not find current player id! (Are we a playing host?)");
        }

        this.respond(result, EngineEventManager.ComponentReference.NET);
    }
}
