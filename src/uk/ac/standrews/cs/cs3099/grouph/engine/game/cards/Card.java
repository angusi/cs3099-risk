package uk.ac.standrews.cs.cs3099.grouph.engine.game.cards;

import uk.ac.standrews.cs.cs3099.grouph.engine.interfaces.game.cards.ICard;

/**
 * A standard RISK card.
 */
public class Card implements ICard {

    protected ICard.Type type;
    protected String countryId;

    public Card(ICard.Type type, String countryId) {
        this.type = type;
        this.countryId = countryId;
    }

    public ICard.Type getType() {
        return this.type;
    }

    public String getCountryId() {
        return this.countryId;
    }

    @Override
    public boolean equals(Object o) {
        return (o instanceof Card &&
                ((Card) o).type == this.type &&
                ((Card) o).countryId.equals(this.countryId));
    }
}
