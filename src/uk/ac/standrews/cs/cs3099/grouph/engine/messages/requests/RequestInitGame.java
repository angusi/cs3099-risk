package uk.ac.standrews.cs.cs3099.grouph.engine.messages.requests;

import uk.ac.standrews.cs.cs3099.grouph.shared.AbstractEngineMessage;

import java.util.Queue;

/**
 * Requests the launch of a new game.
 *
 * Destinations:
 * - UI: Signifies the launch of a new game.
 * - Net: Signifies the launch of a new game where we are the host.
 */
public class RequestInitGame extends AbstractEngineMessage {

    private int playerCount;
    Queue<Integer> playerOrderById;

    public RequestInitGame(int index, int playerCount, Queue<Integer> playerOrderById) {
        super(index);

        this.playerCount = playerCount;
        this.playerOrderById = playerOrderById;
    }

    /**
     *
     * @return the number of players in this game.
     */
    public int getPlayerCount() {
        return playerCount;
    }

    /**
     *
     * @return the order of play, by ID.
     */
    public Queue<Integer> getPlayerOrderById() {
        return playerOrderById;
    }
}
