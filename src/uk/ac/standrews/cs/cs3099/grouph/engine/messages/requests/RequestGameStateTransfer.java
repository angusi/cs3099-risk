package uk.ac.standrews.cs.cs3099.grouph.engine.messages.requests;

import uk.ac.standrews.cs.cs3099.grouph.engine.game.managers.GameManager;
import uk.ac.standrews.cs.cs3099.grouph.shared.AbstractEngineMessage;

/**
 * Created by 120011452 on 19/04/15.
 */
public class RequestGameStateTransfer extends AbstractEngineMessage {

    private GameManager.Game gameState;

    public RequestGameStateTransfer(int index, GameManager.Game gameState) {
        super(index);

        this.gameState = gameState;
    }

    public GameManager.Game getGameState() {
        return this.gameState;
    }
}
