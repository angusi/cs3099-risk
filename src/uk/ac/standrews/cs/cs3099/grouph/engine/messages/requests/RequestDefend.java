package uk.ac.standrews.cs.cs3099.grouph.engine.messages.requests;

import uk.ac.standrews.cs.cs3099.grouph.shared.AbstractEngineMessage;
import uk.ac.standrews.cs.cs3099.grouph.shared.struct.players.Player;

/**
 * Signifies a defence against an attack, therefore this request will always proceed after a RequestAttack.
 *
 * Destinations:
 * - UI: Signifies that another player has requested a defence against an attack.
 * - Net: Signifies that the current player has requested a defence against an attack.
 *
 * @see uk.ac.standrews.cs.cs3099.grouph.engine.messages.requests.RequestAttack
 */
public class RequestDefend extends AbstractEngineMessage {

    private final Player defendingPlayer;
    private final int defendingArmyCount;

    public RequestDefend(int index, Player defendingPlayer, int defendingArmyCount) {
        super(index);

        this.defendingPlayer = defendingPlayer;
        this.defendingArmyCount = defendingArmyCount;
    }

    /**
     *
     * @return the Id of the defending player.
     */
    public Player getDefendingPlayer() {
        return defendingPlayer;
    }

    /**
     *
     * @return the number of armies (1-2) the defending player is defending with.
     */
    public int getDefendingArmyCount() {
        return defendingArmyCount;
    }
}
