package uk.ac.standrews.cs.cs3099.grouph.engine.interfaces.game.cards;

import uk.ac.standrews.cs.cs3099.grouph.engine.game.exceptions.InvalidPileState;
import uk.ac.standrews.cs.cs3099.grouph.engine.game.exceptions.UnsupportedPileAction;

import java.util.Random;

/**
 * The standard interface for piles of cards.
 *
 * This represents an ordered queue of cards that can be drawn from or that a card can be placed on top of.
 */
public interface IPile {

    /**
     * Draw a card from the pile.
     * @return Null if the pile is empty, otherwise the card that was drawn.
     * @throws UnsupportedPileAction if pile does not support this action.
     */
    public ICard drawCard() throws UnsupportedPileAction, InvalidPileState;

    /**
     * Place a card on top of a pile.
     * @param card the card to be placed on top of the pile.
     */
    public void placeCard(ICard card) throws UnsupportedPileAction, InvalidPileState;

    /**
     * Get the number of cards left in the pile.
     * @return the number of cards left.
     */
    public int getNumberOfCards();

    /**
     * Shuffle the cards in a pile.
     * @param rnd the Random number generator to use.
     */
    public void shuffle(Random rnd);

}
