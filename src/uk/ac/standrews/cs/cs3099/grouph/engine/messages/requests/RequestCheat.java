package uk.ac.standrews.cs.cs3099.grouph.engine.messages.requests;

import uk.ac.standrews.cs.cs3099.grouph.shared.AbstractEngineMessage;
import uk.ac.standrews.cs.cs3099.grouph.shared.struct.players.Player;

/**
 * Signifies that a cheating player has been detected.
 *
 * Destinations:
 * - UI: The stated player has been detected as a cheat.
 * - Net: The stated player has been detected as a cheat.
 */
public class RequestCheat extends AbstractEngineMessage {

    private final Player player;

    public RequestCheat(int index, Player player) {
        super(index);

        this.player = player;
    }

    /**
     *
     * @return the player that was detected cheating.
     */
    public Player getPlayer() {
        return player;
    }
}
