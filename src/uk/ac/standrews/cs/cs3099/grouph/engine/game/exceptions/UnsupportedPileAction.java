package uk.ac.standrews.cs.cs3099.grouph.engine.game.exceptions;

/**
 * Created by gs63 on 23/02/15.
 */
public class UnsupportedPileAction extends RuntimeException {

    public UnsupportedPileAction() {
        super("Pile does not support this action!");
    }
}
