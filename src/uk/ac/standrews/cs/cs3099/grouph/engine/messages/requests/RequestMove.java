package uk.ac.standrews.cs.cs3099.grouph.engine.messages.requests;

import uk.ac.standrews.cs.cs3099.grouph.shared.AbstractEngineMessage;
import uk.ac.standrews.cs.cs3099.grouph.shared.struct.players.Player;

/**
 * Signifies a movement from one country to another country without combat occurring.
 *
 * This is termed "fortification" in the rules and can only be carried out by a player once at the end of their turn.
 *
 * Destinations:
 * - UI: Signifies that another player has made a move.
 * - Net: Signifies that the current player has made a move.
 */
public class RequestMove extends AbstractEngineMessage {

    private final Player player;
    private final int armyCount;
    private final int startCountryId;
    private final int endCountryId;

    public RequestMove(int index, Player player, int armyCount,
                       int startCountryId, int endCountryId) {
        super(index);

        this.player = player;
        this.armyCount = armyCount;
        this.startCountryId = startCountryId;
        this.endCountryId = endCountryId;
    }

    /**
     *
     * @return the player Id that is requesting the move.
     */
    public Player getPlayer() {
        return player;
    }

    /**
     *
     * @return the number of armies that the player is moving.
     */
    public int getArmyCount() {
        return armyCount;
    }

    /**
     *
     * @return the Id of the country where the move is originating.
     */
    public int getStartCountryId() {
        return startCountryId;
    }

    /**
     *
     * @return the Id of the country where the move is ending.
     */
    public int getEndCountryId() {
        return endCountryId;
    }

}
