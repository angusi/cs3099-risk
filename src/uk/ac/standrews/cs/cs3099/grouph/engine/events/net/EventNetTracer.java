package uk.ac.standrews.cs.cs3099.grouph.engine.events.net;

import uk.ac.standrews.cs.cs3099.grouph.engine.events.Event;
import uk.ac.standrews.cs.cs3099.grouph.engine.events.EngineEventManager;
import uk.ac.standrews.cs.cs3099.grouph.ui.messages.PingMessage;

/**
 * A basic tracer event that reacts to tracer messages sent from the network. Tracer events are 
 * logged in the engine internally then simply rerouted to the UI layer. 
 * 
 * - Event Trigger
 * 		TracerM from network component.
 * - Event Action
 * 		Trigger internal event EngineTracer then reroute TracerM to network as TracerMessage to UI layer.
 * 
 * TODO: Implement triggering of EventInternalTracer
 */
public class EventNetTracer extends Event {
    
    public EventNetTracer() {
        super(Event.Priority.LOWEST);
    }

    @Override
    public void run() {
        try {
            this.messageInterface.push(
                    new PingMessage(this.messageInterface.getMessageCounter()),
                    EngineEventManager.ComponentReference.UI);
        } catch (InterruptedException e) {
            this.log.error("Interrupted whilst trying to send tracer.");
        } 
    }

}

