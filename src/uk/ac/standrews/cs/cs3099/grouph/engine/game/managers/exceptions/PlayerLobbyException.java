package uk.ac.standrews.cs.cs3099.grouph.engine.game.managers.exceptions;

public class PlayerLobbyException extends Exception {

    private int id;

    public PlayerLobbyException(String msg, int id) {
        super(msg);

        this.id = id;
    }

    public int getId() {
        return this.id;
    }
}
