package uk.ac.standrews.cs.cs3099.grouph.engine.game.map.exception;

import java.lang.RuntimeException;

public class MissingCountry extends RuntimeException {}
