package uk.ac.standrews.cs.cs3099.grouph.engine.game.cards;

import uk.ac.standrews.cs.cs3099.grouph.engine.interfaces.game.cards.ICard;
import uk.ac.standrews.cs.cs3099.grouph.engine.interfaces.game.cards.IPile;

import java.util.Collections;
import java.util.List;
import java.util.Queue;
import java.util.Random;
import java.util.concurrent.ArrayBlockingQueue;

/**
 * A standard implementation of a pile of cards.
 */
public abstract class Pile implements IPile {
    protected Queue<ICard> cards;

    public Pile(int pileSize) {
        this.cards = new ArrayBlockingQueue<>(pileSize, false);
    }

    /**
     * Appends the contents of the given pile into this pile.
     * @param pile the pile whose contents are to be appended into this pile.
     */
    public synchronized void append(Pile pile) {
        this.cards.addAll(pile.cards);
    }

    @Override
    public synchronized int getNumberOfCards() {
        return this.cards.size();
    }

    @Override
    public void shuffle(Random rnd) {
        Collections.shuffle((List<?>) this.cards, rnd);
    }
}
