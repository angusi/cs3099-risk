package uk.ac.standrews.cs.cs3099.grouph.engine.game.cards;

import uk.ac.standrews.cs.cs3099.grouph.engine.interfaces.game.cards.ICard;
import uk.ac.standrews.cs.cs3099.grouph.engine.game.exceptions.InvalidPileState;
import uk.ac.standrews.cs.cs3099.grouph.engine.game.exceptions.UnsupportedPileAction;

import java.util.Collection;

/**
 * A standard draw pile that contains cards that can be drawn from a deck.
 *
 * @implNote Standard draw piles cannot have cards placed on them.
 *
 * @see uk.ac.standrews.cs.cs3099.grouph.engine.interfaces.game.cards.IDeck
 */
public class DrawPile extends Pile {

    public DrawPile(int pileSize, Collection<ICard> initialCards) {
        super(pileSize);

        this.cards.addAll(initialCards);
    }

    @Override
    public ICard drawCard() throws UnsupportedPileAction, InvalidPileState {
        ICard card = this.cards.poll();

        if (card == null) {
            throw new InvalidPileState();
        }

        return card;
    }

    @Override
    public void placeCard(ICard card) throws UnsupportedPileAction, InvalidPileState {
        throw new UnsupportedPileAction();
    }
}
