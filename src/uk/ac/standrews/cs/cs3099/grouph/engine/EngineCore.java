package uk.ac.standrews.cs.cs3099.grouph.engine;

import java.util.AbstractQueue;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.standrews.cs.cs3099.grouph.engine.events.EngineEventManager;
import uk.ac.standrews.cs.cs3099.grouph.engine.events.net.EventNetTracer;

import uk.ac.standrews.cs.cs3099.grouph.engine.events.ui.*;
import uk.ac.standrews.cs.cs3099.grouph.engine.events.net.*;

import uk.ac.standrews.cs.cs3099.grouph.net.messages.*;
import uk.ac.standrews.cs.cs3099.grouph.ui.messages.*;

import uk.ac.standrews.cs.cs3099.grouph.shared.IComponent;
import uk.ac.standrews.cs.cs3099.grouph.shared.IEngineComponent;
import uk.ac.standrews.cs.cs3099.grouph.shared.AbstractMessage;
import uk.ac.standrews.cs.cs3099.grouph.shared.INetComponent;
import uk.ac.standrews.cs.cs3099.grouph.shared.IUIComponent;

public class EngineCore implements IEngineComponent {
    public static boolean isHost = false;

	public static class ParamIdentifiers {
		static String COMP_QUEUE_CAPACITY 	= "component_queue_capacity";
		static String COMP_QUEUE_FAIRNESS 	= "component_queue_fairness";
		static String ENGI_QUEUE_CAPACITY 	= "engine_queue_capacity";
		static String ENGI_QUEUE_FAIRNESS 	= "engine_queue_fairness";
		static String ENGI_THREAD_CORE		= "engine_thread_core";
		static String ENGI_THREAD_MAX 		= "engine_thread_max";
		static String ENGI_THREAD_ALIVETIME = "engine_thread_alivetime";
	}
	
	protected Logger log;
	
	private Map<String, Object> paramMap;
	
	protected EngineComponentConnector uiConnector;
	protected EngineComponentConnector netConnector;
	
	private EngineEventManager eventManager;
	
	public EngineCore(String[] params) {
		this.log = LoggerFactory.getLogger(EngineCore.class);
		
		this.initComponentConnectors();
		this.initEventManager();
	}
	
	protected void initComponentConnectors() {
		//int capacity = (Integer) this.paramMap.get(EngineCore.ParamIdentifiers.COMP_QUEUE_CAPACITY);
		//boolean fairness = (Boolean) this.paramMap.get(EngineCore.ParamIdentifiers.COMP_QUEUE_FAIRNESS);
		
		int capacity = 100;
		boolean fairness = false;
		
		this.uiConnector = new EngineComponentConnector(capacity, fairness);
		this.log.info("Initialised UI Connector with capacity {} and fairness set to {}", capacity, fairness);
		this.netConnector = new EngineComponentConnector(capacity, fairness);
		this.log.info("Initialised Network Connector with capacity {} and fairness set to {}", capacity, fairness);
	}
	
	protected void initEventManager() {
        //int msgCapacity = (Integer) this.paramMap.get(EngineCore.ParamIdentifiers.ENGI_QUEUE_CAPACITY);
        //boolean msgFair = (Boolean) this.paramMap.get(EngineCore.ParamIdentifiers.ENGI_QUEUE_FAIRNESS);
        //int coreThreads = (Integer) this.paramMap.get(EngineCore.ParamIdentifiers.ENGI_THREAD_CORE);
        //int maxThreads	= (Integer) this.paramMap.get(EngineCore.ParamIdentifiers.ENGI_THREAD_MAX);
        //long keepAlive	= (Long) 	this.paramMap.get(EngineCore.ParamIdentifiers.ENGI_THREAD_ALIVETIME);

        int msgCapacity = 100;
        boolean msgFair = false;
        int coreThreads = 10;
        int maxThreads = 15;
        long keepAlive = 10000;

        this.eventManager = new EngineEventManager(msgCapacity, msgFair,
                coreThreads, maxThreads, keepAlive, this.uiConnector, this.netConnector);
    }

    protected void registerMessageEvents() {
        this.eventManager.registerEvent(PingMessage.class.getSimpleName(), new EventUITracer());
        this.eventManager.registerEvent(TracerM.class.getSimpleName(), new EventNetTracer());
        
        // Register UI Events
        this.eventManager.registerEvent(CreateGameMessage.class.getSimpleName(), new EventUICreateGame());
        this.eventManager.registerEvent(ClaimTerritoryMessage.class.getSimpleName(), new EventUIClaim());
        this.eventManager.registerEvent(GameStartMessage.class.getSimpleName(), new EventUIGameStart());
        this.eventManager.registerEvent(DeployMessage.class.getSimpleName(), new EventUIDeploy());
        this.eventManager.registerEvent(AttackMessage.class.getSimpleName(), new EventUIAttack());
        this.eventManager.registerEvent(DefendDiceMessage.class.getSimpleName(), new EventUIDefend());
        this.eventManager.registerEvent(FortifyMessage.class.getSimpleName(), new EventUIFortify());
        this.eventManager.registerEvent(JoinGameMessage.class.getSimpleName(), new EventUIJoinGame());
        this.eventManager.registerEvent(YieldMessage.class.getSimpleName(), new EventUIYield());

        // Register Net Events
        this.eventManager.registerEvent(JoinGameRequestM.class.getSimpleName(), new EventNetJoinGameRequest());
        this.eventManager.registerEvent(JoinGameResultM.class.getSimpleName(), new EventNetJoinGameResult());
        this.eventManager.registerEvent(SetupM.class.getSimpleName(), new EventNetClaim());
        this.eventManager.registerEvent(DeployArmiesM.class.getSimpleName(), new EventNetDeploy());
        this.eventManager.registerEvent(GameStartM.class.getSimpleName(), new EventNetGameStart());
        this.eventManager.registerEvent(PlayerCountM.class.getSimpleName(), new EventNetPlayerCount());
        this.eventManager.registerEvent(PlayerJoinedM.class.getSimpleName(), new EventNetPlayerJoin());
        this.eventManager.registerEvent(DiceHashM.class.getSimpleName(), new EventNetDiceHash());
        this.eventManager.registerEvent(DiceNumberM.class.getSimpleName(), new EventNetDiceNumber());
        this.eventManager.registerEvent(AttackOccurM.class.getSimpleName(), new EventNetAttack());
        this.eventManager.registerEvent(DefendOccurM.class.getSimpleName(), new EventNetDefend());
        this.eventManager.registerEvent(MoveM.class.getSimpleName(), new EventNetFortify());
    }
	
	@Override
	public AbstractQueue<AbstractMessage> getSink(IComponent component) {
		if (component instanceof INetComponent) {
			return this.netConnector.getOutboundQueue();
		} else if (component instanceof IUIComponent) {
			return this.uiConnector.getOutboundQueue();
		} else {
			return null;
		}
	}

	@Override
	public void setSource(AbstractQueue<AbstractMessage> queue, IComponent component) {
		if (component instanceof INetComponent) {
			this.netConnector.setInboundQueue(queue);
		} else if (component instanceof IUIComponent) {
			this.uiConnector.setInboundQueue(queue);
		}
	}

	@Override
	public void run() {
        this.registerMessageEvents();

		this.uiConnector.startWorkers();
		this.netConnector.startWorkers();
		this.eventManager.start();
	}

}
