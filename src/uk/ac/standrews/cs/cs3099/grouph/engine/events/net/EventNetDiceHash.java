package uk.ac.standrews.cs.cs3099.grouph.engine.events.net;

import uk.ac.standrews.cs.cs3099.grouph.engine.EngineCore;
import uk.ac.standrews.cs.cs3099.grouph.engine.dice.DiceRoller;
import uk.ac.standrews.cs.cs3099.grouph.engine.events.EngineEventManager;
import uk.ac.standrews.cs.cs3099.grouph.engine.events.Event;
import uk.ac.standrews.cs.cs3099.grouph.engine.messages.requests.RequestRollHash;
import uk.ac.standrews.cs.cs3099.grouph.net.messages.DiceHashM;
import uk.ac.standrews.cs.cs3099.grouph.shared.struct.players.InvalidPlayerException;
import uk.ac.standrews.cs.cs3099.grouph.shared.struct.players.Players;

/**
 * Created by gs63 on 20/04/15.
 */
public class EventNetDiceHash extends Event {

    public EventNetDiceHash() {
        super(Priority.HIGHEST);
    }

    @Override
    public void run() {
        boolean result = false;
        DiceHashM diceHashM = (DiceHashM) this.trigger;

        try {
            if (diceHashM.getPlayer().getId() != Players.me().getId()) {
                DiceRoller.getInstance().registerHash(diceHashM.getPlayer(), diceHashM.getHash());
                result = true;
            }
        } catch (InvalidPlayerException e) {
            this.log.error("Invalid Player Exception: {}", e.getMessage());
        }

        this.respond(result, EngineEventManager.ComponentReference.NET);
    }
}
