package uk.ac.standrews.cs.cs3099.grouph.engine.events.net;

import uk.ac.standrews.cs.cs3099.grouph.engine.events.EngineEventManager;
import uk.ac.standrews.cs.cs3099.grouph.engine.events.Event;
import uk.ac.standrews.cs.cs3099.grouph.engine.game.managers.GameController;
import uk.ac.standrews.cs.cs3099.grouph.engine.game.managers.exceptions.GameRuleException;
import uk.ac.standrews.cs.cs3099.grouph.engine.messages.requests.RequestReinforceCount;
import uk.ac.standrews.cs.cs3099.grouph.engine.messages.requests.RequestTerritoryChange;
import uk.ac.standrews.cs.cs3099.grouph.engine.messages.requests.RequestTurnOver;
import uk.ac.standrews.cs.cs3099.grouph.net.messages.MoveM;
import uk.ac.standrews.cs.cs3099.grouph.shared.struct.players.InvalidPlayerException;
import uk.ac.standrews.cs.cs3099.grouph.shared.struct.players.Players;

/**
 * Created by 120011452 on 21/04/15.
 */
public class EventNetFortify extends Event {

    public EventNetFortify() {
        super(Priority.MEDIUM);
    }

    @Override
    public void run() {
        boolean result = false;

        GameController gc;
        if ((gc = this.gameManager.getGameController("BASIC")) != null) {
            MoveM moveM = (MoveM) this.trigger;

            try {
                if (gc.getModifier().isAttack()) {
                    if (moveM.getNumberArmies() != 0) {
                        gc.getModifier().fortify(moveM.getPlayer().getId(), moveM.getFrom(), moveM.getTo(), moveM.getNumberArmies());

                        this.message(new RequestTerritoryChange(this.messageInterface.getMessageCounter(), moveM.getFrom(),
                                        moveM.getPlayer(), (int) gc.getModifier().getGameMap().getCountryArmies(moveM.getFrom())),
                                EngineEventManager.ComponentReference.UI);

                        this.message(new RequestTerritoryChange(this.messageInterface.getMessageCounter(), moveM.getTo(),
                                        moveM.getPlayer(), (int) gc.getModifier().getGameMap().getCountryArmies(moveM.getTo())),
                                EngineEventManager.ComponentReference.UI);
                    } else {
                        gc.getModifier().yield();
                        gc.getModifier().setupPlayerTurn(gc.getModifier().getCurrentPlayer().getId());
                    }

                    gc.getModifier().turnover(moveM.getPlayer().getId());

                    this.message(new RequestTurnOver(this.messageInterface.getMessageCounter(), moveM.getPlayer(),
                                    Players.getPlayerById(gc.getModifier().getCurrentPlayer().getId()), RequestTurnOver.Phase.STANDARD),
                            EngineEventManager.ComponentReference.UI);

                    if (Players.me().getId() == gc.getModifier().getCurrentPlayer().getId()) {
                        gc.getModifier().setupPlayerTurn(Players.me().getId());

                        this.message(new RequestReinforceCount(this.messageInterface.getMessageCounter(), Players.me(),
                                gc.getModifier().getCurrentPlayer().getReinforcements()), EngineEventManager.ComponentReference.UI);
                    }

                    result = true;
                } else {
                    gc.getModifier().move(moveM.getPlayer().getId(), moveM.getNumberArmies());

                    this.message(new RequestTerritoryChange(this.messageInterface.getMessageCounter(), moveM.getTo(),
                                    moveM.getPlayer(), (int) gc.getModifier().getGameMap().getCountryArmies(moveM.getTo())),
                            EngineEventManager.ComponentReference.UI);

                    this.message(new RequestTerritoryChange(this.messageInterface.getMessageCounter(), moveM.getFrom(),
                                    moveM.getPlayer(), (int) gc.getModifier().getGameMap().getCountryArmies(moveM.getFrom())),
                            EngineEventManager.ComponentReference.UI);

                    result = true;
                }
            } catch (GameRuleException e) {
                this.log.error("Game rule exception: {}", e.getRule());
            } catch (InvalidPlayerException e) {
                this.log.error("Invalid player exception: {}", e.getMessage());
            }
        }

        this.respond(result, EngineEventManager.ComponentReference.NET);
    }
}
