package uk.ac.standrews.cs.cs3099.grouph.engine.messages.requests;

import uk.ac.standrews.cs.cs3099.grouph.shared.AbstractEngineMessage;

/**
 * Signifies that the acceptance or rejection of a request to join a game.
 *
 * Destinations:
 * - UI: Signifies either acceptance or rejection of current player to join a game.
 * - Net: Not Applicable
 */
public class RequestJoinResult extends AbstractEngineMessage {
    private boolean accepted;

    public RequestJoinResult(int index, boolean accepted) {
        super(index);

        this.accepted = accepted;
    }

    public boolean getAccepted() {
        return this.accepted;
    }

}
