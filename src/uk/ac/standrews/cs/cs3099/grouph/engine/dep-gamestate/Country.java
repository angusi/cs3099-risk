package uk.ac.standrews.cs.cs3099.grouph.engine.gamestate;

import java.util.ArrayList;

public class Country {
    private String name;
    private String continent;
    private ArrayList<String> connections = new ArrayList<String>();
    private int value;
    private int PlayerID= -1;

    public Country(String name, int value, ArrayList<String> connections,String continent){
        this.name=name;
        this.continent=continent;
        this.connections=connections;
        this.value=value;
    }

    public void printDetails(){
        System.out.println("name :" + name);
        System.out.println("on continent: " + continent);
        System.out.print("connects to: ");
        for(String a:connections){
            System.out.print(a + " ");
        }
        System.out.println("\n");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContinent() {
        return continent;
    }

    public void setContinent(String continent) {
        this.continent = continent;
    }

    public ArrayList<String> getConnections() {
        return connections;
    }

    public void setConnections(ArrayList<String> connections) {
        this.connections = connections;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public int getPlayerID() {
        return PlayerID;
    }

    public void setPlayerID(int playerID) {
        PlayerID = playerID;
    }
}
