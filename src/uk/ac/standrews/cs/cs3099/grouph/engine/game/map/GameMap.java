package uk.ac.standrews.cs.cs3099.grouph.engine.game.map;

import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.Pseudograph;

import org.jgrapht.alg.DijkstraShortestPath;

import org.apache.commons.lang.SerializationUtils;

import uk.ac.standrews.cs.cs3099.grouph.engine.game.map.exception.MissingCountry;
import uk.ac.standrews.cs.cs3099.grouph.engine.game.players.Player;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.List;
import java.util.ArrayList;

/**
 * Represents a standard RISK game map.
 */
public class GameMap {

    private Map<Integer, Player> countryPlayerMap;
    private Map<Integer, Country> countryMap;
    private Map<Integer, Continent> continentMap;

    private Pseudograph<Integer, DefaultEdge> graph;

    public static GameMap createMapFromParser(MapParser parser) {
        Set<Country> countrySet = new HashSet<>(parser.getCountryStore().getCountryList().size());
        Set<Continent> continentSet = new HashSet<>(parser.getContinentStore().getContinentMap().size());
        Map<Integer, List<Integer>> connectionMap = new HashMap<>(parser.getConnectionStore().getConnectionList().size());

        // Temp mapping for use when populating the continent set 
        Map<Integer, Country> countryMap = new HashMap<>(parser.getCountryStore().getCountryList().size());

        // Populate country set 
        for (MapParser.CountryStore.CountryData countryData : parser.getCountryStore().getCountryList()) {
            Country country = new Country(countryData.getId(), countryData.getName(), 0);
            countrySet.add(country);
            countryMap.put(country.getId(), country);
        }

        // Populate continent set 
        for (MapParser.ContinentStore.ContinentData continentData : parser.getContinentStore().getContinentMap().values()) {
            // Populate country references
            Set<Country> continentMemberList = new HashSet<>();
            for (Integer countryId : continentData.getCountryIdList()) {
                continentMemberList.add(countryMap.get(countryId));
            }
            
            continentSet.add(new Continent(continentData.getId(), continentData.getName(), continentMemberList, continentData.getValue()));
        }

        // Populate connections 
        for (MapParser.ConnectionStore.Connection connection : parser.getConnectionStore().getConnectionList()) {
            if (connectionMap.get(connection.getLeftId()) == null) 
                connectionMap.put(connection.getLeftId(), new ArrayList<>());

            connectionMap.get(connection.getLeftId()).add(connection.getRightId());
        }

        return GameMap.createMap(countrySet, continentSet, connectionMap);
    }    

    public static GameMap createMap(Set<Country> countrySet,
                                    Set<Continent> continentSet,
                                    Map<Integer, List<Integer>> countryConnections) {
        Map<Integer, Country> countryMap        = new HashMap<>(countrySet.size());
        Map<Integer, Player> countryPlayerMap   = new HashMap<>(countrySet.size());
        Map<Integer, Continent> continentMap    = new HashMap<>(continentSet.size());

        for (Country c : countrySet) {
            countryMap.put(c.getId(), c);
            countryPlayerMap.put(c.getId(), null);
        }

        for (Continent c : continentSet) {
            continentMap.put(c.getId(), c);
        }

        return new GameMap(countryMap, countryPlayerMap, continentMap, countryConnections);
    }

    private GameMap(Map<Integer, Country> countryMap,
                    Map<Integer, Player> countryPlayerMap,
                    Map<Integer, Continent> continentMap,
                    Map<Integer, List<Integer>> countryConnections) {
        
        this.countryMap = countryMap;
        this.countryPlayerMap = countryPlayerMap;
        this.continentMap = continentMap;

        this.graph = this.createGraph(countryConnections);
    }

    /**
     *  Generate a graph representation of country connections.
     */
    private Pseudograph<Integer, DefaultEdge> createGraph(Map<Integer, List<Integer>> countryConnections) {
        Pseudograph<Integer, DefaultEdge> outputGraph = new Pseudograph(DefaultEdge.class);

        for (Integer countryId : this.countryMap.keySet()) {
            outputGraph.addVertex(countryId);
        }

        for (Map.Entry<Integer, List<Integer>> connectionEntry : countryConnections.entrySet()) {
            for (Integer adjacentCountryId : connectionEntry.getValue()) {
                outputGraph.addEdge(connectionEntry.getKey(), adjacentCountryId);
            }
        }

        return outputGraph;
    }

    /**
     * Check if two countries are connected to each other.
     * @return true if the countries are connected, false otherwise.
     */
    public boolean areConnected(int countryAId, int countryBId) {
        return this.graph.containsEdge(countryAId, countryBId);
    }

    /**
     * Check if a country exists within the game map.
     * @param countryId the Id of the country to check.
     * @return true if the country exists, false otherwise.
     */
    public boolean containsCountry(int countryId) {
        return this.countryMap.containsKey(countryId);
    }

    /**
     * Check if there are any coountries that do not have any owners within the game map.
     *
     * @return true if there is at least one country without an owner, false otherwise.
     */
    public boolean containsEmptyCountry() {
        for (Player p : this.countryPlayerMap.values()) {
            if (p == null)
                return true;
        }

        return false;
    }


    /**
     * Checks if two countries are connected.
     * @param countryIdA
     * @param countryIdB
     * @return true if the countries are connected, false otherwise.
     */
    public boolean isAdjacent(int countryIdA, int countryIdB) {
        return this.graph.containsEdge(countryIdA, countryIdB);
    }

    /**
     * Check if a player has a fortification path (path of owned territories).
     * @return true if such a path exists, false otherwise.
     *
     * @deprecated Use isAdjacent(...) instead as this is an alternative rule.
     */
    public boolean hasFortifyPath(Player player, int startingCountryId, int endingCountryId) {
        Pseudograph localGraph = (Pseudograph) SerializationUtils.clone(this.graph);

        // Remove all nodes not owned by the player
        for (Map.Entry<Integer,Player> entry : this.countryPlayerMap.entrySet()) {
            if (entry.getValue() != player) {
                localGraph.removeVertex(entry.getKey());
            }
        }

        // Attempt to find path
        DijkstraShortestPath<Integer,DefaultEdge> pathfinder =
            new DijkstraShortestPath<>(localGraph, startingCountryId, endingCountryId);

        pathfinder.getPath();
        return true;
    }

    /**
     * Gets the number of armies stationed at the specified country.
     * @param countryId the Id of the country.
     * @return the number of armies stationed in the country.
     * @throws MissingCountry thrown if the country with the given Id could not be found.
     */
    public long getCountryArmies(int countryId) throws MissingCountry {
        if (!this.containsCountry(countryId))
            throw new MissingCountry();

        return this.countryMap.get(countryId).getNumberOfArmies();
    }

    /**
     * Sets the number of armies stationed at the specified country.
     * @param countryId the Id of the country
     * @param numberOfArmies the number of armies to be stationed in the country.
     * @throws MissingCountry thrown if the country with the given Id could not be found.
     */
    public void setCountryArmies(int countryId, long numberOfArmies) throws MissingCountry {
        if (!this.containsCountry(countryId))
            throw new MissingCountry();

        this.countryMap.get(countryId).setNumberOfArmies(numberOfArmies);
    }

    /**
     * Gets the Player owner of the specified country.
     * @param countryId the Id of the country.
     * @return the Player that owns the country.
     * @throws MissingCountry thrown if the country with the given Id could not be found.
     */
    public Player getCountryOwner(int countryId) throws MissingCountry {
        if (!this.containsCountry(countryId))
            throw new MissingCountry();

        return this.countryPlayerMap.get(countryId);
    }

    /**
     * Set the Player owner of the specified country.
     * @param countryId the Id of the country.
     * @param player the Player to own that country.
     * @throws MissingCountry thrown if the country with the given Id could not be found.
     */
    public void setCountryOwner(int countryId, Player player) throws MissingCountry {
        if (!this.containsCountry(countryId))
            throw new MissingCountry();

        this.countryPlayerMap.put(countryId, player);
    }

    public int calculateContinentBonus(Player player) {
        return this.continentMap.entrySet()
                .stream()
                .filter(entry -> this.isContinentOwner(entry.getKey(), player))
                .mapToInt(entry -> entry.getValue().getReinforcementBonus())
                .sum();
    }

    /**
     * Check if the specified player is the owner of the stated continent.
     * @param continentId the Id of the continent.
     * @param player the Player to check.
     * @return true if the player owns the continent, otherwise false.
     */
    public boolean isContinentOwner(int continentId, Player player) {
        // Set P [ p is a country && memberOf Continent && memberOf playerCountry ]
        Set<Country> playerOwnedSet = new HashSet<>();
        // Set C [ c is a country && memberOf Continent ]
        Set<Country> continentCountrySet = this.continentMap.get(continentId).getCountries();

        for (Country c : continentCountrySet) {
            if (this.countryPlayerMap.get(c.getId()).equals(player)) {
                playerOwnedSet.add(c);
            }
        }

        // Set Difference of P and C (i.e. P - C)
        playerOwnedSet.removeAll(continentCountrySet);

        // If the player owns the continent then the set difference of P and C should be empty
        return playerOwnedSet.isEmpty();
    }
    public ArrayList<Country> getCountriesOfPlayer(int playerId){
    	ArrayList<Country> countryList = new ArrayList<Country>();
    	for(int i=0;i<this.countryPlayerMap.size();i++){
    		if(this.countryPlayerMap.get(playerId)!=null){
    			countryList.add(countryMap.get(playerId));
    		}
    	}
    	return countryList;
    }
    
    public ArrayList<Country> getEmptyCountriesList(){
    	
    	ArrayList<Country> countryList = new ArrayList<Country>();
    	for(int i=0;i<this.countryPlayerMap.size();i++){
    		if(this.countryPlayerMap.get(i)==null){
    			countryList.add(countryMap.get(this.countryPlayerMap.get(i)));
    		}
    	}
    	return countryList;
    }

}

