package uk.ac.standrews.cs.cs3099.grouph.engine.events;

import java.io.Serializable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.standrews.cs.cs3099.grouph.shared.AbstractMessage;

import uk.ac.standrews.cs.cs3099.grouph.engine.game.managers.GameManager;

import uk.ac.standrews.cs.cs3099.grouph.engine.messages.responses.ResponseMessage;

public abstract class Event implements Serializable, Runnable, Comparable<Event> {

    protected Logger log;

	public enum Priority {
		LOWEST(-2),
		LOW(-1),
		MEDIUM(0),
		HIGH(1),
		HIGHEST(2);
		
		private int pVal;
		
		Priority(int pVal) {
			this.pVal = pVal;
		}
		
		public int asInt() {
			return this.pVal;
		}
	}
	
	public final String name;
	public final Priority priority;
	
	protected AbstractMessage trigger;

    protected EngineEventManager.MessageInterface messageInterface;
	protected EngineEventManager.EventInterface eventInterface;

    protected GameManager gameManager;

	public Event(Priority priority) {
        this.name = this.getClass().getSimpleName();
		this.priority = priority;

        this.log = LoggerFactory.getLogger(this.name);
	}
	
	public AbstractMessage getTrigger() {
		return this.trigger;
	}
	
	public void setTrigger(AbstractMessage trigger) {
		this.trigger = trigger;
	}

    public void setMessageInterface(EngineEventManager.MessageInterface messageInterface) {
        this.messageInterface = messageInterface;
    }

    public void setEventInterface(EngineEventManager.EventInterface eventInterface) {
        this.eventInterface = eventInterface;
    }

    public void setGameManager(GameManager gameManager) {
        this.gameManager = gameManager;
    }

    public void respond(boolean accept, EngineEventManager.ComponentReference dest) {
        ResponseMessage.Code code = (accept) ? ResponseMessage.Code.ACCEPT : ResponseMessage.Code.REJECT;
        
        try {
            this.messageInterface.push(
                    new ResponseMessage(
                        this.messageInterface.getMessageCounter(), this.trigger, code), dest);
            this.log.info("Event responded to trigger with code = {}", code);
        } catch (InterruptedException i) {
            this.log.error("Execution interrupted while trying to respond with {}!", code);
        }
    }

    public void message(AbstractMessage msg, EngineEventManager.ComponentReference dest) {
        try {
            this.messageInterface.push(msg, dest);
            this.log.info("Event sent message {}", msg);
        } catch (InterruptedException i) {
            this.log.error("Execution interrupted while trying to send msg {}!", msg);
        }
    }
	
	@Override
	public int compareTo(Event event) {
		return this.priority.asInt() - event.priority.asInt();
	}
}
