package uk.ac.standrews.cs.cs3099.grouph.engine.gamestate;

import java.util.ArrayList;

public class Continent {

    public ArrayList<Country> countries;

    public Continent(ArrayList<Country> countries) {
        this.countries = countries;
    }

    public boolean isOccupiedByOnePlayer() {

        for (Country a : countries) {
            for (Country b : countries) {
                if (a.getPlayerID() != b.getPlayerID()) {
                    return false;
                }
            }
        }
        return true;
    }

}
