package uk.ac.standrews.cs.cs3099.grouph.engine.messages.requests;

import uk.ac.standrews.cs.cs3099.grouph.shared.AbstractEngineMessage;

/**
 * Signifies a request for us to host a game.
 *
 * Destinations:
 * - UI: Not Applicable
 * - Net: Signifies that the current player wishes to host a game.
 */
public class RequestHost extends AbstractEngineMessage {

    private final int port;
    private String jsonMapString;

    public RequestHost(int id, int port, String jsonMapString) {
        super(id);
        this.port = port;
        this.jsonMapString = jsonMapString;
    }

    public int getPort() {
        return port;
    }
    public String getJsonMapString() { return this.jsonMapString; }
}
