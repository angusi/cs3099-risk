package uk.ac.standrews.cs.cs3099.grouph.engine.game.cards;

import uk.ac.standrews.cs.cs3099.grouph.engine.interfaces.game.cards.ICard;
import uk.ac.standrews.cs.cs3099.grouph.engine.game.exceptions.InvalidPileState;
import uk.ac.standrews.cs.cs3099.grouph.engine.game.exceptions.UnsupportedPileAction;

/**
 * A standard discard pile that contains discarded cards from a deck.
 *
 * @implNote Standard discard piles have cards drawn from them.
 *
 * @see uk.ac.standrews.cs.cs3099.grouph.engine.interfaces.game.cards.IDeck
 */
public class DiscardPile extends Pile {

    public DiscardPile(int pileSize) {
        super(pileSize);
    }

    @Override
    public ICard drawCard() throws UnsupportedPileAction, InvalidPileState {
        throw new UnsupportedPileAction();
    }

    @Override
    public void placeCard(ICard card) throws UnsupportedPileAction, InvalidPileState {
        if (!this.cards.offer(card)) {
            throw new InvalidPileState();
        }
    }
}
