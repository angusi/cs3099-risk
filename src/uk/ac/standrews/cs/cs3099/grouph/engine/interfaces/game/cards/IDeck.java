package uk.ac.standrews.cs.cs3099.grouph.engine.interfaces.game.cards;

import java.util.Queue;
import java.util.Random;

/**
 * A standard interface for normal decks.
 *
 * Normal decks contain two piles:
 * - Draw Pile: The pile of cards that have not been drawn from the deck.
 * - Discard Pile: The pile of cards that have been drawn and played from the deck.
 *
 * @see uk.ac.standrews.cs.cs3099.grouph.engine.interfaces.game.cards.IPile
 */
public interface IDeck {

    /**
     * Draw a card from the deck.
     * @return Null if the deck has no cards left to draw, otherwise the card that was drawn.
     */
    public ICard drawCard();

    /**
     * Discard a card back to the deck.
     * @param card the card to discard.
     */
    public void discardCard(ICard card);

    /**
     * Check if the deck contains the given card.
     * @param card the card to check
     * @return True if the deck contains the card, otherwise false.
     */
    public boolean containsCard(ICard card);

    /**
     * Shuffle the deck using the specified random number generator.
     * @param rnd the random number generator to use.
     */
    public void shuffle(Random rnd);

    /**
     * Returns the flag that dictates if the deck automatically reshuffles the discard deck into the draw deck
     * when there are no cards left to draw.
     * @return autoReshuffle flag
     */
    public boolean getAutoReshuffle();

    /**
     * Sets the flag that dictates if the deck automatically reshuffles the discard deck into the draw deck
     * when there are no cards left to draw.
     * @return autoReshuffle flag
     */
    public void setAutoReshuffle(boolean b);

}
