package uk.ac.standrews.cs.cs3099.grouph.engine.events.ui;

import java.util.List;
import java.util.HashSet;

import java.net.InetAddress;
import java.net.UnknownHostException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.standrews.cs.cs3099.grouph.engine.events.Event;
import uk.ac.standrews.cs.cs3099.grouph.engine.events.EngineEventManager;

import uk.ac.standrews.cs.cs3099.grouph.engine.game.managers.GameManager;

import uk.ac.standrews.cs.cs3099.grouph.engine.messages.requests.RequestJoin;
import uk.ac.standrews.cs.cs3099.grouph.engine.messages.requests.RequestMapTransfer;
import uk.ac.standrews.cs.cs3099.grouph.engine.messages.requests.RequestJoinResult;

import uk.ac.standrews.cs.cs3099.grouph.engine.game.map.MapParser;
import uk.ac.standrews.cs.cs3099.grouph.engine.game.map.GameMap;
import uk.ac.standrews.cs.cs3099.grouph.ui.messages.JoinGameMessage;

import uk.ac.standrews.cs.cs3099.grouph.shared.struct.players.Player;

/**
 *
 * @implNote the response message of ACCEPT from this event only signifies a request to create
 * join a game and not that a game has successfully been joined.
 */
public class EventUIJoinGame extends Event {

    public static final String JSON_MAP_STRING = "{\"data\":\"map\",\"continents\":{\"0\":[\"0\",\"1\",\"2\",\"3\",\"4\",\"5\",\"6\",\"7\",\"8\"],\"1\":[\"9\",\"10\",\"11\",\"12\"],\"2\":[\"13\",\"14\",\"15\",\"16\",\"17\",\"18\",\"19\"],\"3\":[\"20\",\"21\",\"22\",\"23\",\"24\",\"25\"],\"4\":[\"26\",\"27\",\"28\",\"29\",\"30\",\"31\",\"32\",\"33\",\"34\",\"35\",\"36\",\"37\"],\"5\":[\"38\",\"39\",\"40\",\"41\"]},\"connections\":[[0,1],[0,3],[0,29],[1,3],[1,2],[1,4],[2,4],[2,5],[2,13],[3,4],[3,6],[4,5],[4,6],[4,7],[5,7],[6,7],[6,8],[7,8],[8,9],[9,10],[9,10],[10,11],[10,12],[11,12],[11,20],[13,14],[13,16],[14,16],[14,17],[14,15],[15,17],[15,19],[15,26],[15,33],[15,35],[16,17],[16,18],[17,18],[17,19],[18,19],[18,20],[19,20],[19,21],[19,35],[20,21],[20,22],[20,23],[21,23],[21,35],[22,23],[22,24],[23,25],[23,24],[23,35],[24,25],[26,27],[26,34],[26,33],[27,28],[27,30],[27,31],[27,34],[28,29],[28,30],[29,30],[29,32],[29,31],[30,31],[31,32],[31,34],[33,34],[33,35],[33,36],[34,36],[34,37],[35,36],[36,37],[37,38],[38,39],[38,40],[39,40],[39,41],[40,41]],\"continent_values\":{\"0\":5,\"1\":2,\"2\":5,\"3\":3,\"4\":7,\"5\":2},\"country_names\":{\"0\":\"Alaska\",\"1\":\"Northwestterritory\",\"2\":\"Greenland\",\"3\":\"Alberta\",\"4\":\"Ontario\",\"5\":\"Quebec\",\"6\":\"WesternUnitedStates\",\"7\":\"EasternUnitedStates\",\"8\":\"CentralAmerica\",\"9\":\"Venezuela\",\"10\":\"Peru\",\"11\":\"Brazil\",\"12\":\"Argentina\",\"13\":\"Iceland\",\"14\":\"Scandinavia\",\"15\":\"Ukraine\",\"16\":\"GreatBritain\",\"17\":\"NorthernEurope\",\"18\":\"WesternEurope\",\"19\":\"SouthernEurope\",\"20\":\"NorthAfrica\",\"21\":\"Egypt\",\"22\":\"Congo\",\"23\":\"EastAfrica\",\"24\":\"SouthAfrica\",\"25\":\"Madagaskar\",\"26\":\"Ural\",\"27\":\"Siberia\",\"28\":\"Yakutsk\",\"29\":\"Kamichatka\",\"30\":\"Irkutsk\",\"31\":\"Mongolia\",\"32\":\"Japan\",\"33\":\"Afghanistan\",\"34\":\"China\",\"35\":\"MiddleEast\",\"36\":\"India\",\"37\":\"Siam\",\"38\":\"Indonesia\",\"39\":\"NewGuinea\",\"40\":\"WesternAustralia\",\"41\":\"EasternAustralia\"},\"continent_names\":{\"0\":\"NorthAmercia\",\"1\":\"SouthAmerica\",\"2\":\"Europe\",\"3\":\"Africa\",\"4\":\"Asia\",\"5\":\"Australia\"},\"country_card\":{\"0\":2,\"1\":1,\"2\":1,\"3\":2,\"4\":2,\"5\":2,\"6\":2,\"7\":2,\"8\":0,\"9\":1,\"10\":1,\"11\":0,\"12\":0,\"13\":1,\"14\":0,\"15\":0,\"16\":0,\"17\":1,\"18\":0,\"19\":0,\"20\":0,\"21\":1,\"22\":2,\"23\":0,\"24\":2,\"25\":1,\"26\":0,\"27\":0,\"28\":2,\"29\":2,\"30\":2,\"31\":1,\"32\":1,\"33\":0,\"34\":2,\"35\":2,\"36\":1,\"37\":1,\"38\":0,\"39\":1,\"40\":2,\"41\":1},\"wildcards\":2}";

    public EventUIJoinGame() {
        super(Event.Priority.HIGH);
    }

    @Override
    public void run() {
        boolean result = false;
        MapParser mapParser = MapParser.createStringParser(JSON_MAP_STRING);

        if (mapParser != null && mapParser.parse()) {
            if (this.gameManager.hostLobby("BASIC", GameMap.createMapFromParser(mapParser))) {   
                try {   
                    InetAddress address = InetAddress.getByName(
                            ((JoinGameMessage) this.trigger).getHostname());

                    this.message(new RequestJoin(this.messageInterface.getMessageCounter(),
                                    new HashSet<String>(), address),
                                EngineEventManager.ComponentReference.NET);

                    result = true;
                } catch (UnknownHostException e) {
                    this.log.error("Host {} not found!", ((JoinGameMessage) this.trigger).getHostname());
                    result = false;
                }
            }
        }

        // this.message(new RequestJoinResult(this.messageInterface.getMessageCounter(), result),
        //        EngineEventManager.ComponentReference.UI);

        this.respond(result, EngineEventManager.ComponentReference.UI);

        if (result) {
            // Send the UI the map that we are using
            this.message(new RequestMapTransfer(this.messageInterface.getMessageCounter(),
                        JSON_MAP_STRING), EngineEventManager.ComponentReference.UI);
        }
    }
}
