package uk.ac.standrews.cs.cs3099.grouph.engine.game.managers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.standrews.cs.cs3099.grouph.engine.game.players.Player;
import uk.ac.standrews.cs.cs3099.grouph.engine.game.players.PlayerQueue;

import uk.ac.standrews.cs.cs3099.grouph.engine.game.state.exceptions.InvalidState;

import uk.ac.standrews.cs.cs3099.grouph.engine.game.state.TurnState;

import uk.ac.standrews.cs.cs3099.grouph.engine.game.map.exception.MissingCountry;

import uk.ac.standrews.cs.cs3099.grouph.engine.game.map.GameMap;

import uk.ac.standrews.cs.cs3099.grouph.engine.game.managers.exceptions.GameRuleException;

public class GameController {
    private Logger logger = LoggerFactory.getLogger(GameController.class);

    public class Validator {
        private GameManager.Game gameData;

        public Validator(GameManager.Game gameData) {
            this.gameData = gameData;
        }

        public void validateCountry(int countryId) throws GameRuleException {
            if (!this.gameData.getGameMap().containsCountry(countryId))
                throw new GameRuleException(GameRuleException.Rule.COUNTRY_ILLEGAL_ID);
        }

        public void validatePlayer(Player player) throws GameRuleException {
            if (!(this.gameData.getTurnState().getCurrentPlayer() == player))
                throw new GameRuleException(GameRuleException.Rule.PLAYER_ILLEGAL_TURN);
        }

        public void validateStep(TurnState.Step step) throws GameRuleException {
            if (!(this.gameData.getTurnState().getCurrentStep() == step))
                throw new GameRuleException(GameRuleException.Rule.PLAYER_ILLEGAL_STEP);
        }

        public void validateCountryOwner(Player player, int countryId) throws GameRuleException {
            if (!(this.gameData.getGameMap().getCountryOwner(countryId) == player))
                throw new GameRuleException(GameRuleException.Rule.COUNTRY_INCORRECT_OWNER);
        }

        public void validateNotCountryOwner(Player player, int countryId)
            throws GameRuleException {
            if (this.gameData.getGameMap().getCountryOwner(countryId) == player)
                throw new GameRuleException(GameRuleException.Rule.COUNTRY_INCORRECT_OWNER);
        }

        public void validateEmptyCountry(int countryId) throws GameRuleException {
            if (this.gameData.getGameMap().getCountryOwner(countryId) != null)
                throw new GameRuleException(GameRuleException.Rule.COUNTRY_INCORRECT_OWNER);
        }

        public void validateCountryConnection(int countryAId, int countryBId)
            throws GameRuleException {
            if (!this.gameData.getGameMap().areConnected(countryAId, countryBId))
                throw new GameRuleException(GameRuleException.Rule.COUNTRY_ILLEGAL_CONNECTION);
        }

        public void validatePlayerAvailableReinforcements(Player player, int numberOfArmies)
            throws GameRuleException {
            if (player.getReinforcements() - numberOfArmies < 0)
                throw new GameRuleException(GameRuleException.Rule.ARMY_REINFORCE_TOO_LARGE);
        }

        public void validateArmyBattle(boolean defender,
            int countryId, int numberOfArmies) throws GameRuleException {
            if (numberOfArmies < 0)
                throw new GameRuleException(GameRuleException.Rule.ARMY_NEGATIVE);
            
            if (numberOfArmies > 3 && !defender) 
                throw new GameRuleException(GameRuleException.Rule.ARMY_ATTACKER_TOO_LARGE);
            else if (numberOfArmies > 2 && defender)
                throw new GameRuleException(GameRuleException.Rule.ARMY_DEFENDER_TOO_LARGE);

            if (!defender && this.gameData.getGameMap().getCountryArmies(countryId) < numberOfArmies + 1) {
                throw new GameRuleException(GameRuleException.Rule.ARMY_ATTACKER_INSUFFICIENT);
            } else if (this.gameData.getGameMap().getCountryArmies(countryId) < numberOfArmies) {
                throw new GameRuleException(GameRuleException.Rule.ARMY_DEFENDER_INSUFFICIENT);
            }
        }

        public void validatePath(Player player, int startingCountryId, int endingCountryId) 
            throws GameRuleException {
            if (!this.gameData.getGameMap().isAdjacent(
                    startingCountryId, endingCountryId)) {
                throw new GameRuleException(GameRuleException.Rule.FORTIFY_NO_PATH); 
            }
        }
        
        public void validateAttack(Player player, int attackingCountryId,
                int defendingCountryId, int numberOfArmies) throws GameRuleException {
            // Validate attacking player is current player
            this.validatePlayer(player);
            // Validate player is in the correct turn state
            this.validateStep(TurnState.Step.ATTACK);
            // Validate player owns country attacking from
            this.validateCountryOwner(player, attackingCountryId);
            // Validate player doesn't own defending country
            this.validateNotCountryOwner(player, defendingCountryId);
            // Validate if attacking and defending countries are connected
            this.validateCountryConnection(attackingCountryId, defendingCountryId);
            // Validate that attacker has 1 + numberOfArmies to conduct attack 
            this.validateArmyBattle(false, attackingCountryId, numberOfArmies);
        }

        public void validateDefend(Player player, int defendingCountryId, int numberOfArmies)
            throws GameRuleException {
            // Validate correct player defending 
            this.validatePlayer(player);
            // Validate correct state
            this.validateStep(TurnState.Step.DEFEND);
            // Validate player owns defending country
            this.validateCountryOwner(player, defendingCountryId);
            // Validate defender has 1 + numberOfArmies to defend from attacking 
            this.validateArmyBattle(true, defendingCountryId, numberOfArmies);
        }

        public void validateClaim(Player player, int countryId) throws GameRuleException {
            // Validate claiming player is current player
            this.validatePlayer(player);
            // Validate player is in right turn state 
            this.validateStep(TurnState.Step.CLAIM);
            // Validate country to be claimed is empty
            this.validateEmptyCountry(countryId);
        }

        public void validateDeploy(Player player, int countryId, int numberOfArmies)
            throws GameRuleException {
            // Validate deploying player is current player
            this.validatePlayer(player);
            // Validate player is in right turn state 
            this.validateStep(TurnState.Step.DEPLOY);
            // Validate country to be deployed to is owned by player 
            this.validateCountryOwner(player, countryId);
            // Validate player has enough reinforcements
            this.validatePlayerAvailableReinforcements(player, numberOfArmies);
        }

        public void validateReinforce(Player player, int countryId, int numberOfArmies)
            throws GameRuleException {
            // Validate deploying player is current player
            this.validatePlayer(player);
            // Validate player is in right turn state 
            this.validateStep(TurnState.Step.REINFORCE);
            // Validate country to be deployed to is owned by player 
            this.validateCountryOwner(player, countryId);
            // Validate player has enough reinforcements
            this.validatePlayerAvailableReinforcements(player, numberOfArmies);
        }

        public void validateMove(Player player, int startingCountryId, int endingCountryId,
                int numberOfArmies) throws GameRuleException {
            if (this.gameData.getGameMap().getCountryArmies(startingCountryId) < 0) {
                throw new GameRuleException(GameRuleException.Rule.ARMY_NEGATIVE);
            }
            if (this.gameData.getGameMap().getCountryArmies(startingCountryId) <
                    1 + numberOfArmies) {
                throw new GameRuleException(GameRuleException.Rule.ARMY_MOVE_TOO_LARGE);
            }
            if (!this.gameData.getGameMap().isAdjacent(startingCountryId, endingCountryId)) {
                throw new GameRuleException(GameRuleException.Rule.FORTIFY_NO_PATH);
            }
        }
        
        public void validateFortify(Player player, int startingCountryId, 
            int endingCountryId, int numberOfArmies) throws GameRuleException {
            // Validate fortifying player is current player 
            this.validatePlayer(player);
            // Validate player is in right turn state
            this.validateStep(TurnState.Step.FORTIFY);
            // Validate player owns starting and ending country 
            this.validateCountryOwner(player, startingCountryId);
            this.validateCountryOwner(player, endingCountryId);
            // Validate starting country has enough armies 
            this.validateMove(player, startingCountryId, endingCountryId, numberOfArmies);
            // Validate player has uninterrupted path from starting and ending country 
            this.validatePath(player, startingCountryId, endingCountryId);
        }

    }

    public class Modifier {
        private GameManager.Game gameData;
        private Validator validator;

        private Modifier(GameManager.Game gameData, Validator validator) {
            this.gameData = gameData;
            this.validator = validator;
        }

        public TurnState.Step getTurnStep() { return this.gameData.getTurnState().getCurrentStep(); }

        public GameMap getGameMap() { return this.gameData.getGameMap(); }

        public boolean isDeploy() {
            return (this.gameData.getTurnState().getCurrentStep() == TurnState.Step.DEPLOY);
        }

        public boolean isClaim() {
            return (this.gameData.getTurnState().getCurrentStep() == TurnState.Step.CLAIM);
        }

        public boolean isFortify() { return (this.gameData.getTurnState().getCurrentStep() == TurnState.Step.FORTIFY); }

        public boolean isAttack() { return (this.gameData.getTurnState().getCurrentStep() == TurnState.Step.ATTACK); }

        public Player getCurrentPlayer() {
            return this.gameData.getTurnState().getCurrentPlayer();
        }

        public String printTurnOrder() {
            return this.gameData.getTurnState().printTurnOrder();
        }

        // TODO: Replace runtime with custom exception
        public Player getPlayerById(int playerId) throws RuntimeException {
            Player p;

            if ((p = this.gameData.getPlayerQueue().getPlayer(playerId)) == null)
                throw new RuntimeException("Player Id does not exist!");

            return p;
        }

        public void setup() {
            int numberOfPlayers = this.gameData.getPlayerQueue().getNumberOfPlayers();
            //int startingReinforcements = 35 - (5 * (numberOfPlayers - 3));
            this.gameData.getPlayerQueue().setAllReinforcements(5);
        }

        public void setupPlayerTurn(int playerId) {
            Player player = this.gameData.getPlayerQueue().getPlayer(playerId);
            player.calculateTurnReinforcements();
            player.setReinforcements(player.getReinforcements() + this.gameData.getGameMap().calculateContinentBonus(player));

            if (player.getReinforcements() < 3) {
                player.setReinforcements(3);
            }
        }

        public void claim(int playerId, int countryId)
            throws GameRuleException {
            Player player = this.getPlayerById(playerId);
            
            this.validator.validateClaim(player, countryId); 
            
            synchronized(this) {
                try {
                    this.gameData.getGameMap().setCountryOwner(countryId, player);
                    this.gameData.getGameMap().setCountryArmies(countryId, 1);

                    player.setTerritories(player.getTerritories() + 1);
                    
                    if (this.gameData.getGameMap().containsEmptyCountry()) {
                        logger.info("GameMap contains empty country - Proceeding with claims");
                        this.gameData.getTurnState().nextPlayer();
                    } else {
                        logger.info("GameMap does not contain an empty country - Moving to play phase and reset turn order");
                        this.gameData.getTurnState().finishClaim();
                        this.gameData.getTurnState().resetTurnOrder();
                    }
                } catch (MissingCountry c) {
                    throw new GameRuleException(GameRuleException.Rule.COUNTRY_ILLEGAL_ID);
                } catch (InvalidState s) {
                    throw new GameRuleException(GameRuleException.Rule.PLAYER_ILLEGAL_STEP);
                }
            } 
        }

        public void deploy(int playerId, int countryId, int numberOfArmies)
            throws GameRuleException {
            Player player = this.getPlayerById(playerId);
            
            this.validator.validateDeploy(player, countryId, numberOfArmies);
            
            synchronized(this) {
                try {
                    player.setReinforcements(player.getReinforcements() - numberOfArmies);

                    this.gameData
                        .getGameMap()
                        .setCountryArmies(countryId, 
                                this.gameData.getGameMap().getCountryArmies(countryId) + numberOfArmies);

                    if (this.gameData.getPlayerQueue().allReinforcementsDepleted()) {
                        this.gameData.getTurnState().finishDeploy();
                        this.gameData.getTurnState().resetTurnOrder();
                    } else {
                        this.gameData.getTurnState().nextPlayer();
                    }
                } catch (MissingCountry c) {
                    throw new GameRuleException(GameRuleException.Rule.COUNTRY_ILLEGAL_ID);
                } catch (InvalidState s) {
                    throw new GameRuleException(GameRuleException.Rule.PLAYER_ILLEGAL_STEP);
                }

            }
        }

        public int getAttackingCountryId() { return this.gameData.getTurnState().getAttackingCountryId(); }

        public int getDefendingCountryId() { return this.gameData.getTurnState().getDefendingCountryId(); }

        public int getDefendingPlayerId() { return this.gameData.getTurnState().getDefendingPlayer().getId(); }

        public int getAttackingPlayerId() { return this.gameData.getTurnState().getAttackingPlayer().getId(); }

        public int getNumberOfPlayers() { return this.gameData.getPlayerQueue().getNumberOfPlayers(); }

        public int getNumberOfAttackers() {
            return this.gameData.getTurnState().getNumberOfAttackers();
        }

        public int getNumberOfDefenders() {
            return this.gameData.getTurnState().getNumberOfDefenders();
        }


        public void reinforce(int playerId, int countryId, int numberOfArmies) 
            throws GameRuleException {
            Player player = this.getPlayerById(playerId);

            this.validator.validateReinforce(player, countryId, numberOfArmies);
            synchronized(this) {
                try {
                    player.setReinforcements(player.getReinforcements() - numberOfArmies);
    
                    this.gameData.getGameMap().setCountryArmies(countryId,
                            this.gameData.getGameMap().getCountryArmies(countryId) + numberOfArmies);

                    if (player.getReinforcements() == 0) {
                        this.gameData.getTurnState().nextStep();
                    }
                } catch (MissingCountry c) {
                    throw new GameRuleException(GameRuleException.Rule.COUNTRY_ILLEGAL_ID);
                }
            }
        }

        public void attack(int playerId, int attackingCountryId, int defendingCountryId, int numberOfArmies) 
            throws GameRuleException {
            Player player = this.getPlayerById(playerId);

            this.validator.validateAttack(player, attackingCountryId, defendingCountryId, numberOfArmies);
            synchronized(this) {
                try {
                    this.gameData.getTurnState().setNumberOfAttackers(numberOfArmies);
                    this.gameData.getTurnState().setDefendingPlayer(this.gameData.getGameMap().getCountryOwner(defendingCountryId));
                    this.gameData.getTurnState().setAttackingCountryId(attackingCountryId);
                    this.gameData.getTurnState().setDefendingCountryId(defendingCountryId);
                    this.gameData.getTurnState().nextStep();
                } catch (MissingCountry c) {
                    throw new GameRuleException(GameRuleException.Rule.COUNTRY_ILLEGAL_ID);
                }
            }
        }

        public void defend(int playerId, int numberOfArmies) throws GameRuleException {
            Player player = this.getPlayerById(playerId);

            this.validator.validateDefend(player, this.gameData.getTurnState().getDefendingCountryId(), numberOfArmies);
            synchronized(this) {
                try {
                    this.gameData.getTurnState().setNumberOfDefenders(numberOfArmies);
                    this.gameData.getTurnState().nextStep();
                } catch (MissingCountry c) {
                    throw new GameRuleException(GameRuleException.Rule.COUNTRY_ILLEGAL_ID);
                }
            }
        }

        public void result(int playerId, int attackingLosses, int defendingLosses) throws GameRuleException {
            Player player = this.getPlayerById(playerId);

            this.validator.validatePlayer(player);
            synchronized (this) {
                try {
                    int attackingId = this.gameData.getTurnState().getAttackingCountryId();
                    int defendingId = this.gameData.getTurnState().getDefendingCountryId();
                    
                    // Calculate losses
                    this.gameData.getGameMap().setCountryArmies(attackingId, 
                            this.gameData.getGameMap().getCountryArmies(attackingId) - attackingLosses);
                    this.gameData.getGameMap().setCountryArmies(defendingId, 
                            this.gameData.getGameMap().getCountryArmies(defendingId) - defendingLosses);

                    // Check if territory changes
                    if (this.gameData.getGameMap().getCountryArmies(defendingId) == 0) {
                        this.gameData.getGameMap().setCountryOwner(defendingId, this.getCurrentPlayer());
                        this.gameData.getTurnState().triggerMoveStep();
                    } else {
                        this.gameData.getTurnState().nextStep();
                    }
                } catch (MissingCountry c) {
                    throw new GameRuleException(GameRuleException.Rule.COUNTRY_ILLEGAL_ID);
                }
            }
        }

        public void move(int playerId, int numberOfArmies) throws GameRuleException {
            Player player = this.getPlayerById(playerId);
            Player defender = this.gameData.getTurnState().getDefendingPlayer();

            int attackingCountryId = this.gameData.getTurnState().getAttackingCountryId();
            int defendingCountryId = this.gameData.getTurnState().getDefendingCountryId();

            this.validator.validateMove(player, attackingCountryId, defendingCountryId, numberOfArmies);
            synchronized(this) {
                try {
                    this.gameData.getGameMap().setCountryArmies(attackingCountryId,
                            this.gameData.getGameMap().getCountryArmies(attackingCountryId) - numberOfArmies);
                    this.gameData.getGameMap().setCountryArmies(defendingCountryId,
                            this.gameData.getGameMap().getCountryArmies(defendingCountryId) + numberOfArmies);

                    player.setTerritories(player.getTerritories() + 1);
                    defender.setTerritories(defender.getTerritories() - 1);

                    this.gameData.getTurnState().nextStep();
                } catch (MissingCountry c) {
                    throw new GameRuleException(GameRuleException.Rule.COUNTRY_ILLEGAL_ID);
                }
            }
        }

        public void fortify(int playerId, int startingCountryId, int endingCountryId, int numberOfArmies)
            throws GameRuleException {
            Player player = this.getPlayerById(playerId);
            
            // Finish the player's attack phase
            this.validator.validatePlayer(player);
            this.gameData.getTurnState().finishAttack();

            this.validator.validateFortify(player, startingCountryId, endingCountryId, numberOfArmies);
            synchronized(this) {
                try {
                    this.gameData.getGameMap().setCountryArmies(startingCountryId,
                            this.gameData.getGameMap().getCountryArmies(startingCountryId) - numberOfArmies);
                    this.gameData.getGameMap().setCountryArmies(endingCountryId,
                            this.gameData.getGameMap().getCountryArmies(endingCountryId) + numberOfArmies);
                    this.gameData.getTurnState().nextStep();
                } catch (MissingCountry c) {
                    throw new GameRuleException(GameRuleException.Rule.COUNTRY_ILLEGAL_ID);
                }
            }
        }

        public void yield() {
            this.gameData.getTurnState().yield();
        }

        public void turnover(int playerId) throws GameRuleException {
            Player player = this.getPlayerById(playerId);

            this.validator.validatePlayer(player);
            this.validator.validateStep(TurnState.Step.FINISH);
            synchronized(this) {
                try {
                    if (!this.gameData.getPlayerQueue().allTerritoriesOwnedBySinglePlayer()) {
                        this.gameData.getTurnState().nextPlayer();
                        this.gameData.getTurnState().nextStep();
                    }
                } catch (InvalidState s) {
                    throw new GameRuleException(GameRuleException.Rule.PLAYER_ILLEGAL_STEP);
                }
            }
        }
    }

    private Validator validator;
    private Modifier modifier;

    public GameController(GameManager.Game gameData) {
        this.validator = new Validator(gameData);
        this.modifier = new Modifier(gameData, this.validator);
    }

    public Validator getValidator() {
        return this.validator;
    }

    public Modifier getModifier() {
        return this.modifier;
    }

}
