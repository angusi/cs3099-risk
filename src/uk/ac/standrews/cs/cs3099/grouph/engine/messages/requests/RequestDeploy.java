package uk.ac.standrews.cs.cs3099.grouph.engine.messages.requests;

import uk.ac.standrews.cs.cs3099.grouph.shared.AbstractEngineMessage;
import uk.ac.standrews.cs.cs3099.grouph.shared.struct.players.Player;

/**
 * Signifies a deployment request during the reinforcement stage of a player's turn. This is also used
 * at the start of the game.
 *
 * Destinations:
 * - UI: Signifies the deployment request of another player.
 * - Net: Signifies the deployment request of the current player.
 */
public class RequestDeploy extends AbstractEngineMessage {

    private final Player player;
    private final int[][] reinforcements;

    public RequestDeploy(int index, Player player, int[][] reinforcements) {
        super(index);

        this.player = player;
        this.reinforcements = reinforcements;
    }

    public int[][] getReinforcements() { return this.reinforcements; }

    /**
     *
     * @return the Id of the player doing the deployment.
     */
    public Player getPlayer() {
        return player;
    }
}
