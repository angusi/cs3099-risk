package uk.ac.standrews.cs.cs3099.grouph.engine.gamestate;

import java.io.Serializable;

/**
 * Represents a single RISK game territory card.
 */
public class Card implements Serializable {
    public enum Type {
        INFANTRY,
        CALVARY,
        ARTILLERY,
        WILD
    }

    public final Type type;
    public final Country country;

    public Card(Type type, Country country) {
        this.type = type;
        this.country = country;
    }

    @Override
    public boolean equals(Object obj) {
        return  obj instanceof Card &&
                this.type       == ((Card) obj).type &&
                this.country    == ((Card) obj).country;
    }
}
