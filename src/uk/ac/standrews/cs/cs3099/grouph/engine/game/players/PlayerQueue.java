package uk.ac.standrews.cs.cs3099.grouph.engine.game.players;

import java.util.LinkedList;
import java.util.Queue;
import java.util.SortedSet;

/**
 * Contains all of the current players stored in turn order.
 */
public class PlayerQueue {

    private Queue<Player> originalQueue;
    private Queue<Player> playerQueue;

    public PlayerQueue(SortedSet<Player> initialPlayerSet) {
        this.playerQueue = new LinkedList<>(initialPlayerSet);
        this.originalQueue = new LinkedList<>(initialPlayerSet);
    }

    public PlayerQueue(Queue<Player> initialPlayerOrder) {
        this.playerQueue = new LinkedList<>(initialPlayerOrder);
        this.originalQueue = new LinkedList<>(initialPlayerOrder);
    }

    /**
     * Return the first player in the queue, at the same time sending him back to the 
     * end of the queue.
     * @return the player at the head of the queue.
     */
    public Player nextPlayer() {
        Player p = this.playerQueue.remove();
        this.playerQueue.add(p);

        return p;
    }

    public Player currentPlayer() {
        return this.playerQueue.peek();
    }

    public Queue<Integer> getPlayerQueueById() {
        Queue<Integer> queue = new LinkedList<>();
        
        for (Player p : this.originalQueue) {
            queue.add(p.getId());
        }

        return queue;
    }

    public Player getPlayer(int playerId) {
        for (Player p : this.playerQueue) {
            if (p.getId() == playerId)
                return p;
        }

        return null;
    }

    public int getNumberOfPlayers() {
        return this.playerQueue.size();
    }

    public void setAllReinforcements(int reinforcements) {
        for (Player player : this.playerQueue) {
            player.setReinforcements(reinforcements);
        }
    }

    public boolean allReinforcementsDepleted() {
        for (Player player : this.playerQueue) {
            if (player.getReinforcements() > 0) {
                return false;
            }
        }

        return true;
    }

    public boolean allTerritoriesOwnedBySinglePlayer() {
        for (Player player : this.playerQueue) {
            if (player.getTerritories() > 0) {
                return false;
            }
        }

        return true;
    }

    public void resetOrder() {
        this.playerQueue = new LinkedList<>();
        this.originalQueue.forEach(this.playerQueue::add);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder("TAIL -> (");
        boolean hasPrevious = false;

        for (Player p : this.playerQueue) {
            if (hasPrevious)
                builder.append(",");
            builder.append(p.getId());
            hasPrevious = true;
        }

        builder.append(") -> HEAD");
        return builder.toString();
    }
}

