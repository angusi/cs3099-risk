package uk.ac.standrews.cs.cs3099.grouph.engine.messages.requests;

import uk.ac.standrews.cs.cs3099.grouph.shared.AbstractEngineMessage;

import uk.ac.standrews.cs.cs3099.grouph.shared.struct.players.Player;

/**
 * Signifies the number of reinforcements that the current (turn) player gets.
 *
 * This message can be expected after the following requests:
 * - RequestTurnOver
 * - RequestTradeCard
 *
 * Destinations:
 * - UI: Signifies the number of reinforcements to display.
 * - Net: N/A
 */
public class RequestReinforceCount extends AbstractEngineMessage {

    private final Player player;    
    private final int armyCount;

    public RequestReinforceCount(int index, Player player, int armyCount) {
        super(index);

        this.player = player;
        this.armyCount = armyCount;
    }

    public int getArmyCount() { return this.armyCount; }
    public Player getPlayer() { return this.player; }
}
