package uk.ac.standrews.cs.cs3099.grouph.engine.events.ui;

import java.util.Queue;
import java.util.LinkedList;
import java.util.stream.IntStream;

import uk.ac.standrews.cs.cs3099.grouph.engine.EngineCore;
import uk.ac.standrews.cs.cs3099.grouph.engine.dice.DiceRoller;
import uk.ac.standrews.cs.cs3099.grouph.engine.dice.random.exceptions.HashMismatchException;
import uk.ac.standrews.cs.cs3099.grouph.engine.messages.requests.*;
import uk.ac.standrews.cs.cs3099.grouph.engine.events.Event;
import uk.ac.standrews.cs.cs3099.grouph.engine.events.EngineEventManager;
import uk.ac.standrews.cs.cs3099.grouph.engine.messages.requests.RequestInitGame;
import uk.ac.standrews.cs.cs3099.grouph.engine.messages.requests.RequestTurnOver;
import uk.ac.standrews.cs.cs3099.grouph.engine.messages.requests.RequestReinforceCount;
import uk.ac.standrews.cs.cs3099.grouph.shared.struct.players.Players;
import uk.ac.standrews.cs.cs3099.grouph.shared.struct.players.InvalidPlayerException;

public class EventUIGameStart extends Event {

    public EventUIGameStart() {
        super(Event.Priority.MEDIUM);
    }

    @Override
    public void run() {
        boolean result = false;

            //TODO: Use when implemented PING/READY
            //Queue<Integer> playerOrderById = this.determineStartingTurnOrder();

            if (this.gameManager.startLobby("BASIC")) {
                int numberOfPlayers = this.gameManager.getGamePlayers("BASIC").getNumberOfPlayers();
                Queue<Integer> playerQueue = this.gameManager.getGamePlayers("BASIC").getPlayerQueueById();

                //this.message(new RequestInitGame(this.messageInterface.getMessageCounter(),
                //                numberOfPlayers, playerOrderById),
                //        EngineEventManager.ComponentReference.NET);

                this.message(new RequestInitGame(this.messageInterface.getMessageCounter(),
                                numberOfPlayers, playerQueue),
                        EngineEventManager.ComponentReference.NET);

                this.gameManager.getGameController("BASIC").getModifier().setup();

                result = true;
            }

        this.respond(result, EngineEventManager.ComponentReference.UI);

        if (result) {
            EngineCore.isHost = true;

            try {
                this.message(new RequestTurnOver(this.messageInterface.getMessageCounter(),
                            null, Players.me(), RequestTurnOver.Phase.CLAIM), EngineEventManager.ComponentReference.UI);

                this.message(new RequestReinforceCount(this.messageInterface.getMessageCounter(),
                            Players.me(), this.gameManager.getGamePlayer("BASIC", Players.me().getId()).getReinforcements()), EngineEventManager.ComponentReference.UI);
            } catch (InvalidPlayerException e) {
                this.log.error("Error {}", e.getMessage());
            }
        }
    }

    private Queue<Integer> determineStartingTurnOrder() throws InvalidPlayerException {
        // Load Dice Roller
        DiceRoller.loadInstance(1, Players.getPlayerCount(), Players.getPlayerCount());
        // Generate our hash and number
        DiceRoller.getInstance().generateMe();

        // Send out our roll hash and number
        this.message(new RequestRollHash(this.messageInterface.getMessageCounter(),
                DiceRoller.getInstance().getMyHashAsHexString()), EngineEventManager.ComponentReference.NET);

        this.log.info("Waiting for hashes...");
        // Block until we have all hashes
        while (!DiceRoller.getInstance().hasAllHashes()) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
                return null;
            }
        }

        this.log.info("Received hashes!");
        this.log.info("Waiting for numbers...");

        // We have all hashes now send out our number
        this.message(new RequestRollNumber(this.messageInterface.getMessageCounter(),
                DiceRoller.getInstance().getMyNumberAsHexString()), EngineEventManager.ComponentReference.NET);

        // Block until we have all numbers
        while (!DiceRoller.getInstance().isReady()) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
                return null;
            }
        }

        this.log.info("Received numbers!");
        this.log.info("Rolling {} dice with {} faces...",
                DiceRoller.getInstance().getNumberOfDice(), DiceRoller.getInstance().getNumberOfFaces());

        // Generate dice rolls
        try {
            if (DiceRoller.getInstance().verify().size() == 0) {
                DiceRoller.getInstance().roll();
            }
        } catch (HashMismatchException e) {
            // TODO: There is no specification in the protocol to handle hash mismatches
            e.printStackTrace();
            return null;
        }

        int firstPlayer = DiceRoller.getInstance().getResultList().get(0);
        Queue<Integer> playerOrderById = new LinkedList<>();

        this.log.info("Rolled {}, player with that id is now the first player!");

        IntStream.range(firstPlayer, Players.getPlayerCount())
                .sequential()
                .forEach(playerOrderById::add);

        IntStream.range(0, firstPlayer)
                .sequential()
                .forEach(playerOrderById::add);

        return playerOrderById;
    }
}

