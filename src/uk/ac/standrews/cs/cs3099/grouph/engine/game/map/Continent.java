package uk.ac.standrews.cs.cs3099.grouph.engine.game.map;

import java.util.Set;

/**
 * Represents a group of associated countries.
 */
public class Continent {

    private final int id;
    private final String name;
    private final Set<Country> countries;
    private final int reinforcementBonus;

    public Continent(int id, String name, Set<Country> countries, int reinforcementBonus) {
        this.id = id;
        this.name = name;
        this.countries = countries;
        this.reinforcementBonus = reinforcementBonus;
    }

    public int getReinforcementBonus() {
        return reinforcementBonus;
    }

    public Set<Country> getCountries() {
        return countries;
    }

    @Override
    public boolean equals(Object obj) {
        return (obj instanceof Continent &&
                this.getReinforcementBonus() == ((Continent) obj).getReinforcementBonus() &&
                this.getCountries().containsAll(((Continent) obj).getCountries()));
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}

