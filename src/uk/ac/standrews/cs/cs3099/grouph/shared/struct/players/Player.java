package uk.ac.standrews.cs.cs3099.grouph.shared.struct.players;

/**
 * Created by 120023239 on 23/02/15.
 */
public class Player {
    private String hostname;
    private String name;
    private int id;

    protected Player(String hostname, int id) {
        this.hostname = hostname;
        this.id = id;
        this.name = "Unnamed";
    }

    protected Player(String hostname, int id, String name) {
        this(hostname, id);

        this.name = name;
    }

    public String getHostname() {
        return hostname;
    }

    public int getId() {
        return id;
    }

    public String getName() { return name; }

    @Override
    public boolean equals(Object object) {
        return (object instanceof Player &&
                ((Player) object).id == this.id);
    }
}
