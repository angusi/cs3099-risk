package uk.ac.standrews.cs.cs3099.grouph.shared;

public abstract class AbstractNetMessage extends AbstractMessage {
    private final static String originator = "Net";
    private int ackId;
    public AbstractNetMessage(int index) {
        super(index, originator);
    }

    public int getAckId() {
        return ackId;
    }

    public void setAckId(int ackId) {
        this.ackId = ackId;
    }
}
