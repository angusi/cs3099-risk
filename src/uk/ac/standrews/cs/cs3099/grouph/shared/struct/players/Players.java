package uk.ac.standrews.cs.cs3099.grouph.shared.struct.players;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by 120023239 on 23/02/15.
 */
public class Players {
    // TODO is there a better approach than all static?
    private static Map<String, Player> hosts = new HashMap<>();
    private static int pidCount = 1;
    private static Player me = null;

    private static int pid() {
        return pidCount++;
    }

    public static Player addPlayerByName(String hostname) {
        Player player = new Player(hostname, pid());
        hosts.put(player.getHostname(), player);
        return player;
    }

    public static Player addPlayerById(String hostname, int id) {
        Player player = new Player(hostname, id);
        hosts.put(hostname, player);
        return player;
    }

    public static Player getPlayerByHostname(String hostname) throws InvalidPlayerException {
        if(hosts.containsKey(hostname)) {
            return hosts.get(hostname);
        } else {
            throw new InvalidPlayerException();
        }
    }

    public static Player getPlayerById(int id) throws InvalidPlayerException {
        for(Player p : hosts.values()) {
            if(p.getId() == id) {
                return p;
            }
        }
        throw new InvalidPlayerException();
    }

    public static Player me() throws InvalidPlayerException {
        if(me == null) {
            throw new InvalidPlayerException(); // TODO - better exception?
        } else {
            return me;
        }
    }

    public static Player generateMeAsHost() {
        try {
            me = new Player(InetAddress.getLocalHost().getHostName(), 0);
        } catch (UnknownHostException e) {
            me = null;
            return null;
        }
        hosts.put(me.getHostname(), me);
        return me;
    }

    public static Player generateMeAsClient(int id) {
        try {
            me = new Player(InetAddress.getLocalHost().getHostName(), id);
        } catch (UnknownHostException e) {
            me = null;
            return null;
        }
        hosts.put(me.getHostname(), me);
        return me;
    }

    public static Integer getPlayerCount() {
        return pidCount;
    }
}
