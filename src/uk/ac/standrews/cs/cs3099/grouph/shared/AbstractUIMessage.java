package uk.ac.standrews.cs.cs3099.grouph.shared;

public abstract class AbstractUIMessage extends AbstractMessage {
    private final static String originator = "UI";
    public AbstractUIMessage(int index) {
        super(index, originator);
    }
}
