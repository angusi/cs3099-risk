package uk.ac.standrews.cs.cs3099.grouph.shared;

import java.util.AbstractQueue;

public interface IComponent extends Runnable {
    
	public AbstractQueue<AbstractMessage> getSink(IComponent component);
	public void setSource(AbstractQueue<AbstractMessage> queue, IComponent component);
}
