package uk.ac.standrews.cs.cs3099.grouph.shared;

public abstract class AIMessage extends AbstractMessage {
	private final static String originator = "AI";

	public AIMessage(int index) {
		super(index, originator);
	}
}
