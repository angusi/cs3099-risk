package uk.ac.standrews.cs.cs3099.grouph.shared;

public abstract class AbstractEngineMessage extends AbstractMessage {
    private final static String originator = "ENG";
    public AbstractEngineMessage(int index) {
        super(index, originator);
    }
}
