package uk.ac.standrews.cs.cs3099.grouph.shared;

public abstract class AbstractMessage {
    public final String name;
    public final String id;
    public final String originator;
    public final long timestamp;
    
    public AbstractMessage(int index, String originator) {
        this.name = this.getClass().getSimpleName();
        this.originator = originator;
        this.timestamp = System.currentTimeMillis();
        this.id = originator + index;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AbstractMessage message = (AbstractMessage) o;

        if (!id.equals(message.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }
}
