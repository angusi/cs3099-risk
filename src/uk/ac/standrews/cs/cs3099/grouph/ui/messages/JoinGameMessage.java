package uk.ac.standrews.cs.cs3099.grouph.ui.messages;

import uk.ac.standrews.cs.cs3099.grouph.shared.AbstractUIMessage;

/**
 * A message indicating that we wish to join an existing game on the network
 */
public class JoinGameMessage extends AbstractUIMessage {

    /**
     * The hostname of the server running the game
     */
    private final String hostname;

    /**
     * Constructs a message telling the engine that we wish to join an existing game on the network
     * @param index A unique identifier for the message
     * @param hostname The hostname of the server running the game
     */
    public JoinGameMessage(int index, String hostname) {
        super(index);
        this.hostname = hostname;
    }

    /**
     * Gets the hostname of the server running the game
     * @return The hostname of the server running the game
     */
    public String getHostname() {
        return hostname;
    }
}
