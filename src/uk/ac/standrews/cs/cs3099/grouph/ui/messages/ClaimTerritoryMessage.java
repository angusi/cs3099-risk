package uk.ac.standrews.cs.cs3099.grouph.ui.messages;

import uk.ac.standrews.cs.cs3099.grouph.shared.AbstractUIMessage;

/**
 * A message indicating we wish to claim a specific territory (during the setup phase)
 */
public class ClaimTerritoryMessage extends AbstractUIMessage {

    /**
     * The country we are trying to claim
     */
    private final int countryId;

    /**
     * Constructs a new message for the engine telling them we wish to claim a territory
     * @param index A unique message identifier
     * @param countryId The ID of the country we are trying to claim
     */
    public ClaimTerritoryMessage(int index, int countryId) {
        super(index);
        this.countryId = countryId;
    }

    /**
     * Gets the ID of the country we are trying to claim
     * @return The ID of the country we are trying to claim
     */
    public int getCountryId() {
        return countryId;
    }
}
