package uk.ac.standrews.cs.cs3099.grouph.ui;

import fi.iki.elonen.NanoHTTPD;

import java.io.*;
import java.nio.file.Files;
import java.util.regex.Pattern;
import uk.ac.standrews.cs.cs3099.grouph.ui.interfaces.IFileServer;

/**
 * This class is responsible for serving static files
 */
public class FileServer implements IFileServer {

    /**
     * This pattern checks for "../" in the path, which would allow directory traversal
     */
    private static final Pattern DIRECTORY_TRAVERSAL = Pattern.compile("\\.\\.[/\\\\]");

    /**
     * Creates a new instance of the File Server
     */
    public FileServer() {
        super();
    }

    /**
     * Loads files from the local store and serves them.
     * Also automatically loads directory index file, index.html, if path ends in /
     * @param session The HTTP Session
     * @return A NanoHTTPD Response object with data for the client
     */
    @Override
    public NanoHTTPD.Response serve(final NanoHTTPD.IHTTPSession session) {
        final NanoHTTPD.Response response;
        FileInputStream fileInputStream = null;
        NanoHTTPD.Response.Status responseStatus = NanoHTTPD.Response.Status.OK;
        String contentType;

        String path = session.getUri();
        if(path.substring(path.length()-1).equals("/")) {
            path = path + "index.html";
        }

        path = DIRECTORY_TRAVERSAL.matcher(path).replaceAll("");

        File fileToServe = new File("./res/web-assets/"+path);
        if(!fileToServe.exists()) {
            fileToServe = new File("./res/web-assets/errors/404.html");
            responseStatus = NanoHTTPD.Response.Status.NOT_FOUND;
        }
        try {
            fileInputStream = new FileInputStream(fileToServe);
        } catch (final FileNotFoundException e) {
            e.printStackTrace();
        }
        try {
            contentType = Files.probeContentType(fileToServe.toPath());
        } catch (final IOException e) {
            e.printStackTrace();
            contentType = "text/plain";
        }
        response = new NanoHTTPD.Response(responseStatus, contentType, fileInputStream);
        return response;
    }
}
