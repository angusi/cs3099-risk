package uk.ac.standrews.cs.cs3099.grouph.ui.interfaces;

/**
 * Classes which implement this interface are responsible for sending messages to the UI.
 * Typically this is through a WebSocket or by filling a queue which the UI polls.
 */
public interface IUIMessageHandler extends Runnable {
}
