package uk.ac.standrews.cs.cs3099.grouph.ui.builders;

import uk.ac.standrews.cs.cs3099.grouph.ui.Server;
import uk.ac.standrews.cs.cs3099.grouph.ui.UIComponent;

/**
 * Builds a new Server instance
 */
public class ServerBuilder {
    /**
     * The port the server listens on
     */
    private int port;
    /**
     * The UI Component this Server is a sub-component of
     */
    private UIComponent uiComponent;

    /**
     * Defines the port the server listens on
     * @param port The port the server listens on
     * @return The modified ServerBuilder object
     */
    public ServerBuilder setPort(final int port) {
        this.port = port;
        return this;
    }

    /**
     * Defines the UI Component this Server is a sub-component of
     * @param uiComponent The UI Component this Server is a sub-component of
     * @return The modified ServerBuilder object
     */
    public ServerBuilder setUiComponent(final UIComponent uiComponent) {
        this.uiComponent = uiComponent;
        return this;
    }

    /**
     * Creates a new ServerBuilder instance
     * @return A new ServerBuilder instance
     */
    public Server createServer() {
        return new Server(port, uiComponent);
    }
}