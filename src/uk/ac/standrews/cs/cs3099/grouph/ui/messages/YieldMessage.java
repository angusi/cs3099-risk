package uk.ac.standrews.cs.cs3099.grouph.ui.messages;

import uk.ac.standrews.cs.cs3099.grouph.shared.AbstractUIMessage;

/**
 * A message saying we are yielding to another player.
 * This message is sent when we are ending our turn without fortifying any territories
 */
public class YieldMessage extends AbstractUIMessage {
    /**
     * Constructs a new message to the engine to declare we are not fortifying and are ending our turn
     * @param index A unique identifier for this message
     */
    public YieldMessage(int index) {
        super(index);
    }
}
