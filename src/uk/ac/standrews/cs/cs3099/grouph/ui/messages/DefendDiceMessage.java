package uk.ac.standrews.cs.cs3099.grouph.ui.messages;

import uk.ac.standrews.cs.cs3099.grouph.shared.AbstractUIMessage;

/**
 * A message indicating that we wish to defend an attack with the specified number of dice
 */
public class DefendDiceMessage extends AbstractUIMessage {
    /**
     * The number of dice we wish to defend with
     */
    private final int numDice;

    /**
     * Constructs a new message indicating to the engine that we wish to defend an attack with the specified number
     * of dice
     * @param index A unique identifier for the message
     * @param numDice The number of dice we wish to defend with
     */
    public DefendDiceMessage(int index, int numDice) {
        super(index);
        this.numDice = numDice;
    }

    /**
     * Gets the number of dice we wish to defend with
     * @return The number of dice we wish to defend with
     */
    public int getNumDice() {
        return numDice;
    }
}
