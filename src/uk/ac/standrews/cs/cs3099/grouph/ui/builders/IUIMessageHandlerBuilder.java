package uk.ac.standrews.cs.cs3099.grouph.ui.builders;

import uk.ac.standrews.cs.cs3099.grouph.ui.UIComponent;
import uk.ac.standrews.cs.cs3099.grouph.ui.UIMessageHandler;
import uk.ac.standrews.cs.cs3099.grouph.ui.WebSocketEndpoint;
import uk.ac.standrews.cs.cs3099.grouph.ui.interfaces.IUIMessageHandler;

/**
 * Builder for an IUIMessageHandler
 */
public class IUIMessageHandlerBuilder {
    /**
     * The WebSocket this Message Handler sends communications down
     */
    private WebSocketEndpoint webSocketEndpoint;
    /**
     * The UI Component this Message Handler is a sub-component of
     */
    private UIComponent uiComponent;

    /**
     * Defines the WebSocket this Message Handler sends communications down.
     * @param webSocketEndpoint The WebSocket this Message Handler sends communications down.
     * @return The modified IUIMessageHandlerBuilder
     */
    public IUIMessageHandlerBuilder setWebSocketEndpoint(WebSocketEndpoint webSocketEndpoint) {
        this.webSocketEndpoint = webSocketEndpoint;
        return this;
    }

    /**
     * Defines the UI Component this Message Handler is a sub-component of
     * @param uiComponent The UI Component this Message Handler is a sub-component of
     * @return The modified IUIMessageHandlerBuilder
     */
    public IUIMessageHandlerBuilder setUiComponent(UIComponent uiComponent) {
        this.uiComponent = uiComponent;
        return this;
    }

    /**
     * Builds a new IUIMessageHandler
     * @return A new concrete IUIMessageHandler instance
     */
    public IUIMessageHandler createUIMessageHandler() {
        return new UIMessageHandler(webSocketEndpoint, uiComponent);
    }
}