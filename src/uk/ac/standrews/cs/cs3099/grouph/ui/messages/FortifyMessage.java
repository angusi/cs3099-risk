package uk.ac.standrews.cs.cs3099.grouph.ui.messages;



import uk.ac.standrews.cs.cs3099.grouph.shared.AbstractUIMessage;


/**
 * A message indicating that we wish to fortify a territory.
 * This message is sent in the "fortify" phase of the turn, the last phase before yielding to another player.
 */


public class FortifyMessage extends AbstractUIMessage {

    /**
     * The ID of country we are moving units from
     */
     private final int sourceCountry;
    /**
     * The ID of the country we are moving units to
     */
     private final int destinationCountry;
    /**
     * The number of units we wish to move
     */
     private final int numberOfArmies;
 
    /**
     * Constructs a new message telling the engine we wish to make a fortification
     * @param index A unique identifier for the message
     * @param sourceCountry The ID of country we wish to move units from
     * @param destinationCountry The ID of country we wish to move units to
     * @param numberOfArmies The number of units we wish to move
     */
     public FortifyMessage(int index, int sourceCountry, int destinationCountry, int numberOfArmies) {
         super(index);
         this.sourceCountry = sourceCountry;
         this.numberOfArmies = numberOfArmies;
         this.destinationCountry = destinationCountry;
     }
 
    /**
     * Gets the ID of the country we wish to move units from
     * @return The ID of the contry we wish to move units from
     */
     public int getSourceCountry() {
         return sourceCountry;
     }
 
    /**
     * Gets the Id of the country we wish to move units to
     * @return The ID of the country we wish to move units to
     */
     public int getDestinationCountry() {
         return destinationCountry;
     }
 
    /**
     * Gets the number of units we wish to move
Add a comment to this line
     * @return The number of units we wish to move
     */
     public int getNumberOfArmies() {
         return numberOfArmies;
     }
 }
