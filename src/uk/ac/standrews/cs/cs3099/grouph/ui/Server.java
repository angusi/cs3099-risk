package uk.ac.standrews.cs.cs3099.grouph.ui;

import fi.iki.elonen.NanoHTTPD;
import uk.ac.standrews.cs.cs3099.grouph.ui.builders.IFrontendAPIBuilder;
import uk.ac.standrews.cs.cs3099.grouph.ui.builders.IFileServerBuilder;
import uk.ac.standrews.cs.cs3099.grouph.ui.interfaces.IFileServer;
import uk.ac.standrews.cs.cs3099.grouph.ui.interfaces.IFrontendAPI;

import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * The UI Server.
 *
 * This class handles interaction with the frontend API and file requests.
 * This server is a subclass of the NanoHTTPD server.
 */
public class Server extends NanoHTTPD {
    /**
     * The Frontend API handler
     */
    private final IFrontendAPI apiEndpoint;
    /**
     * The front end file request handler
     */
    private final IFileServer fileServer;

    /**
     * Instantiate a new server object
     * @param port The port to listen on
     * @param uiComponent The UI Component this Server is a sub-component of
     */
    public Server(final int port, final UIComponent uiComponent) {
        super(port);
        this.apiEndpoint = new IFrontendAPIBuilder().setUiComponent(uiComponent).createAPIEndpoint();
        this.fileServer = new IFileServerBuilder().createFileServer();
    }

    /**
     * Start the UI Server listening.
     * @throws java.io.IOException Thrown if the server could not be started
     */
    public void startServer() throws IOException {
            this.start();
    }

    /**
     * Delegates handling responses to either an IFrontendAPI or IFileServer
     * This method is automatically called whenever an incoming request is received.
     * @param session The HTTP session
     * @return A Response object, from either the API or as a File
     */
    @Override
    public Response serve(final IHTTPSession session) {
        final Response response;

        //This is here so that future methods can correctly get parameters!
        final Map<String, List<String>> decodedQueryParameters =
                decodeParameters(session.getQueryParameterString());

        int posOfSlash = session.getUri().indexOf("/", 1);
        if(posOfSlash == -1) {
            //If the slash is at -1, there is no slash.
            // This means there is only one URI segment - thus the entire string should be used.
            posOfSlash = session.getUri().length();
        }
        final String uriHandler = session.getUri().substring(0, posOfSlash);

        switch(uriHandler) {
            case "/api":
                response = apiEndpoint.serve(session);
                break;
            default:
                response = fileServer.serve(session);
                break;
        }

        return response;
    }
}
