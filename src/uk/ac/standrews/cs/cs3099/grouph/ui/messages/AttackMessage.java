package uk.ac.standrews.cs.cs3099.grouph.ui.messages;

import uk.ac.standrews.cs.cs3099.grouph.shared.AbstractUIMessage;

/**
 * A message indicating we wish to attack another territory
 */
public class AttackMessage extends AbstractUIMessage {

    /**
     * The ID of the territory we are attacking from
     */
    private final int sourceCountry;
    /**
     * The ID of the territory we wish to attack
     */
    private final int destinationCountry;
    /**
     * The number of units we wish to attack with
     */
    private final int numberOfArmies;

    /**
     * Constructs a new message for the engine to say we wish to attack another territory.
     * @param index The unique message identifier
     * @param sourceCountry The ID of the territory we are attacking from
     * @param destinationCountry The ID of the territory we wish to attack
     * @param numberOfArmies The number of units we wish to attack with
     */
    public AttackMessage(int index, int sourceCountry, int destinationCountry, int numberOfArmies) {
        super(index);
        this.sourceCountry = sourceCountry;
        this.destinationCountry = destinationCountry;
        this.numberOfArmies = numberOfArmies;
    }

    /**
     * Gets the ID of the territory we are attacking from
     * @return The ID of the territory we are attacking from
     */
    public int getSourceCountry() {
        return sourceCountry;
    }

    /**
     * Gets the ID of the territory we wish to attack
     * @return The ID of the territory we wish to attack
     */
    public int getDestinationCountry() {
        return destinationCountry;
    }

    /**
     * Gets the number of units we wish to attack with
     * @return The number of units we wish to attack with
     */
    public int getNumberOfArmies() {
        return numberOfArmies;
    }
}
