package uk.ac.standrews.cs.cs3099.grouph.ui;
import fi.iki.elonen.NanoHTTPD;
import org.json.*;
import uk.ac.standrews.cs.cs3099.grouph.shared.AbstractMessage;
import uk.ac.standrews.cs.cs3099.grouph.shared.AbstractUIMessage;
import uk.ac.standrews.cs.cs3099.grouph.ui.interfaces.IFrontendAPI;
import uk.ac.standrews.cs.cs3099.grouph.ui.messages.*;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

/**
 * The frontend API manager.
 * Handles requests from clients on each endpoint.
 * @version 1.0
 */
public class APIEndpoint implements IFrontendAPI {
    /**
     * The UI Component this endpoint is a sub-component of
     */
    private final UIComponent UICOMPONENT;
    /**
     * The logger class
     */
    private final Logger logger = Logger.getLogger(APIEndpoint.class.getSimpleName());

    /**
     * Constructs a new concrete API Endpoint Manager.
     * This is API Version 1.0
     * @param uiComponent The UI Component this endpoint is a sub-component of
     */
    public APIEndpoint(final UIComponent uiComponent) {
        super();
        this.UICOMPONENT = uiComponent;
    }

    /**
     * The routing method for API requests
     * @param session The HTTP Session
     * @return The NanoHTTPD response object with data for the client
     */
    @SuppressWarnings("OverlyComplexMethod")
    public NanoHTTPD.Response serve(final NanoHTTPD.IHTTPSession session) {
        final NanoHTTPD.Response response;

        switch(session.getUri()) {
            case "/api/v1/create":
                response = new NanoHTTPD.Response(this.create(session).toString());
                break;
            case "/api/v1/join":
                response = new NanoHTTPD.Response(this.join(session).toString());
                break;
            case "/api/v1/play":
                response = new NanoHTTPD.Response(this.play().toString());
                break;
            case "/api/v1/claim":
                response = new NanoHTTPD.Response(this.claim(session).toString());
                break;
            case "/api/v1/ping":
                response = new NanoHTTPD.Response(this.ping().toString());
                break;
            case "/api/v1/status":
                response = new NanoHTTPD.Response(this.status().toString());
                break;
            case "/api/v1/poll":
                response = new NanoHTTPD.Response(this.poll().toString());
                break;
            case "/api/v1/defend":
                response = new NanoHTTPD.Response(this.defend(session).toString());
                break;
            case "/api/v1/attack":
                response = new NanoHTTPD.Response(this.attack(session).toString());
                break;
            case "/api/v1/deploy":
                response = new NanoHTTPD.Response(this.deploy(session).toString());
                break;
            case "/api/v1/fortify":
                response = new NanoHTTPD.Response(this.fortify(session).toString());
                break;
            case "/api/v1/yield":
                response = new NanoHTTPD.Response(this.yield(session).toString());
                break;
            case "/api/v1/tradecards":
                response = new NanoHTTPD.Response(this.tradeCards(session).toString());
                break;
            default:
                response = new NanoHTTPD.Response(this.errorNotFound().toString());
                response.setStatus(NanoHTTPD.Response.Status.NOT_FOUND);
                break;
        }

        response.setMimeType("text/json");
        return response;
    }

    /**
     * Claim a territory, during setup phase
     * @param session The HTTP Session
     * @return JSON, either status OK or fail with reason
     */
    private JSONObject claim(final NanoHTTPD.IHTTPSession session) {
        logger.info("Claim notification received");
        final JSONObject responseObject = new JSONObject();

        final Map<String, String> files = new HashMap<>();
        try {
            session.parseBody(files);
        } catch (IOException | NanoHTTPD.ResponseException e) {
            e.printStackTrace();
            return responseObject.put("status", "fail").put("reason", "IO Exception");
        }
        final Map<String, String> params = session.getParms();

        if(params.get("countryId") != null) {
            final ClaimTerritoryMessage claimTerritoryMessage = new ClaimTerritoryMessage(UICOMPONENT.getSinkCounter(),
                    Integer.parseInt(params.get("countryId")));

            UICOMPONENT.sendMessage(claimTerritoryMessage);

            responseObject.put("status", "ok");
        } else {
            logger.warning("No country specified in claim request!");
        }

        return responseObject;
    }

    /**
     * Start game play (when hosting)
     * @return A JSON object with status ok
     */
    private JSONObject play() {
        final JSONObject responseObject = new JSONObject();

        final GameStartMessage gameStartMessage = new GameStartMessage(UICOMPONENT.getSinkCounter());

        UICOMPONENT.sendMessage(gameStartMessage);
        responseObject.put("status", "ok");

        return responseObject;
    }

    /**
     * Request to trade cards
     * @param session The HTTP Session
     * @return JSON, either status OK or fail with reason
     */
    private JSONObject tradeCards(final NanoHTTPD.IHTTPSession session) {
        final JSONObject responseObject = new JSONObject();

        final Map<String, String> files = new HashMap<>();
        try {
            session.parseBody(files);
        } catch (IOException | NanoHTTPD.ResponseException e) {
            e.printStackTrace();
            return responseObject.put("status", "fail").put("reason", "IO Exception");
        }
        final Map<String, String> params = session.getParms();

        final JSONArray cards = new JSONArray(params.get("cards"));
        TradeMessage.CardTypes cardTypes = null;

        final TradeMessage tradeMessage = new TradeMessage(UICOMPONENT.getSinkCounter());
        for(int i = 0; i < cards.length(); i++) {
            final JSONObject thisCard = cards.getJSONObject(i);
            final TradeMessage.CardTypes thisCardType = TradeMessage.CardTypes.typeFromInt(thisCard.getInt("cardType"));

            tradeMessage.addCountryId(thisCard.getInt("countryId"));

            if(thisCardType != TradeMessage.CardTypes.WILD) {
                if(thisCardType != cardTypes) {
                    cardTypes = (cardTypes == null) ? thisCardType : TradeMessage.CardTypes.MIXED;
                }
            }
        }
        tradeMessage.setCardType(cardTypes);

        UICOMPONENT.sendMessage(tradeMessage);

        return responseObject;
    }

    /**
     * Yield turn to another player without fortifying
     * @param session The HTTP Session
     * @return JSON, either status OK or fail with reason
     */
    private JSONObject yield(final NanoHTTPD.IHTTPSession session) {
        final JSONObject responseObject = new JSONObject();

        final YieldMessage yieldMessage = new YieldMessage(UICOMPONENT.getSinkCounter());

        UICOMPONENT.sendMessage(yieldMessage);

        return responseObject;
    }

    /**
     * Fortify country
     * @param session The HTTP Session
     * @return JSON, either status OK or fail with reason
     */
    private JSONObject fortify(final NanoHTTPD.IHTTPSession session) {
        final JSONObject responseObject = new JSONObject();

        final Map<String, String> files = new HashMap<>();
        try {
            session.parseBody(files);
        } catch (IOException | NanoHTTPD.ResponseException e) {
            e.printStackTrace();
            return responseObject.put("status", "fail").put("reason", "IO Exception");
        }
        final Map<String, String> params = session.getParms();

        final FortifyMessage fortifyMessage = new FortifyMessage(UICOMPONENT.getSinkCounter(), Integer.parseInt(params.get("sourceCountry")),
                Integer.parseInt(params.get("destinationCountry")), Integer.parseInt(params.get("armyCount")));

        UICOMPONENT.sendMessage(fortifyMessage);

        return responseObject;
    }

    /**
     * Deploy (or reinforce - both use same endpoint) to country
     * @param session The HTTP Session
     * @return JSON, either status OK or fail with reason
     */
    private JSONObject deploy(final NanoHTTPD.IHTTPSession session) {
        final JSONObject responseObject = new JSONObject();

        final Map<String, String> files = new HashMap<>();
        try {
            session.parseBody(files);
        } catch (IOException | NanoHTTPD.ResponseException e) {
            e.printStackTrace();
            return responseObject.put("status", "fail").put("reason", "IO Exception");
        }
        final Map<String, String> params = session.getParms();

        final JSONArray deployJSON = new JSONArray(params.get("deploy"));
        final int[][] deployData = new int[deployJSON.length()][2];
        for(int i = 0; i < deployJSON.length(); i++) {
            final JSONArray thisDeployment = deployJSON.getJSONArray(i);
            deployData[i][0] = thisDeployment.getInt(0);
            deployData[i][1] = thisDeployment.getInt(1);
        }

        final DeployMessage deployMessage = new DeployMessage(UICOMPONENT.getSinkCounter(), deployData);

        UICOMPONENT.sendMessage(deployMessage);

        return responseObject;
    }

    /**
     * Attack someone else
     * @param session The HTTP Session
     * @return JSON, either status OK or fail with reason
     */
    private JSONObject attack(final NanoHTTPD.IHTTPSession session) {
        final JSONObject responseObject = new JSONObject();

        final Map<String, String> files = new HashMap<>();
        try {
            session.parseBody(files);
        } catch (IOException | NanoHTTPD.ResponseException e) {
            e.printStackTrace();
            return responseObject.put("status", "fail").put("reason", "IO Exception");
        }
        final Map<String, String> params = session.getParms();

        final AttackMessage attackMessage = new AttackMessage(UICOMPONENT.getSinkCounter(), Integer.parseInt(params.get("sourceCountry")),
                Integer.parseInt(params.get("destinationCountry")), Integer.parseInt(params.get("armyCount")));

        UICOMPONENT.sendMessage(attackMessage);

        return responseObject;
    }

    /**
     * Defend - choose the number of dice
     * @param session The HTTP Session
     * @return JSON, either status OK or fail with reason
     */
    private JSONObject defend(final NanoHTTPD.IHTTPSession session) {
        final JSONObject responseObject = new JSONObject();

        final Map<String, String> files = new HashMap<>();
        try {
            session.parseBody(files);
        } catch (IOException | NanoHTTPD.ResponseException e) {
            e.printStackTrace();
            return responseObject.put("status", "fail").put("reason", "IO Exception");
        }
        final Map<String, String> params = session.getParms();

        final DefendDiceMessage defendDiceMessage = new DefendDiceMessage(UICOMPONENT.getSinkCounter(), Integer.parseInt(params.get("numDice")));
        UICOMPONENT.sendMessage(defendDiceMessage);

        return responseObject;
    }

    /**
     * Request to create a new game.
     * Asks the Engine if we can start a new game.
     * @param session The HTTP Session
     * @return JSON, either status OK or fail with reason
     */
    private JSONObject create(final NanoHTTPD.IHTTPSession session) {
        final JSONObject responseObject = new JSONObject();

        if(!session.getMethod().equals(NanoHTTPD.Method.POST)) {
            return responseObject.put("status", "fail").put("reason", "Wrong HTTP Method");
        }

        final Map<String, String> files = new HashMap<>();
        try {
            session.parseBody(files);
        } catch (IOException | NanoHTTPD.ResponseException e) {
            e.printStackTrace();
            return responseObject.put("status", "fail").put("reason", "IO Exception");
        }
        final Map<String, String> params = session.getParms();

        final CreateGameMessage createGameMessage = new CreateGameMessage(UICOMPONENT.getSinkCounter(), Integer.parseInt(params.get("maxplayers")), params.get("map"), Integer.parseInt(params.get("hostport")));
        UICOMPONENT.sendMessage(createGameMessage);

        return responseObject;
    }

    /**
     * Ask the engine if we can join an existing game on the network
     * @param session The HTTP Session
     * @return JSON, either status OK or fail with reason
     */
    private JSONObject join(final NanoHTTPD.IHTTPSession session) {
        final JSONObject responseObject = new JSONObject();

        if(!session.getMethod().equals(NanoHTTPD.Method.POST)) {
            return responseObject.put("status", "fail").put("reason", "Wrong HTTP Method");
        }

        final Map<String, String> files = new HashMap<>();
        try {
            session.parseBody(files);
        } catch (IOException | NanoHTTPD.ResponseException e) {
            e.printStackTrace();
            return responseObject.put("status", "fail").put("reason", "IO Exception");
        }
        final Map<String, String> params = session.getParms();

        final JoinGameMessage joinGameMessage = new JoinGameMessage(UICOMPONENT.getSinkCounter(), params.get("hostname"));
        UICOMPONENT.sendMessage(joinGameMessage);

        return responseObject.put("status", "ok");
    }

    @Override
    public JSONObject ping() {
        final JSONObject responseObject = new JSONObject();
        final AbstractUIMessage pingMessage = new PingMessage(UICOMPONENT.getSinkCounter());
        UICOMPONENT.sendMessage(pingMessage);
        return responseObject.put("status", "ok").put("data", "pong");
    }

    @Override
    public JSONObject status() {
        final JSONObject responseObject = new JSONObject();
        //TODO: Ask the engine for game status
        return responseObject.put("status", "fail").put("reason", "Not Implemented");
    }

    @Override
    public JSONObject poll() {
        final JSONObject responseObject = new JSONObject();
        final AbstractMessage engineMessage = UICOMPONENT.getMessage();
        return (engineMessage == null) ?
                responseObject.put("status", "ok").put("messageid", 0) :
                responseObject.put("status", "ok").put("messageid", engineMessage.id);
    }



    @Override
    public JSONObject errorNotFound() {
        return new JSONObject().put("status", "fail").put("reason", "not found");
    }
}
