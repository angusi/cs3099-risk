package uk.ac.standrews.cs.cs3099.grouph.ui.messages;

import uk.ac.standrews.cs.cs3099.grouph.shared.AbstractUIMessage;

/**
 * A message indicating we would like to host a new game
 */
public class CreateGameMessage extends AbstractUIMessage {

    /**
     * The maximum number of players we wish to permit in the game
     */
    private final int maxPlayers;
    /**
     * The name of the map we wish to play on
     */
    private final String mapName;
    /**
     * The port we wish to start the server on
     */
    private final int port;

    /**
     * Constructs a new message asking the engine to start hosting a new game for us
     * @param index A unique identifier for the message
     * @param maxPlayers The maximum number of players we wish to permit in the game
     * @param mapName The name of the map we with to play on
     * @param port The port we wish to start the server on
     */
    public CreateGameMessage(int index, int maxPlayers, String mapName, int port) {
        super(index);
        this.maxPlayers = maxPlayers;
        this.mapName = mapName;
        this.port = port;
    }

    /**
     * Gets the maximum number of players we wish to permit in the game
     * @return The maximum number of players we wish to permit in the game
     */
    public int getMaxPlayers() {
        return maxPlayers;
    }

    /**
     * Gets the name of the map we wish to play on
     * @return The name of the map we wish to play on
     */
    public String getMapName() {
        return mapName;
    }

    /**
     * Gets the port we wish to start the server on
     * @return The port we wish to start the server on
     */
    public int getPort() {
        return port;
    }
}
