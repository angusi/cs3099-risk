package uk.ac.standrews.cs.cs3099.grouph.ui.messages;

import uk.ac.standrews.cs.cs3099.grouph.shared.AbstractUIMessage;

/**
 * A ping message, simply indicating that the connection is still active
 */
public class PingMessage extends AbstractUIMessage {
    /**
     * Constructs a new ping message for the engine indicating that the connection is still active
     * @param index A unique identifier for the message
     */
    public PingMessage(int index) {
        super(index);
    }
}
