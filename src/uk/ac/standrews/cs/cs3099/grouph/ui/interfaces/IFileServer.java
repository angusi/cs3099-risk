package uk.ac.standrews.cs.cs3099.grouph.ui.interfaces;

import fi.iki.elonen.NanoHTTPD;

/**
 * Classes which implement this are responsible for serving files
 */
public interface IFileServer {
    /**
     * Serve the requested file from the local file store
     * @param session The HTTP Session
     * @return The served file
     */
    NanoHTTPD.Response serve(NanoHTTPD.IHTTPSession session);
}
