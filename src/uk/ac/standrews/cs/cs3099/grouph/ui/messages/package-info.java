/**
 * Messages which are sent by the UI to other layers in the stack.
 */
package uk.ac.standrews.cs.cs3099.grouph.ui.messages;