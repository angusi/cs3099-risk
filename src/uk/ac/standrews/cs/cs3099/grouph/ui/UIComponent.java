
package uk.ac.standrews.cs.cs3099.grouph.ui;

import uk.ac.standrews.cs.cs3099.grouph.shared.IComponent;
import uk.ac.standrews.cs.cs3099.grouph.shared.IUIComponent;
import uk.ac.standrews.cs.cs3099.grouph.shared.AbstractMessage;
import uk.ac.standrews.cs.cs3099.grouph.ui.builders.ServerBuilder;
import uk.ac.standrews.cs.cs3099.grouph.ui.builders.IUIMessageHandlerBuilder;
import uk.ac.standrews.cs.cs3099.grouph.ui.builders.WebSocketEndpointBuilder;
import uk.ac.standrews.cs.cs3099.grouph.ui.interfaces.IUIMessageHandler;

import java.io.IOException;
import java.util.AbstractQueue;
import java.util.concurrent.ArrayBlockingQueue;

/**
 * The UI Component of the Risk stack.
 */
public class UIComponent implements IUIComponent {

    /**
     * Incoming message queue
     */
    private AbstractQueue<AbstractMessage> source;
    /**
     * Outgoing message queue
     */
    private final AbstractQueue<AbstractMessage> sink;
    /**
     * Number of messages sent out
     */
    private int sinkCounter = 0;
    /**
     * The request Server instance
     */
    private final Server uiServer;
    /**
     * The WebSocket which data is sent down, asynchronously
     */
    final WebSocketEndpoint webSocketEndpoint;
    /**
     * The Message handler, which deals with incoming messages (from the Engine)
     */
    private final IUIMessageHandler uiMessageHandler;

    /**
     * Instantiates a new UI Component.
     * @param port The port that the server listens on. Note that this and this+1 are used.
     */
    public UIComponent(final int port) {
        super();
        sink = new ArrayBlockingQueue<>(1000);
        uiServer = new ServerBuilder().setPort(port).setUiComponent(this).createServer();
        //TODO: Parameterise "port+1" for custom ports?
        this.webSocketEndpoint = new WebSocketEndpointBuilder().setPort(port + 1).createWebSocketEndpoint();
        this.uiMessageHandler = new IUIMessageHandlerBuilder().setWebSocketEndpoint(webSocketEndpoint)
                .setUiComponent(this).createUIMessageHandler();
    }

    /**
     * Gets this component's message sink
     * @param comp The component requesting the sink
     * @return The outgoing message queue (i.e. messages FROM the UI)
     */
    @Override
    public AbstractQueue<AbstractMessage> getSink(final IComponent comp) {
        return sink;
    }

    /**
     * Sets this component's message source
     * @param queue The incoming message source (i.e. messages TO the UI)
     * @param comp The component setting the source
     */
    @Override
    public void setSource(final AbstractQueue<AbstractMessage> queue, final IComponent comp) {
        //if(comp instanceof "Engine") {
            source = queue;
        //} else {
        // Error condition
        // }
    }

    /**
     * Sends a message from the UI to the listening sink
     * @param message The message to send
     */
    void sendMessage(final AbstractMessage message) {
        sink.add(message);
    }

    /**
     * Gets the oldest message in the incoming message queue
     * @return A message, if one is waiting, or null otherwise.
     */
    AbstractMessage getMessage() {
        return (source.peek() != null) ? source.poll() : null;
    }

    /**
     * Gets the next message ID for messages in the sink.
     * Should be called when constructing messages to get the next ID.
     * @return The number of the next message.
     */
    public int getSinkCounter() {
        return sinkCounter++;
    }

    /**
     * Start the UI Component's sub-components
     */
    @Override
    public void run() {
        try {
            uiServer.startServer();
            webSocketEndpoint.start();
            new Thread(uiMessageHandler).start();
        } catch(final IOException e) {
            e.printStackTrace();
        }
    }
}
