package uk.ac.standrews.cs.cs3099.grouph.ui.builders;

import uk.ac.standrews.cs.cs3099.grouph.ui.FileServer;
import uk.ac.standrews.cs.cs3099.grouph.ui.interfaces.IFileServer;

/**
 * Builder for an IFileServer
 */
public class IFileServerBuilder {
 /**
  * Creates a new instance of a IFileServer
  * @return A concrete IFileServer instance
  */
 public IFileServer createFileServer() {
  return new FileServer();
 }
}