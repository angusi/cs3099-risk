package uk.ac.standrews.cs.cs3099.grouph.ui;

import org.json.JSONArray;
import org.json.JSONObject;
import uk.ac.standrews.cs.cs3099.grouph.engine.messages.requests.*;
import uk.ac.standrews.cs.cs3099.grouph.engine.messages.responses.ResponseMessage;
import uk.ac.standrews.cs.cs3099.grouph.shared.AbstractMessage;
import uk.ac.standrews.cs.cs3099.grouph.shared.struct.players.Player;

import java.io.IOException;
import java.util.List;
import java.util.Queue;
import uk.ac.standrews.cs.cs3099.grouph.ui.interfaces.IUIMessageHandler;

/**
 * Handles messages sent to the UI
 */
public class UIMessageHandler implements IUIMessageHandler {

    /**
     * The WebSocket instance that any messages forwarded to the UI frontend will be sent down
     */
    private final WebSocketEndpoint webSocketEndpoint;
    /**
     * The UI Component that this is a sub-component of
     */
    private final UIComponent uiComponent;

    /**
     * Constructs a new UI Message Handler
     * @param webSocketEndpoint The WebSocket instance that any messages forwarded to the UI frontend will be sent down
     * @param uiComponent The UI Component that this is a sub-component of
     */
    public UIMessageHandler(final WebSocketEndpoint webSocketEndpoint, final UIComponent uiComponent) {
        super();
        this.webSocketEndpoint = webSocketEndpoint;
        this.uiComponent = uiComponent;
    }

    /**
     * Starts the Message processer
     */
    @Override
    public void run() {
        //noinspection InfiniteLoopStatement
        while(true) {
            final AbstractMessage message = uiComponent.getMessage();
            if(message != null) {
                try {
                    final JSONObject response;

                    switch(message.name) {
                        case "ResponseMessage":
                            response = this.handleResponseMessage((ResponseMessage) message);
                            break;
                        case "RequestJoin":
                            response = this.handleRequestJoin((RequestJoin) message);
                            break;
                        case "RequestJoinResult":
                            response = this.handleRequestJoinResult((RequestJoinResult) message);
                            break;
                        case "RequestLobbyUpdate":
                            response = this.handleRequestLobbyUpdate((RequestLobbyUpdate) message);
                            break;
                        case "RequestMapTransfer":
                            response = this.handleRequestMapTransfer((RequestMapTransfer) message);
                            break;
                        case "RequestInitGame":
                            response = this.handleRequestInitGame((RequestInitGame) message);
                            break;
                        case "RequestTurnOver":
                            response = this.handleRequestTurnOver((RequestTurnOver) message);
                            break;
                        case "RequestAttack":
                            response = this.handleRequestAttack((RequestAttack) message);
                            break;
                        case "RequestDefend":
                            response = this.handleRequestDefend((RequestDefend) message);
                            break;
                        case "RequestRoll":
                            response = this.handleRequestRoll((RequestRoll) message);
                            break;
                        case "RequestRollResult":
                            response = this.handleRequestRollResult((RequestRollResult) message);
                            break;
                        case "RequestTerritoryChange":
                            response = this.handleRequestTerritoryChange((RequestTerritoryChange) message);
                            break;
                        case "RequestDrawCard":
                            response = this.handleRequestDrawCard((RequestDrawCard) message);
                            break;
                        case "RequestCheat":
                            response = this.handleRequestCheat((RequestCheat) message);
                            break;
                        case "RequestBattleResult":
                            response = this.handleRequestBattleResult((RequestBattleResult) message);
                            break;
                        case "RequestReinforceCount":
                            response = this.handleRequestReinforceCount((RequestReinforceCount) message);
                            break;
                        case "RequestTracer":
                            response = this.handleRequestTracer((RequestTracer) message);
                            break;
                        case "RequestTrade":
                            response = this.handleRequestTrade((RequestTrade) message);
                            break;
                        default:
                            response = new JSONObject();
                            response.put("status", "fail").put("reason", "Unknown Response").put("type", message.name);
                    }

                    response.put("id", message.id);
                    webSocketEndpoint.sendMessage(response.toString());
                } catch (final IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * Generic responses to requests
     * @param message The message from the engine
     * @return a JSON string to send to the user
     */
    private JSONObject handleResponseMessage(final ResponseMessage message) {
        switch(message.trigger.name) {
            case "CreateGameMessage":
                if(message.code == ResponseMessage.Code.ACCEPT) {
                    return new JSONObject().put("status", "ok").put("type", "gameCreated");
                }
                break;
            case "GameStartMessage":
                if(message.code == ResponseMessage.Code.ACCEPT) {
                    return new JSONObject().put("status", "ok").put("type", "gameStarted");
                }
                break;
        }
        return (message.code == ResponseMessage.Code.ACCEPT) ?
                new JSONObject().put("status", "ok").put("type", "ack") :
                new JSONObject().put("status", "fail").put("reason", "Unknown Response")
                        .put("type", message.trigger.name).put("messageCode", message.code);
    }

    /**
     * Notification of a game join
     * @param message The message from the engine
     * @return a JSON string to send to the user
     */
    private JSONObject handleRequestJoin(final RequestJoin message) {
        return new JSONObject().put("status", "ok").put("type", "joinNotification");
    }

    /**
     * Incoming game map
     * @param message The message from the engine
     * @return a JSON string to send to the user
     */
    private JSONObject handleRequestMapTransfer(final RequestMapTransfer message) {
        final JSONObject map = new JSONObject(message.getJsonMapString());
        return new JSONObject().put("status", "ok").put("type", "mapTransfer").put("payload", map);
    }

    /**
     * Notification that the game is commencing
     * @param message The message from the engine
     * @return a JSON string to send to the user
     */
    private JSONObject handleRequestInitGame(final RequestInitGame message) {
        final Queue<Integer> playerOrderById = message.getPlayerOrderById();
        final JSONArray playOrder = new JSONArray();
        playerOrderById.forEach(playOrder::put);
        return new JSONObject().put("status", "ok").put("type", "gameStarted").put("playOrder", playOrder);
    }

    /**
     * Notification of an attack between two players
     * @param message The message from the engine
     * @return a JSON string to send to the user
     */
    private JSONObject handleRequestAttack(final RequestAttack message) {
        return new JSONObject().put("status", "ok").put("type", "attackNotification")
                .put("fromPlayerId", message.getAttackingPlayer().getId()).put("fromCountryId", message.getAttackingCountryId())
                .put("toPlayerId", message.getDefendingPlayer().getId()).put("toCountryId", message.getDefendingCountryId())
                .put("armyCount", message.getAttackingArmyCount());
    }

    /**
     * Notification that a player is defending an attack
     * @param message The message from the engine
     * @return a JSON string to send to the user
     */
    private JSONObject handleRequestDefend(final RequestDefend message) {
        return new JSONObject().put("status", "ok").put("type", "defendNotification")
                .put("fromPlayerId", message.getDefendingPlayer()).put("armyCount", message.getDefendingArmyCount());
    }

    /**
     * Notification that we are being asked to roll dice
     * @param message The message from the engine
     * @return a JSON string to send to the user
     */
    private JSONObject handleRequestRoll(final RequestRoll message) {
        return new JSONObject().put("status", "ok").put("type", "rollRequestNotification").put("numDice", message.getNumberOfDice());
    }

    /**
     * The results of a dice roll
     * @param message The message from the engine
     * @return a JSON string to send to the user
     */
    private JSONObject handleRequestRollResult(final RequestRollResult message) {
        final List<Integer> rollResultsList = message.getRollResults();
        final JSONArray rollResults = new JSONArray();
        rollResultsList.forEach(rollResults::put);
        return new JSONObject().put("status", "ok").put("type", "rollResult").put("results", rollResults);
    }

    /**
     * A territory is changing ruler or unit count
     * @param message The message from the engine
     * @return a JSON string to send to the user
     */
    private JSONObject handleRequestTerritoryChange(final RequestTerritoryChange message) {
        return new JSONObject().put("status", "ok").put("type", "territoryChangeNotification")
                .put("countryId", message.getCountryId()).put("playerId", message.getPlayer().getId())
                .put("armyCount", message.getAdvancingArmyCount());
    }

    /**
     * A card has been drawn
     * @param message The message from the engine
     * @return a JSON string to send to the user
     */
    private JSONObject handleRequestDrawCard(final RequestDrawCard message) {
        return new JSONObject().put("status", "ok").put("type", "cardDrawNotification")
                .put("playerId", message.getPlayer()).put("countryId", Integer.parseInt(message.getCard().getCountryId()))
                .put("cardType", message.getCard().getType());
    }

    /**
     * A player has been identified as a cheater
     * @param message The message from the engine
     * @return a JSON string to send to the user
     */
    private JSONObject handleRequestCheat(final RequestCheat message) {
        return new JSONObject().put("status", "ok").put("type", "cheatNotification")
                .put("playerId", message.getPlayer().getId());
    }

    /**
     * Our attempt to join a game has been either accepted or rejected
     * @param message The message from the engine
     * @return a JSON string to send to the user
     */
    private JSONObject handleRequestJoinResult(final RequestJoinResult message) {
        return new JSONObject().put("status", message.getAccepted() ? "ok" : "fail")
                .put("type", "joinAccepted");
    }

    /**
     * The lobby is to be updated - typically a player has joined or left
     * @param message The message from the engine
     * @return a JSON string to send to the user
     */
    private JSONObject handleRequestLobbyUpdate(final RequestLobbyUpdate message) {
        final JSONObject response = new JSONObject().put("status", "ok");
        response.put("type", "lobbyUpdate");
        response.put("myPlayerId", message.getMe().getId());

        final JSONArray players = new JSONArray();
        for(final Player player : message.getPlayerSet()) {
            final JSONObject newPlayer = new JSONObject().put("playerId", player.getId());
            players.put(newPlayer);
        }
        response.put("players", players);

        return response;
    }

    /**
     * The turn has changed
     * @param message The message from the engine
     * @return a JSON string to send to the user
     */
    private JSONObject handleRequestTurnOver(final RequestTurnOver message) {
        final JSONObject response = new JSONObject().put("status", "ok").put("type", "turnNotification")
                .put("newPlayer", message.getNextPlayer().getId()).put("phase", message.getPhase().getClaimPhaseNumber());

        if(message.getPlayer() != null) {
            response.put("oldPlayer", message.getPlayer().getId());
        }

        return response;
    }

    /**
     * A battle has ended
     * @param message The message from the engine
     * @return a JSON string to send to the user
     */
    private JSONObject handleRequestBattleResult(final RequestBattleResult message) {
        return  new JSONObject().put("status", "ok").put("type", "battleResult")
                .put("attackingPlayer", message.getAttackingPlayer().getId()).put("attackingCountry", message.getAttackingCountryId())
                .put("attackingLosses", message.getAttackingLosses()).put("defendingPlayer", message.getDefendingPlayer().getId())
                .put("defendingCountry", message.getDefendingCountryId()).put("defendingLosses", message.getDefendingLosses());
    }

    /**
     * We are being given some armies to place
     * @param message The message from the engine
     * @return a JSON string to send to the user
     */
    private JSONObject handleRequestReinforceCount(final RequestReinforceCount message) {
        return new JSONObject().put("status", "ok").put("type", "requestReinforceCount")
                .put("armyCount", message.getArmyCount());
    }

    /**
     * A tracer message from higher up the chain
     * @param message The message from the engine
     * @return a JSON string to send to the user
     */
    private JSONObject handleRequestTracer(final RequestTracer message) {
        return new JSONObject().put("status", "ok").put("type", "requestTracer")
                .put("tracer", message.getTracer());
    }

    /**
     * A player is trading cards
     * @param message The message from the engine
     * @return a JSON string to send to the user
     */
    private JSONObject handleRequestTrade(final RequestTrade message) {
        final JSONObject response = new JSONObject().put("status", "ok").put("type", "requestTrade")
                .put("player", message.getPlayer().getId());
        final JSONArray countryIds = new JSONArray();
        message.getCardContinentIdSet().forEach(countryIds::put);
        response.put("countryIds", countryIds);
        return response;
    }
}
