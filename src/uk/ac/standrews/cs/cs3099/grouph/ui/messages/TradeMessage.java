package uk.ac.standrews.cs.cs3099.grouph.ui.messages;

import uk.ac.standrews.cs.cs3099.grouph.shared.AbstractUIMessage;

import java.util.ArrayList;

/**
 * A message indicating that we wish to trade in some cards
 */
public class TradeMessage extends AbstractUIMessage {

    /**
     * This enumerated type matches the enum in the client-side Javascript
     * for identifying card types, with the addition of the MIXED(4) type
     */
    public enum CardTypes {
        /**
         * The type of card being traded is infantry
         */
        INFANTRY(0),
        /**
         * The type of card being traded is cavalry
         */
        CAVALRY(1),
        /**
         * The type of card being traded is artillery
         */
        ARTILLERY(2),
        /**
         * The type of card being traded is wild.
         */
        WILD(3),
        /**
         * The type of cards being traded is not restricted to a single type.
         * Typically this means one of each non-wild type.
         */
        MIXED(4);

        /**
         * The numeric representation, matching the client-side Javascript mapping
         */
        final int cardType;

        /**
         * Creates a new CardType instance
         * @param cardType the numeric representation of the card
         */
        CardTypes(final int cardType) {
            this.cardType = cardType;
        }

        /**
         * Gets a CardType from the numeric representation of the card
         * @param i The numeric representation of the card
         * @return A CardType
         */
        public static CardTypes typeFromInt(final int i) {
            switch(i) {
                case 0:
                    return INFANTRY;
                case 1:
                    return CAVALRY;
                case 2:
                    return ARTILLERY;
                case 3:
                    return WILD;
                case 4:
                    return MIXED;
                default:
                    return null;
            }
        }
    }

    /**
     * The type of cards involved in the trade
     */
    private CardTypes cardType;
    /**
     * The IDs of the countries on each card.
     * This is essentially what identifies each individual card.
     */
    private final ArrayList<Integer> countryIds;

    /**
     * Constructs a new message to tell the engine we wish to trade in some cards
     * @param index A unique identifier for this message
     */
    public TradeMessage(final int index) {
        super(index);
        countryIds = new ArrayList<>();
    }

    /**
     * Adds a country ID to the set.
     * @param countryId The ID of the country on the card
     */
    public void addCountryId(int countryId) {
        countryIds.add(countryId);
    }

    /**
     * Sets the type of cards being traded
     * @param cardType The type of cards being traded
     */
    public void setCardType(CardTypes cardType) {
        this.cardType = cardType;
    }

    /**
     * Gets the type of cards being traded
     * @return The type of cards being traded
     */
    public CardTypes getCardType() {
        return cardType;
    }

    /**
     * Gets an array of country IDs on the cards being traded.
     * This is essentially synonymous with "get the set of cards being traded" since the country IDs
     * are the defining feature of a card
     * @return An Array of country IDs on the cards being traded
     */
    public Integer[] getCountryIds() {
        return (Integer[]) countryIds.toArray();
    }
}
