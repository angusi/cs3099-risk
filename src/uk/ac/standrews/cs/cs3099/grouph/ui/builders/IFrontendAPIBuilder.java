package uk.ac.standrews.cs.cs3099.grouph.ui.builders;

import uk.ac.standrews.cs.cs3099.grouph.ui.APIEndpoint;
import uk.ac.standrews.cs.cs3099.grouph.ui.UIComponent;
import uk.ac.standrews.cs.cs3099.grouph.ui.interfaces.IFrontendAPI;

/**
 * Builder for APIEndpoint
 */
public class IFrontendAPIBuilder {
    /**
     * The UI Component that this API is a sub-component of
     */
    private UIComponent uiComponent;

    /**
     * Defines the UI Component that this API is a sub-component of
     * @param uiComponent The UI Component that this API Endpoint is a sub-component of
     * @return The modified IFrontendAPIBuilder
     */
    public IFrontendAPIBuilder setUiComponent(UIComponent uiComponent) {
        this.uiComponent = uiComponent;
        return this;
    }

    /**
     * Creates a new instance of an IFrontendAPI
     * @return A concrete IFrontendAPI instance
     */
    public IFrontendAPI createAPIEndpoint() {
        return new APIEndpoint(uiComponent);
    }
}