package uk.ac.standrews.cs.cs3099.grouph.ui.builders;

import uk.ac.standrews.cs.cs3099.grouph.ui.WebSocketEndpoint;

/**
 * Builds a new WebSocket Endpoint
 */
public class WebSocketEndpointBuilder {
    /**
     * The port the WebSocket listens on
     */
    private int port;

    /**
     * Defines the port the WebSocket listens on
     * @param port The port the WebSocket listens on
     * @return The modified WebSocketEndpointBuilder instance
     */
    public WebSocketEndpointBuilder setPort(int port) {
        this.port = port;
        return this;
    }

    /**
     * Builds a new WebSocket Endpoint
     * @return A new WebSocket Endpoint
     */
    public WebSocketEndpoint createWebSocketEndpoint() {
        return new WebSocketEndpoint(port);
    }
}