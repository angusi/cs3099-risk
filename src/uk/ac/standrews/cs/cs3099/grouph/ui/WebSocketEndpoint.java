package uk.ac.standrews.cs.cs3099.grouph.ui;

import fi.iki.elonen.NanoHTTPD;
import fi.iki.elonen.NanoWebSocketServer;
import fi.iki.elonen.WebSocket;
import fi.iki.elonen.WebSocketFrame;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

/**
 * The WebSocket Endpoint manages the connection that asynchronous messages to the UI are sent down
 */
public class WebSocketEndpoint extends NanoWebSocketServer {

    /**
     * The logger for this class
     */
    private final Logger logger = Logger.getLogger(WebSocketEndpoint.class.getSimpleName());

    /**
     * The list of open socket connections
     */
    private final List<WebSocket> openSockets = new ArrayList<>();

    /**
     * Creates a new WebSocket instance
     * @param port The port to listen on
     */
    public WebSocketEndpoint(final int port) {
        super(port);
    }

    /**
     * Sends a message to each connected client
     * @param message The message to send
     * @throws IOException An error occurred sending the message to one of the clients
     */
    public void sendMessage(final String message) throws IOException {
        for(final WebSocket openSocket : openSockets) {
            openSocket.send(message);
        }
    }

    /**
     * Triggered when a client connects to the server.
     * @param handshake The HTTP Session handshake
     * @return The newly opened socket connection
     */
    @Override
    public WebSocket openWebSocket(final IHTTPSession handshake) {
        final WebSocket newSocket = new WebSocketHandler(handshake);
        openSockets.add(newSocket);
        return newSocket;
    }

    /**
     * Handles the Web Socket specific methods
     */
    private class WebSocketHandler extends WebSocket {
        /**
         * Constructs a new handler for the Web Socket methods
         * @param handshakeRequest The HTTP Session handshake
         */
        public WebSocketHandler(final NanoHTTPD.IHTTPSession handshakeRequest) {
            super(handshakeRequest);
        }

        /**
         * Pong messages are part of keep-alive.
         * There is no action needed
         * @param pongFrame The incoming PONG message
         */
        @Override
        protected void onPong(final WebSocketFrame pongFrame) {
            //Keep alive. No need to do anything
        }

        /**
         * This socket is only for SENDING data, so we ignore incmoing messages.
         * (Actually, we print a warning, on the console.)
         * @param messageFrame The incoming message
         */
        @Override
        protected void onMessage(final WebSocketFrame messageFrame) {
            //This endpoint is only used for sending messages, not receiving them.
            //Print a warning.
            logger.warning("Unexpected message received on WebSocket. Ignoring...");
        }

        /**
         * The socket was close
         * @param code The close code
         * @param reason The reason for the close
         * @param initiatedByRemote Whether or not the close was initiated by the remote end
         */
        @Override
        protected void onClose(final WebSocketFrame.CloseCode code, final String reason, final boolean initiatedByRemote) {
            openSockets.remove(this);
        }

        /**
         * An error happened - print the error
         * @param e The exception that occurred
         */
        @Override
        protected void onException(final IOException e) {
            //Uh oh. Something went wrong.
            e.printStackTrace();
        }

        /**
         * Send a message - the whole point this class exists
         * @param payload The message to send
         * @throws IOException Thrown if an error occurs
         */
        @Override
        public void send(final String payload) throws IOException {
            super.send(payload);
            logger.info("Sending "+payload);
        }
    }
}
