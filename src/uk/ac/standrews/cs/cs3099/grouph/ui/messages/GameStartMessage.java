package uk.ac.standrews.cs.cs3099.grouph.ui.messages;

import uk.ac.standrews.cs.cs3099.grouph.shared.AbstractUIMessage;

/**
 * A message indicating we wish to commence playing a game we are hosting.
 * This message is sent when we are satisfied that all the players we wish to join have joined
 * and that we are now ready to begin the setup/claim phase of the game.
 */
public class GameStartMessage extends AbstractUIMessage {
    /**
     * Constructs a new message to the Engine, telling it we are ready to commence play
     * @param index A unique identifier for the message
     */
    public GameStartMessage(int index) {
        super(index);
    }
}
