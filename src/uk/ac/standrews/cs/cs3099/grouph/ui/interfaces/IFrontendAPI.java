package uk.ac.standrews.cs.cs3099.grouph.ui.interfaces;

import fi.iki.elonen.NanoHTTPD;
import org.json.JSONObject;

/**
 * Classes which implement this interface are responsible for any non-file requests - i.e. the game API.
 * The interface includes the minimal methods necessary for communication.
 */
public interface IFrontendAPI {

    /**
     * Serves API Endpoints
     * @param session The HTTP Session
     * @return The served data
     */
    NanoHTTPD.Response serve(NanoHTTPD.IHTTPSession session);

    /**
     * Request for "PING".
     * Sends the Engine a "PING" to indicate system still alive.
     * @return A JSONObject, with "status":"ok" on success
     */
    JSONObject ping();

    /**
     * Request for game status.
     * Asks the engine what the current game status is.
     * @return A JSONObject, with "status":"ok" on success
     */
    JSONObject status();

    /**
     * Request for new messages
     * Checks the message queue for anything new to tell the UI
     * @return Any messages waiting in the queue
     */
    JSONObject poll();

    /**
     * Invalid requests
     * Give an error if an invalid request was made
     * @return status message fail
     */
    JSONObject errorNotFound();
}
