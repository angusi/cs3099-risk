package uk.ac.standrews.cs.cs3099.grouph.ui.messages;

import uk.ac.standrews.cs.cs3099.grouph.shared.AbstractUIMessage;

/**
 * A message indicating we wish to deploy a number of units to a specific country.
 * This message is sent in the "Deploy" phase of the turn, after trading has completed.
 * The country must be one that we are in control of, and the number of units must be less than or equal
 * to the number of units we have available to deploy
 */
public class DeployMessage extends AbstractUIMessage {

    /**
     * The ID,count mapping of the countries we wish to deploy units to
     */
    private final int[][] deployments;

    /**
     * Constructs a new message detailing our deployment
     * @param index A unique identifier for the message
     * @param deployments The ID,count mapping of the countries we wish to deploy units to
     */
    public DeployMessage(final int index, final int[][] deployments) {
        super(index);
        this.deployments = deployments;
    }

    /**
     * Gets the ID,count mappings of the countries we wish to deploy units to
     * The country must be one that we are in control of.
     * The number of units must be less than or equal to the number of units we have available to deploy
     * @return The ID,count mapping of the countries we wish to deploy units to
     */
    public int[][] getDeployments() {
        return deployments;
    }
}
