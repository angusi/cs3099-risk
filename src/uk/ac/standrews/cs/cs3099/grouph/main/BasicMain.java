package uk.ac.standrews.cs.cs3099.grouph.main;

import uk.ac.standrews.cs.cs3099.grouph.net.NetJSON;
import uk.ac.standrews.cs.cs3099.grouph.shared.IEngineComponent;
import uk.ac.standrews.cs.cs3099.grouph.shared.INetComponent;
import uk.ac.standrews.cs.cs3099.grouph.shared.IUIComponent;
import uk.ac.standrews.cs.cs3099.grouph.ai.AIcomponent;
import uk.ac.standrews.cs.cs3099.grouph.engine.EngineCore;

/**
 * Created by john on 11/20/14.
 */
public class BasicMain {
    public static void main(String[] args) {
        // These names can change
        INetComponent net = new NetJSON();
        IUIComponent ui = new uk.ac.standrews.cs.cs3099.grouph.ui.UIComponent(8080);
        IEngineComponent engine = new EngineCore(args);
        AIcomponent ai = new AIcomponent(1000);
        // Setup network queues
        net.setSource(engine.getSink(net), engine);
        
        // Setup UI queues
        ui.setSource(engine.getSink(ui), engine);
        
        // Setup engine queues
        engine.setSource(net.getSink(engine), net);
        engine.setSource(ui.getSink(engine), ui);
        
        // Run
        new Thread(net).start();
        new Thread(engine).start();
        new Thread(ui).start();
        new Thread(ai).start();
    }
}
