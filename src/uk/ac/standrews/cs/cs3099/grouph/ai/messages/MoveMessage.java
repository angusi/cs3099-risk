package uk.ac.standrews.cs.cs3099.grouph.ai.messages;

import uk.ac.standrews.cs.cs3099.grouph.shared.AIMessage;

public class MoveMessage extends AIMessage{
	private int numberOfArmies, attackerId;
	public MoveMessage(int index,int attackerId, int numOfArmies){
		super(index);
		this.attackerId=attackerId;
		this.numberOfArmies=numOfArmies;
		
	}
	private int getAttackerId(){
		return this.attackerId;
	}
	private int getNumOfArmies(){
		return this.numberOfArmies;
	}
}
