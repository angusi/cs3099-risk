package uk.ac.standrews.cs.cs3099.grouph.ai.messages;

import uk.ac.standrews.cs.cs3099.grouph.shared.AIMessage;

public class DeployMessage extends AIMessage{
	 int countryId;
	    int numberOfArmies;

	    public DeployMessage(int index, int countryId, int numberOfArmies) {
	        super(index);
	        this.countryId = countryId;
	        this.numberOfArmies = numberOfArmies;
	    }

	    public int getCountryId() {
	        return countryId;
	    }

	    public int getNumberOfArmies() {
	        return numberOfArmies;
	    }
}
