package uk.ac.standrews.cs.cs3099.grouph.ai;

import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.standrews.cs.cs3099.grouph.ai.AIGameManager.Game;
import uk.ac.standrews.cs.cs3099.grouph.ai.messages.AttackMessage;
import uk.ac.standrews.cs.cs3099.grouph.ai.messages.ClaimMessage;
import uk.ac.standrews.cs.cs3099.grouph.ai.messages.DefendMessage;
import uk.ac.standrews.cs.cs3099.grouph.ai.messages.DeployMessage;
import uk.ac.standrews.cs.cs3099.grouph.ai.messages.FortifyMessage;
import uk.ac.standrews.cs.cs3099.grouph.ai.messages.MoveMessage;
import uk.ac.standrews.cs.cs3099.grouph.ai.messages.ReinforceMessage;
import uk.ac.standrews.cs.cs3099.grouph.ai.messages.RequestRollMessage;
import uk.ac.standrews.cs.cs3099.grouph.engine.game.players.Player;
import uk.ac.standrews.cs.cs3099.grouph.engine.game.players.PlayerQueue;
import uk.ac.standrews.cs.cs3099.grouph.engine.game.state.exceptions.InvalidState;
import uk.ac.standrews.cs.cs3099.grouph.engine.game.state.TurnState;
import uk.ac.standrews.cs.cs3099.grouph.engine.game.map.exception.MissingCountry;
import uk.ac.standrews.cs.cs3099.grouph.engine.game.map.Country;
import uk.ac.standrews.cs.cs3099.grouph.engine.game.map.GameMap;
import uk.ac.standrews.cs.cs3099.grouph.engine.game.managers.exceptions.GameRuleException;
import uk.ac.standrews.cs.cs3099.grouph.engine.messages.requests.RequestSetupClaim;
import uk.ac.standrews.cs.cs3099.grouph.net.messages.MoveM;
import uk.ac.standrews.cs.cs3099.grouph.shared.struct.players.InvalidPlayerException;
import uk.ac.standrews.cs.cs3099.grouph.shared.struct.players.Players;

public class AIGameController {
    private Logger logger = LoggerFactory.getLogger(AIGameController.class);

    public class Validator {
        private AIGameManager.Game gameData;

        public Validator(AIGameManager.Game gameData) {
            this.gameData = gameData;
        }

        public void validateCountry(int countryId) throws GameRuleException {
            if (!this.gameData.getGameMap().containsCountry(countryId))
                throw new GameRuleException(GameRuleException.Rule.COUNTRY_ILLEGAL_ID);
        }

        public void validatePlayer(Player player) throws GameRuleException {
            if (!(this.gameData.getTurnState().getCurrentPlayer() == player))
                throw new GameRuleException(GameRuleException.Rule.PLAYER_ILLEGAL_TURN);
        }

        public void validateStep(TurnState.Step step) throws GameRuleException {
            if (!(this.gameData.getTurnState().getCurrentStep() == step))
                throw new GameRuleException(GameRuleException.Rule.PLAYER_ILLEGAL_STEP);
        }

        public void validateCountryOwner(Player player, int countryId) throws GameRuleException {
            if (!(this.gameData.getGameMap().getCountryOwner(countryId) == player))
                throw new GameRuleException(GameRuleException.Rule.COUNTRY_INCORRECT_OWNER);
        }

        public void validateNotCountryOwner(Player player, int countryId)
            throws GameRuleException {
            if (this.gameData.getGameMap().getCountryOwner(countryId) == player)
                throw new GameRuleException(GameRuleException.Rule.COUNTRY_INCORRECT_OWNER);
        }

        public void validateEmptyCountry(int countryId) throws GameRuleException {
            if (this.gameData.getGameMap().getCountryOwner(countryId) != null)
                throw new GameRuleException(GameRuleException.Rule.COUNTRY_INCORRECT_OWNER);
        }

        public void validateCountryConnection(int countryAId, int countryBId)
            throws GameRuleException {
            if (!this.gameData.getGameMap().areConnected(countryAId, countryBId))
                throw new GameRuleException(GameRuleException.Rule.COUNTRY_ILLEGAL_CONNECTION);
        }

        public void validatePlayerAvailableReinforcements(Player player, int numberOfArmies)
            throws GameRuleException {
            if (player.getReinforcements() - numberOfArmies < 0)
                throw new GameRuleException(GameRuleException.Rule.ARMY_REINFORCE_TOO_LARGE);
        }

        public void validateArmyBattle(boolean defender,
            int countryId, int numberOfArmies) throws GameRuleException {
            if (numberOfArmies < 0)
                throw new GameRuleException(GameRuleException.Rule.ARMY_NEGATIVE);
            
            if (numberOfArmies > 3 && !defender) 
                throw new GameRuleException(GameRuleException.Rule.ARMY_ATTACKER_TOO_LARGE);
            else if (numberOfArmies > 2)
                throw new GameRuleException(GameRuleException.Rule.ARMY_DEFENDER_TOO_LARGE);

            if (!(this.gameData.getGameMap().getCountryArmies(countryId) < numberOfArmies + 1)) {
                throw new GameRuleException(
                        (!defender) ? GameRuleException.Rule.ARMY_ATTACKER_INSUFFICIENT
                                    : GameRuleException.Rule.ARMY_DEFENDER_INSUFFICIENT);
            }
        }

        public void validatePath(Player player, int startingCountryId, int endingCountryId) 
            throws GameRuleException {
            if (!this.gameData.getGameMap().hasFortifyPath(player,
                        startingCountryId, endingCountryId)) {
                throw new GameRuleException(GameRuleException.Rule.FORTIFY_NO_PATH); 
            }
        }
        
        public void validateAttack(Player player, int attackingCountryId,
                int defendingCountryId, int numberOfArmies) throws GameRuleException {
            // Validate attacking player is current player
            this.validatePlayer(player);
            // Validate player is in the correct turn state
            this.validateStep(TurnState.Step.ATTACK);
            // Validate player owns country attacking from
            this.validateCountryOwner(player, attackingCountryId);
            // Validate player doesn't own defending country
            this.validateNotCountryOwner(player, defendingCountryId);
            // Validate if attacking and defending countries are connected
            this.validateCountryConnection(attackingCountryId, defendingCountryId);
            // Validate that attacker has 1 + numberOfArmies to conduct attack 
            this.validateArmyBattle(false, attackingCountryId, numberOfArmies);
        }

        public void validateDefend(Player player, int defendingCountryId, int numberOfArmies)
            throws GameRuleException {
            // Validate correct player defending 
            this.validatePlayer(player);
            // Validate correct state
            this.validateStep(TurnState.Step.DEFEND);
            // Validate player owns defending country
            this.validateCountryOwner(player, defendingCountryId);
            // Validate defender has 1 + numberOfArmies to defend from attacking 
            this.validateArmyBattle(true, defendingCountryId, numberOfArmies);
        }

        public void validateClaim(Player player, int countryId) throws GameRuleException {
            // Validate claiming player is current player
            this.validatePlayer(player);
            // Validate player is in right turn state 
            this.validateStep(TurnState.Step.CLAIM);
            // Validate country to be claimed is empty
            this.validateEmptyCountry(countryId);
        }

        public void validateDeploy(Player player, int countryId, int numberOfArmies)
            throws GameRuleException {
            // Validate deploying player is current player
            this.validatePlayer(player);
            // Validate player is in right turn state 
            this.validateStep(TurnState.Step.DEPLOY);
            // Validate country to be deployed to is owned by player 
            this.validateCountryOwner(player, countryId);
            // Validate player has enough reinforcements
            this.validatePlayerAvailableReinforcements(player, numberOfArmies);
        }

        public void validateReinforce(Player player, int countryId, int numberOfArmies)
            throws GameRuleException {
            // Validate deploying player is current player
            this.validatePlayer(player);
            // Validate player is in right turn state 
            this.validateStep(TurnState.Step.REINFORCE);
            // Validate country to be deployed to is owned by player 
            this.validateCountryOwner(player, countryId);
            // Validate player has enough reinforcements
            this.validatePlayerAvailableReinforcements(player, numberOfArmies);
        }

        public void validateMove(Player player, int startingCountryId, int endingCountryId,
                int numberOfArmies) throws GameRuleException {
            if (this.gameData.getGameMap().getCountryArmies(startingCountryId) < 0) {
                throw new GameRuleException(GameRuleException.Rule.ARMY_NEGATIVE);
            }
            if (this.gameData.getGameMap().getCountryArmies(startingCountryId) < 
                    1 + numberOfArmies) {
                throw new GameRuleException(GameRuleException.Rule.ARMY_MOVE_TOO_LARGE);
            }
        }
        
        public void validateFortify(Player player, int startingCountryId, 
            int endingCountryId, int numberOfArmies) throws GameRuleException {
            // Validate fortifying player is current player 
            this.validatePlayer(player);
            // Validate player is in right turn state
            this.validateStep(TurnState.Step.FORTIFY);
            // Validate player owns starting and ending country 
            this.validateCountryOwner(player, startingCountryId);
            this.validateCountryOwner(player, endingCountryId);
            // Validate starting country has enough armies 
            this.validateMove(player, startingCountryId, endingCountryId, numberOfArmies);
            // Validate player has uninterrupted path from starting and ending country 
            this.validatePath(player, startingCountryId, endingCountryId);
        }

    }

    public class Modifier {
        private AIGameManager.Game gameData;
        private Validator validator;

        private Modifier(AIGameManager.Game gameData, Validator validator) {
            this.gameData = gameData;
            this.validator = validator;
        }

        public boolean isDeploy() {
            return (this.gameData.getTurnState().getCurrentStep() == TurnState.Step.DEPLOY);
        }

        public boolean isClaim() {
            return (this.gameData.getTurnState().getCurrentStep() == TurnState.Step.CLAIM);
        }

        public Player getCurrentPlayer() {
            return this.gameData.getTurnState().getCurrentPlayer();
        }

        public String printTurnOrder() {
            return this.gameData.getTurnState().printTurnOrder();
        }

        // TODO: Replace runtime with custom exception
        public Player getPlayerById(int playerId) throws RuntimeException {
            Player p;

            if ((p = this.gameData.getPlayerQueue().getPlayer(playerId)) == null)
                throw new RuntimeException("Player Id does not exist!");

            return p;
        }

        public void setup() {
            int numberOfPlayers = this.gameData.getPlayerQueue().getNumberOfPlayers();
            // Set each players initial reinforcements to the appropriate value
            int startingReinforcements = 35 - (5 * (numberOfPlayers - 3));
            
            this.gameData.getPlayerQueue().setAllReinforcements(startingReinforcements);
        }

        public  ClaimMessage claim()
            throws GameRuleException, InvalidPlayerException {
        	int countryId=0;
        	Player player=new Player(Players.me().getId());
        	
           ArrayList<Country> unoccupiedCountries = this.gameData.getGameMap().getEmptyCountriesList();
           Country temp = unoccupiedCountries.get(unoccupiedCountries.size()-1);
           countryId = temp.getId();
           
            this.validator.validateClaim(player, countryId); 
            
            synchronized(this) {
                try {
                    this.gameData.getGameMap().setCountryOwner(countryId, player);
                    this.gameData.getGameMap().setCountryArmies(countryId, 1);

                    player.setTerritories(player.getTerritories() + 1);
                    
                    if (this.gameData.getGameMap().containsEmptyCountry()) {
                        logger.info("GameMap contains empty country - Proceeding with claims");
                        this.gameData.getTurnState().nextPlayer();
                    } else {
                        logger.info("GameMap does not contain an empty country - Moving to play phase and reset turn order");
                        this.gameData.getTurnState().finishClaim();
                        this.gameData.getPlayerQueue().resetOrder();
                    }
                } catch (MissingCountry c) {
                    throw new GameRuleException(GameRuleException.Rule.COUNTRY_ILLEGAL_ID);
                } catch (InvalidState s) {
                    throw new GameRuleException(GameRuleException.Rule.PLAYER_ILLEGAL_STEP);
                }
            } ClaimMessage cm = new ClaimMessage(99, player.getId(), countryId);
            return cm;
        }
        public RequestRollMessage getRollMessage(int numberOfDice) throws InvalidPlayerException{
        	return new RequestRollMessage(99, Players.me(), numberOfDice, 6);
        }
        
        public DeployMessage deploy()
            throws GameRuleException, InvalidPlayerException {
        	Player player=gameData.getPlayerQueue().getPlayer(Players.me().getId());
        	
            ArrayList<Country> myCountries = this.gameData.getGameMap().getCountriesOfPlayer(player.getId());
            Country temp = myCountries.get(myCountries.size()-1);
            int countryId=temp.getId();
            int numberOfArmies=player.getReinforcements();
            this.validator.validateDeploy(player, countryId, player.getReinforcements());
            
            synchronized(this) {
                try {
                    player.setReinforcements(player.getReinforcements() - numberOfArmies);

                    this.gameData
                        .getGameMap()
                        .setCountryArmies(countryId, 
                                this.gameData.getGameMap().getCountryArmies(countryId) + numberOfArmies);

                    if (this.gameData.getPlayerQueue().allReinforcementsDepleted()) {
                        this.gameData.getTurnState().finishDeploy();
                        this.gameData.getPlayerQueue().resetOrder();
                    } else {
                        this.gameData.getTurnState().nextPlayer();
                    }
                } catch (MissingCountry c) {
                    throw new GameRuleException(GameRuleException.Rule.COUNTRY_ILLEGAL_ID);
                } catch (InvalidState s) {
                    throw new GameRuleException(GameRuleException.Rule.PLAYER_ILLEGAL_STEP);
                }

            }
            return new DeployMessage(99, countryId, player.getReinforcements());
        }


        public ReinforceMessage reinforce() 
            throws GameRuleException, RuntimeException, InvalidPlayerException {
            Player player = this.getPlayerById(Players.me().getId());
            ArrayList<Country> myCountries = this.gameData.getGameMap().getCountriesOfPlayer(player.getId());
            Country temp = myCountries.get(myCountries.size()-1);
            int countryId = temp.getId();
            int numberOfArmies=player.getReinforcements();
            
            this.validator.validateReinforce(player, countryId, numberOfArmies);
            synchronized(this) {
                try {
                    player.setReinforcements(player.getReinforcements() - numberOfArmies);
    
                    this.gameData.getGameMap().setCountryArmies(countryId,
                            this.gameData.getGameMap().getCountryArmies(countryId) + numberOfArmies);

                    if (player.getReinforcements() == 0) {
                        this.gameData.getTurnState().nextStep();
                    }
                } catch (MissingCountry c) {
                    throw new GameRuleException(GameRuleException.Rule.COUNTRY_ILLEGAL_ID);
                }
            }
            return new ReinforceMessage(99, temp.getId(), numberOfArmies);
        }

        public AttackMessage attack() 
            throws GameRuleException, RuntimeException, InvalidPlayerException {
            Player player = this.getPlayerById(Players.me().getId());
            ArrayList<Country> myCountries = this.gameData.getGameMap().getCountriesOfPlayer(Players.me().getId());
            ArrayList<Country> elsesCountries;
            		if(this.gameData.getGameMap().getCountriesOfPlayer(Players.me().getId()-1)!=null){
            			 elsesCountries=this.gameData.getGameMap().getCountriesOfPlayer(Players.me().getId()-1);
            		}
            		else{
            			 elsesCountries=this.gameData.getGameMap().getCountriesOfPlayer(Players.me().getId()+1);
            		}
            		;
            	for (int i = 0; i < myCountries.size(); i++) {
            		for(int j=0; j<elsesCountries.size();j++){
					if(myCountries.get(i).getNumberOfArmies()>1){
						if(this.gameData.getGameMap().areConnected(myCountries.get(i).getId(), elsesCountries.get(j).getId())){
							  this.validator.validateAttack(player, myCountries.get(i).getId(), elsesCountries.get(j).getId(),(int) myCountries.get(i).getNumberOfArmies()-1);
					            synchronized(this) {
					                try {
					                    this.gameData.getTurnState().setNumberOfAttackers((int) myCountries.get(i).getNumberOfArmies()-1);
					                    this.gameData.getTurnState().setDefendingPlayer(this.gameData.getGameMap().getCountryOwner(elsesCountries.get(j).getId()));
					                    this.gameData.getTurnState().setAttackingCountryId(myCountries.get(i).getId());
					                    this.gameData.getTurnState().setDefendingCountryId(elsesCountries.get(j).getId());
					                    this.gameData.getTurnState().nextStep();
					                } catch (MissingCountry c) {
					                    throw new GameRuleException(GameRuleException.Rule.COUNTRY_ILLEGAL_ID);
					                }
					            }
					            return  new AttackMessage(99,myCountries.get(i).getId(), elsesCountries.get(j).getId(),(int) myCountries.get(i).getNumberOfArmies()-1);
						}
					}
				}
            	}
				return null;
          
        }

        public DefendMessage defend() throws GameRuleException, RuntimeException, InvalidPlayerException {
            Player player = this.getPlayerById(Players.me().getId());
            int numberOfArmies = (int) this.gameData.getGameMap().getCountryArmies(this.gameData.getTurnState().getDefendingCountryId());
            if(numberOfArmies>=2){
            	numberOfArmies=2;
            }
            else{
            	numberOfArmies=1;
            }
            this.validator.validateDefend(player, this.gameData.getTurnState().getDefendingCountryId(), numberOfArmies);
            synchronized(this) {
                try {
                    this.gameData.getTurnState().setNumberOfDefenders(numberOfArmies);
                    this.gameData.getTurnState().nextStep();
                } catch (MissingCountry c) {
                    throw new GameRuleException(GameRuleException.Rule.COUNTRY_ILLEGAL_ID);
                }
            }
            
            return new DefendMessage(99,numberOfArmies );
        }

        public void result(int playerId, int attackingLosses, int defendingLosses) throws GameRuleException {
            Player player = this.getPlayerById(playerId);
            
            this.validator.validatePlayer(player);
            synchronized (this) {
                try {
                    int attackingId = this.gameData.getTurnState().getAttackingCountryId();
                    int defendingId = this.gameData.getTurnState().getDefendingCountryId();
                    
                    // Calculate losses
                    this.gameData.getGameMap().setCountryArmies(attackingId, 
                            this.gameData.getGameMap().getCountryArmies(attackingId) - attackingLosses);
                    this.gameData.getGameMap().setCountryArmies(defendingId, 
                            this.gameData.getGameMap().getCountryArmies(defendingId) - defendingLosses);

                    // Check if territory changes
                    if (this.gameData.getGameMap().getCountryArmies(defendingId) == 0) {
                        this.gameData.getTurnState().triggerMoveStep();
                    } else {
                        this.gameData.getTurnState().nextStep();
                    }
                } catch (MissingCountry c) {
                    throw new GameRuleException(GameRuleException.Rule.COUNTRY_ILLEGAL_ID);
                }
            }
        }

        public MoveMessage move() throws GameRuleException, RuntimeException, InvalidPlayerException {
            Player player = this.getPlayerById(Players.me().getId());
            Player defender = this.gameData.getTurnState().getDefendingPlayer();

            int attackingCountryId = this.gameData.getTurnState().getAttackingCountryId();
            int defendingCountryId = this.gameData.getTurnState().getDefendingCountryId();
            int numberOfArmies = (int) this.gameData.getGameMap().getCountryArmies(attackingCountryId-1);
            this.validator.validateMove(player, attackingCountryId, defendingCountryId, numberOfArmies);
            synchronized(this) {
                try {
                    this.gameData.getGameMap().setCountryArmies(attackingCountryId,
                            this.gameData.getGameMap().getCountryArmies(attackingCountryId) - numberOfArmies);
                    this.gameData.getGameMap().setCountryArmies(defendingCountryId,
                            this.gameData.getGameMap().getCountryArmies(defendingCountryId) + numberOfArmies);

                    player.setTerritories(player.getTerritories() + 1);
                    defender.setTerritories(defender.getTerritories() - 1);

                    this.gameData.getTurnState().nextStep();
                } catch (MissingCountry c) {
                    throw new GameRuleException(GameRuleException.Rule.COUNTRY_ILLEGAL_ID);
                }
            }
            return new MoveMessage(99, attackingCountryId, numberOfArmies);
            
        }

        public FortifyMessage fortify()
            throws GameRuleException, RuntimeException, InvalidPlayerException {
            Player player = this.getPlayerById((int)Players.me().getId());
            int startingCountryId=0;
            int endingCountryId=0;
            int numberOfArmies=0;
            ArrayList<Country> myCountries = this.gameData.getGameMap().getCountriesOfPlayer(player.getId());
            	for(Country a: myCountries){
            		for(Country b:myCountries){
            			if(this.gameData.getGameMap().areConnected(a.getId(), b.getId()) && (a.getNumberOfArmies()>b.getNumberOfArmies())){
            				startingCountryId=a.getId();
            				endingCountryId = b.getId();
            				numberOfArmies=(int) (a.getNumberOfArmies()-1);
            			}
            		}
            	}
            // Finish the player's attack phase
            this.validator.validatePlayer(player);
            this.gameData.getTurnState().finishAttack();

            this.validator.validateFortify(player, startingCountryId, endingCountryId, numberOfArmies);
            synchronized(this) {
                try {
                    this.gameData.getGameMap().setCountryArmies(startingCountryId,
                            this.gameData.getGameMap().getCountryArmies(startingCountryId) - numberOfArmies);
                    this.gameData.getGameMap().setCountryArmies(endingCountryId,
                            this.gameData.getGameMap().getCountryArmies(endingCountryId) + numberOfArmies);
                    this.gameData.getTurnState().nextStep();
                } catch (MissingCountry c) {
                    throw new GameRuleException(GameRuleException.Rule.COUNTRY_ILLEGAL_ID);
                }
            }
            return new FortifyMessage(99,startingCountryId,endingCountryId,numberOfArmies);
        }

        public void turnover() throws GameRuleException, RuntimeException, InvalidPlayerException {
            Player player = this.getPlayerById(Players.me().getId());

            this.validator.validatePlayer(player);
            this.validator.validateStep(TurnState.Step.FINISH);
            synchronized(this) {
                try {
                    if (!this.gameData.getPlayerQueue().allTerritoriesOwnedBySinglePlayer()) {
                        this.gameData.getTurnState().nextPlayer();
                        this.gameData.getTurnState().nextStep();
                    } 
                } catch (InvalidState s) {
                    throw new GameRuleException(GameRuleException.Rule.PLAYER_ILLEGAL_STEP);
                }
            }
        }
    }

    private Validator validator;
    private Modifier modifier;

    public AIGameController(Game gameData) {
        this.validator = new Validator(gameData);
        this.modifier = new Modifier(gameData, this.validator);
    }

    public Validator getValidator() {
        return this.validator;
    }

    public Modifier getModifier() {
        return this.modifier;
    }

}
