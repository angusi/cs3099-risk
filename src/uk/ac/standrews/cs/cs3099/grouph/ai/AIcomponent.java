package uk.ac.standrews.cs.cs3099.grouph.ai;
import org.json.JSONObject;	

import sun.reflect.generics.visitor.Reifier;
import uk.ac.standrews.cs.cs3099.grouph.ai.AIGameManager.Game;
import uk.ac.standrews.cs.cs3099.grouph.ai.messages.ClaimMessage;
import uk.ac.standrews.cs.cs3099.grouph.ai.messages.RequestRollMessage;
import uk.ac.standrews.cs.cs3099.grouph.ai.messages.ResultMessage;
import uk.ac.standrews.cs.cs3099.grouph.engine.events.EngineEventManager;
import uk.ac.standrews.cs.cs3099.grouph.engine.game.managers.GameController;
import uk.ac.standrews.cs.cs3099.grouph.engine.game.managers.GameManager;
import uk.ac.standrews.cs.cs3099.grouph.engine.game.managers.exceptions.GameRuleException;
import uk.ac.standrews.cs.cs3099.grouph.engine.game.map.GameMap;
import uk.ac.standrews.cs.cs3099.grouph.engine.game.state.TurnState;
import uk.ac.standrews.cs.cs3099.grouph.engine.game.state.TurnState.Step;
import uk.ac.standrews.cs.cs3099.grouph.engine.messages.requests.RequestGameStateTransfer;
import uk.ac.standrews.cs.cs3099.grouph.engine.messages.requests.RequestMove;
import uk.ac.standrews.cs.cs3099.grouph.engine.messages.requests.RequestSetupClaim;
import uk.ac.standrews.cs.cs3099.grouph.shared.IComponent;
import uk.ac.standrews.cs.cs3099.grouph.shared.AbstractEngineMessage;
import uk.ac.standrews.cs.cs3099.grouph.shared.AbstractMessage;
import uk.ac.standrews.cs.cs3099.grouph.shared.IEngineComponent;
import uk.ac.standrews.cs.cs3099.grouph.shared.struct.players.InvalidPlayerException;
import uk.ac.standrews.cs.cs3099.grouph.shared.struct.players.Players;
import uk.ac.standrews.cs.cs3099.grouph.ui.builders.WebSocketEndpointBuilder;
import uk.ac.standrews.cs.cs3099.grouph.ai.messages.*;

import java.io.IOException;
import java.util.AbstractQueue;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class AIcomponent implements uk.ac.standrews.cs.cs3099.grouph.shared.AIComponent{

	

    private AbstractQueue<AbstractMessage> source;
    private AbstractQueue<AbstractMessage> sink;
    private int sinkCounter = 0;
    private AIGameManager.Game gameData;
    private AIGameController agc = new AIGameController(gameData);
    
    public AIcomponent(int port) {
        sink = new ArrayBlockingQueue<>(10000);
    }

    @Override
    public AbstractQueue<AbstractMessage> getSink(IComponent comp) {
        return sink;
    }

    @Override
    public void setSource(AbstractQueue<AbstractMessage> queue, IComponent comp) {
    	if(comp instanceof IEngineComponent){
            source = queue;
    	}
        //} else {
        // Error condition
        // }
    }

    void sendMessage(AbstractMessage message) {
        sink.add(message);
    }

    AbstractMessage getMessage() {
        if(source.peek() != null) {
            return source.poll();
        } else {
            return null;
        }
    }


    public int getSinkCounter() {
        return sinkCounter++;
    }
    void updateGameData(AbstractMessage msg){
    	RequestGameStateTransfer rgst = (RequestGameStateTransfer)((AbstractEngineMessage)msg);
    	GameMap currMap = this.gameData.getGameMap();
    	currMap=  rgst.getGameState().getGameMap();
    	this.gameData.setGameMap(currMap);
    	this.gameData.setTurnState(rgst.getGameState().getTurnState());
    	this.gameData.setPlayerQueue(rgst.getGameState().getPlayerQueue());
    }
    
    public void play() throws GameRuleException, InvalidPlayerException{
    	AbstractMessage msg=getMessage();
    	if(msg==null){
    		 play();
    		 return;
    		 
    	}
    	if(msg instanceof RequestGameStateTransfer){
    		updateGameData(msg);
    	}
    	AbstractEngineMessage curr = (AbstractEngineMessage) msg;
    			if(this.gameData.getTurnState().equals(TurnState.Step.CLAIM)){
    			ClaimMessage cm = agc.getModifier().claim();
    			sendMessage(cm);
    			}
    			if(this.gameData.getTurnState().equals(TurnState.Step.DEPLOY)){
    				DeployMessage dm = agc.getModifier().deploy();
    				sendMessage(dm);
    			}
    			
    			if(this.gameData.getTurnState().equals(TurnState.Step.REINFORCE)){
    				ReinforceMessage rm = agc.getModifier().reinforce();
    				sendMessage(rm);
    			}
    			if(this.gameData.getTurnState().equals(TurnState.Step.ATTACK)){
    				AttackMessage am = agc.getModifier().attack();
    				sendMessage(am);
    				RequestRollMessage rrm  = new RequestRollMessage(99, Players.me(), 1, 6);
    				sendMessage(rrm);
    			}
    			if(this.gameData.getTurnState().equals(TurnState.Step.DEFEND)){
    				DefendMessage dm = agc.getModifier().defend();
    				sendMessage(dm);
    			}
    			if(this.gameData.getTurnState().equals(TurnState.Step.RESULT)){
    				ResultMessage rm = (ResultMessage) msg;
    				agc.getModifier().result(Players.me().getId(), rm.getAttackinglosses(), rm.getDefendinglosses());
    			}
    			if(this.gameData.getTurnState().equals(TurnState.Step.MOVE)){
    				MoveMessage mm = agc.getModifier().move();
    				sendMessage(mm);
    			}
    			if(this.gameData.getClass().equals(TurnState.Step.FORTIFY)){
    				FortifyMessage fm = agc.getModifier().fortify();
    				sendMessage(fm);
    			}
    			if(this.gameData.getClass().equals(TurnState.Step.FINISH)){
    				agc.getModifier().turnover();
    				
    			}
    			
    		}
    	
    	
    	
    
	@Override
	public void run() {
			try {

				play();

			} catch (GameRuleException | InvalidPlayerException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
	}


	}




