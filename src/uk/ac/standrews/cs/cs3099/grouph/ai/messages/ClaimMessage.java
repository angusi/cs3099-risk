package uk.ac.standrews.cs.cs3099.grouph.ai.messages;

import uk.ac.standrews.cs.cs3099.grouph.shared.AIMessage;

public class ClaimMessage extends AIMessage{


    private final int countryId;
    private final int playerId;

    public ClaimMessage(int index, int countryId, int playerId) {
        super(index);
        this.countryId = countryId;
        this.playerId=playerId;
    }

    public int getCountryId() {
        return countryId;
    }
}
