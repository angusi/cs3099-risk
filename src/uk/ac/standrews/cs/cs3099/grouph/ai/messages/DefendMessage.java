package uk.ac.standrews.cs.cs3099.grouph.ai.messages;

import uk.ac.standrews.cs.cs3099.grouph.shared.AIMessage;

public class DefendMessage extends AIMessage{
	

	    private int numOfArmies;
	    public DefendMessage(int index, int numOfArmies) {
	        super(index);
	        this.numOfArmies=numOfArmies;
	    }

}

