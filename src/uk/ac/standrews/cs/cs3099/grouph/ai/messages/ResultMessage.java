package uk.ac.standrews.cs.cs3099.grouph.ai.messages;

import uk.ac.standrews.cs.cs3099.grouph.shared.AIMessage;



public class ResultMessage extends AIMessage{
	private int attackinglosses;
	private int defendinglosses;
	
	public ResultMessage(int index, int attackinglosses, int defendinglosses){
		super(index);
		this.attackinglosses=attackinglosses;
		this.defendinglosses=defendinglosses;
	}

	public int getDefendinglosses() {
		return defendinglosses;
	}


	public int getAttackinglosses() {
		return attackinglosses;
	}

}
