package uk.ac.standrews.cs.cs3099.grouph.ai.messages;

import uk.ac.standrews.cs.cs3099.grouph.shared.AIMessage;

public class AttackMessage extends AIMessage {

    private final int sourceCountry;
    private final int destinationCountry;
    private final int numberOfArmies;

    public AttackMessage(int index, int sourceCountry, int destinationCountry, int numberOfArmies) {
        super(index);
        this.sourceCountry = sourceCountry;
        this.destinationCountry = destinationCountry;
        this.numberOfArmies = numberOfArmies;
    }

    public int getSourceCountry() {
        return sourceCountry;
    }

    public int getDestinationCountry() {
        return destinationCountry;
    }

    public int getNumberOfArmies() {
        return numberOfArmies;
    }
}