package uk.ac.standrews.cs.cs3099.grouph.net.messages;

import uk.ac.standrews.cs.cs3099.grouph.shared.AbstractNetMessage;

import java.util.ArrayList;

/**
 * Created by 120023239 on 3/11/15.
 */
public class PlayerJoinedM extends AbstractNetMessage {
    public class PlayerJoinedData {
        public int id;
        public String name;
    }

    public PlayerJoinedM(int index) {
        super(index);
    }

    public PlayerJoinedData createPlayer(int id, String name) {
        PlayerJoinedData pjd = new PlayerJoinedData();
        pjd.id = id;
        pjd.name = name;
        return pjd;
    }

    private ArrayList<PlayerJoinedData> playerData;

    public ArrayList<PlayerJoinedData> getPlayerData() {
        return playerData;
    }

    public void setPlayerData(ArrayList<PlayerJoinedData> playerData) {
        this.playerData = playerData;
    }
}
