package uk.ac.standrews.cs.cs3099.grouph.net.messages;

import uk.ac.standrews.cs.cs3099.grouph.shared.AbstractNetMessage;

/**
 * Created by 120023239 on 14/02/15.
 */
public class JoinGameResultM extends AbstractNetMessage {
    private boolean joinSuccess;
    private String message; // Only set if joinSuccess is false
    private String map;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public JoinGameResultM(int index) {
        super(index);
    }

    public boolean isJoinSuccess() {
        return joinSuccess;
    }

    public void setJoinSuccess(boolean joinSuccess) {
        this.joinSuccess = joinSuccess;
    }

    public String getMap() {
        return map;
    }

    public void setMap(String map) {
        this.map = map;
    }
}
