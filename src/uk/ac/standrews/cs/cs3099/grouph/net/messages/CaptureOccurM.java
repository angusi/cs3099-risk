package uk.ac.standrews.cs.cs3099.grouph.net.messages;

import uk.ac.standrews.cs.cs3099.grouph.shared.AbstractNetMessage;
import uk.ac.standrews.cs.cs3099.grouph.shared.struct.players.Player;

/**
 * Allows signaling to the Engine that another player has captured.
 * The capture may be directed as us or another player.
 */
public class CaptureOccurM extends AbstractNetMessage {
    private Player player;
    private int sourceTerritory;
    private int destTerritory;
    private int numberArmies;

    public CaptureOccurM(int index) {
        super(index);
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public int getSourceTerritory() {
        return sourceTerritory;
    }

    public void setSourceTerritory(int sourceTerritory) {
        this.sourceTerritory = sourceTerritory;
    }

    public int getDestTerritory() {
        return destTerritory;
    }

    public void setDestTerritory(int destTerritory) {
        this.destTerritory = destTerritory;
    }

    public int getNumberArmies() {
        return numberArmies;
    }

    public void setNumberArmies(int numberArmies) {
        this.numberArmies = numberArmies;
    }
}
