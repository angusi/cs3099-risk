package uk.ac.standrews.cs.cs3099.grouph.net.json.struct;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import uk.ac.standrews.cs.cs3099.grouph.net.messages.SetupM;
import uk.ac.standrews.cs.cs3099.grouph.shared.struct.players.InvalidPlayerException;
import uk.ac.standrews.cs.cs3099.grouph.shared.struct.players.Players;
import uk.ac.standrews.cs.cs3099.grouph.net.util.MessageCounter;
import uk.ac.standrews.cs.cs3099.grouph.shared.AbstractNetMessage;

/**
 * Created by 120023239 on 2/23/15.
 */
@JsonIgnoreProperties({"remoteHost"})
public class Setup extends JSONStructAck {
    // TODO acknowledge
    public Integer payload;

    // TODO set this
    public String remoteHost;

    @Override
    public AbstractNetMessage toMessage() {
        SetupM setupM = new SetupM(MessageCounter.next());
        setupM.setTerritory(payload); // TODO fix - player id int
        try {
            setupM.setPlayer(Players.getPlayerById(playerId));
        } catch (InvalidPlayerException e) {
            return null;
        }
        return setupM;
    }
}
