package uk.ac.standrews.cs.cs3099.grouph.net.json.struct;

/**
 * Created by 120023239 on 2/23/15.
 */
public class RollData {
    public Integer diceCount;
    public Integer diceFaces;
}
