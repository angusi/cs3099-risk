package uk.ac.standrews.cs.cs3099.grouph.net.json.struct;

import uk.ac.standrews.cs.cs3099.grouph.shared.AbstractNetMessage;
import uk.ac.standrews.cs.cs3099.grouph.net.messages.DiceHashM;
import uk.ac.standrews.cs.cs3099.grouph.net.util.MessageCounter;
import uk.ac.standrews.cs.cs3099.grouph.shared.struct.players.InvalidPlayerException;
import uk.ac.standrews.cs.cs3099.grouph.shared.struct.players.Players;

/**
 * Created by 120023239 on 2/23/15.
 */
public class RollHash extends JSONStruct {
    public String payload;
    public Integer playerId;

    @Override

    public AbstractNetMessage toMessage() {
        DiceHashM diceHashM = new DiceHashM(MessageCounter.next());

        try {
            diceHashM.setPlayer(Players.getPlayerById(playerId));
            diceHashM.setHash(payload);
        } catch (InvalidPlayerException e) {
            diceHashM.setPlayer(null); // TODO: Handle
        }

        return diceHashM;
    }
}
