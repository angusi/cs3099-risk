package uk.ac.standrews.cs.cs3099.grouph.net.messages;

import uk.ac.standrews.cs.cs3099.grouph.shared.AbstractNetMessage;

public class GameStatusM extends AbstractNetMessage {
    // TODO is this needed??
    // TODO shouldn't this be from engine??

    private boolean error;

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public GameStatusM(int index) {
        super(index);
    }
}
