package uk.ac.standrews.cs.cs3099.grouph.net.json.struct;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import uk.ac.standrews.cs.cs3099.grouph.shared.struct.players.Player;
import uk.ac.standrews.cs.cs3099.grouph.shared.struct.players.Players;
import uk.ac.standrews.cs.cs3099.grouph.net.util.MessageCounter;
import uk.ac.standrews.cs.cs3099.grouph.net.messages.JoinGameRequestM;
import uk.ac.standrews.cs.cs3099.grouph.shared.AbstractNetMessage;

/**
 * Created by 120023239 on 2/23/15.
 */
@JsonIgnoreProperties({"remoteHost"})
public class JoinGame extends JSONStruct {
    public JoinGameData payload;
    // TODO net needs to track max supported version
    // TODO net needs to track supported features

    // This is needed to set the remote id
    public String remoteHost;

    @Override
    public AbstractNetMessage toMessage() {
        JoinGameRequestM joinGameRequestM = new JoinGameRequestM(MessageCounter.next());
        Player p = Players.addPlayerByName(remoteHost);
        joinGameRequestM.setPlayer(p);
        return joinGameRequestM;
    }
}
