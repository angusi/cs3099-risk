package uk.ac.standrews.cs.cs3099.grouph.net;

import fi.iki.elonen.NanoHTTPD;
import uk.ac.standrews.cs.cs3099.grouph.net.messages.*;
import uk.ac.standrews.cs.cs3099.grouph.shared.IComponent;
import uk.ac.standrews.cs.cs3099.grouph.shared.AbstractMessage;
import uk.ac.standrews.cs.cs3099.grouph.shared.INetComponent;
import uk.ac.standrews.cs.cs3099.grouph.shared.struct.players.Player;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.AbstractQueue;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;

/**
 * Created by 120023239 on 29/01/15.
 */
public class BasicWebNet extends NanoHTTPD implements INetComponent {
    private static int messageIndex = 0;
    private static int mid() {
        return messageIndex++;
    }
    private AbstractQueue<AbstractMessage> sink;
    private AbstractQueue<AbstractMessage> source;

    //private HashMap<Integer, OldPlayer> players;

    public BasicWebNet(int port) {
        super(port);
        sink = new ArrayBlockingQueue<AbstractMessage>(1000);
        //players = new HashMap<>();

        // Our pid
        String localAddress;
        try {
            localAddress = InetAddress.getLocalHost().getHostAddress();
        } catch (UnknownHostException e) {
            localAddress = "1.2.3.4"; // filler // TODO fix
        }
        // Parse IP parts into an int array
        int[] ip = new int[4];
        String[] parts = localAddress.split("\\.");
        for (int i = 0; i < 4; i++) {
            ip[i] = Integer.parseInt(parts[i]);
        }

        int pid = 0;
        for(int i = 0; i < 4; i++) {
            pid += ip[i] << (24 - (8 * i));
        }

        //OldPlayer p = new OldPlayer(pid);
        //players.put(pid, p);
    }

    private Player getPlayer(String pid) {
        return null;
    }

    @Override
    public AbstractQueue<AbstractMessage> getSink(IComponent comp) {
        return sink;
    }

    @Override
    public void setSource(AbstractQueue<AbstractMessage> queue, IComponent comp) {
        this.source = queue;
    }

    @Override
    public void run() {
        try {
            this.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Response serve(IHTTPSession session) {
        Method method = session.getMethod();
        String uri = session.getUri();

        String msg = "Received";

        Map<String, String> files = new HashMap<>();
        try {
            session.parseBody(files);
        } catch (IOException | NanoHTTPD.ResponseException e) {
            e.printStackTrace();
        }

        Map<String, String> parms = session.getParms();

        switch(uri) {
            case "/attack":
                AttackOccurM attackM = new AttackOccurM(mid());
                attackM.setTo(Integer.parseInt(parms.get("to")));
                attackM.setFrom(Integer.parseInt(parms.get("from")));
                attackM.setNumberArmies(Integer.parseInt("count"));
                //attackM.setPlayer(getPlayer(parms.get("pid")));
                sink.add(attackM);
                break;
            case "/capture":
                CaptureOccurM captureOccurM = new CaptureOccurM(mid());
                //captureOccurM.setPlayer(getPlayer(parms.get("pid")));
                //captureOccurM.setTerritoryCaptured(Integer.parseInt(parms.get("territory")));
                sink.add(captureOccurM);
                break;
            case "/card_draw":
                CardDrawResultM cardDrawResultM = new CardDrawResultM(mid());
                //cardDrawResultM.setPlayer(getPlayer(parms.get("pid")));
                int cardId = Integer.parseInt(parms.get("id"));
                //cardDrawResultM.setCard(new Card(cardId));
                sink.add(cardDrawResultM);
                break;
            case "/card_played":
                CardPlayedM cardPlayedM = new CardPlayedM(mid());
                //cardPlayedM.setPlayer(getPlayer(parms.get("pid")));
                String cards = parms.get("cards");
                String[] cIds = cards.split(":");
                for(String s : cIds) {
                    //cardPlayedM.addCard(new Card(Integer.parseInt(s)));
                }
                sink.add(cardPlayedM);
                break;
            case "/defend":
                DefendOccurM defendOccurM = new DefendOccurM(mid());
                //defendOccurM.setPlayer(getPlayer(parms.get("pid")));
                defendOccurM.setNumberArmies(Integer.parseInt(parms.get("count")));
                sink.add(defendOccurM);
                break;
            case "/deploy":
                DeployArmiesM deployArmiesM = new DeployArmiesM(mid());
                //deployArmiesM.setPlayer(getPlayer(parms.get("pid")));
                String movesRaw = parms.get("moves");
                String[] moves = movesRaw.split(":");
                for(String s : moves) {
                    String[] tuple = s.split(",");
                    int tid = Integer.parseInt(tuple[0]);
                    int nArmies = Integer.parseInt(tuple[1]);
                    DeployArmiesM.Movement movement = deployArmiesM.createMovement(tid, nArmies);
                    deployArmiesM.addMove(movement);
                }
                sink.add(deployArmiesM);
                break;
            case "/roll_hash": // /roll?n=3&lid=98765 -->
                DiceHashM diceHashM = new DiceHashM(mid());

                diceHashM.setHash(parms.get("hash"));

                sink.add(diceHashM);
                //DieResultM dieResultM = new DieResultM(mid());
                //dieResultM.setPlayer(getPlayer(parms.get("pid")));
                //dieResultM.setResult(Integer.parseInt(parms.get("n")));
                //sink.add(dieResultM);
                break;
            case "/roll_number":
                DiceNumberM diceNumberM = new DiceNumberM(mid());

                diceNumberM.setNumber(parms.get("number"));

                sink.add(diceNumberM);
                break;
            case "/join_game":
                JoinGameRequestM joinGameRequestM = new JoinGameRequestM(mid());
                String rem = session.getHeaders().get("http-client-ip");
                // Parse IP parts into an int array
                int[] ip = new int[4];
                String[] parts = rem.split("\\.");
                for (int i = 0; i < 4; i++) {
                    ip[i] = Integer.parseInt(parts[i]);
                }

                int pid = 0;
                for(int i = 0; i < 4; i++) {
                    pid += ip[i] << (24 - (8 * i));
                }

                //OldPlayer p = new OldPlayer(pid);
                //players.put(pid, p);
                //joinGameRequestM.setPlayer(p); // TODO refactor with new player class
                sink.add(joinGameRequestM);
                break;
            case "/join_response":
                JoinGameResultM joinGameResultM = new JoinGameResultM(mid());
                String rem1 = session.getHeaders().get("http-client-ip");
                // Parse IP parts into an int array
                int[] ip1 = new int[4];
                String[] parts1 = rem1.split("\\.");
                for (int i = 0; i < 4; i++) {
                    ip1[i] = Integer.parseInt(parts1[i]);
                }

                int pid1 = 0;
                for(int i = 0; i < 4; i++) {
                    pid1 += ip1[i] << (24 - (8 * i));
                }

                //OldPlayer p1 = new OldPlayer(pid1);
                //players.put(pid1, p1);
                //joinGameResultM.setRemoteHost(p1);
                String res = parms.get("result");
                if(res.equals("success")) {
                    joinGameResultM.setJoinSuccess(true);
                } else {
                    joinGameResultM.setJoinSuccess(false);
                }
                sink.add(joinGameResultM);
                break;
            case "/map": // TODO N.B. that this is the only HTTP POST endpoint
                if(!session.getMethod().equals(Method.POST)) {
                    InvalidWireM invalidWireM2 = new InvalidWireM(mid());
                    //invalidWireM2.setPlayer(getPlayer(parms.get("pid")));
                    invalidWireM2.setNotes("Map endpoint received non-POST request");
                    break;
                }
                MapDataM mapDataM = new MapDataM(mid());

                String jsonData = parms.get("json");
                mapDataM.setJsonData(jsonData);
                sink.add(mapDataM);
                break;
            case "/move": // /move?from=1&to=2&count=3&id=98765 --> ""
                MoveM moveM = new MoveM(mid());
                //moveM.setPlayer(getPlayer(parms.get("pid")));
                moveM.setFrom(Integer.parseInt(parms.get("from")));
                moveM.setTo(Integer.parseInt(parms.get("to")));
                moveM.setNumberArmies(Integer.parseInt(parms.get("count")));
                sink.add(moveM);
                break;
            case "/setup":
                SetupM setupM = new SetupM(mid());
                //setupM.setPlayer(getPlayer(parms.get("pid")));
                setupM.setTerritory(Integer.parseInt(parms.get("territory")));
                sink.add(setupM);
                break;
            case "/error": // /error?lid=98765 --> ""
                GameStatusM gameStatusM = new GameStatusM(mid());
                gameStatusM.setError(true);
                sink.add(gameStatusM);
                break;
            case "/tracer": // /tracer
                TracerM m5 = new TracerM(mid());
                sink.add(m5);
                break;
            default:
                InvalidWireM invalidWireM = new InvalidWireM(mid());
                invalidWireM.setPlayer(null);
                invalidWireM.setNotes(session.getUri());
                break;
        }

        return new Response(msg);
    }
}
