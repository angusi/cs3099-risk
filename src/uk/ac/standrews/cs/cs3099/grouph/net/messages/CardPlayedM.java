package uk.ac.standrews.cs.cs3099.grouph.net.messages;

import uk.ac.standrews.cs.cs3099.grouph.shared.AbstractNetMessage;
import uk.ac.standrews.cs.cs3099.grouph.shared.struct.players.Player;

import java.util.List;

public class CardPlayedM extends AbstractNetMessage {
    private Player player;
    private List<List<Integer>> cards;
    private int armies; // The number of armies the player expects from this trade-in

    public CardPlayedM(int index) {
        super(index);
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public List<List<Integer>> getCards() {
        return this.cards;
    }

    public void setCards(List<List<Integer>> list) {
        this.cards = list;
    }

    public int getArmies() {
        return armies;
    }

    public void setArmies(int armies) {
        this.armies = armies;
    }
}
