package uk.ac.standrews.cs.cs3099.grouph.net.messages;

import uk.ac.standrews.cs.cs3099.grouph.shared.AbstractNetMessage;

import java.util.List;

/**
 * Created by 120023239 on 23/02/15.
 */
// The game has been started by the host
public class GameStartM extends AbstractNetMessage {
    private List<String> supportedFeatures;

    public GameStartM(int index) {
        super(index);
    }

    public List<String> getSupportedFeatures() {
        return supportedFeatures;
    }

    public void setSupportedFeatures(List<String> supportedFeatures) {
        this.supportedFeatures = supportedFeatures;
    }
}
