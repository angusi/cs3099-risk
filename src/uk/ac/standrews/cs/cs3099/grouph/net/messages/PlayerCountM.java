package uk.ac.standrews.cs.cs3099.grouph.net.messages;

import uk.ac.standrews.cs.cs3099.grouph.shared.AbstractNetMessage;

/**
 * Created by 120023239 on 23/02/15.
 */
public class PlayerCountM extends AbstractNetMessage {
    private Integer playerCount;

    public PlayerCountM(int index) {
        super(index);
    }

    public Integer getPlayerCount() {
        return playerCount;
    }

    public void setPlayerCount(Integer playerCount) {
        this.playerCount = playerCount;
    }
}
