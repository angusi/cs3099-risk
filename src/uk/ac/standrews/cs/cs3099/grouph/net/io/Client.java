package uk.ac.standrews.cs.cs3099.grouph.net.io;

import uk.ac.standrews.cs.cs3099.grouph.net.json.Converter;
import uk.ac.standrews.cs.cs3099.grouph.net.json.struct.*;
import uk.ac.standrews.cs.cs3099.grouph.shared.AbstractMessage;
import uk.ac.standrews.cs.cs3099.grouph.shared.struct.players.InvalidPlayerException;
import uk.ac.standrews.cs.cs3099.grouph.shared.struct.players.Players;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.AbstractQueue;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.logging.Logger;

/**
 * Created by 120023239 on 2/23/15.
 */
public class Client {
    private static final Logger l = Logger.getLogger(Client.class.getSimpleName());
    // TODO - This handles comms if we are not the host
    private Socket socket;
    private Converter converter;
    private String hostAddress;
    private int hostPort;
    private AbstractQueue<AbstractMessage> engineQueue;
    private PrintWriter out = null;
    private BufferedReader in = null;
    private Object lock = new Object();

    private Map<String, String> ackIds = new HashMap<>();

    private AbstractQueue<AbstractMessage> hostServerQueue = new ArrayBlockingQueue<AbstractMessage>(1000); // TODO magic number

    public Client(String host, int port, AbstractQueue<AbstractMessage> engineQueue) {
        try {
            this.socket = new Socket(host, port);
        } catch (IOException e) {
            // TODO handle
            e.printStackTrace();
        }
        this.converter = new Converter();
        this.hostAddress = host;
        this.hostPort = port;
        this.engineQueue = engineQueue;
    }

    public void start() {
        l.info("Starting client");
        try {
            out = new PrintWriter(socket.getOutputStream());
        } catch (IOException e) {
            e.printStackTrace(); // TODO handle
        }
        try {
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        } catch (IOException e) {
            e.printStackTrace();
        }
        Receiver receiver = new Receiver();
        new Thread(receiver).start();
    }

    public void sendMessage(JSONStruct message) {
        l.info("Sending message to host");
        synchronized (lock) {
            if (out != null) {
                out.println(converter.objectToString(message));
                out.flush();
            }
        }
    }

    private class Receiver implements Runnable {
        @Override
        public void run() {
            while (!Thread.interrupted()) {
                if (in != null) {
                    String input = null;
                    try {
                        input = in.readLine();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    if(input == null) {
                        l.info("End of stream from host");
                        return;
                    }
                    l.info("Client received message: " + input);
                    if (input != null && !input.trim().equals("null")) {
                        JSONStruct message = converter.stringToObject(input);
                        l.info("Message command is: " + message.command);

                        if(message != null && message.command != null) {

                            if (message instanceof AcceptJoinGame) {
                                try {
                                    Players.me();
                                } catch (InvalidPlayerException e) {
                                    // Not already set
                                    Players.generateMeAsClient(((AcceptJoinGame) message).payload.playerId);
                                    AbstractMessage m = message.toMessage();
                                    if(m != null) { // TODO how?
                                        l.info("Sending message " + m.id + " to engine");
                                        engineQueue.add(m);
                                    }
                                } finally {
                                    continue;
                                }
                            }

                            // Check for host bouncing messages back at us
                            if(message instanceof JSONStructAck) {
                                try {
                                    if(((JSONStructAck)message).playerId == Players.me().getId()) {
                                        // Message from us
                                        continue;
                                    }
                                } catch (InvalidPlayerException e) {
                                    e.printStackTrace(); // TODO handle
                                }
                            }

                            if(message instanceof PlayersJoined) {
                                PlayersJoined playersJoined = (PlayersJoined) message;
                                ArrayList<ArrayList<Object>> data = playersJoined.payload;
                                for(ArrayList<Object> d : data) {
                                    Integer id = (Integer) d.get(0);
                                    String hn = (String) d.get(1);
                                    Players.addPlayerById(hn, id);
                                }
                            }

                            if(message instanceof Ack) {
                                // Quick fix for now
                                // TODO resolve
                                continue;
                            }
                            if (message instanceof Ping) {
                                Ping p = new Ping();
                                p.payload = null;
                                try {
                                    p.playerId = Players.me().getId();
                                } catch (InvalidPlayerException e) {
                                    e.printStackTrace(); // TODO handle
                                }
                                sendMessage(p);
                                continue;
                            }

                            AbstractMessage m = message.toMessage();
                            if(m != null) { // TODO how?
                                l.info("Sending message " + m.id + " to engine");
                                engineQueue.add(m);
                            }
                        }
                    }
                }
            }
        }
    }
}
