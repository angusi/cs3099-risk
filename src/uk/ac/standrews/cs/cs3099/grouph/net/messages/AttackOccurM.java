package uk.ac.standrews.cs.cs3099.grouph.net.messages;

import uk.ac.standrews.cs.cs3099.grouph.shared.AbstractNetMessage;
import uk.ac.standrews.cs.cs3099.grouph.shared.struct.players.Player;

/**
 * Allows signaling to the Engine that another player has attacked.
 * The attack may be directed as us or another player.
 */
public class AttackOccurM extends AbstractNetMessage {

    private Player player;
    private int to;
    private int from;
    private int numberArmies;

    public AttackOccurM(int index) {
        super(index);
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public int getTo() {
        return to;
    }

    public void setTo(int to) {
        this.to = to;
    }

    public int getFrom() {
        return from;
    }

    public void setFrom(int from) {
        this.from = from;
    }

    public int getNumberArmies() {
        return numberArmies;
    }

    public void setNumberArmies(int numberArmies) {
        this.numberArmies = numberArmies;
    }
}
