package uk.ac.standrews.cs.cs3099.grouph.net.json.struct;

import uk.ac.standrews.cs.cs3099.grouph.net.messages.DeployArmiesM;
import uk.ac.standrews.cs.cs3099.grouph.net.util.MessageCounter;
import uk.ac.standrews.cs.cs3099.grouph.shared.AbstractNetMessage;
import uk.ac.standrews.cs.cs3099.grouph.shared.struct.players.InvalidPlayerException;
import uk.ac.standrews.cs.cs3099.grouph.shared.struct.players.Players;

import java.util.List;

/**
 * Created by 120023239 on 2/23/15.
 */
public class Deploy extends JSONStructAck {
    public List<Integer[]> payload;
    
    @Override
    public AbstractNetMessage toMessage() {
        DeployArmiesM deployArmiesM = new DeployArmiesM(MessageCounter.next());
        try {
            deployArmiesM.setPlayer(Players.getPlayerById(playerId));
        } catch (InvalidPlayerException e) {
            deployArmiesM.setPlayer(null);
        }

        payload.forEach(deployment -> deployArmiesM.createMovement(deployment[0], deployment[1]));

        return deployArmiesM;
    }
}
