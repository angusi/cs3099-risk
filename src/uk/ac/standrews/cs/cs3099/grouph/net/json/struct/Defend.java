package uk.ac.standrews.cs.cs3099.grouph.net.json.struct;

import uk.ac.standrews.cs.cs3099.grouph.net.messages.DefendOccurM;
import uk.ac.standrews.cs.cs3099.grouph.net.util.MessageCounter;
import uk.ac.standrews.cs.cs3099.grouph.shared.AbstractNetMessage;
import uk.ac.standrews.cs.cs3099.grouph.shared.struct.players.InvalidPlayerException;
import uk.ac.standrews.cs.cs3099.grouph.shared.struct.players.Players;

/**
 * Created by 120023239 on 2/23/15.
 */
public class Defend extends JSONStructAck {
    // TODO ack
    public Integer payload;

    @Override
    public AbstractNetMessage toMessage() {
        DefendOccurM defendOccurM = new DefendOccurM(MessageCounter.next());
        try {
            defendOccurM.setPlayer(Players.getPlayerById(playerId));
        } catch (InvalidPlayerException e) {
            defendOccurM.setPlayer(null); // TODO handle
        }
        defendOccurM.setNumberArmies(payload);
        return defendOccurM;
    }
}
