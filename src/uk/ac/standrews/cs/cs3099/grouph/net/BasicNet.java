package uk.ac.standrews.cs.cs3099.grouph.net;

import uk.ac.standrews.cs.cs3099.grouph.net.messages.MoveM;
import uk.ac.standrews.cs.cs3099.grouph.shared.IComponent;
import uk.ac.standrews.cs.cs3099.grouph.shared.AbstractMessage;
import uk.ac.standrews.cs.cs3099.grouph.shared.INetComponent;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.AbstractQueue;
import java.util.concurrent.ArrayBlockingQueue;

public class BasicNet implements INetComponent {
    private AbstractQueue<AbstractMessage> sink;
    private AbstractQueue<AbstractMessage> source;

    private int index = 0;

    public BasicNet() {

        sink = new ArrayBlockingQueue<AbstractMessage>(1000);
    }

    @Override
    public AbstractQueue<AbstractMessage> getSink(IComponent comp) {
        return sink;
    }

    @Override
    public void setSource( AbstractQueue<AbstractMessage> queue, IComponent comp) {
        source = queue;
    }

    @Override
    public void run() {
        DummyNetComponent dnc = new DummyNetComponent();
        Thread t = new Thread(dnc);
        t.start();
    }

    private class DummyNetComponent implements Runnable {

        @Override
        public void run() {
            System.out.println("Press return to send a message:");
            BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
            while(true) {
                if (source.poll() != null) {
                    System.out.println("Net received message from Engine");
                }
                try {
                    if(in.ready()) {
                        in.readLine();
                        AbstractMessage m = new MoveM(index++);
                        sink.add(m);
                        System.out.println("Sending message");
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}