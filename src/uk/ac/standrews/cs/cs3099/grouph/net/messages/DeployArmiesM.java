package uk.ac.standrews.cs.cs3099.grouph.net.messages;

import uk.ac.standrews.cs.cs3099.grouph.shared.AbstractNetMessage;
import uk.ac.standrews.cs.cs3099.grouph.shared.struct.players.Player;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by 120023239 on 14/02/15.
 */
public class DeployArmiesM extends AbstractNetMessage {
    private Player player;
    private List<Movement> moves;

    public DeployArmiesM(int index) {
        super(index);
        moves = new ArrayList<>();
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public Movement[] getMoves() {
        return moves.stream().toArray(Movement[]::new);
    }

    public void addMove(Movement movement) {
        this.moves.add(movement);
    }

    public class Movement {
        private int territoryId;
        private int nArmies;

        public Movement(int territoryId, int nArmies) {
            this.territoryId = territoryId;
            this.nArmies = nArmies;
        }

        public int getTerritoryId() {
            return territoryId;
        }

        public int getnArmies() {
            return nArmies;
        }
    }

    public Movement createMovement(int territoryId, int nArmies) {
        Movement m = new Movement(territoryId, nArmies);
        moves.add(m);
        return m;
    }
}
