package uk.ac.standrews.cs.cs3099.grouph.net.json.struct;

import uk.ac.standrews.cs.cs3099.grouph.net.messages.CaptureOccurM;
import uk.ac.standrews.cs.cs3099.grouph.net.util.MessageCounter;
import uk.ac.standrews.cs.cs3099.grouph.shared.AbstractNetMessage;
import uk.ac.standrews.cs.cs3099.grouph.shared.struct.players.InvalidPlayerException;
import uk.ac.standrews.cs.cs3099.grouph.shared.struct.players.Players;

import java.util.List;

/**
 * Created by 120023239 on 2/23/15.
 */
public class AttackCapture extends JSONStructAck {
    public List<Integer> payload;

    @Override
    public AbstractNetMessage toMessage() {
        CaptureOccurM captureOccurM = new CaptureOccurM(MessageCounter.next());
        try {
            captureOccurM.setPlayer(Players.getPlayerById(playerId ));
        } catch (InvalidPlayerException e) {
            captureOccurM.setPlayer(null); // TODO handle
        }
        captureOccurM.setSourceTerritory(payload.get(0));
        captureOccurM.setDestTerritory(payload.get(0));
        captureOccurM.setNumberArmies(payload.get(0));
        return captureOccurM;
    }
}
