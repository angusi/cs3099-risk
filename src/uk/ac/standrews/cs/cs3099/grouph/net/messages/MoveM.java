package uk.ac.standrews.cs.cs3099.grouph.net.messages;

import uk.ac.standrews.cs.cs3099.grouph.shared.AbstractNetMessage;
import uk.ac.standrews.cs.cs3099.grouph.shared.struct.players.Player;

public class MoveM extends AbstractNetMessage {
    private Player player;
    private int from;
    private int to;
    private int numberArmies;

    public MoveM(int index) {
        super(index);
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public int getFrom() {
        return from;
    }

    public void setFrom(int from) {
        this.from = from;
    }

    public int getTo() {
        return to;
    }

    public void setTo(int to) {
        this.to = to;
    }

    public int getNumberArmies() {
        return numberArmies;
    }

    public void setNumberArmies(int numberArmies) {
        this.numberArmies = numberArmies;
    }


}
