package uk.ac.standrews.cs.cs3099.grouph.net.io;

import uk.ac.standrews.cs.cs3099.grouph.net.json.Converter;
import uk.ac.standrews.cs.cs3099.grouph.net.json.struct.*;
import uk.ac.standrews.cs.cs3099.grouph.net.messages.JoinGameRequestM;
import uk.ac.standrews.cs.cs3099.grouph.net.util.MessageCounter;
import uk.ac.standrews.cs.cs3099.grouph.shared.AbstractMessage;
import uk.ac.standrews.cs.cs3099.grouph.shared.struct.players.InvalidPlayerException;
import uk.ac.standrews.cs.cs3099.grouph.shared.struct.players.Players;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.AbstractQueue;
import java.util.ArrayList;
import java.util.logging.Logger;

/**
 * Created by 120023239 on 2/23/15.
 */
public class Host {
    private static final Logger l = Logger.getLogger(Host.class.getSimpleName());
    // TODO - This handles everything if we are the host
    private int port;
    private ServerSocket serverSocket;
    private AbstractQueue<AbstractMessage> engineQueue;
    private String map;

    private Converter converter = null;

    private ArrayList<PrintWriter> socketOuts;
    private ArrayList<BufferedReader> socketIns;

    private Object lock1 = new Object();
    private Object lock2 = new Object();

    public Host(int port, String map, AbstractQueue<AbstractMessage> engineQueue) {
        setup(port, map, engineQueue);
    }

    private void setup(int port, String map, AbstractQueue<AbstractMessage> engineQueue) {
        this.port = port;
        this.map = map;
        this.engineQueue = engineQueue;

        try {
            serverSocket = new ServerSocket(port);
        } catch (IOException e) {
            e.printStackTrace();
            return; // TODO handle
        }

        converter = new Converter();
        socketIns = new ArrayList<>();
        socketOuts = new ArrayList<>();

    }

    public void start() {
        SocketListener socketListener = new SocketListener();
        new Thread(socketListener).start();
        Players.generateMeAsHost();
        Broadcaster broadcaster = new Broadcaster();
        new Thread(broadcaster).start();
    }

    public void sendMessage(JSONStruct message) {
        synchronized (lock1) {
            for (PrintWriter p : socketOuts) {
                p.println(converter.objectToString(message));
                p.flush();
            }
        }
        l.info("Sent a message over the wire: " + converter.objectToString(message));
    }

    private class SocketListener implements Runnable {
        @Override
        public void run() {
            while (!Thread.interrupted()) {
                try {
                    Socket s = serverSocket.accept();
                    l.info("Accepted new inbound socket request on address: " + s.getInetAddress().getHostAddress());

                    JoinGameRequestM m = new JoinGameRequestM(MessageCounter.next());
                    int playerId = 0;
                    engineQueue.add(m);
                    PrintWriter pw = new PrintWriter(s.getOutputStream());
                    BufferedReader read = new BufferedReader(new InputStreamReader(s.getInputStream()));
                    if (s.getInetAddress().getHostAddress().equals("127.0.0.1")) {
                        // Client is us
                        l.info("Connection from our own client. ");

                        try {
                            m.setPlayer(Players.me());
                        } catch (InvalidPlayerException e) {
                            e.printStackTrace(); // TODO handle
                        }
                        playerId = 0;
                    } else {
                        String input = new BufferedReader(new InputStreamReader(s.getInputStream())).readLine();
                        JSONStruct messageJson = converter.stringToObject(input);

                        l.info("Read Input: " + input);
                        if (messageJson.command.equals("join_game")) {
                            m.setPlayer(Players.addPlayerByName(s.getInetAddress().getHostName()));
                            playerId = m.getPlayer().getId();

                            AcceptJoinGame acceptJoinGame = new AcceptJoinGame();
                            AcceptJoinGameData acceptJoinGameData = new AcceptJoinGameData();
                            acceptJoinGameData.acknowledgmentTimeout = 3; // TODO these values?
                            acceptJoinGameData.moveTimeout = 999999999;
                            acceptJoinGameData.playerId = playerId;
                            acceptJoinGameData.map = map;
                            l.info("Accepted new player with id " + playerId);
                            acceptJoinGame.payload = acceptJoinGameData;

                            pw.println(converter.objectToString(acceptJoinGame));
                            pw.flush();
                        }
                    }

                    synchronized (lock2) {
                        socketOuts.add(pw);
                    }
                    synchronized (lock1) {
                        socketIns.add(read);
                    }

                    PlayersJoined playersJoined = new PlayersJoined();
                    ArrayList<ArrayList<Object>> playersJoinedData = new ArrayList<>();
                    int playerSize = Players.getPlayerCount();
                    for (int i = 0; i < playerSize; i++) {
                        ArrayList<Object> data = new ArrayList<>();
                        data.add(new Integer(i));
                        try {
                            data.add(Players.getPlayerById(i).getHostname());
                        } catch (InvalidPlayerException e) {
                            e.printStackTrace(); // TODO handle
                        }
                        playersJoinedData.add(data);
                    }
                    playersJoined.payload = playersJoinedData;
                    sendMessage(playersJoined);
                } catch (IOException e) {
                    e.printStackTrace();
                    return;
                }
            }
        }
    }

    private class Broadcaster implements Runnable {
        @Override
        public void run() {
            while (!Thread.interrupted()) {
                //for (BufferedReader r : socketIns) {
                for(int i = 0; i < socketIns.size(); i++) {
                    BufferedReader r;
                    synchronized (lock1) {
                        r = socketIns.get(i);
                    }
                    try {
                        if (r.ready()) {
                            String s = r.readLine();
                            if (s != null && !s.trim().equals("null")) {
                                synchronized (lock2) {
                                    for (PrintWriter pw : socketOuts) {
                                        pw.println(s);
                                        pw.flush();
                                    }
                                }
                            }
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }
}
