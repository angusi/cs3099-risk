package uk.ac.standrews.cs.cs3099.grouph.net.json.struct;

import uk.ac.standrews.cs.cs3099.grouph.net.messages.TimeoutM;
import uk.ac.standrews.cs.cs3099.grouph.net.util.MessageCounter;
import uk.ac.standrews.cs.cs3099.grouph.shared.AbstractNetMessage;
import uk.ac.standrews.cs.cs3099.grouph.shared.struct.players.InvalidPlayerException;
import uk.ac.standrews.cs.cs3099.grouph.shared.struct.players.Players;

/**
 * Created by 120023239 on 2/23/15.
 */
public class Timeout extends JSONStructAck {
    // TODO ack
    public Integer payload;

    @Override
    public AbstractNetMessage toMessage() {
        TimeoutM timeoutM = new TimeoutM(MessageCounter.next());
        try {
            timeoutM.setPlayer(Players.getPlayerById(playerId));
        } catch (InvalidPlayerException e) {
            timeoutM.setPlayer(null); // TODO handle
        }
        try {
            timeoutM.setExitingPlayer(Players.getPlayerById(payload));
        } catch (InvalidPlayerException e) {
            timeoutM.setExitingPlayer(null); // TODO
        }
        return timeoutM;
    }
}
