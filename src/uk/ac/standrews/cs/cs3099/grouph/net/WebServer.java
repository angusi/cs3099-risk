package uk.ac.standrews.cs.cs3099.grouph.net;

import fi.iki.elonen.NanoHTTPD;
import uk.ac.standrews.cs.cs3099.grouph.ui.FileServer;
import uk.ac.standrews.cs.cs3099.grouph.ui.builders.IFileServerBuilder;
import uk.ac.standrews.cs.cs3099.grouph.ui.interfaces.IFileServer;
import uk.ac.standrews.cs.cs3099.grouph.ui.interfaces.IFrontendAPI;

import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * Created by 120023239 on 29/01/15.
 */
public class WebServer extends NanoHTTPD {
    private IFrontendAPI apiEndpoint;
    private IFileServer fileServer;

    public WebServer(int port) {
        super(port);
        this.fileServer = new IFileServerBuilder().createFileServer();
    }

    /**
     * Start the UI Server.
     * @throws java.io.IOException Thrown if the server could not be started
     */
    public void startServer() throws IOException {
        this.start();
    }

    @Override
    public Response serve(IHTTPSession session) {
        Response response;

        Map<String, List<String>> decodedQueryParameters =
                decodeParameters(session.getQueryParameterString());

        int posOfSlash = session.getUri().indexOf("/", 1);
        if(posOfSlash == -1) {
            //If the slash is at -1, there is no slash.
            // This means there is only one URI segment - thus the entire string should be used.
            posOfSlash = session.getUri().length();
        }
        String uriHandler = session.getUri().substring(0, posOfSlash);

        switch(uriHandler) {
            case "/api":
                //Todo: Versioning of API
                response = apiEndpoint.serve(session);
                break;
            default:
                response = fileServer.serve(session);
                break;
        }

        return response;
    }
}
