package uk.ac.standrews.cs.cs3099.grouph.net.json;

/**
 * Created by 120023239 on 2/23/15.
 */
public class RiskJSONFields {
    /*
     * Commands
     */
    // Join Game
    public static final String commandJoinGame = "join_game";
    // Accept a Join Game Requeswt
    public static final String commandAcceptJoinGame = "accept_join_game";
    // Reject a Join Game Request
    public static final String commandRejectJoinGame = "reject_join_game";
    // Players joined announce
    public static final String commandPlayerJoined = "players_joined";
    // Ping request/response
    public static final String commandPing = "ping";
    // Command from central host indicating game ready
    public static final String commandReady = "ready";
    // Begin a game
    public static final String commandBeginGame = "initialise_game";
    // Claim a territory during game play setup
    public static final String commandSetup = "setup";
    // Play a card
    public static final String commandPlayCard = "play_cards";
    // Draw a card
    public static final String commandDrawCard = "draw_card";
    // Deploy a newly gained army
    public static final String commandDeploy = "deploy";
    // Attack another territory
    public static final String commandAttack = "attack";
    // Defend your territory
    public static final String commandDefend = "defend";
    // Announce capture of another territory
    public static final String commandCapture = "attack_capture";
    // Fortify a current position
    public static final String commandFortify = "fortify";
    // Acknowledge another message
    public static final String commandAck = "acknowledgement";
    // Distribute a hash for pre-roll of die
    public static final String commandRollHash = "roll_hash";
    // Announce a roll number
    public static final String commandRollNumber = "roll_number";
    // Central host announces a player timeout
    public static final String commandTimeoout = "timeout";
    // Announcement that a player is leaving
    public static final String commandLeaveGame = "leave_game";

    /*
     * Response codes
     */
    // Undefined logic error
    public static final int undefLogicError = 100;
    // Invalid card combination
    public static final int invalidCardComboError = 101;
    // Invalid move
    public static final int invalidMoveError = 102;
    // Undefined protocol error
    public static final int undefProtError = 200;
    // Undefined network error
    public static final int undefNetError = 300;

}
