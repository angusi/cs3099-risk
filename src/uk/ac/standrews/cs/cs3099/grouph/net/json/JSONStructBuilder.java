package uk.ac.standrews.cs.cs3099.grouph.net.json;

import org.apache.commons.lang.ArrayUtils;
import uk.ac.standrews.cs.cs3099.grouph.engine.messages.requests.*;
import uk.ac.standrews.cs.cs3099.grouph.engine.messages.responses.ResponseMessage;
import uk.ac.standrews.cs.cs3099.grouph.net.json.struct.*;
import uk.ac.standrews.cs.cs3099.grouph.shared.AbstractMessage;
import uk.ac.standrews.cs.cs3099.grouph.shared.AbstractNetMessage;
import uk.ac.standrews.cs.cs3099.grouph.shared.struct.players.InvalidPlayerException;
import uk.ac.standrews.cs.cs3099.grouph.shared.struct.players.Players;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by 120023239 on 24/02/15.
 */
public class JSONStructBuilder {
    public static JSONStruct build(AbstractMessage message) throws InvalidPlayerException {
        switch (message.name) {
            case "RequestAttack":
                RequestAttack localMessage1 = (RequestAttack) message;
                Attack attack = new Attack();
                ArrayList<Integer> list1 = new ArrayList<>();
                list1.add(localMessage1.getAttackingCountryId());
                list1.add(localMessage1.getDefendingCountryId());
                list1.add(localMessage1.getAttackingArmyCount());
                attack.payload = list1;
                attack.playerId = Players.me().getId();
                return attack;
            case "RequestCheat":
                RequestCheat localMessage2 = (RequestCheat) message;
                LeaveGame leaveGame = new LeaveGame();
                leaveGame.payload.receiveUpdates = true;
                leaveGame.payload.response = RiskJSONFields.undefLogicError;
                leaveGame.playerId = Players.me().getId();
                return leaveGame;
            case "RequestDefend":
                RequestDefend localMessage3 = (RequestDefend) message;
                Defend defend = new Defend();
                defend.payload = localMessage3.getDefendingArmyCount();
                defend.playerId = Players.me().getId();
                return defend;
            case "RequestMove":
                RequestMove requestMove = (RequestMove) message;
                Fortify fortify = new Fortify();
                ArrayList<Integer> list2 = new ArrayList<>();
                list2.add(requestMove.getStartCountryId());
                list2.add(requestMove.getEndCountryId());
                list2.add(requestMove.getArmyCount());
                fortify.payload = list2;
                fortify.playerId = Players.me().getId();
                return fortify;
            case "RequestDeploy":
                RequestDeploy requestDeploy = (RequestDeploy) message;
                Deploy deploy = new Deploy();

                deploy.payload = new ArrayList<>();
                deploy.playerId = Players.me().getId();

                Arrays.stream(requestDeploy.getReinforcements()).forEach(array ->
                        deploy.payload.add(ArrayUtils.toObject(array))
                );

                return deploy;
            case "RequestDrawCard":
                RequestDrawCard localMessage6 = (RequestDrawCard) message;
                DrawCard drawCard = new DrawCard();
                drawCard.payload = Integer.parseInt(localMessage6.getCard().getCountryId());
                drawCard.playerId = Players.me().getId();
                return drawCard;
            case "RequestHost":
                // TODO handle this in a special way - no JSON, but we need to act
                return null;
            case "RequestInitGame":
                RequestInitGame localMessage7 = (RequestInitGame) message;
                /*
                Ping ping = new Ping();
                ping.payload = localMessage7.getPlayerCount();
                ping.playerId = Players.me().getId();
                return ping;
                */
                BeginGame beginGame = new BeginGame();
                BeginGameData beginGameData = new BeginGameData();
                beginGameData.version = 1; // TODO - fix the hard coding
                ArrayList<String> d = new ArrayList<>();
                d.add("custom_map");
                beginGameData.supportedFeatures = d;
                beginGame.payload = beginGameData;
                return beginGame;
            case "RequestJoin":
                // TODO extract host address/port
                RequestJoin requestJoin = (RequestJoin) message;
                JoinGame joinGame = new JoinGame();
                joinGame.payload.supportedVersions = new Integer[]{1}; // TODO check version
                ArrayList<String> features = new ArrayList<>();
                for(String s : requestJoin.getSupportedFeatures()) {
                    features.add(s);
                }
                joinGame.payload.supportedFeatures = features;
                joinGame.remoteHost = requestJoin.getHostAddress().getHostName();
                return joinGame;
            case "RequestMapTransfer":
                // TODO I have no idea how this is spec'd right now
                return null;
            case "RequestRollHash":
                // TODO math and request response needs handled
                // TODO need to pass end result up
                // RequestRoll requestRoll = (RequestRoll) message;
                // Roll roll = new Roll();
                //roll.payload.diceCount = requestRoll.getNumberOfDice();
                //roll.payload.diceFaces = 6; // TODO I assume this is the case. Magic number
                //roll.playerId = Players.me().getId();
                RequestRollHash requestRollHash = (RequestRollHash) message;
                RollHash rollHash = new RollHash();

                rollHash.payload = requestRollHash.getHashHex();
                rollHash.playerId = Players.me().getId();

                return rollHash;
            case "RequestRollNumber":
                RequestRollNumber requestRollNumber = (RequestRollNumber) message;
                RollNumber rollNumber = new RollNumber();

                rollNumber.payload = requestRollNumber.getNumberHex();
                rollNumber.playerId = Players.me().getId();

                return rollNumber;
            case "RequestSetupClaim":
                RequestSetupClaim requestSetupClaim = (RequestSetupClaim) message;
                Setup setup1 = new Setup();
                setup1.payload = requestSetupClaim.getCountryId();
                setup1.playerId = requestSetupClaim.getPlayer().getId();
                return setup1;
            case "RequestTerritoryChange":
                RequestTerritoryChange requestTerritoryChange = (RequestTerritoryChange) message;
                AttackCapture attackCapture = new AttackCapture();
                ArrayList<Integer> list3 = new ArrayList<>();
                list3.add(-1); // TODO I need a source id from engine
                list3.add(requestTerritoryChange.getCountryId());
                list3.add(requestTerritoryChange.getAdvancingArmyCount());
                attackCapture.payload = list3;
                attackCapture.playerId = Players.me().getId();
                return attackCapture;
            case "ResponseMessage":
                ResponseMessage responseMessage = (ResponseMessage) message;
                if(responseMessage.trigger instanceof AbstractNetMessage) {
                    Ack ack = new Ack();
                    try {
                        ack.playerId = Players.me().getId();
                    } catch (InvalidPlayerException e) {
                        e.printStackTrace(); // TODO handle
                    }
                    ack.payload = ((AbstractNetMessage) responseMessage.trigger).getAckId();
                    return ack;
                } else {
                    return null;
                }

            default:
                break;
        }

        return null;
    }
}
