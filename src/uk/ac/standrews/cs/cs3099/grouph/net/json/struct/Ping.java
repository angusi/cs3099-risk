package uk.ac.standrews.cs.cs3099.grouph.net.json.struct;

import uk.ac.standrews.cs.cs3099.grouph.net.messages.PlayerCountM;
import uk.ac.standrews.cs.cs3099.grouph.net.util.MessageCounter;
import uk.ac.standrews.cs.cs3099.grouph.shared.AbstractNetMessage;

/**
 * Created by 120023239 on 2/23/15.
 */
public class Ping extends JSONStruct {
    // TODO respond to these messages
    public Integer payload;
    public Integer playerId;

    @Override
    public AbstractNetMessage toMessage() {
        if(playerId == 0) { // Message from host
            PlayerCountM playerCountM = new PlayerCountM(MessageCounter.next());
            playerCountM.setPlayerCount(payload);
            return playerCountM;
        } else {
            return null;
        }
    }
}
