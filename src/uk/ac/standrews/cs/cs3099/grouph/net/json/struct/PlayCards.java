package uk.ac.standrews.cs.cs3099.grouph.net.json.struct;

import uk.ac.standrews.cs.cs3099.grouph.net.messages.CardPlayedM;
import uk.ac.standrews.cs.cs3099.grouph.net.util.MessageCounter;
import uk.ac.standrews.cs.cs3099.grouph.shared.AbstractNetMessage;
import uk.ac.standrews.cs.cs3099.grouph.shared.struct.players.InvalidPlayerException;
import uk.ac.standrews.cs.cs3099.grouph.shared.struct.players.Players;

/**
 * Created by 120023239 on 2/23/15.
 */
public class PlayCards extends JSONStructAck {
    // TODO Ack
    public PlayCardsData payload;

    @Override
    public AbstractNetMessage toMessage() {
        CardPlayedM cardPlayedM = new CardPlayedM(MessageCounter.next());
        try {
            cardPlayedM.setPlayer(Players.getPlayerById(playerId));
        } catch (InvalidPlayerException e) {
            cardPlayedM.setPlayer(null); // TODO - Engine - how to parse?
        }
        cardPlayedM.setArmies(payload.armies);
        cardPlayedM.setCards(payload.cards);
        return cardPlayedM;
    }
}
