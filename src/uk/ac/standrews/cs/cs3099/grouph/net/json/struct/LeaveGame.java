package uk.ac.standrews.cs.cs3099.grouph.net.json.struct;

import uk.ac.standrews.cs.cs3099.grouph.net.messages.PlayerLeaveGameM;
import uk.ac.standrews.cs.cs3099.grouph.net.util.MessageCounter;
import uk.ac.standrews.cs.cs3099.grouph.shared.AbstractNetMessage;
import uk.ac.standrews.cs.cs3099.grouph.shared.struct.players.InvalidPlayerException;
import uk.ac.standrews.cs.cs3099.grouph.shared.struct.players.Players;

/**
 * Created by 120023239 on 2/23/15.
 */
public class LeaveGame extends JSONStruct {
    // TODO handle receive_updates
    public LeaveGameData payload;
    public Integer playerId;

    @Override
    public AbstractNetMessage toMessage() {
        PlayerLeaveGameM playerLeaveGameM = new PlayerLeaveGameM(MessageCounter.next());
        try {
            playerLeaveGameM.setPlayer(Players.getPlayerById(playerId));
        } catch (InvalidPlayerException e) {
            playerLeaveGameM.setPlayer(null); // TODO handle
        }
        playerLeaveGameM.setResponseCode(payload.response);
        return playerLeaveGameM;
    }
}
