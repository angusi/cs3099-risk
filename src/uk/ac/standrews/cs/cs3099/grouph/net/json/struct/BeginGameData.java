package uk.ac.standrews.cs.cs3099.grouph.net.json.struct;

import java.util.List;

/**
 * Created by 120023239 on 2/23/15.
 */
public class BeginGameData {
    public Integer version;
    public List<String> supportedFeatures;
}
