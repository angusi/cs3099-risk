package uk.ac.standrews.cs.cs3099.grouph.net.json.struct;

import uk.ac.standrews.cs.cs3099.grouph.shared.AbstractNetMessage;

/**
 * Created by 120023239 on 2/23/15.
 */
public class Ready extends JSONStructAck {
    // TODO respond to this message
    public Object payload; // Should always be null

    @Override
    public AbstractNetMessage toMessage() {
        // This is net-level, doesn't need to be passed
        return null;
    }
}
