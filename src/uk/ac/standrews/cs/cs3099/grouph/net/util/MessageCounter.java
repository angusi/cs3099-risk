package uk.ac.standrews.cs.cs3099.grouph.net.util;

/**
 * Created by 120023239 on 23/02/15.
 */
public class MessageCounter {
    private static int count = 0;

    public static int next() {
        return count++;
    }
}
