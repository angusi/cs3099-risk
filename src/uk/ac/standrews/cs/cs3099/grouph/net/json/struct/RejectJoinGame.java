package uk.ac.standrews.cs.cs3099.grouph.net.json.struct;

import uk.ac.standrews.cs.cs3099.grouph.net.messages.JoinGameResultM;
import uk.ac.standrews.cs.cs3099.grouph.net.util.MessageCounter;
import uk.ac.standrews.cs.cs3099.grouph.shared.AbstractNetMessage;

/**
 * Created by 120023239 on 2/23/15.
 */
public class RejectJoinGame extends JSONStruct {
    public String payload;

    @Override
    public AbstractNetMessage toMessage() {
        JoinGameResultM joinGameResultM = new JoinGameResultM(MessageCounter.next());
        joinGameResultM.setJoinSuccess(false);
        joinGameResultM.setMessage(payload);
        return joinGameResultM;
    }
}
