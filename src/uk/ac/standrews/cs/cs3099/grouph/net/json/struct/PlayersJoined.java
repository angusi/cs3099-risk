package uk.ac.standrews.cs.cs3099.grouph.net.json.struct;

import uk.ac.standrews.cs.cs3099.grouph.net.messages.PlayerJoinedM;
import uk.ac.standrews.cs.cs3099.grouph.net.util.MessageCounter;
import uk.ac.standrews.cs.cs3099.grouph.shared.AbstractNetMessage;

import java.util.ArrayList;

/**
 * Created by 120023239 on 3/11/15.
 */
public class PlayersJoined extends JSONStruct {
    public ArrayList<ArrayList<Object>> payload;

    @Override
    public AbstractNetMessage toMessage() {
        PlayerJoinedM playerJoinedM = new PlayerJoinedM(MessageCounter.next());
        ArrayList<PlayerJoinedM.PlayerJoinedData> players = new ArrayList<>();

        for(ArrayList<Object> d : payload) {
            Integer id = (Integer) d.get(0);
            String name = (String) d.get(1);
            PlayerJoinedM.PlayerJoinedData p = playerJoinedM.createPlayer(id, name);
            players.add(p);
        }

        playerJoinedM.setPlayerData(players);
        return playerJoinedM;
    }
}
