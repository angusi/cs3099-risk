package uk.ac.standrews.cs.cs3099.grouph.net.json.struct;

/**
 * Created by 120023239 on 2/23/15.
 */
public abstract class JSONStructAck extends JSONStruct {
    public Integer playerId;
    public Integer ackId;
}
