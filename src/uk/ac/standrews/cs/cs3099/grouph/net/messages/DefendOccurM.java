package uk.ac.standrews.cs.cs3099.grouph.net.messages;

import uk.ac.standrews.cs.cs3099.grouph.shared.AbstractNetMessage;
import uk.ac.standrews.cs.cs3099.grouph.shared.struct.players.Player;

/**
 * Created by 120023239 on 14/02/15.
 */
public class DefendOccurM extends AbstractNetMessage {
    private Player player;
    int numberArmies;

    public DefendOccurM(int index) {
        super(index);
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public int getNumberArmies() {
        return numberArmies;
    }

    public void setNumberArmies(int numberArmies) {
        this.numberArmies = numberArmies;
    }
}
