package uk.ac.standrews.cs.cs3099.grouph.net.json.struct;

import uk.ac.standrews.cs.cs3099.grouph.net.messages.GameStartM;
import uk.ac.standrews.cs.cs3099.grouph.net.util.MessageCounter;
import uk.ac.standrews.cs.cs3099.grouph.shared.AbstractNetMessage;

/**
 * Created by 120023239 on 2/23/15.
 */
public class BeginGame extends JSONStruct {
    // TODO handle version ??
    public BeginGameData payload;

    @Override
    public AbstractNetMessage toMessage() {
        GameStartM gameStartM = new GameStartM(MessageCounter.next());
        gameStartM.setSupportedFeatures(payload.supportedFeatures);
        return gameStartM;
    }
}
