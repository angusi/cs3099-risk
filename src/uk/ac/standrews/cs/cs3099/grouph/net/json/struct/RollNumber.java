package uk.ac.standrews.cs.cs3099.grouph.net.json.struct;

import uk.ac.standrews.cs.cs3099.grouph.net.messages.DiceNumberM;
import uk.ac.standrews.cs.cs3099.grouph.net.util.MessageCounter;
import uk.ac.standrews.cs.cs3099.grouph.shared.AbstractNetMessage;
import uk.ac.standrews.cs.cs3099.grouph.shared.struct.players.InvalidPlayerException;
import uk.ac.standrews.cs.cs3099.grouph.shared.struct.players.Players;

/**
 * Created by 120023239 on 2/23/15.
 */
public class RollNumber extends JSONStruct {
    public String payload;
    public Integer playerId;

    @Override
    public AbstractNetMessage toMessage() {
        DiceNumberM diceNumberM = new DiceNumberM(MessageCounter.next());

        try {
            diceNumberM.setPlayer(Players.getPlayerById(playerId));
            diceNumberM.setNumber(payload);
        } catch (InvalidPlayerException e) {
            diceNumberM.setPlayer(null); // TODO: Handle
        }

        return diceNumberM;
    }
}
