package uk.ac.standrews.cs.cs3099.grouph.net.messages;

import uk.ac.standrews.cs.cs3099.grouph.shared.AbstractNetMessage;
import uk.ac.standrews.cs.cs3099.grouph.shared.struct.players.Player;

/**
 * Created by 120023239 on 14/02/15.
 */
public class SetupM extends AbstractNetMessage {
    private int territory;
    private Player player;
    
    public SetupM(int index) {
        super(index);
    }

    public int getTerritory() {
        return territory;
    }

    public void setTerritory(int territory) {
        this.territory = territory;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }
}
