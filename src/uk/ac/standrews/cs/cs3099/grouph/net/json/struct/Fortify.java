package uk.ac.standrews.cs.cs3099.grouph.net.json.struct;

import uk.ac.standrews.cs.cs3099.grouph.net.messages.MoveM;
import uk.ac.standrews.cs.cs3099.grouph.net.util.MessageCounter;
import uk.ac.standrews.cs.cs3099.grouph.shared.AbstractNetMessage;
import uk.ac.standrews.cs.cs3099.grouph.shared.struct.players.InvalidPlayerException;
import uk.ac.standrews.cs.cs3099.grouph.shared.struct.players.Players;

import java.util.List;

/**
 * Created by 120023239 on 2/23/15.
 */
public class Fortify extends JSONStructAck {
    public List<Integer> payload;

    @Override
    public AbstractNetMessage toMessage() {
        MoveM moveM = new MoveM(MessageCounter.next());
        try {
            moveM.setPlayer(Players.getPlayerById(playerId));
        } catch (InvalidPlayerException e) {
            moveM.setPlayer(null); // TODO handle
        }
        // TODO magic number and error handling
        moveM.setFrom(payload.get(0));
        moveM.setTo(payload.get(1));
        moveM.setNumberArmies(payload.get(2));
        return moveM;
    }
}
