package uk.ac.standrews.cs.cs3099.grouph.net.json.struct;

import uk.ac.standrews.cs.cs3099.grouph.net.messages.RemoteErrorM;
import uk.ac.standrews.cs.cs3099.grouph.net.util.MessageCounter;
import uk.ac.standrews.cs.cs3099.grouph.shared.AbstractNetMessage;
import uk.ac.standrews.cs.cs3099.grouph.shared.struct.players.InvalidPlayerException;
import uk.ac.standrews.cs.cs3099.grouph.shared.struct.players.Players;

/**
 * Created by 120023239 on 2/23/15.
 */
public class Ack extends JSONStruct {
    // TODO XXX - How do we handle errors? We need a mechanism to parse errors from the engine
    // TODO - for now, we return 'OK'
    public Integer payload;
    public Integer playerId;

    @Override
    public AbstractNetMessage toMessage() {
        if(payload == -1) { // TODO XXX Fix this
            // Someone reports an error
            RemoteErrorM remoteErrorM = new RemoteErrorM(MessageCounter.next());
            try {
                remoteErrorM.setPlayer(Players.getPlayerById(playerId));
            } catch (InvalidPlayerException e) {
                remoteErrorM.setPlayer(null); // TODO handle
            }
            remoteErrorM.setNotes("Another player has sent a bad acknowledgment to a command");
            return remoteErrorM;
        } else {
            return null;
        }
    }
}
