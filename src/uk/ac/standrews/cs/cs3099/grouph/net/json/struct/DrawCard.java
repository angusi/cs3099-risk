package uk.ac.standrews.cs.cs3099.grouph.net.json.struct;

import uk.ac.standrews.cs.cs3099.grouph.net.messages.CardDrawResultM;
import uk.ac.standrews.cs.cs3099.grouph.net.util.MessageCounter;
import uk.ac.standrews.cs.cs3099.grouph.shared.AbstractNetMessage;
import uk.ac.standrews.cs.cs3099.grouph.shared.struct.players.InvalidPlayerException;
import uk.ac.standrews.cs.cs3099.grouph.shared.struct.players.Players;

/**
 * Created by 120023239 on 2/23/15.
 */
public class DrawCard extends JSONStructAck {
    // TODO Ack
    public Integer payload;

    @Override
    public AbstractNetMessage toMessage() {
        CardDrawResultM cardDrawResultM = new CardDrawResultM(MessageCounter.next());
        try {
            cardDrawResultM.setPlayer(Players.getPlayerById(playerId));
        } catch (InvalidPlayerException e) {
            cardDrawResultM.setPlayer(null);
        }
        cardDrawResultM.setCard(payload);
        return cardDrawResultM;
    }
}
