package uk.ac.standrews.cs.cs3099.grouph.net;

import uk.ac.standrews.cs.cs3099.grouph.engine.messages.requests.*;
import uk.ac.standrews.cs.cs3099.grouph.net.io.Client;
import uk.ac.standrews.cs.cs3099.grouph.net.io.Host;
import uk.ac.standrews.cs.cs3099.grouph.net.json.JSONStructBuilder;
import uk.ac.standrews.cs.cs3099.grouph.net.json.struct.JoinGame;
import uk.ac.standrews.cs.cs3099.grouph.net.json.struct.JoinGameData;
import uk.ac.standrews.cs.cs3099.grouph.shared.*;
import uk.ac.standrews.cs.cs3099.grouph.shared.struct.players.InvalidPlayerException;

import java.net.InetAddress;
import java.util.AbstractQueue;
import java.util.ArrayList;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * Created by 120023239 on 04/03/15.
 */
public class NetJSON implements INetComponent {
    private static final Logger l = Logger.getLogger(NetJSON.class.getSimpleName());
    private ArrayBlockingQueue<AbstractMessage> sink = null;
    private ArrayBlockingQueue<AbstractMessage> source = null;

    private Boolean weAreHost = null;

    public NetJSON() {
        sink = new ArrayBlockingQueue<AbstractMessage>(1000); // TODO size config
    }

    @Override
    public AbstractQueue<AbstractMessage> getSink(IComponent component) {
        return this.sink;
    }

    @Override
    public void setSource(AbstractQueue<AbstractMessage> queue, IComponent component) {
        if(component instanceof IEngineComponent) {
            this.source = (ArrayBlockingQueue<AbstractMessage>) queue;
        }
    }

    @Override
    public void run() {
        l.info("NetJSON started");
        Client client = null;
        Host host = null;
        while(!Thread.interrupted()) {
            if(source != null) {
                if (weAreHost == null) {
                    AbstractEngineMessage setupMessage = null;
                    try {
                        setupMessage = (AbstractEngineMessage) source.take();
                        l.info("Received setup message from Engine");
                    } catch (InterruptedException e) {
                        return;
                    }
                    if (setupMessage.name.equals("RequestHost")) {
                        // We're hosting
                        l.info("We are a host");
                        weAreHost = true;
                        int p = ((RequestHost)setupMessage).getPort();
                        String map = (((RequestHost) setupMessage).getJsonMapString());
                        host = new Host(p, map, sink);
                        host.start();

                        // Start client
                        String sHost = "127.0.0.1";
                        int port = 9000; // TODO this should come from engine
                        client = new Client(sHost, port, sink);
                        client.start();
                    } else if (setupMessage.name.equals("RequestJoin")) {
                        l.info("We are joining an existing game");
                        weAreHost = false;
                        //RequestJoin joinMessage = (RequestJoin) setupMessage;
                        InetAddress address = ((RequestJoin) setupMessage).getHostAddress();
                        String sHost = address.getHostAddress();
                        int port = 9000; // TODO This should come from Engine
                        client = new Client(sHost, port, sink);
                        client.start();

                        RequestJoin requestJoin = (RequestJoin) setupMessage;
                        JoinGame joinGame = new JoinGame();
                        joinGame.payload = new JoinGameData();
                        joinGame.payload.supportedVersions = new Integer[]{1}; // TODO check version
                        joinGame.payload.supportedFeatures = requestJoin.getSupportedFeatures()
                                .stream().collect(Collectors.toList());
                        joinGame.remoteHost = requestJoin.getHostAddress().getHostName();

                        client.sendMessage(joinGame);
                    } else {
                        // Shouldn't be here
                    }
                } else {
                    AbstractMessage message = null;
                    try {
                        message = source.take();
                        l.info("Got message " + message.id + " from engine");
                    } catch (InterruptedException e) {
                        return;
                    }
                    if (client != null) {
                        try {
                            client.sendMessage(JSONStructBuilder.build(message));
                        } catch (InvalidPlayerException e) {
                            e.printStackTrace(); // TODO handle
                        }
                    }
                }
            }
        }
    }
}
