package uk.ac.standrews.cs.cs3099.grouph.net.json.struct;

import java.util.List;

/**
 * Created by 120023239 on 2/23/15.
 */
public class PlayCardsData {
    public List<List<Integer>> cards;
    public Integer armies;
}
