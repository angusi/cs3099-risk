package uk.ac.standrews.cs.cs3099.grouph.net.json;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import uk.ac.standrews.cs.cs3099.grouph.net.json.struct.JSONStruct;

import java.io.IOException;

/**
 * Created by 120023239 on 2/23/15.
 */
public class Converter {
    private ObjectMapper objectMapper;

    public Converter() {
        objectMapper = new ObjectMapper();
        objectMapper.setPropertyNamingStrategy(new CamelNamingStrategy());
    }

    public JSONStruct stringToObject(String input) {
        //JavaType javaType = objectMapper.getTypeFactory().constructType(JSONStruct.class);
        try {
            return objectMapper.readValue(input, JSONStruct.class);
        } catch (IOException e) {
            // TODO - handle - under what conditions is this thrown?
            e.printStackTrace();
            return null;
        }
    }

    public String objectToString(JSONStruct input) {
        try {
            return objectMapper.writeValueAsString(input);
        } catch (com.fasterxml.jackson.core.JsonProcessingException e) {

            // TODO handle
            e.printStackTrace();
            return null;
        }
    }
}
