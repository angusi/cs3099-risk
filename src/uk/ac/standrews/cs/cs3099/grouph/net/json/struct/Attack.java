package uk.ac.standrews.cs.cs3099.grouph.net.json.struct;

import uk.ac.standrews.cs.cs3099.grouph.net.messages.AttackOccurM;
import uk.ac.standrews.cs.cs3099.grouph.net.util.MessageCounter;
import uk.ac.standrews.cs.cs3099.grouph.shared.AbstractNetMessage;
import uk.ac.standrews.cs.cs3099.grouph.shared.struct.players.InvalidPlayerException;
import uk.ac.standrews.cs.cs3099.grouph.shared.struct.players.Players;

import java.util.List;

/**
 * Created by 120023239 on 2/23/15.
 */
public class Attack extends JSONStructAck {
    // TODO acknowledge
    public List<Integer> payload;

    @Override
    public AbstractNetMessage toMessage() {
        // TODO - error handling of payload.length
        AttackOccurM attackOccurM = new AttackOccurM(MessageCounter.next());
        try {
            attackOccurM.setPlayer(Players.getPlayerById(playerId));
        } catch (InvalidPlayerException e) {
            attackOccurM.setPlayer(null); // TODO handle
        }
        // TODO magic numbers and error handling
        attackOccurM.setFrom(payload.get(0));
        attackOccurM.setTo(payload.get(1));
        attackOccurM.setNumberArmies(payload.get(2));
        return attackOccurM;
    }
}
