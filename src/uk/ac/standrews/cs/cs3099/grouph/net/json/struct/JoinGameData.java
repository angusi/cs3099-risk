package uk.ac.standrews.cs.cs3099.grouph.net.json.struct;

import java.util.List;

/**
 * Created by 120023239 on 2/23/15.
 */
public class JoinGameData {
    public Integer[] supportedVersions;
    public List<String> supportedFeatures;
}
