package uk.ac.standrews.cs.cs3099.grouph.net.messages;

import uk.ac.standrews.cs.cs3099.grouph.shared.AbstractNetMessage;
import uk.ac.standrews.cs.cs3099.grouph.shared.struct.players.Player;

/**
 * Created by 120023239 on 23/02/15.
 */
// Sent when the host reports a player timeout
public class TimeoutM extends AbstractNetMessage {
    private Player player; // Player who announced
    private Player exitingPlayer; // Player who left

    public TimeoutM(int index) {
        super(index);
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public Player getExitingPlayer() {
        return exitingPlayer;
    }

    public void setExitingPlayer(Player exitingPlayer) {
        this.exitingPlayer = exitingPlayer;
    }
}
