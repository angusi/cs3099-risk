package uk.ac.standrews.cs.cs3099.grouph.net.json.struct;

import uk.ac.standrews.cs.cs3099.grouph.net.messages.JoinGameResultM;
import uk.ac.standrews.cs.cs3099.grouph.net.util.MessageCounter;
import uk.ac.standrews.cs.cs3099.grouph.shared.AbstractNetMessage;

/**
 * Created by 120023239 on 2/23/15.
 */
public class AcceptJoinGame extends JSONStruct {
    public AcceptJoinGameData payload;

    @Override
    public AbstractNetMessage toMessage() {
        JoinGameResultM joinGameResultM = new JoinGameResultM(MessageCounter.next());
        joinGameResultM.setJoinSuccess(true);
        joinGameResultM.setMap(payload.map);
        return joinGameResultM;
    }
}
