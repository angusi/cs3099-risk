package uk.ac.standrews.cs.cs3099.grouph.net.json.struct;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import uk.ac.standrews.cs.cs3099.grouph.net.json.RiskJSONFields;

/**
 * Created by 120023239 on 2/23/15.
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "command", visible = true)
@JsonSubTypes({
        @JsonSubTypes.Type(value = AcceptJoinGame.class, name = RiskJSONFields.commandAcceptJoinGame),
        @JsonSubTypes.Type(value = Ack.class, name = RiskJSONFields.commandAck),
        @JsonSubTypes.Type(value = Attack.class, name = RiskJSONFields.commandAttack),
        @JsonSubTypes.Type(value = AttackCapture.class, name = RiskJSONFields.commandCapture),
        @JsonSubTypes.Type(value = BeginGame.class, name = RiskJSONFields.commandBeginGame),
        @JsonSubTypes.Type(value = Defend.class, name = RiskJSONFields.commandDefend),
        @JsonSubTypes.Type(value = Deploy.class, name = RiskJSONFields.commandDeploy),
        @JsonSubTypes.Type(value = DrawCard.class, name = RiskJSONFields.commandDrawCard),
        @JsonSubTypes.Type(value = Fortify.class, name = RiskJSONFields.commandFortify),
        @JsonSubTypes.Type(value = JoinGame.class, name = RiskJSONFields.commandJoinGame),
        @JsonSubTypes.Type(value = LeaveGame.class, name = RiskJSONFields.commandLeaveGame),
        @JsonSubTypes.Type(value = Ack.class, name = RiskJSONFields.commandAck),
        @JsonSubTypes.Type(value = Ping.class, name = RiskJSONFields.commandPing),
        @JsonSubTypes.Type(value = PlayCards.class, name = RiskJSONFields.commandPlayCard),
        @JsonSubTypes.Type(value = Ready.class, name = RiskJSONFields.commandReady),
        @JsonSubTypes.Type(value = RejectJoinGame.class, name = RiskJSONFields.commandRejectJoinGame),
        @JsonSubTypes.Type(value = RollHash.class, name = RiskJSONFields.commandRollHash),
        @JsonSubTypes.Type(value = RollNumber.class, name = RiskJSONFields.commandRollNumber),
        @JsonSubTypes.Type(value = Setup.class, name = RiskJSONFields.commandSetup),
        @JsonSubTypes.Type(value = Timeout.class, name = RiskJSONFields.commandTimeoout),
        @JsonSubTypes.Type(value = PlayersJoined.class, name = RiskJSONFields.commandPlayerJoined)
})
public abstract class JSONStruct implements MessageWriter {
    public String command;

    @Override
    public String toString() {
        return "JSONStruct{" +
                "command='" + command + "'" +
                "}";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        JSONStruct that = (JSONStruct) o;

        if (!command.equals(that.command)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return command.hashCode();
    }
}
