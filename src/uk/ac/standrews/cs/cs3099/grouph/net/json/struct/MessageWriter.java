package uk.ac.standrews.cs.cs3099.grouph.net.json.struct;

import uk.ac.standrews.cs.cs3099.grouph.shared.AbstractNetMessage;

/**
 * Created by 120023239 on 2/23/15.
 */
public interface MessageWriter {
    public AbstractNetMessage toMessage();
}
