package uk.ac.standrews.cs.cs3099.grouph.net.json.struct;

/**
 * Created by 120023239 on 23/02/15.
 */
public class LeaveGameData {
    public Integer response;
    public boolean receiveUpdates;
}
